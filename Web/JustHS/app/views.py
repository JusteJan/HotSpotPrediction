from django.shortcuts import render, redirect
from django.http import HttpResponse, Http404, FileResponse

import threading
import time
import logging
logger = logging.getLogger(__name__)

from . import models
from . import forms


def index(request):
    logger.info('Loading index page.')
    return render(request, 'start.html')


def input(request):
    if request.method == 'POST':
        input_form = forms.InputData(request.POST, request.FILES)
        if input_form.is_valid():
            job_id, job_settings_file = input_form.save_to_disk()
            logger.debug('Starting calculation thread at %s', time.asctime())
            t = threading.Thread(
                target=models.process_job,
                args=[job_id, job_settings_file, request.POST['email']]
                )
            t.start()
            logger.debug('Redirecting to results page of %s.', job_id)
            return redirect('results', job_id=job_id)
        else:
            input_form = forms.InputData(request.POST, request.FILES)
    else:
        input_form = forms.InputData()
    return render(request, 'input.html', {'form': input_form})


def results(request, job_id):
    "Display job results"
    logger.info('Calculation results for %s.' % job_id)
    job_failed, results, log = models.get_results_or_404(job_id)
    context = {
        'job_id': job_id,
        'refresh': False,
        'log': log,
        }
    if job_failed:
        template_name = 'failed.html'
    else:
        template_name = 'results.html'
        if results:
            context['refresh'] = False
            context['results'] = results
            context['jsmol_scripts'] = models.generate_jsmol_scripts(results)
        else:
            context['refresh'] = True
    return render(request, template_name, context)


def download_csv(request, job_id):
    "Download CSV file of results"
    job_failed, results_csv, log = models.get_results_or_404(
        job_id, as_csv_file=True
        )
    if job_failed:
        raise Http404
    return HttpResponse(results_csv, content_type='text/plain')


def download_all(request, job_id):
    "Download results as archive"
    job_failed, zip_fname, log = models.get_results_or_404(
        job_id, as_archive=True
        )
    return FileResponse(open(zip_fname, 'rb'))


def download_pdb_file(request, job_id):
    "Download input PDB file"
    job_failed, results_csv, log = models.get_results_or_404(
        job_id, as_csv_file=True
        )
    if job_failed:
        raise Http404
    else:
        pdb_file = models.pdb_file_name(job_id)
        return FileResponse(open(pdb_file, 'rb'))


def help(request):
    pass

