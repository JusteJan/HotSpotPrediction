from django.http import Http404
from django.core.mail import send_mail

import os
import csv
import shutil
import time
import logging
logger = logging.getLogger(__name__)

from JustHS import settings


def process_job(job_id, settings_file, email):
    "Save input data and run calculation"
    exe = []
    if settings.USE_SLURM:
        exe.append('sbatch --wait')
    exe.append(settings.CALCULATION_SCRIPT)
    exe.append(settings_file)
    log_file = os.path.join(os.path.dirname(settings_file), 'log')
    exe.append('> %s 2>&1' % log_file)
    exe = ' '.join(exe)
    logger.debug('Running calculation: %s', exe)
    os.system(exe)
    logger.debug('Finished calculation at %s.', time.asctime())
    if email:
        try:
            send_mail(
                subject='Hot spots server JustHS job',
                message='JustHS job %s has finished.' % job_id,
                from_email=None,
                recipient_list=[email]
                )
        except:
            logging.error('Sending confirmation email failed.')
            import traceback
            traceback.print_exc()


def get_results_or_404(job_id, as_csv_file=False, as_archive=False):
    "Get results or Error 404"
    try:
        job_failed, results, log = read_results(job_id, as_csv_file, as_archive)
    except (IOError, OSError):
        raise Http404
    return job_failed, results, log


def read_results(job_id, as_csv_file, as_archive):
    "Read job results"
    failed = False
    job_directory = os.path.join(settings.JOBS_DIRECTORY, job_id)
    log_file = os.path.join(job_directory, 'log')
    with open(log_file) as f:
        log = f.read()
        lines = log.splitlines()
        status_line = lines[-1].rstrip()
    if status_line == 'Finished.':
        logger.debug('Job finished successfully, getting results.')
        if as_archive:
            logger.debug('Retrieving archive with all results.')
            results = create_job_archive(job_id, job_directory)
        else:
            results_file = os.path.join(
                job_directory, 'output', 'Predictions.csv'
                )
            with open(results_file) as f:
                results_text = f.read()
                if as_csv_file:
                    logger.debug('Retrieving CSV results.')
                    results = results_text
                else:
                    logger.debug('Retrieving results for display.')
                    r = csv.DictReader(results_text.splitlines(), delimiter=';')
                    # Reading and sorting by Consensus-max prediction by
                    # default.
                    results = sorted(r, key=lambda r: -float(r['max']))
    elif status_line == 'Failed!':
        logger.debug('Job failed!')
        failed = True
        results = create_job_archive(job_id, job_directory)
    else:
        logger.debug('Job is still running.')
        results = None
    return failed, results, log


def read_settings_file(job_id):
    "Read job settings file"
    job_settings_file = os.path.join(
        settings.JOBS_DIRECTORY, job_id, 'settings'
        )
    job_settings = {}
    with open(job_settings_file) as f:
        for line in f:
            k, v = line.rstrip().split('=')
            if v == 'true':
                v = True
            elif v == 'false':
                v = False
            job_settings[k] = v
    return job_settings


def pdb_file_name(job_id):
    "Retrieve name of job PDB file"
    job_settings = read_settings_file(job_id)
    pdb_file = os.path.join(
        job_settings['working_directory'], 'files',
        job_settings['job_name']+'.pdb'
        )
    return pdb_file


def generate_jsmol_scripts(results):
    "Generate scripts for JSmol selections"
    scripts = {}
    scripts['init'] = 'cpk off; wireframe off; cartoon on; color cartoon chain;'
    hs_methods = ('max', 'mean', 'pls', 'gbm', 'rf', 'rpart', 'svm')
    threshold = 0.4
    selection_residues = {}
    for r in results:
        for method in hs_methods:
            if float(r[method]) >= threshold:
                residue_selection = '%s:%s' % (r['Position'], r['Chain'])
                try:
                    selection_residues[method].append(residue_selection)
                except KeyError:
                    selection_residues[method] = [residue_selection]
    for method, residues in selection_residues.items():
        script = scripts['init']
        script += 'select (%s);' % ','.join(residues)
        script += 'define %s_hs selected;' % method
        script += 'wireframe 0.15; spacefill 23%;'
        script += 'select selected and *.ca;'
        script += 'label %m%R%E; color labels gray; font label 14 bold;'
        script += 'select %s_hs;' % method
        scripts[method] = script
    return scripts


def create_job_archive(job_id, job_directory):
    temporary_zip_prefix = os.path.join('/tmp', job_id)
    temporary_zip_file = shutil.make_archive(
        temporary_zip_prefix,
        'zip',
        job_directory,
        )
    results = temporary_zip_file
    return temporary_zip_file

