from django import forms
from django.core.validators import RegexValidator
from django.core.exceptions import ValidationError

import os
import random
import string
import logging
logger = logging.getLogger(__name__)

from JustHS import settings

alphanumeric_validator = RegexValidator(
    regex=r'^[0-9a-zA-Z]*$',
    message='Only letters and numbers are allowed!'
    )


class InputData(forms.Form):
    input_file = forms.FileField()
    chains_1 = forms.CharField(validators=[alphanumeric_validator])
    input_alignments_1 = forms.FileField(
        widget=forms.ClearableFileInput(attrs={'multiple': True}),
        required=False
        )
    chains_2 = forms.CharField(validators=[alphanumeric_validator])
    input_alignments_2 = forms.FileField(
        widget=forms.ClearableFileInput(attrs={'multiple': True}),
        required=False
        )
    antibody = forms.BooleanField(
        required=False,
        label='Is this an antibody-antigen complex?'
        )
    email = forms.EmailField(required=False, label='E-mail (optional)')

    def clean(self):
        cleaned_data = super().clean()
        def check_chains_and_alignments_files(chains, aln_files, field):
            num_chains = len(chains)
            if aln_files:
                alignments_present = True
                num_alignment_files = len(aln_files)
                if num_chains != num_alignment_files:
                    self.add_error(
                        field,
                        'Number of alignments should be the same as '\
                            'number of chains!'
                        )
            else:
                alignments_present = False
            return alignments_present
        aln_1_present = check_chains_and_alignments_files(
            cleaned_data['chains_1'],
            self.files.getlist('input_alignments_1'),
            'input_alignments_1'
            )
        aln_2_present = check_chains_and_alignments_files(
            cleaned_data['chains_2'],
            self.files.getlist('input_alignments_2'),
            'input_alignments_2'
            )
        # Alignments should be present for both chains_1 and chains_2 or no
        # alignments at all.
        if aln_1_present ^ aln_2_present:
            raise ValidationError(
                'Multiple sequence alignments should be present for both '\
                    'first and second chains!'
                )
        else:
            if aln_1_present:
                self.cleaned_data['alignments_given'] = True
            else:
                self.cleaned_data['alignments_given'] = False

    def save_to_disk(self):
        data = self.cleaned_data
        files = self.files
        def save_input_file(destination, input_file):
            with open(destination, 'wb+') as f:
                for chunk in input_file.chunks():
                    f.write(chunk)
        def save_alignment_file(chain, name, directory, input_aln_file):
            aln_file_dest = os.path.join(directory, f'{name}_{chain}.aln')
            save_input_file(aln_file_dest, input_aln_file)
        def save_all_alignment_files(chains, name, directory, aln_file_field):
            aln_files = files.getlist(aln_file_field)
            if aln_files:
                for c, af in zip(chains, aln_files):
                    save_alignment_file(c, name, directory, af)
        # Create new job ID.
        job_id = ''.join(
            random.choice(string.ascii_letters+string.digits) for x in range(8)
            )
        logger.info('Processing input data for %s.', job_id)
        # Create directory for job.
        job_directory = os.path.join(settings.JOBS_DIRECTORY, job_id)
        job_files_directory = os.path.join(job_directory, 'files')
        logger.debug('Creating job directory %s.', job_directory)
        os.makedirs(job_files_directory)
        # Save files and create settings file.
        # Saving structure file.
        input_file = files['input_file']
        input_name = os.path.splitext(input_file.name.replace(' ', '_'))[0]
        input_file_dest = os.path.join(job_files_directory, input_name+'.pdb')
        logger.debug('Saving input file to %s.', input_file_dest)
        save_input_file(input_file_dest, input_file)
        # Saving alignment files.
        logger.debug('Saving sequence alignments if they are provided.')
        save_all_alignment_files(
            data['chains_1'], input_name, job_files_directory,
            'input_alignments_1'
            )
        save_all_alignment_files(
            data['chains_2'], input_name, job_files_directory,
            'input_alignments_2'
            )
        # Creating settings file for calculation.
        settings_file = os.path.join(job_directory, 'settings')
        with open(settings_file, 'w') as f:
            f.write('working_directory=%s\n' % job_directory)
            f.write('job_name=%s\n' % input_name)
            f.write('chains1=%s\n' % data['chains_1'])
            f.write('chains2=%s\n' % data['chains_2'])
            alignments_given = 'true' if data['alignments_given'] else 'false'
            f.write('alignments_given=%s\n' % alignments_given)
            antibody = 'true' if data['antibody'] else 'false'
            f.write('antibody=%s\n' % antibody)

        return job_id, settings_file

