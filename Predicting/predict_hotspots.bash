#! /bin/bash

usage="Usage: $0 settings_file"

scripts_directory="/opt/JustHS/bin"

set -o nounset
set -o errexit
trap 'echo "Failed!"' ERR

if [[ $1 == "-h" ]] || [[ $1 == "--help" ]]; then
    echo "$usage"
    exit
elif [ $# -ne 1 ]; then
    echo "$usage"
    exit
else
    settings_file="$1"
fi

. $settings_file
# Settings file should define:
#  * working directory
#  * job name, given from the webserver, that would correspond to input file name
#  * names of chains that form interface
#  * if sequence alignment is given
#  * if complex is antibody-antigen complex

echo "Starting in $working_directory, job name $job_name."
cd $working_directory
mkdir -p $working_directory/data
mkdir -p $working_directory/logs

echo "Calculating H-bonds."
hbplus files/$job_name.pdb > logs/hbplus.log 2>&1
hbplus_file="$job_name.hb2"
if [ -f $hbplus_file ]; then
    mv $hbplus_file data
else
    echo "Error calculating H-bonds."
    exit 1
fi

if $alignments_given; then
    echo "Using user-generated MSAs for conservation scoring."
else
    echo "Generating sequence alignments for conservation scoring."
    if $antibody; then
        echo "No alignment generation will be done, it is an antibody."
    else
        mkdir -p $working_directory/files/sequences
        python3 $scripts_directory/scripts/CreateAlignment.py $working_directory/files/$job_name.pdb $chains1 $chains2 > logs/alignments.log 2>&1
    fi
fi

if $antibody; then
    echo "This is antibody structure, no need to score conservation."
else
    echo "Calculating conservation."
    export SCORE_MAT_PATH="$scripts_directory/scripts/mstatx/data/aaindex"
    mkdir -p $working_directory/data/mstatx
    mkdir -p $working_directory/data/caprasingh
    mkdir -p $working_directory/data/al2co
    for alignment_file in `ls $working_directory/files/*.aln`; do
        echo "Using mstatx for $alignment_file"
        output_filename=$(basename $alignment_file | sed s/.aln$// )
        $scripts_directory/scripts/mstatx/mstatx -i $alignment_file -s trident -o $working_directory/data/mstatx/$output_filename > logs/mstatx_$output_filename.log 2>&1

        echo "Using Capra-Singh for $alignment_file"
        python2 $scripts_directory/scripts/capra/score_conservation.py -o $working_directory/data/caprasingh/$output_filename -s property_entropy -m $scripts_directory/scripts/capra/matrix/blosum62.bla $alignment_file > logs/caprasingh_$output_filename.log 2>&1

        echo "Using al2co for $alignment_file"
        python3 $scripts_directory/scripts/ConvertAlignment.py $alignment_file $alignment_file.clustal
        $scripts_directory/scripts/al2co -i $alignment_file.clustal -o $working_directory/data/al2co/$output_filename -a T > logs/al2co_$output_filename.log 2>&1
    done
fi

echo "Calculating contacts."
bash $scripts_directory/scripts/contacts.sh $settings_file

echo "Calculating interface shelling."
cd $scripts_directory/scripts
echo "$chains1$chains2" | grep -o . | while read chain; do
    echo "Chain $chain"
    bash $scripts_directory/scripts/calc-interface-depth.bash -i $working_directory/files/$job_name.pdb --chain-name $chain --output-dir $working_directory/data/${job_name}_${chain} 2>> $working_directory/logs/voronota.log
done
cd $working_directory

echo "Calculating Voronoi volumes..."
echo "... for the whole complex."
mkdir -p $working_directory/data/volume_complex
voronota-volumes -i $working_directory/files/$job_name.pdb --per-residue > $working_directory/data/volume_complex/$job_name 2>> $working_directory/logs/voronota.log
mkdir -p $working_directory/data/volume_monomers
for chain in $chains1 $chains2; do
    echo "... for chains $chain."
    chain_separated=$(echo $chain | sed 's/\(.\)/\1,/g' | sed 's/,$//')
    voronota-volumes -i $working_directory/files/$job_name.pdb --input-filter-query "--match 'c<$chain_separated>'" --per-residue > $working_directory/data/volume_monomers/${job_name}_${chain}
done

echo "Extracting structural features of residues."
python3 $scripts_directory/scripts/CalculateForPDBFile.py $job_name $chains1 $chains2 $working_directory

echo "Extracting evolutionary features of residues."
if $antibody; then
    antibody_mark="Y"
else
    antibody_mark="N"
fi
python3 $scripts_directory/scripts/EvolutionaryScoresForPredicting.py $antibody_mark $working_directory/files/${job_name}_${chains1}_${chains2}.csv ${job_name}_${chains1} ${job_name}_${chains2}

echo "Calculating graph centrality."
Rscript $scripts_directory/scripts/CalculateCentrality.R $working_directory/files/${job_name}_${chains1}_${chains2}
python3 $scripts_directory/scripts/ExtractCentrality.py $working_directory/files/${job_name}_${chains1}_${chains2}.csv $working_directory/files/${job_name}_${chains1}_${chains2}_centrality.csv

echo "Predicting hot-spot residues."
mkdir -p $working_directory/output
current_directory=`pwd`
cd $scripts_directory
Rscript Predict.R $working_directory/files/${job_name}_${chains1}_${chains2}.csv > $working_directory/logs/predict.log 2>&1
cd $current_directory
echo "Finished."
