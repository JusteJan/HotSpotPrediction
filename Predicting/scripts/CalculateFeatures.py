from TwoResidueMethodsVoronota import *
from constants import get_chains, types_list

# For Same calculations, raw contact files should be used and a method that
# checks if getFirst() and getSecond() are not the same should be implemented
# (we don't want to check contact area of aminoacid with itself)

import os
import re
import csv
from os import listdir
from os.path import isfile, join

d_residues = {}
d_residues['Monomer'] = {}
d_residues['Complex'] = {}


def keys(row, convertAminoacids):
    row = [x for x in row.rstrip('\n').split(' ') if x != ' ' and x != '']
    ann, vol = row
    chain = ''.join(re.findall(r'c<(.+?)>', ann))  # Chain of protein 1
    resi = ''.join(re.findall(r'r<(.+?)>', ann))  # Residue number of protein 1
    aminoacid = ''.join(re.findall(r'R<(.+?)>', ann))  # Residue name of protein 1
    aa = convertAminoacids[aminoacid] if aminoacid in convertAminoacids.keys() else ''
    atom = ''.join(re.findall(r'A<(.+?)>', ann))  # Arom name of protein 1
    i = ''.join(re.findall(r'i<(.+?)>', ann)).lower() #Residue index of protein 1
    key = chain + ' ' + resi + i + ' ' + aa
    return key, float(vol)

class CalculateFeatures(object):

    def __init__(self, chains):
        if len(chains) == 0:
            self.chains = get_chains()
        else:
            self.chains = chains
        self.area_h = {'Diff': {}, 'Same': {}}
        self.hydrophobic = {'Diff': {}, 'Same': {}}
        self.bsa = {'Hydrophobic': {}, 'Hydrophylic': {}}
        self.asa = {'Hydrophobic': {}, 'Hydrophylic': {}}
        self.cont = {'Diff': {}, 'Same': {}}
        self.ss = {'Diff': {}, 'Same': {}}
        self.sb = {'Diff': {}, 'Same': {}}
        self.hb = {'Diff': {}, 'Same': {}}
        self.volume_monomer = {}
        self.volume_complex = {}
        self.shelling = {}
        self.type = {}
        self.interface = []
        self.graphs = []

    def calculate_contacts(self, pdb, path):
        with open(path, 'r') as fin:
            for row in fin:
                row = TwoResidueMethodsVoronota(row, pdb, self.chains)
                if row.is_both_aminoacids():
                    self.calculate_area_h(row, self.area_h)
                    self.calculate_hydrophobic(row, self.hydrophobic)
                    self.calculate_bsa(row, self.bsa)
                if row.is_one_aminoacid():
                    self.calculate_asa(row, self.asa)

    def calculate_tags_and_type(self, pdb, path):
        with open(path, 'r') as fin:
            for row in fin:
                row = TwoResidueMethodsVoronota(row, pdb, self.chains)
                tags = row.get_first_object().get_tags().split(',')
                self.calculate_type(row)
                if row.is_both_aminoacids():
                    self.write_to_same_or_diff(row, 1, self.cont)
                    for tag in tags:
                        if tag == 'ds':
                            self.write_to_same_or_diff(row, 1, self.ss)
                        if tag == 'hb':
                            self.write_to_same_or_diff(row, 1, self.hb)
                        if tag == 'sb':
                            self.write_to_same_or_diff(row, 1, self.sb)

    def calculate_type(self, row):
        aminoacid = row.get_first_object().get_aminoacid()
        key = row.get_first()
        if key not in self.type:
            self.type[key] = {}
        for k, v in types_list.items():
            if aminoacid not in v:
                self.type[key][k] = 0
            else:
                self.type[key][k] = 1

    def calculate_shelling(self, pdb, shelling_path):
        all_chains = self.chains[pdb][0] + self.chains[pdb][1]
        for chain in all_chains:
            identifier = pdb + '_' + chain
            no_interface = ['3SEK_A']
            if identifier not in no_interface:
                with open(shelling_path % (identifier), 'r') as fin:
                    for row in fin:
                        row = OneResidueMethodsVoronota(row, 'Ann')
                        key = pdb + ' ' + row.get_key()
                        self.shelling[key] = row.get_shelling()

    def calculate_volume(self, pdb, volume_monomer_path, volume_complex_path):
        chains = [self.chains[pdb][0], self.chains[pdb][1]]
        identifier = pdb + '_'
        for chain in chains:
            id = identifier + chain
            with open(volume_monomer_path % (id), 'r') as fin:
                for row in fin:
                    row = OneResidueMethodsVoronota(row, 'Ann')
                    key = pdb + ' ' + row.get_key()
                    self.volume_monomer[key] = row.get_volume()
            with open(volume_complex_path % (pdb), 'r') as fin:
                for row in fin:
                    row = OneResidueMethodsVoronota(row, 'Ann')
                    key = pdb + ' ' + row.get_key()
                    self.volume_complex[key] = row.get_volume()

    def calculate_everything(self, pdb, residue_path, atom_path, shelling_path, volume_monomer_path, volume_complex_path, graphs):
        self.calculate_contacts(pdb, atom_path)
        self.calculate_tags_and_type(pdb, residue_path)
        self.calculate_shelling(pdb, shelling_path)
        self.calculate_volume(pdb, volume_monomer_path, volume_complex_path)
        self.get_graphs(pdb, residue_path)
        if graphs == 'Y':
            return self.area_h, self.hydrophobic, self.bsa, self.asa, self.hb, self.sb, self.shelling, self.cont, self.type, self.interface, self.volume_complex, self.volume_monomer, self.graphs
        return self.area_h, self.hydrophobic, self.bsa, self.asa, self.hb, self.sb, self.shelling, self.cont, self.type, self.interface, self.volume_complex, self.volume_monomer

    def write_to_dictionary(self, key, value, dictionary):
        if key not in dictionary.keys():
            dictionary[key] = []
        if value not in dictionary[key]:
            dictionary[key].append(value)

    def write_to_dictionary_addition(self, key, value, dictionary):
        if key not in dictionary.keys():
            dictionary[key] = 0
        dictionary[key] += value

    def write_to_same_or_diff(self, row: TwoResidueMethodsVoronota, value, dictionary):
        if row.is_different_proteins():
            self.write_to_dictionary_addition(row.get_first(), value, dictionary['Diff'])
            self.write_to_dictionary_addition(row.get_second(), value, dictionary['Diff'])
        elif not row.is_different_proteins():
            self.write_to_dictionary_addition(row.get_first(), value, dictionary['Same'])
            self.write_to_dictionary_addition(row.get_second(), value, dictionary['Same'])

    # write to area_h_d
    def calculate_area_h(self, row: TwoResidueMethodsVoronota, dictionary):
        if not row.is_hydrophobic_contact():
            self.write_to_same_or_diff(row, row.get_area(), dictionary)

    # write to hydrophobic
    def calculate_hydrophobic(self, row: TwoResidueMethodsVoronota, dictionary):
        if row.is_hydrophobic_contact():
            self.write_to_same_or_diff(row, row.get_area(), dictionary)

    # used for bsa and asa
    def write_to_hydrophobic_or_hydrophylic(self, key, res: OneResidueMethodsVoronota, dictionary):
        if res.is_hydrophobic_atom():
            self.write_to_dictionary_addition(key, res.get_area(), dictionary['Hydrophobic'])
        else:
            self.write_to_dictionary_addition(key, res.get_area(), dictionary['Hydrophylic'])

    def calculate_bsa(self, row: TwoResidueMethodsVoronota, dictionary):
        if row.is_different_proteins():
            self.write_to_hydrophobic_or_hydrophylic(row.get_first(), row.get_first_object(), dictionary)
            self.write_to_hydrophobic_or_hydrophylic(row.get_second(), row.get_second_object(), dictionary)
            self.write_to_interface(row.get_first(), self.interface)
            self.write_to_interface(row.get_second(), self.interface)

    def calculate_asa(self, row: TwoResidueMethodsVoronota, dictionary):
        if 'solv' in row.get_second():
            self.write_to_hydrophobic_or_hydrophylic(row.get_first(), row.get_first_object(), dictionary)

    def write_to_interface(self, key, list):
        if key not in list:
            list.append(key)

    def get_graphs(self, pdb, file):
        with open(file, 'r') as fin:
            for row in fin:
                row = TwoResidueMethodsVoronota(row, pdb, self.chains)
                first = row.get_first()
                second = row.get_second()
                if first in self.interface and second in self.interface:
                    self.graphs.append([first, second, row.get_area()])
