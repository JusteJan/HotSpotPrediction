Workflow:

PDB file should be in files folder: 1A4Y.pdb
hb2 file should be in data folder: 1A4Y.hb2

aln files for each chain in files folder: 1A4Y_A.aln, 1A4Y_B.aln
if there are no aln files, run from scripts folder:
python CreateAlignment.py 1A4Y A B
hmmer, easel (comes with hmmer), cd-hit and mafft must be installed

bash contacts.sh 1A4Y
cd mstatx
./mstatx -i ../../files/1A4Y_A.aln -s trident -o ../../data/mstatx/1A4Y_A
./mstatx -i ../../files/1A4Y_B.aln -s trident -o ../../data/mstatx/1A4Y_B
cd ../capra
python score_conservation.py -o ../../data/caprasingh/1A4Y_A -s property_entropy ../../files/1A4Y_A.aln
python score_conservation.py -o ../../data/caprasingh/1A4Y_B -s property_entropy ../../files/1A4Y_B.aln
cd ..
./al2co -i ../files/1A4Y_A_clustalw.aln -o ../data/al2co/1A4Y_A -a T
./al2co -i ../files/1A4Y_B_clustalw.aln -o ../data/al2co/1A4Y_B -a T
bash calculate-interface-depth.bash for each chain
voronota-volumes for complex and chains
python3 CalculateForPDBFile.py 1A4Y A B
python3 EvolutionaryScoresForPredicting.py N ../files/1A4Y_A_B.csv 1A4Y_A 1A4Y_B
Rscript CalculateCentrality.R ../files/1A4Y_A_B
python ExtractCentrality.py ../files/1A4Y_A_B.csv ../files/1A4Y_A_B_centrality.csv
cd ..
Rscript Predict.R files/1A4Y_A_B

