import re
from constants import CONVERT_AMINOACIDS


class OneResidueMethodsVoronota(object):

    def __init__(self, row, annotation):
        self.row = row
        self.annotation = annotation

    def get_splitted_row(self, *args):
        attributes = {}
        row_splitted = [x for x in self.row.split(' ') if x != '']
        if len(args) > 0:
            return self.get_splitted_row_volumes()
        if len(row_splitted) < 5:
            return self.get_splitted_row_shelling()
        try:
            (attributes['Ann1'], attributes['Ann2'],
             attributes['Area'], attributes['Distance'],
             attributes['Tags'], attributes['Adjuncts']) = row_splitted
        except ValueError:
            print(row_splitted)

        return attributes

    def get_splitted_row_shelling(self):
        attributes = dict()
        row_splitted = [x for x in self.row.rstrip('\n').split(' ') if x != '']
        (attributes['Ann'],
         attributes['Shelling']) = row_splitted
        return attributes

    def get_splitted_row_volumes(self):
        attributes = dict()
        row_splitted = [x for x in self.row.rstrip('\n').split(' ') if x != '']
        (attributes['Ann'],
         attributes['Volume']) = row_splitted
        return attributes

    def get_attribute(self, attribute):
        return ''.join(re.findall(r'%s<(.+?)>' % (attribute), self.get_splitted_row()[self.annotation]))

    def get_chain(self):
        return self.get_attribute('c')

    def get_aminoacid(self):
        if self.get_attribute(('R')) not in CONVERT_AMINOACIDS.keys():
            return 'NONE'
        return CONVERT_AMINOACIDS[self.get_attribute('R')]

    def get_position(self):
        return self.get_attribute('r')

    def get_identifier(self):
        return self.get_attribute('i')

    def get_atom(self):
        return self.get_attribute('A')

    def get_key(self):
        chain = self.get_chain()
        aminoacid = self.get_aminoacid()
        position = self.get_position() + self.get_identifier().lower()
        if 'solv' not in self.get_splitted_row()[self.annotation]:
            return chain + ' ' + aminoacid + ' ' + position
        return 'solv' + ' ' + 'solv'

    def get_area(self):
        return float(self.get_splitted_row()['Area'])

    def get_tags(self):
        return self.get_splitted_row()['Tags']

    def get_shelling(self):
        return float(self.get_splitted_row_shelling()['Shelling'])

    def get_volume(self):
        return float(self.get_splitted_row_volumes()['Volume'])

    # can be used for ASA and BSA
    def is_hydrophobic_atom(self):
        aa = self.get_aminoacid()
        a = self.get_atom()[0]
        if (a in 'C') or (a in 'S' and aa in 'M'):
            return True
        return False
