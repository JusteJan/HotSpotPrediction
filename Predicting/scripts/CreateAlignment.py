from Bio.SubsMat import MatrixInfo as matlist
from Bio.PDB.PDBParser import PDBParser
from Bio.SeqRecord import SeqRecord
from Bio import pairwise2
from Bio import SearchIO
from Bio.Seq import Seq
from Bio import SeqIO
import subprocess
import sys
import os
import logging

import settings

parser = PDBParser(PERMISSIVE=1)

def usage():
    print('Usage: CreateAlignment.py pdb_file chain1 chain2')

def main(argv):
    if len(argv) != 3:
        usage()
        sys.exit(1)
    logging.basicConfig(format='%(levelname)s: %(message)s', level=logging.INFO)

    files = []
    sequences = {}
    filenames = {}
    filename = argv[0]
    working_directory = os.path.dirname(filename)
    job_name = os.path.basename(filename).rstrip('.pdb')
    chains = argv[1] + argv[2]
    handle = open(filename, 'r')

    #extract sequences from PDB file
    logging.info('Extracting sequences from given PDB file.')
    for record in SeqIO.parse(handle, 'pdb-seqres'):
        chain = record.id.split(':')[-1]
        if chain in chains:
            seq = SeqRecord(record.seq, id='%s_%s' %(job_name, chain))
            f = os.path.join(working_directory, 'sequences', '%s_%s.fasta' %(job_name, chain))
            files.append(f)
            sequences[f] = seq
            filenames[f] = job_name + '_' + chain
            SeqIO.write(seq, f, 'fasta')
    handle.close()

    logging.info('Running sequence search.')
    for f in files:
        logging.info('Running phmmer and finding homologs for %s.', f)
        subprocess.run([settings.phmmer, '-E', '0.0001', '--domE', '0.0001', '--incE', '0.0001', '-o', '%s.hmmer' %(f), f, settings.uniref90])
        qresult = SearchIO.read('%s.hmmer' %(f), 'hmmer3-text')
        for hit in qresult:
            with open('%s.list' %(f), 'a') as fout:
                print(hit.id, file=fout)

        logging.info('Running easel to get homologue sequences.')
        easel = os.path.join(settings.easel_dir, 'miniapps', 'esl-sfetch')
        with open('%s.sequence' %(f), 'w') as seq_file:
            subprocess.run([easel, '-f', settings.uniref90, '%s.list' %(f)],  stdout=seq_file)

        logging.info('Running cd-hit.')
        subprocess.run([settings.cdhit, '-i', '%s.sequence' %(f), '-o', '%s.cdhit' %(f), '-c', '0.95', '-d', '0'])

        logging.info('Filtering sequences - leaving only relevant.')
        clustered_sequences = []
        with open('%s.cdhit.clstr' %(f), 'r') as cluster:
            for row in cluster:
                if '...' in row and '*' in row:
                    seq_name = row.split('>')[1].split('...')[0]
                    clustered_sequences.append(seq_name)

        logging.info('Filtering sequences - removing based on sequence identity.')
        write_sequences = [sequences[f]]
        with open('%s.sequence' %(f), 'r') as seq_file_handle:
            for record in SeqIO.parse(seq_file_handle, 'fasta'):
                id = record.id.split(' ')[0]
                if id not in clustered_sequences:
                    continue
                seq_sequence = record.seq
                align = pairwise2.align.globalxx(sequences[f].seq, seq_sequence)
                matches = align[0][2]
                seq_id = (100 * matches) / len(align[0][0])
                if seq_id < 95 and seq_id > 35:
                    write_sequences.append(record)
                if len(write_sequences) >= 150:
                    break
        filename_fasta_for_msa = '%s_filtered' % (f)
        SeqIO.write(write_sequences, filename_fasta_for_msa, 'fasta')

        logging.info('Running mafft.')
        with open(os.path.join(working_directory, '%s.aln' %(filenames[f])), 'w') as msa_file:
            subprocess.run([settings.mafft, '--quiet', filename_fasta_for_msa], stdout=msa_file)


if __name__ == '__main__':
    main(sys.argv[1:])
