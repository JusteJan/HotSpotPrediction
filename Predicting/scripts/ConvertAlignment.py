#! /usr/bin/env python3
"""Convert Fasta alignment file to Clustal format. Usage:
$0 input_fasta_file (output_clustalw_file)
"""

import sys
import os

from Bio import AlignIO

def main(arguments):
    if ('-h' in arguments) or ('--help' in arguments):
        print(__doc__)
        return

    input_fasta_file = arguments[1]
    try:
        output_clustalw_file = arguments[2]
        output = open(output_clustalw_file, 'w')
    except IndexError:
        output = sys.stdout

    with open(input_fasta_file, 'r') as f:
        alignment = AlignIO.read(f, 'fasta')

    output.write(alignment.format('clustal'))
    output.close


if __name__ == '__main__':
    sys.exit(main(sys.argv))

