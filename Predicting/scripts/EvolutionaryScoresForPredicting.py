import csv
import sys
from tempfile import NamedTemporaryFile
import shutil
import os


def main(argv):
    d_scores = {}
    d_get_keys = {}
    usable_positions = {}
    normalization = {}
    working_directory = os.path.dirname(os.path.dirname(argv[1]))
    
    if len(argv) < 3:
        usage()
        sys.exit(1)

    ab = argv[0]
    files = argv[2:]
    if ab not in ('Y', 'y'):
        #-----------------al2co-----------------
        path = os.path.join(working_directory, 'data/al2co')
        for i in range(1, 10):
            normalization['out%s' %(i)] = []
        for file in files:
            pdb, chain = file.split('_')
            usable_positions[file] = {}
            n = 0
            with open(path+'/%s' %(file), 'r') as fin:
                for row in fin:
                    if row[0] == '*':
                        break
                    if '#' not in row:
                        row = row.split(' ')
                        row = [x for x in row if x != ' ' and x != '']
                        pos = row[0]
                        aa = row[1]
                        if aa != '-':
                            n += 1
                            usable_positions[file][int(pos)] = n
                            key = pdb + ' ' + chain + ' ' + str(n)
                            d_get_keys[key] = key + ' ' + aa
                            key = key + ' ' + aa
                            if key not in d_scores.keys():
                                d_scores[key] = {}
                            d_scores[key]['al2co'] = {}
                            counter_rows = 2
                            for i in range(1, 10):
                                d_scores[key]['al2co']['out%s' %(i)] = float(row[counter_rows])
                                if float(row[counter_rows]) > -1000:
                                    normalization['out%s' %(i)].append(float(row[counter_rows]))
                                counter_rows += 1
        #----------------caprasingh-------------
        path = os.path.join(working_directory, 'data/caprasingh')
        for file in files:
            pdb = file.split('_')[0]
            chain = file.split('_')[1]
            with open(path+'/%s' %(file), 'r') as fin:
                for row in fin:
                    if '#' not in row:
                        row = row.split('\t')
                        row = [x for x in row if x != ' ' and x != '']
                        pos = int(row[0]) + 1
                        if pos in usable_positions[file].keys():
                            pos = usable_positions[file][pos]
                            me = 'caprasingh_property_entropy'
                            if me not in normalization.keys():
                                normalization[me] = []
                            aa = row[2][0]
                            key = pdb + ' ' + chain + ' ' + str(pos) + ' ' + aa
                            d_scores[key][me] = float(row[1])
                            if float(row[1]) > -1000:
                                normalization[me].append(float(row[1]))
        #---------------------------------------

        #----------------mstatax----------------
        path = os.path.join(working_directory, 'data/mstatx')
        for file in files:
            pdb, chain = file.split('_')
            with open(path+'/%s' %(file), 'r') as fin:
                for row in fin:
                    row = row.rstrip('\n').split('\t')
                    row = [x for x in row if x != ' ' and x != '']
                    pos = int(row[0])
                    if pos in usable_positions[file].keys():
                        pos = usable_positions[file][pos]
                        me = 'mstatx_trident'
                        if me not in normalization.keys():
                            normalization[me] = []
                        key = pdb + ' ' + chain + ' ' + str(pos)
                        key = d_get_keys[key]
                        d_scores[key][me] = float(row[1])
                        normalization[me].append(float(row[1]))
        #---------------------------------------
    with open(argv[1], 'r') as fin:
        tempfile = NamedTemporaryFile(mode='w', delete=False)
        reader = csv.DictReader(fin, delimiter=';')
        header = reader.fieldnames
        evoheader = ['out6', 'out9', 'mstatx_trident', 'caprasingh_property_entropy']
        header.extend(evoheader)
        writer = csv.DictWriter(tempfile, delimiter=';', fieldnames=header, lineterminator='\n')
        writer.writeheader()
        for row in reader:
            key = row['PDB'] + ' ' + row['Chain'] + ' ' + row['Position'] + ' ' + row['Aminoacid']
            r = row
            if ab in ('Y', 'y'):
                r['out6'] = 5
                r['out9'] = 5
                r['mstatx_trident'] = 0.5
                r['caprasingh_property_entropy'] = 0.5
            else:
                if key in d_scores.keys():
                    r['out6'] = int(9.99*((d_scores[key]['al2co']['out6'] - min(normalization['out6']))/(max(normalization['out6']) - min(normalization['out6']))))
                    r['out9'] = int(9.99*((d_scores[key]['al2co']['out9'] - min(normalization['out9']))/(max(normalization['out9']) - min(normalization['out9']))))
                    r['mstatx_trident'] = 0 if d_scores[key]['mstatx_trident'] == -1000 else round((d_scores[key]['mstatx_trident'] - min(normalization['mstatx_trident']))/(max(normalization['mstatx_trident']) - min(normalization['mstatx_trident'])), 2)
                    r['caprasingh_property_entropy'] = 0 if d_scores[key]['caprasingh_property_entropy'] == -1000 else round((d_scores[key]['caprasingh_property_entropy'] - min(normalization['caprasingh_property_entropy']))/(max(normalization['caprasingh_property_entropy']) - min(normalization['caprasingh_property_entropy'])), 2)
                else:
                    r['out6'] = 5
                    r['out9'] = 5
                    r['mstatx_trident'] = 0.5
                    r['caprasingh_property_entropy'] = 0.5
            writer.writerow(r)
        tempfile.close()
    shutil.move(tempfile.name, argv[1])

def usage():
    print('Usage: EvolutionaryScoresForPredicting.py antibody("Y"/"N") path_to_structural_features alignment_files')

if __name__ == '__main__':
    main(sys.argv[1:])
