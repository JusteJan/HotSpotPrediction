#!/bin/bash

settings_file="$1"

. $settings_file

readonly SCRIPTDIR=$(readlink -m $(dirname $0))

mkdir -p $working_directory/data

true > $working_directory/logs/voronota.log

( \
cat $working_directory/files/$job_name.pdb\
| voronota get-balls-from-atoms-file --radii-file <(voronota-resources radii) --annotated \
| tee $working_directory/data/balls \
| voronota calculate-contacts --probe 1.4 --annotated \
| voronota query-contacts --preserve-graphics --set-hbplus-tags $working_directory/data/$job_name.hb2 \
| voronota query-contacts --preserve-graphics --set-tags 'sb' --match-first 'R<ASP,GLU>&A<OD1,OD2,OE1,OE2,OXT>' --match-second 'R<ARG,HIS,LYS>&A<NH1,NH2,ND1,NE2,NZ>' --match-max-dist 4.0 \
| voronota query-contacts --preserve-graphics --set-tags 'ds' --match-first 'R<CYS>&A<SG>' --match-second 'R<CYS>&A<SG>' --match-max-dist 3.0 \
| tee $working_directory/data/contacts_raw \
| voronota query-contacts --no-same-chain \
> $working_directory/data/contacts_clean \
) 2>> $working_directory/logs/voronota.log

( \
cat $working_directory/data/contacts_raw \
| voronota query-contacts --inter-residue --preserve-graphics \
| tee $working_directory/data/contacts_ir_raw \
| voronota query-contacts \
> $working_directory/data/contacts_ir_clean \
) 2>> $working_directory/data/log

true > $working_directory/data/finished
