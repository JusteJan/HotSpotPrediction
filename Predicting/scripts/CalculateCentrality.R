library(igraph)

args <- commandArgs(trailingOnly=TRUE)
l <- data.frame(key=c(), betweenness.w_interface=c(), closeness.w_interface=c())

dat <- read.csv(paste0(args[1], '_graph.csv'), sep=';', header=TRUE)
dat <- as.matrix(dat)
g <- graph.edgelist(dat[, 1:2], directed=FALSE)
E(g)$weight=as.numeric(dat[, 3])
E(g)$inverseweight=1/as.numeric(dat[, 3])
E(g)$unweighted=1

betweenness.w <- data.frame(betweenness.w = betweenness(g, weights = E(g)$inverseweight))
c <- betweenness.w$betweenness.w
betweenness.w$betweenness.w <- (c - min(c))/(max(c)-min(c))
betweenness.w$key <- row.names(betweenness.w)
closeness.w <- data.frame(closeness.w = closeness(g, weights = E(g)$inverseweight))
c <- closeness.w$closeness.w
closeness.w$closeness.w <- (c - min(c))/(max(c)-min(c))
closeness.w$key <- row.names(closeness.w)

k <- merge(closeness.w, betweenness.w, by=c("key"))
l <- rbind(l, k)

write.table(l, paste0(args[1], '_centrality.csv'), sep=';', row.names=FALSE)




