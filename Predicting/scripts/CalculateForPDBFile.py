from CalculateFeatures import *
import csv
import sys
import os

def usage():
    print('Usage: CalculateForPDBFile.py pdb chains1 chains2 working_directory')


def main(argv):
    if len(argv) != 4:
        usage()
        sys.exit(1)

    pdb = argv[0]
    ch1 = argv[1]
    ch2 = argv[2]
    data_directory = argv[3]

    header = ['PDB', 'Aminoacid', 'Chain', 'Position',
              'Area_H_Diff', 'Hydrophobic_Diff', 'Cont_Diff',
              'hb_Diff', 'hb_Diff_10', 'BSA', 'ASA',
              'Shelling', 'Aromatic', 'Aliphatic',
              'Polar', 'Positive', 'Negative', 'deltaVolume']

    pathresi = os.path.join(data_directory, 'data/contacts_ir_clean')
    pathatom = os.path.join(data_directory, 'data/contacts_clean')
    pathshelling = os.path.join(data_directory, 'data/%s/residue_depth_values.txt')
    pathvolumem = os.path.join(data_directory, 'data/volume_monomers/%s')
    pathvolumec =os.path.join(data_directory,  'data/volume_complex/%s')

    allchains = ch1+ch2
    d = {}
    d[pdb] = [ch1, ch2]
    print(d)
    calculator = CalculateFeatures(d)
    area_h, hydrophobic, bsa, asa, hb, sb, shelling, contacts, types, interf, vol_c, vol_m, graphs = calculator.calculate_everything(pdb, pathresi, pathatom, pathshelling, pathvolumem, pathvolumec, 'Y')
    output_file1 = os.path.join(data_directory, 'files', pdb+'_'+ch1+'_'+ch2+'.csv')
    output_file2 = os.path.join(data_directory, 'files', pdb+'_'+ch1+'_'+ch2+'_graph'+'.csv')
    with open(output_file1, 'w') as fout, open(output_file2, 'w') as fout2:
        writer = csv.DictWriter(fout, delimiter=';', fieldnames=header, lineterminator='\n')
        writer.writeheader()
        graphfieldnames = ['V1', 'V2', 'weight']
        writer2 = csv.DictWriter(fout2, delimiter=';', fieldnames=graphfieldnames, lineterminator='\n')
        writer2.writeheader()

        for row in graphs:
            rg = {}
            rg['V1'], rg['V2'], rg['weight'] = row
            writer2.writerow(rg)

        for key in interf:
            r = {}
            r['PDB'], r['Chain'], r['Aminoacid'], r['Position'] = key.split(' ')
            if r['Chain'] in allchains:
                r['Area_H_Diff'] = area_h['Diff'][key] if key in area_h['Diff'].keys() else 0
                r['Hydrophobic_Diff'] = hydrophobic['Diff'][key] if key in hydrophobic['Diff'].keys() else 0
                r['Cont_Diff'] = contacts['Diff'][key] if key in contacts['Diff'].keys() else 0
                bsahph = bsa['Hydrophobic'][key] if key in bsa['Hydrophobic'].keys() else 0
                bsanhph = bsa['Hydrophylic'][key] if key in bsa['Hydrophylic'].keys() else 0
                r['BSA'] = bsahph + bsanhph
                asahph = asa['Hydrophobic'][key] if key in asa['Hydrophobic'].keys() else 0
                asanhph = asa['Hydrophylic'][key] if key in asa['Hydrophylic'].keys() else 0
                r['ASA'] = asahph + asanhph
                r['Shelling'] = shelling[key] if key in shelling.keys() else 0
                r['hb_Diff'] = hb['Diff'][key] if key in hb['Diff'].keys() else 0
                r['hb_Diff_10'] = 1 if r['hb_Diff'] > 0 else 0
                r['deltaVolume'] = vol_m[key] - vol_c[key]
                r.update(types[key])
                writer.writerow(r)

if __name__ == '__main__':
    main(sys.argv[1:])
