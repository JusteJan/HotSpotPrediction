import csv
import sys
from tempfile import NamedTemporaryFile
import shutil


def usage():
    print('Usage: ExtractCentrality.py path_to_features centrality_file')


def main(argv):
    if len(argv) < 2:
        usage()
    else:
        d_centrality = {}
        with open(argv[1], 'r') as fin:
            reader = csv.DictReader(fin, delimiter=';')
            for row in reader:
                key = row['key']
                d_centrality[key] = {}
                d_centrality[key]['betweenness.w'] = row['betweenness.w']
                d_centrality[key]['closeness.w'] = row['closeness.w']

        with open(argv[0], 'r') as fin:
            tempfile = NamedTemporaryFile(mode='w', delete=False)
            reader = csv.DictReader(fin, delimiter=';')
            header = reader.fieldnames
            header.extend(['betweenness.w_interface', 'closeness.w_interface'])
            writer = csv.DictWriter(tempfile, delimiter=';', fieldnames=header, lineterminator='\n')
            writer.writeheader()
            for row in reader:
                key = row['PDB'] + ' ' + row['Chain'] + ' ' + row['Aminoacid'] + ' ' + row['Position']
                r = row
                r['betweenness.w_interface'] = d_centrality[key]['betweenness.w']
                r['closeness.w_interface'] = d_centrality[key]['closeness.w']
                writer.writerow(r)
        tempfile.close()
        shutil.move(tempfile.name, argv[0])

if __name__ == '__main__':
    main(sys.argv[1:])
