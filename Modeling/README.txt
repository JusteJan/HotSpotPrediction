Scripts:
CalculateForDatasets: calculate structural predictors
CreateEvolutionaryScores: calculate evolutionary predictors
CreateCleanOtherServersFile: parse other servers
CreateCleanThresholdFiles: generate results from Threshold/Files directory files into Clean_Results
CreateThresholdFiles: generate thresholdIndependent and thresholdTraining
Datasets_For_Graphs: generate data frames for graph calculation
ExtractGraphs: generate graph predictors
JoinCleanResults: generate joined result files from Clean_Results
parse_cross_validation: generate hyperparameters from cross-validation

CrossValidation.R: perform cross-validation
TrainingAndTesting: generate training and testing results (modeling)
ThresholdAndOtherServers: generate threshold files


Data:
Datasets/MachineLearning: predictor files. independent/training/testing: structural
predictors, Evolutionary: evolutionary predictors, Graph: graph predictors

Datasets/MachineLearning/CleanResults: result files for models

Datasets/MachineLearning/CrossValidation: cross validation result files

Datasets/MachineLearning/Results: various modeling result files:
modelnamePredictions - predictions for testing set
modelnamePredictions_Independent: predictions for independent set
Threshold/: folder for working with threshold files. 
thresholdIndependent or Testing: working file for various thresholds
Files/: results for thresholds



