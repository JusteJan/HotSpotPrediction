import csv

training = {}
testing = {}
independent = {}
with open('../Datasets/MachineLearning/training.csv') as fin1,\
     open('../Datasets/MachineLearning/testing.csv') as fin2,\
     open('../Datasets/MachineLearning/independent.csv') as fin3:
        reader = csv.DictReader(fin1, delimiter=';')
        reader2 = csv.DictReader(fin2, delimiter=';')
        reader3 = csv.DictReader(fin3, delimiter=';')
        for row in reader:
            key = row['PDB'] + ' ' + row['Chain'] + ' ' + row['Aminoacid'] + ' ' + row['Position']
            training[key] = row['HS']
        for row in reader2:
            key = row['PDB'] + ' ' + row['Chain'] + ' ' + row['Aminoacid'] + ' ' + row['Position']
            testing[key] = row['HS']
        for row in reader3:
            key = row['PDB'] + ' ' + row['Chain'] + ' ' + row['Aminoacid'] + ' ' + row['Position']
            independent[key] = row['HS']
c = ['betweenness', 'betweenness.w', 'closeness', 'closeness.w', 'degree']
training_c = {}
testing_c = {}
independent_c = {}

header = ['PDB', 'Chain', 'Aminoacid', 'Position', 'HS']

with open('../Datasets/Graphs/Monomer.csv', 'r') as fin:
    reader = csv.DictReader(fin, delimiter=';')
    for row in reader:
        k = row['key']
        if k in training.keys():
            if k not in training_c.keys():
                training_c[k] = {}
            for i in c:
                j = i + '_monomer'
                if j not in header:
                    header.append(j)
                training_c[k][j] = float(row[i])
        if k in testing.keys():
            if k not in testing_c.keys():
                testing_c[k] = {}
            for i in c:
                j = i + '_monomer'
                testing_c[k][j] = float(row[i])
        if k in independent.keys():
            if k not in independent_c.keys():
                independent_c[k] = {}
            for i in c:
                j = i + '_monomer'
                independent_c[k][j] = float(row[i])

with open('../Datasets/Graphs/Complex.csv', 'r') as fin:
    reader = csv.DictReader(fin, delimiter=';')
    for row in reader:
        k = row['key']
        if k in training.keys():
            if k not in training_c.keys():
                training_c[k] = {}
            for i in c:
                j = i + '_complex'
                if j not in header:
                    header.append(j)
                training_c[k][j] = float(row[i])
        if k in testing.keys():
            if k not in testing_c.keys():
                testing_c[k] = {}
            for i in c:
                j = i + '_complex'
                testing_c[k][j] = float(row[i])
        if k in independent.keys():
            if k not in independent_c.keys():
                independent_c[k] = {}
            for i in c:
                j = i + '_complex'
                independent_c[k][j] = float(row[i])

with open('../Datasets/Graphs/Interface.csv', 'r') as fin:
    reader = csv.DictReader(fin, delimiter=';')
    for row in reader:
        k = row['key']
        if k in training.keys():
            if k not in training_c.keys():
                training_c[k] = {}
            for i in c:
                j = i + '_interface'
                if j not in header:
                    header.append(j)
                training_c[k][j] = float(row[i])
        if k in testing.keys():
            if k not in testing_c.keys():
                testing_c[k] = {}
            for i in c:
                j = i + '_interface'
                testing_c[k][j] = float(row[i])
        if k in independent.keys():
            if k not in independent_c.keys():
                independent_c[k] = {}
            for i in c:
                j = i + '_interface'
                independent_c[k][j] = float(row[i])

with open('../Datasets/MachineLearning/Graphtraining.csv', 'w') as fout1,\
     open('../Datasets/MachineLearning/Graphtesting.csv', 'w') as fout2,\
     open('../Datasets/MachineLearning/Graphindependent.csv', 'w') as fout3:
        writer1 = csv.DictWriter(fout1, delimiter=';', fieldnames=header, lineterminator='\n')
        writer2 = csv.DictWriter(fout2, delimiter=';', fieldnames=header, lineterminator='\n')
        writer3 = csv.DictWriter(fout3, delimiter=';', fieldnames=header, lineterminator='\n')
        writer1.writeheader()
        writer2.writeheader()
        writer3.writeheader()
        for key in training.keys():
            r = {}
            r['PDB'], r['Chain'], r['Aminoacid'], r['Position'] = key.split(' ')
            r['HS'] = training[key]
            for item in header:
                if item not in r.keys():
                    print(key)
                    if item not in training_c[key].keys():
                        r[item] = '0*'
                    else:
                        r[item] = training_c[key][item]
            writer1.writerow(r)
        for key in testing.keys():
            r = {}
            r['PDB'], r['Chain'], r['Aminoacid'], r['Position'] = key.split(' ')
            r['HS'] = testing[key]
            for item in header:
                if item not in r.keys():
                    r[item] = testing_c[key][item]
            writer2.writerow(r)
        for key in independent.keys():
            r = {}
            r['PDB'], r['Chain'], r['Aminoacid'], r['Position'] = key.split(' ')
            r['HS'] = independent[key]
            for item in header:
                if item not in r.keys():
                    if item not in independent_c[key].keys():
                        r[item] = '0*'
                    else:
                        r[item] = independent_c[key][item]
            writer3.writerow(r)