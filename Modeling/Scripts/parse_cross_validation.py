import os
from os import listdir
from os.path import isfile, join

def addmetrics(metric, list_of_rows, maxroc):
    if metric == 'gbm':
        header = ["interaction", "minobs", "trees", "ROC", "Sens"]
        if n >= 13 and n < 137:
            r = row.rstrip('\n').split(' ')
            r = [x for x in r if x != ' ' and x != '']
            r = r[:5]
            list_of_rows.append(r)
            if float(r[3]) > maxroc:
                maxroc = float(r[3])
    elif metric == 'pls':
        header = ["nt", "alpha", "ROC", "Sens"]
        if n >= 13 and n < 19:
            r = row.rstrip('\n').split(' ')
            r = [x for x in r if x != ' ' and x != '']
            r = r[:4]
            list_of_rows.append(r)
            if float(r[2]) > maxroc:
                maxroc = float(r[2])
    elif metric == "rf":
        header = ["mtry", "ROC", "Sens"]
        if n >= 13 and n < 18:
            r = row.rstrip('\n').split(' ')
            r = [x for x in r if x != ' ' and x != '']
            r = r[:3]
            list_of_rows.append(r)
            if float(r[1]) > maxroc:
                maxroc = float(r[1])
    elif metric == 'rpart':
        header = ["cp", "ROC", "Sens"]
        if n >= 13 and n < 113:
            r = row.rstrip('\n').split(' ')
            r = [x for x in r if x != ' ' and x != '']
            r = r[:3]
            list_of_rows.append(r)
            if float(r[1]) > maxroc:
                maxroc = float(r[1])
    elif metric == 'svm':
        header = ["cost", "ROC", "Sens"]
        if n >= 13 and n < 23:
            r = row.rstrip('\n').split(' ')
            r = [x for x in r if x != ' ' and x != '']
            r = r[:3]
            list_of_rows.append(r)
            if float(r[1]) > maxroc:
                maxroc = float(r[1])
    return header, maxroc

def get_all_mods(path, evolutionary_variables, evolutionary_variables2, graph_variables):
    if 'Evolutionary' in path:
        return evolutionary_variables
    if 'MoreEvo' in path:
        return evolutionary_variables2
    if 'Selected' in path:
        return graph_variables
    return ''

#Write relevant path here
# path = '../Datasets/MachineLearning/CrossValidation/BasicModels/'
# path = '../Datasets/MachineLearning/CrossValidation/BasicModelsVol/'
# path = '../Datasets/MachineLearning/CrossValidation/MoreEvo2Volume/'
# path = '../Datasets/MachineLearning/CrossValidation/EvolutionaryModelsAvg2Volume/'
path = '../Datasets/MachineLearning/CrossValidation/SelectedModels/'


onlyfiles = [f for f in listdir(path) if isfile(join(path, f))]
onlyfiles = [f for f in onlyfiles if "grid" not in f]
methods = {}
methods["cost"] = {}
methods["mtry"] = {}
methods["trees"] = {}
methods["minobs"] = {}
methods["interaction"] = {}
methods["nt"] = {}
methods["alpha"] = {}
methods["cp"] = {}

evolutionary_variables = ['out1', 'out2', 'out3', 'out4', 'out5', 'out6',
                            'out7', 'out8', 'out9', 'caprasingh_shannon_entropy',
                            'caprasingh_sum_of_pairs','mstatx_trident','mstatx_kabat',
                            'caprasingh_property_entropy','caprasingh_property_relative_entropy',
                            'caprasingh_js_divergence','mstatx_jensen','mstatx_wentropy',
                            'caprasingh_relative_entropy', 'ConSurf']


evolutionary_variables2 = ['out1+out4+out7+ConSurf','out1+out5+out7','caprasingh_relative_entropy+out5+out9',
         'caprasingh_property_relative_entropy+out4+out8+ConSurf',
         'out1+out5+out7+ConSurf','out2+out6+out9+ConSurf','out3+out4+out9',
         'out2+out5+out8+ConSurf','mstatx_trident+out4+out9',
         'caprasingh_js_divergence+mstatx_kabat+out9','out1+mstatx_kabat+out7+ConSurf',
         'caprasingh_js_divergence+mstatx_kabat+out9+ConSurf',
         'out3+out4+caprasingh_sum_of_pairs','out2+out5+caprasingh_sum_of_pairs',
         'out2+out6+out7+ConSurf','caprasingh_js_divergence+mstatx_kabat+out7',
         'mstatx_trident+out4+out8','caprasingh_relative_entropy+out4+out7+ConSurf',
         'out1+out4+out9','caprasingh_property_relative_entropy+out5+out8'
]

graph_variables = [
    'betweenness_interface+closeness_interface+degree_interface',
    'closeness_interface+closeness.w_interface',
    'betweenness_interface+betweenness.w_interface',
    'betweenness_interface+betweenness.w_interface+closeness_interface+closeness.w_interface+degree_interface',
    'degree_interface',
    'closeness.w_interface',
    'closeness_interface',
    'betweenness.w_interface',
    'betweenness_interface',
    'betweenness.w_interface+closeness.w_interface',
    'betweenness_interface+closeness_interface'
]

#Determine hyperparameters
for file in onlyfiles:
    with open(path+file, 'r') as fin:
        list_of_rows = []
        filtered_roc = []
        maxroc = 0
        sens = 0
        n = 0
        for row in fin:
            n += 1
            if "_gbm" in file and 'BasicModels' not in path:
                method = file.replace("_gbm.txt", '')
                header, maxroc = addmetrics('gbm', list_of_rows, maxroc)
            elif "gbm" in file and "BasicModels" in path:
                method = file.replace('.txt', '')
                header, maxroc = addmetrics('gbm', list_of_rows, maxroc)
            elif "_pls" in file and 'BasicModels' not in path:
                method = file.replace("_pls.txt", '')
                header, maxroc = addmetrics('pls', list_of_rows, maxroc)
            elif "pls" in file and "BasicModels" in path:
                method = file.replace('.txt', '')
                header, maxroc = addmetrics('pls', list_of_rows, maxroc)
            elif "_rf" in file and 'BasicModels' not in path:
                method = file.replace("_rf.txt", '')
                header, maxroc = addmetrics('rf', list_of_rows, maxroc)
            elif "rf" in file and "BasicModels" in path:
                method = file.replace('.txt', '')
                header, maxroc = addmetrics('rf', list_of_rows, maxroc)
            elif "_rpart" in file and 'BasicModels' not in path:
                method = file.replace("_rpart.txt", '')
                header, maxroc = addmetrics('rpart', list_of_rows, maxroc)
            elif "rpart" in file and 'BasicModels' in path:
                method = file.replace('.txt', '')
                header, maxroc = addmetrics('rpart', list_of_rows, maxroc)
            elif "_svm" in file and 'BasicModels' not in path:
                method = file.replace("_svm.txt", '')
                header, maxroc = addmetrics('svm', list_of_rows, maxroc)
            elif "svm" in file and "BasicModels" in path and "wsvm" not in file:
                method = file.replace(".txt", '')
                header, maxroc = addmetrics('svm', list_of_rows, maxroc)

        #Filter rows that are [ROCmax-0.01;ROCmax+0.01]; Get hypermarameters with maximum recall in that range
        rocindex = header.index('ROC')
        sensindex = rocindex + 1
        maxsens = 0
        maxspec = 0
        for row in list_of_rows:
            if float(row[rocindex]) - maxroc >= -0.01 and float(row[rocindex]) - maxroc <= 0.01:
                filtered_roc.append(row)
                if float(row[sensindex]) > maxsens:
                    maxsens = float(row[sensindex])
        for row in filtered_roc:
            if float(row[sensindex]) == maxsens:
                for i in range(len(header)):
                    if i < rocindex:
                        if header[i] in ["mtry", "trees", "minobs", "interaction", "nt"]:
                            methods[header[i]][method] = int(row[i])
                        else:
                            methods[header[i]][method] = float(row[i])
                break




#Arrays for hyperparameters
cost = []
mtry = []
trees = []
minobs = []
interaction = []
nt = []
alpha = []
cp = []

#Get hyperparameters for training
def add_to_list(parameter, list, dictionary_methods, evolutionary_list):
    for item in evolutionary_list:
        if item not in dictionary_methods[parameter].keys() and parameter in ['wcost', 'weight']:
            list.append('0.1')
        else:
            list.append(str(dictionary_methods[parameter][item]))
    print(parameter + "<- c" +"(" + ",".join(list) + ")")

all_mods = get_all_mods(path, evolutionary_variables, evolutionary_variables2, graph_variables)

if "BasicModels" not in path:
    add_to_list("cost", cost, methods, all_mods)
    add_to_list("mtry", mtry, methods, all_mods)
    add_to_list("trees", trees, methods, all_mods)
    add_to_list("minobs", minobs, methods, all_mods)
    add_to_list("interaction", interaction, methods, all_mods)
    add_to_list("nt", nt, methods, all_mods)
    add_to_list("alpha", alpha, methods, all_mods)
    add_to_list("cp", cp, methods, all_mods)

else:
    for key in methods.keys():
        print(key, methods[key])



