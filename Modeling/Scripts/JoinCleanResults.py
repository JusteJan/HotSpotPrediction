import csv
from math import sqrt

def mcc(file):
    m = ['TP', 'FP', 'TN', 'FN']
    dm = {}
    with open(file, 'r') as f:
        reader = csv.DictReader(f, delimiter=';')
        for row in reader:
            if row['Metric'] in m:
                dm[row['Metric']] = float(row['Testing_Set'])
    try:
        mcc = ((dm['TP'] * dm['TN']) - (dm['FP'] * dm['FN']))/sqrt((dm['TP'] + dm['FP']) * (dm['TP'] + dm['FN']) * (dm['TN'] + dm['FP']) * (dm['TN'] + dm['FN']))
        mcc = round(mcc, 2)
    except ZeroDivisionError:
        mcc = 'NA'
    return mcc


path = '../Datasets/MachineLearning/Clean_Results/'
pathmcc = '../Datasets/MachineLearning/Results/'
type = ['Testing.csv', 'Independent.csv']
methods = ['BasicModels', 'BasicModelsVol', 'EvolutionaryModelsAvg2Volume',
           'MoreEvo2Volume', 'SelectedModels']

for t in type:
    with open(path+t+'JoinedResultsFFF', 'w') as fout:
        header = ['Model', 'Accuracy', 'Precision', 'Recall', 'F1', 'MCC', 'Threshold', 'Type', 'EvolutionaryVariable', 'FirstStep', 'SecondStep']
        writer = csv.DictWriter(fout, delimiter=';', fieldnames=header, lineterminator='\n')
        writer.writeheader()
        if t == 'Independent.csv':
            with open(path+'OtherServers/OtherServers.csv', 'r') as fin:
                reader = csv.DictReader(fin, delimiter=';')
                for row in reader:
                    r = {}
                    r['Accuracy'] = row['Accuracy']
                    r['Precision'] = row['Precision']
                    r['Recall'] = row['Recall']
                    r['F1'] = row['F1']
                    r['Type'] = 'OtherServers'
                    r['Model'] = row['Model']
                    r['Threshold'] = '-'
                    r['EvolutionaryVariable'] = '-'
                    r['FirstStep'] = '-'
                    r['SecondStep'] = '-'
                    fil = pathmcc+r['Type']+'/Files/'+row['Model']+'.csv'
                    r['MCC'] = mcc(fil)
                    writer.writerow(r)
        for method in methods:
            print(method)
            with open(path+'/'+method+'/'+t, 'r') as fin:
                reader = csv.DictReader(fin, delimiter=';')
                for row in reader:
                    r = {}
                    r['Accuracy'] = row['Accuracy']
                    r['Precision'] = row['Precision']
                    r['Recall'] = row['Recall']
                    r['F1'] = row['F1']
                    r['Type'] = method
                    fil = pathmcc+method+'/Threshold/Files/'+row['Model']+t
                    r['MCC'] = mcc(fil)
                    if 'BasicModels' in method:
                        r['Model'], r['Threshold'] = row['Model'].split('_')
                        r['FirstStep'] = '-'
                        r['SecondStep'] = '-'
                        r['EvolutionaryVariable'] = '-'
                    elif method in ['EvolutionaryModels', 'EvolutionaryModelsAvg2', 'EvolutionaryModelsAvg2Volume', 'EvolutionaryModelsWeighted2',
                                    'EvolutionaryModelsAvg', 'EvolutionaryModelsWeighted', 'EvolutionaryModelsAvg2Voromqa', 'EvolutionaryModelsAvg2V',
                                    'SelectedModels']:
                        if method == 'SelectedModels':
                            r['Model'], r['Threshold'] = row['Model'].split(' ')[-1].split('_')
                            r['EvolutionaryVariable'] = '_'.join(row['Model'].split(' ')[:-1])

                        else:
                            r['Model'], r['Threshold'] = row['Model'].split(' ')[1].split('_')
                            r['EvolutionaryVariable'] = row['Model'].split(' ')[0]
                        r['FirstStep'] = '-'
                        r['SecondStep'] = '-'
                    elif method in ['TwoStepModels', 'TwoStepModelsAvg2', 'TwoStepModelsAvg2Volume',
                                    'TwoStepModelsAvg', 'TwoStepModelsAvg2VolumeSplit', 'TwoStepModelsAvg2Voromqa', 'TwoStepModelsAvg2V']:
                        r['Model'], r['Threshold'] = row['Model'].split(' ')[-1].split('_')
                        r['EvolutionaryVariable'] = row['Model'].split(' ')[1]
                        r['FirstStep'] = row['Model'].split(' ')[0].replace('Prob', '').lower()
                        r['SecondStep'] = r['Model']
                    elif method == 'TwoStepMoreEvo2' or method == 'TwoStepMoreEvo2Volume' or method == 'TwoStepMoreEvo2Voromqa' or method == 'TwoStepMoreEvo2V' or method == 'TwoStepMoreEvo' or method == 'TwoStepMoreEvo2VolumeSplit':

                        r['Model'], r['Threshold'] = row['Model'].split(' ')[-1].split('_')
                        r['EvolutionaryVariable'] = ' '.join(row['Model'].split(' ')[1:-1])
                        r['FirstStep'] = row['Model'].split(' ')[0].replace('Prob', '').lower()
                        r['SecondStep'] = r['Model']
                    elif method == 'MoreEvo2' or method == 'MoreEvo2Volume' or method == 'MoreEvo' or method == 'MoreEvo2Voromqa' or method == 'MoreEvo2V':
                        r['Model'], r['Threshold'] = row['Model'].split(' ')[-1].split('_')
                        r['EvolutionaryVariable'] = ' '.join(row['Model'].split(' ')[:-1])
                        r['FirstStep'] = '-'
                        r['SecondStep'] = '-'
                    writer.writerow(r)

