import csv
from math import log

def calculate_dG(k, temperature):
    print(k, temperature)
    if temperature == '':
        temperature = '273'
    return (1.987/1000) * float(temperature.replace('(assumed)', '')) * log(float(k))

def calculate_ddG(kdwt, kdmut, temperature):
    return calculate_dG(kdmut, temperature) - calculate_dG(kdwt, temperature)


with open('../Datasets/SKEMPI2.csv', 'r') as fin,\
     open('../Datasets/Dataset.csv', 'w') as fout:
        reader = csv.DictReader(fin, delimiter=';')
        d = {}
        header = ['PDB', 'Aminoacid', 'Chain', 'Position', 'ddG', 'HS']
        writer = csv.DictWriter(fout, delimiter=';', fieldnames=header, lineterminator='\n')
        writer.writeheader()
        for row in reader:
            mutated_to = row['Mutation(s)_PDB']
            mutations = row['Mutation(s)_PDB'].split(',')
            if len(mutations) == 1 and mutated_to[-1] == 'A':
                pdb = row['Protein'].split('_')[0]
                mutation = row['Mutation(s)_PDB']
                chain = mutation[1]
                aminoacid = mutation[0]
                index = mutation[2:-1]
                r = {}
                key = pdb + ' ' + chain + ' ' + aminoacid + ' ' + index
                kdwt = row['Affinity_wt (M)']
                kdmut = row['Affinity_mut (M)']
                wt = kdwt.replace('>', '').replace('<', '').replace('~', '')
                mut = kdmut.replace('>', '').replace('<', '').replace('~', '')
                if mut == 'n.b':
                    ddg = 2
                else:
                    ddg = round(calculate_ddG(wt, mut, row['Temperature']), 2)
                if key not in d.keys():
                    d[key] = []
                d[key].append(ddg)
        for key in d.keys():
            r = {}
            r['PDB'], r['Chain'], r['Aminoacid'], r['Position'] = key.split(' ')
            r['ddG'] = sum(d[key])/len(d[key])
            r['HS'] = 'HS' if r['ddG'] >= 2 else 'NHS'
            writer.writerow(r)

