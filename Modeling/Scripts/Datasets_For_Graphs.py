import csv
from Helper_functions import keys, convertAminoacids
pdbs = []
hs = {}
with open('../Datasets/MachineLearning/training.csv', 'r') as fin1, \
     open('../Datasets/MachineLearning/testing.csv', 'r') as fin2:
        reader1 = csv.DictReader(fin1, delimiter=';')
        reader2 = csv.DictReader(fin2, delimiter=';')
        for row in reader1:
            p = row['PDB']
            if p not in pdbs:
                pdbs.append(p)
            key = row['PDB'] + ' ' + row['Chain'] + ' ' + row['Aminoacid'] + ' ' + row['Position']
            hs[key] = row['HS']
        for row in reader2:
            key = row['PDB'] + ' ' + row['Chain'] + ' ' + row['Aminoacid'] + ' ' + row['Position']
            hs[key] = row['HS']

types = ['Face', 'Interface', 'Complex', 'Monomer']
path = '../Datasets/MachineLearning/Graphs'

chains = {}
chf = '../txtFiles/PDB_chains.txt'
with open(chf, 'r') as fin:
    for row in fin:
        pdb, ch1, ch2 = row.rstrip('\n').split(' ')
        chains[pdb] = [ch1, ch2]

pdbs = chains.keys()

for p in pdbs:
    complex = []
    monomer = {}
    interface = []
    face = {}
    face[chains[p][0]] = []
    face[chains[p][1]] = []
    monomer[chains[p][0]] = []
    monomer[chains[p][1]] = []
    with open('../Voronota_files/%s/contacts_ir_clean' %p, 'r') as fin:
        for row in fin:
            k1, k2 = keys(row, convertAminoacids, 'K')
            k1 = p + ' ' + k1
            k2 = p + ' ' + k2
            c1 = k1.split(' ')[1]
            c2 = k2.split(' ')[1]
            if 'solv' not in k1 and 'solv' not in k2:
                if ((c1 in chains[p][0] and c2 in chains[p][1]) or
                   (c1 in chains[p][1] and c2 in chains[p][0])):
                        if k1 not in interface:
                            interface.append(k1)
                        if k2 not in interface:
                            interface.append(k2)
                        kkk1 = chains[p][0]
                        kkk2 = chains[p][1]
                        if c1 in kkk1:
                            face[kkk1].append(k1)
                            face[kkk2].append(k2)
                        else:
                            face[kkk1].append(k2)
                            face[kkk2].append(k1)
            bothchains = chains[p][0]+chains[p][1]
            if c1 in bothchains and c2 in bothchains:
                complex.append(k1)
                complex.append(k2)
                kkk1 = chains[p][0]
                kkk2 = chains[p][1]
                if c1 in kkk1:
                    if k1 not in monomer[kkk1]:
                        monomer[kkk1].append(k1)
                if c1 in kkk2:
                    if k1 not in monomer[kkk2]:
                        monomer[kkk2].append(k1)
                if c2 in kkk1:
                    if k2 not in monomer[kkk1]:
                        monomer[kkk1].append(k2)
                if c2 in kkk2:
                    if k2 not in monomer[kkk2]:
                        monomer[kkk2].append(k2)
        fin.seek(0)
        header = ['V1', 'V2', 'Weight']
        face1 = p+'_'+chains[p][0]
        face2 = p+'_'+chains[p][1]
        monomer1 = p+'_'+chains[p][0]
        monomer2 = p+'_'+chains[p][1]
        with open(path+'/Face/%s.csv' %(face1), 'w') as fout1,\
             open(path+'/Face/%s.csv' %(face2), 'w') as fout2, \
             open(path + '/Interface/%s.csv' % (p), 'w') as fout3, \
             open(path + '/Complex/%s.csv' % (p), 'w') as fout4, \
             open(path + '/Monomer/%s.csv' % (monomer1), 'w') as fout5, \
             open(path + '/Monomer/%s.csv' % (monomer2), 'w') as fout6:
                writer1 = csv.DictWriter(fout1, delimiter=';', fieldnames=header, lineterminator='\n')
                writer2 = csv.DictWriter(fout2, delimiter=';', fieldnames=header, lineterminator='\n')
                writer3 = csv.DictWriter(fout3, delimiter=';', fieldnames=header, lineterminator='\n')
                writer4 = csv.DictWriter(fout4, delimiter=';', fieldnames=header, lineterminator='\n')
                writer5 = csv.DictWriter(fout5, delimiter=';', fieldnames=header, lineterminator='\n')
                writer6 = csv.DictWriter(fout6, delimiter=';', fieldnames=header, lineterminator='\n')
                writer1.writeheader()
                writer2.writeheader()
                writer3.writeheader()
                writer4.writeheader()
                writer5.writeheader()
                writer6.writeheader()
                for row in fin:
                    k1, k2 = keys(row, convertAminoacids, 'K')
                    k1 = p + ' ' + k1
                    k2 = p + ' ' + k2
                    area, tags = keys(row, convertAminoacids, '')
                    r = {}
                    r['V1'] = k1
                    r['V2'] = k2
                    r['Weight'] = area
                    if k1 != k2:
                        if k1 in interface and k2 in interface:
                            writer3.writerow(r)
                        kkk1 = chains[p][0]
                        kkk2 = chains[p][1]
                        if k1 in face[kkk1] and k2 in face[kkk1]:
                            writer1.writerow(r)
                        if k1 in face[kkk2] and k2 in face[kkk2]:
                            writer2.writerow(r)
                        kkk1 = chains[p][0]
                        kkk2 = chains[p][1]
                        if k1 in monomer[kkk1] and k2 in monomer[kkk1]:
                            writer5.writerow(r)
                        if k1 in monomer[kkk2] and k2 in monomer[kkk2]:
                            writer6.writerow(r)
                        if k1 in complex and k2 in complex:
                            writer4.writerow(r)




