import csv

# pathBasic = '../Datasets/MachineLearning/Results/BasicModels/Threshold/Files/'
# pathDestination = '../Datasets/MachineLearning/Clean_Results/BasicModels/'

pathBasic = '../Datasets/MachineLearning/Results/SelectedModels/Threshold/Files/'
pathDestination = '../Datasets/MachineLearning/Clean_Results/SelectedModels/'

# pathBasic = '../Datasets/MachineLearning/Results/BasicModelsVol/Threshold/Files/'
# pathDestination = '../Datasets/MachineLearning/Clean_Results/BasicModelsVol/'

# pathBasic = '../Datasets/MachineLearning/Results/EvolutionaryModelsAvg2Volume/Threshold/Files/'
# pathDestination = '../Datasets/MachineLearning/Clean_Results/EvolutionaryModelsAvg2Volume/'

# pathBasic = '../Datasets/MachineLearning/Results/MoreEvo2Volume/Threshold/Files/'
# pathDestination = '../Datasets/MachineLearning/Clean_Results/MoreEvo2Volume/'


evolutionary_variables = ['out1', 'out2', 'out3', 'out4', 'out5', 'out6',
                          'out7', 'out8', 'out9', 'caprasingh_shannon_entropy',
                          'caprasingh_sum_of_pairs','mstatx_trident','mstatx_kabat',
                          'caprasingh_property_entropy','caprasingh_property_relative_entropy',
                          'caprasingh_js_divergence','mstatx_jensen','mstatx_wentropy',
                          'caprasingh_relative_entropy', 'ConSurf']

evolutionary_variables2 = ['out1 out4 out7 ConSurf','out1 out5 out7','caprasingh_relative_entropy out5 out9',
                           'caprasingh_property_relative_entropy out4 out8 ConSurf',
                           'out1 out5 out7 ConSurf','out2 out6 out9 ConSurf','out3 out4 out9',
                           'out2 out5 out8 ConSurf','mstatx_trident out4 out9',
                           'caprasingh_js_divergence mstatx_kabat out9','out1 mstatx_kabat out7 ConSurf',
                           'caprasingh_js_divergence mstatx_kabat out9 ConSurf',
                           'out3 out4 caprasingh_sum_of_pairs','out2 out5 caprasingh_sum_of_pairs',
                           'out2 out6 out7 ConSurf','caprasingh_js_divergence mstatx_kabat out7',
                           'mstatx_trident out4 out8','caprasingh_relative_entropy out4 out7 ConSurf',
                           'out1 out4 out9','caprasingh_property_relative_entropy out5 out8']
graph_variables = [
    'betweenness_interface closeness_interface degree_interface',
    'closeness_interface closeness w_interface',
    'betweenness_interface betweenness w_interface',
    'betweenness_interface betweenness w_interface closeness_interface closeness w_interface degree_interface',
    'degree_interface',
    'closeness w_interface',
    'closeness_interface',
    'betweenness w_interface',
    'betweenness_interface',
    'betweenness w_interface closeness w_interface',
    'betweenness_interface closeness_interface'
]


def get_models(path, methods, graph_variables, evolutionary_variables, evolutionary_variables2):
    if 'Evolutionary' in path:
        vars = evolutionary_variables
    elif 'Evo' in path:
        vars = evolutionary_variables2
    elif 'Selected' in path:
        vars = graph_variables
    else:
        vars = ''

    models = []
    for i in vars:
        for method in methods:
            models.append(i + ' ' + method)

    return models

methods = ['rpart', 'rf', 'pls', 'svm', 'gbm']

types = ['Testing', 'Independent']

models = get_models(pathBasic, methods, graph_variables, evolutionary_variables, evolutionary_variables2)

model_list = []
thresholds = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9]
for t in thresholds:
    for model in models:
        model_list.append(model+'_'+str(t).replace('.', ''))

list_metrics = {}

metrics = ['Accuracy', 'Precision', 'Recall', 'F1']

for type in types:
    if type not in list_metrics.keys():
        list_metrics[type] = {}
    for model in model_list:
        if model not in list_metrics[type].keys():
            list_metrics[type][model] = {}
        with open(pathBasic+model+type+'.csv', 'r') as fin:
            reader = csv.DictReader(fin, delimiter=';')
            for row in reader:
                m = row['Metric']
                if m in metrics:
                    list_metrics[type][model][m] = round(float(row['Testing_Set']), 2) if row['Testing_Set'] != 'NA' else 'NA'

header = ['Model'] + metrics
for key in list_metrics.keys():
    with open(pathDestination+key+'.csv', 'w') as fout:
        writer = csv.DictWriter(fout, delimiter=';', fieldnames=header, lineterminator='\n')
        writer.writeheader()
        for model in list_metrics[key].keys():
            r = {}
            r['Model'] = model
            for metric in list_metrics[key][model].keys():
                r[metric] = list_metrics[key][model][metric]
            writer.writerow(r)


