import csv

# Uncomment relevant
# basicModelsPath = '../Datasets/MachineLearning/Results/BasicModels/'
basicModelsPath = '../Datasets/MachineLearning/Results/SelectedModels/'
# basicModelsPath = '../Datasets/MachineLearning/Results/BasicModelsVol/'
# basicModelsPath = '../Datasets/MachineLearning/Results/EvolutionaryModelsAvg2Volume/'
# basicModelsPath = '../Datasets/MachineLearning/Results/MoreEvo2Volume/'

types = ['Predictions.csv', 'Predictions_Independent.csv']

models = ['rpart', 'rf', 'pls', 'svm', 'gbm']

evolutionary_variables = ['out1', 'out2', 'out3', 'out4', 'out5', 'out6',
                          'out7', 'out8', 'out9', 'caprasingh_shannon_entropy',
                          'caprasingh_sum_of_pairs', 'mstatx_trident', 'mstatx_kabat',
                          'caprasingh_property_entropy', 'caprasingh_property_relative_entropy',
                          'caprasingh_js_divergence', 'mstatx_jensen', 'mstatx_wentropy',
                          'caprasingh_relative_entropy', 'ConSurf']

evolutionary_variables2 = ['out1+out4+out7+ConSurf','out1+out5+out7','caprasingh_relative_entropy+out5+out9',
                           'caprasingh_property_relative_entropy+out4+out8+ConSurf',
                           'out1+out5+out7+ConSurf','out2+out6+out9+ConSurf','out3+out4+out9',
                           'out2+out5+out8+ConSurf','mstatx_trident+out4+out9',
                           'caprasingh_js_divergence+mstatx_kabat+out9','out1+mstatx_kabat+out7+ConSurf',
                           'caprasingh_js_divergence+mstatx_kabat+out9+ConSurf',
                           'out3+out4+caprasingh_sum_of_pairs','out2+out5+caprasingh_sum_of_pairs',
                           'out2+out6+out7+ConSurf','caprasingh_js_divergence+mstatx_kabat+out7',
                           'mstatx_trident+out4+out8','caprasingh_relative_entropy+out4+out7+ConSurf',
                           'out1+out4+out9','caprasingh_property_relative_entropy+out5+out8']
graph_variables = [
    'betweenness_interface+closeness_interface+degree_interface',
    'closeness_interface+closeness.w_interface',
    'betweenness_interface+betweenness.w_interface',
    'betweenness_interface+betweenness.w_interface+closeness_interface+closeness.w_interface+degree_interface',
    'degree_interface',
    'closeness.w_interface',
    'closeness_interface',
    'betweenness.w_interface',
    'betweenness_interface',
    'betweenness.w_interface+closeness.w_interface',
    'betweenness_interface+closeness_interface'
]


def get_models(path, methods, graph_variables, evolutionary_variables, evolutionary_variables2):
    if 'Evolutionary' in path:
        vars = evolutionary_variables
    elif 'Evo' in path:
        vars = evolutionary_variables2
    elif 'Selected' in path:
        vars = graph_variables
    else:
        vars = ''
    models = []
    for i in vars:
        for j in methods:
            models.append(i + ' ' + j)
    return models

methods = ['rpart', 'rf', 'pls', 'svm', 'gbm']
models = get_models(basicModelsPath, methods, graph_variables, evolutionary_variables, evolutionary_variables2)
resultsIndependent = {}
resultsTesting = {}

ind = {}
with open('../Datasets/MachineLearning/independent.csv', 'r') as fin:
    reader = csv.DictReader(fin, delimiter=';')
    for row in reader:
        key = row['PDB'] + ' ' + row['Aminoacid'] + ' ' + row['Chain'] + ' ' + row['Position']
        ind[key] = row['HS']

for type in types:
    for model in models:
        with open(basicModelsPath+model+type, 'r') as fin:
            reader = csv.DictReader(fin, delimiter=';')
            for row in reader:
                key = row['PDB'] + ' ' + row['Aminoacid'] + ' ' + row['Chain'] + ' ' + row['Position']
                if 'Ind' in type:
                    if key not in resultsIndependent.keys():
                        resultsIndependent[key] = {}
                    resultsIndependent[key][model] = float(row['ProbabilityHS'])
                    resultsIndependent[key]['HS'] = ind[key]
                else:
                    if key not in resultsTesting.keys():
                        resultsTesting[key] = {}
                    resultsTesting[key][model] = float(row['ProbabilityHS'])
                    resultsTesting[key]['HS'] = row['HS']


header = ['PDB', 'Aminoacid', 'Chain', 'Position', 'HS']
thresholds = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9]

for t in thresholds:
    for model in models:
        header.append(model+'_'+str(t).replace('.', ''))

with open(basicModelsPath+'Threshold/thresholdTesting.csv', 'w') as fout1,\
     open(basicModelsPath+'Threshold/thresholdIndependent.csv', 'w') as fout2:
        writer1 = csv.DictWriter(fout1, delimiter=';', fieldnames=header, lineterminator='\n')
        writer2 = csv.DictWriter(fout2, delimiter=';', fieldnames=header, lineterminator='\n')
        writer1.writeheader()
        writer2.writeheader()
        for key in resultsIndependent.keys():
            r = {}
            r['PDB'], r['Aminoacid'], r['Chain'], r['Position'] = key.split(' ')
            r['HS'] = resultsIndependent[key]['HS']
            for t in thresholds:
                for model in models:
                    r[model+'_'+str(t).replace('.', '')] = 'HS' if resultsIndependent[key][model] >= t else 'NHS'
            writer2.writerow(r)
        for key in resultsTesting.keys():
            r = {}
            r['PDB'], r['Aminoacid'], r['Chain'], r['Position'] = key.split(' ')
            r['HS'] = resultsTesting[key]['HS']
            for t in thresholds:
                for model in models:
                    try:
                        r[model + '_' + str(t).replace('.', '')] = 'HS' if resultsTesting[key][model] >= t else 'NHS'
                    except:
                        print('ERR')
                        print(model + ' ' + key)
                        print(resultsTesting[key])
            writer1.writerow(r)
