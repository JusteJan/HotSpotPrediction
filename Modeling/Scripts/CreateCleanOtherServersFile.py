import csv

servers = ['BeatMusic', 'HotPoint', 'HSPred', 'KFCA', 'KFCB', 'KFCCON', 'KFCFADE', 'mCSM', 'Robetta', 'Spoton']
header = ['Model','Accuracy','Precision','Recall','F1']


d_servers = {}
for server in servers:
    d_servers[server] = {}
    with open('../Datasets/MachineLearning/Results/OtherServers/Files/%s.csv' %(server), 'r') as fin:
        reader = csv.DictReader(fin, delimiter=';')
        for row in reader:
            m = row['Metric']
            val = row['Testing_Set']
            if m in header:
                d_servers[server][m] = round(float(val), 2)

with open('../Datasets/MachineLearning/Clean_Results/OtherServers/OtherServers.csv', 'w') as fout:
    writer = csv.DictWriter(fout, delimiter=';', fieldnames=header, lineterminator='\n')
    writer.writeheader()
    for model in d_servers.keys():
        r = {}
        r['Model'] = model
        for item in header[1:]:
            r[item] = d_servers[model][item]
        writer.writerow(r)