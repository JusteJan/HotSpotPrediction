import csv
import os
from os import listdir
from os.path import isfile, join


d_scores = {}
d_get_keys = {}
usable_positions = {}
convert_usable_positions = {}
normalization = {}
d_mean = {}
d_types = {}

with open('../txtFiles/PDB_types.txt', 'r') as fin:
    for row in fin:
        p, t = row.split(' ')
        d_types[p] = t

#-------------ConSurf----------------
path = '../EvolutionaryScores/ConSurf_Files'
directories = [x[0].replace('\\', '/') for x in os.walk(path)]
directories = directories[1:]
for dir in directories:
    with open(dir+'/%s' %('consurf.grades'), 'r') as fin:
        pdb, chain = dir.split('/')[-1].split('_')
        for nn, row in enumerate(fin):
            if nn+1 > 15 and '\t' in row:
                r = row.rstrip('\n').split('\t')
                r = [x for x in r if (x != '' and x != ' ')]
                p = r[2].replace(' ', '')
                aa = r[1].replace(' ','')
                k = pdb + ' ' + chain + ' ' + p.split(':')[0][3:].lower() + ' ' + aa
                get_keys = pdb + ' ' + chain + ' ' + r[0].replace(' ', '')
                cons = r[4].replace(' ', '')
                cons = cons.replace('*', '')
                if k not in d_scores.keys():
                    d_scores[k] = {}
                d_scores[k]['ConSurf'] = cons
                d_get_keys[get_keys] = k
d_mean['ConSurf'] = 5
#---------------------------------------
#-----------------al2co-----------------
path = '../EvolutionaryScores/al2co'
onlyfiles = [f for f in listdir(path) if isfile(join(path, f))]
for i in range(1, 10):
    normalization['out%s' %(i)] = []
    d_mean['out%s' %(i)] = 5
for file in onlyfiles:
    pdb, chain = file.split('_')
    usable_positions[file] = {}
    n = 0
    with open(path+'/%s' %(file), 'r') as fin:
        for row in fin:
            if row[0] == '*':
                break
            if '#' not in row:
                row = row.split(' ')
                row = [x for x in row if x != ' ' and x != '']
                pos = row[0]
                aa = row[1]
                if aa != '-':
                    n += 1
                    usable_positions[file][int(pos)] = n
                    key = pdb + ' ' + chain + ' ' + str(n)
                    if key in d_get_keys.keys():
                        d_scores[d_get_keys[key]]['al2co'] = {}
                        counter_rows = 2
                        for i in range(1, 10):
                            d_scores[d_get_keys[key]]['al2co']['out%s' %(i)] = float(row[counter_rows])
                            if float(row[counter_rows]) > -1000:
                                normalization['out%s' %(i)].append(float(row[counter_rows]))
                            counter_rows += 1
#---------------------------------------

#----------------caprasingh-------------
path = '../EvolutionaryScores/caprasingh'
methods = ['js_divergence', 'property_entropy', 'property_relative_entropy',
           'relative_entropy', 'shannon_entropy', 'sum_of_pairs']
for method in methods:
    onlyfiles = [f for f in listdir(path+'/%s' %(method)) if isfile(join(path+'/%s' %(method), f))]
    for file in onlyfiles:
        pdb = file.split('_')[0]
        chain = file.split('_')[1]
        with open(path+'/%s/%s' %(method, file), 'r') as fin:
            for row in fin:
                if '#' not in row:
                    row = row.split('\t')
                    row = [x for x in row if x != ' ' and x != '']
                    pos = int(row[0]) + 1
                    if pos in usable_positions[file].keys():
                        pos = usable_positions[file][pos]
                        me = 'caprasingh_' + method
                        d_mean[me] = 0.5
                        if me not in normalization.keys():
                            normalization[me] = []
                        key = pdb + ' ' + chain + ' ' + str(pos)
                        d_scores[d_get_keys[key]][me] = float(row[1])
                        if float(row[1]) > -1000:
                            normalization[me].append(float(row[1]))
#---------------------------------------
#----------------mstatax----------------
path = '../EvolutionaryScores/mstatx'
methods = ['jensen', 'kabat', 'trident', 'wentropy']
for method in methods:
    onlyfiles = [f for f in listdir(path + '/%s' % (method)) if isfile(join(path + '/%s' % (method), f))]
    for file in onlyfiles:
        pdb, chain = file.split('_')
        with open(path+'/%s/%s' %(method, file), 'r') as fin:
            for row in fin:
                row = row.rstrip('\n').split('\t')
                row = [x for x in row if x != ' ' and x != '']
                pos = int(row[0])
                if pos in usable_positions[file].keys():
                    pos = usable_positions[file][pos]
                    me = 'mstatx_' + method
                    d_mean[me] = 0.5
                    if me not in normalization.keys():
                        normalization[me] = []
                    key = pdb + ' ' + chain + ' ' + str(pos)
                    d_scores[d_get_keys[key]][me] = float(row[1])
                    normalization[me].append(float(row[1]))
#---------------------------------------

types = ['independent.csv', 'training.csv', 'testing.csv']

for t in types:
    with open('../Datasets/MachineLearning/%s' %(t), 'r') as fin,\
         open('../Datasets/MachineLearning/EvolutionaryScoresNormalizedwoo_%s' %(t), 'w') as fout:
            reader = csv.DictReader(fin, delimiter=';')
            header = ['PDB', 'Chain', 'Position', 'Aminoacid', 'ConSurf']
            for i in range(1, 10):
                header.append('out%s' %(i))
            for method in normalization.keys():
                if method not in header:
                    header.append(method)
            writer = csv.DictWriter(fout, delimiter=';', fieldnames=header, lineterminator='\n')
            writer.writeheader()
            for row in reader:
                key = row['PDB'] + ' ' + row['Chain'] + ' ' + row['Position'] + ' ' + row['Aminoacid']
                r = {}
                r['PDB'], r['Chain'], r['Position'], r['Aminoacid'] = key.split(' ')
                if key in d_scores.keys() and 'AB' not in d_types[row['PDB']] and 'TCR' not in d_types[row['PDB']]:
                    if 'ConSurf' in d_scores[key].keys():
                        r['ConSurf'] = d_scores[key]['ConSurf']
                    else:
                        r['ConSurf'] = 0
                    for i in range(1, 10):
                        o = 'out%s' %(i)
                        try:
                           r[o] = int(9.99*((d_scores[key]['al2co'][o] - min(normalization[o]))/(max(normalization[o]) - min(normalization[o]))))
                            #r[o] = d_scores[key]['al2co'][o]
                        except:
                            print(d_scores[key])
                    for item in header:
                        if item not in r.keys():
                            if d_scores[key][item] == -1000:
                                r[item] = 0
                                #r[item] = d_scores[key][item]
                            else:
                                r[item] = round((d_scores[key][item] - min(normalization[item]))/(max(normalization[item]) - min(normalization[item])), 2)
                                #r[item] = d_scores[key][item]
                else:
                    for item in header:
                        if item not in r.keys():
                            r[item] = d_mean[item]
                writer.writerow(r)