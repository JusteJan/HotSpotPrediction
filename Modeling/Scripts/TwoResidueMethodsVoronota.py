from OneResidueMethodsVoronota import *
from constants import get_chains, AMINOACIDS


class TwoResidueMethodsVoronota(object):

    def __init__(self, row, pdb, chains):
        self.res1 = OneResidueMethodsVoronota(row, 'Ann1')
        self.res2 = OneResidueMethodsVoronota(row, 'Ann2')
        self.chains = chains
        self.pdb = pdb

    def is_different_proteins(self):  # If chain2 = 'solvent', returns false
        c1 = self.res1.get_chain()
        c2 = self.res2.get_chain()
        if (not ((c1 in self.chains[self.pdb][0] and c2 in self.chains[self.pdb][1]) or
                (c1 in self.chains[self.pdb][1] and c2 in self.chains[self.pdb][0]))) or (
                (c1 not in self.chains[self.pdb][0] and c1 not in self.chains[self.pdb][1] and
                 c2 not in self.chains[self.pdb][0] and c2 not in self.chains[self.pdb][1])
        ):
                    return False
        return True

    # can be used for area_h_d and hydrophobic
    def is_hydrophobic_contact(self):
        if not (self.res1.is_hydrophobic_atom() and self.res2.is_hydrophobic_atom()):
            return False
        return True

    def is_both_aminoacids(self):
        if not (self.res1.get_aminoacid() in AMINOACIDS and
           self.res2.get_aminoacid() in AMINOACIDS):
            return False
        return True

    def is_one_aminoacid(self):
        if self.res1.get_aminoacid() in AMINOACIDS:
            return True
        else:
            return False

    def get_area(self):
        return self.res1.get_area()

    def get_first(self):
        return self.pdb + ' ' + self.res1.get_key()

    def get_second(self):
        return self.pdb + ' ' + self.res2.get_key()

    def get_first_object(self):
        return self.res1

    def get_second_object(self):
        return self.res2
