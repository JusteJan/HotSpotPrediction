CONVERT_AMINOACIDS = {'CYS': 'C', 'ASP': 'D', 'SER': 'S', 'GLN': 'Q', 'LYS': 'K',
                      'ILE': 'I', 'PRO': 'P', 'THR': 'T', 'PHE': 'F', 'ASN': 'N',
                      'GLY': 'G', 'HIS': 'H', 'LEU': 'L', 'ARG': 'R', 'TRP': 'W',
                      'ALA': 'A', 'VAL': 'V', 'GLU': 'E', 'TYR': 'Y', 'MET': 'M',
                      'GLX': 'Z', 'DA': 'DA', 'DG': 'DG', 'DT': 'DT', 'DC': 'DC',
                      '': '', 'ZN': 'ZN'}

NUCLEOTIDES = ['DA', 'DC', 'DG', 'DT']

AMINOACIDS = ['C', 'D', 'S', 'Q', 'K',
              'I', 'P', 'T', 'F', 'N',
              'G', 'H', 'L', 'R', 'W',
              'A', 'V', 'E', 'Y', 'M']


def get_chains():
    file = '../txtFiles/PDB_chains.txt'
    chains = {}
    with open(file, 'r') as fin:
        for row in fin:
            pdb, c1, c2 = row.rstrip('\n').split(' ')
            chains[pdb] = [c1, c2]
    return chains


arom = ['F', 'Y', 'W']
aliph = ['G', 'I', 'L', 'M', 'P', 'V']
pol = ['C', 'N', 'Q', 'T', 'S']
pos = ['H', 'K', 'R']
neg = ['D', 'E']

types_list = {'Aromatic': arom, 'Aliphatic': aliph, 'Polar': pol, 'Positive': pos, 'Negative': neg}