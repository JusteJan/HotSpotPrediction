from CalculateFeatures import *
import csv

calculator = CalculateFeatures('')

files_in = ['training.csv', 'testing.csv', 'independent.csv']


header = ['PDB', 'Aminoacid', 'Chain', 'Position',
          'Area_H_Diff', 'Area_H_Same',
          'Area_H',	'Hydrophobic_Diff',	'Hydrophobic_Same',
          'Hydrophobic', 'Cont_Diff', 'Cont_Same', 'Cont',
          'sb_Diff', 'sb_Same', 'sb', 'hb_Diff', 'hb_Same',
          'hb', 'hb_Diff_10', 'BSA_Hydrophobic', 'BSA_Hydrophylic',
          'BSA', 'ASA_Hydrophylic', 'ASA_Hydrophobic', 'ASA',
          'Interface_Area', 'Shelling', 'Aromatic', 'Aliphatic',
          'Polar', 'Positive', 'Negative', 'deltaVolume']


for file in files_in:
    pdbs = []
    keys = []
    with open('../Datasets/MachineLearning/%s' % (file), 'r') as fin,\
         open('../Datasets/MachineLearning/%sStructural.csv' % (file[:-4]), 'w') as fout:
            reader = csv.DictReader(fin, delimiter=';')
            writer = csv.DictWriter(fout, delimiter=';', fieldnames=header, lineterminator='\n')
            writer.writeheader()
            for row in reader:
                p = row['PDB']
                key = row['PDB'] + ' ' + row['Chain'] + ' ' + row['Aminoacid'] + ' ' + row['Position']
                if p not in pdbs:
                    pdbs.append(row['PDB'])
                if key not in keys:
                    keys.append(key)
            for i in pdbs:
                print(i)
                path1 = '../Voronota_files/%s/contacts_raw' % (i)
                path2 = '../Voronota_files/%s/contacts_ir_raw' % (i)
                path3 = '../Shelling_files/%s/residue_depth_values.txt'
                path4 = '../Volume_files/Monomers/Residues/%s'
                path5 = '../Volume_files/Complexes/Residues/%s'

                area_h, hydrophobic, bsa, asa, hb, sb, shelling, contacts, types, interf, vol_c, vol_m = calculator.calculate_everything(i, path2, path1, path3, path4, path5, 'N')
                for key in keys:
                    if i in key:
                        r = {}
                        r['PDB'], r['Chain'], r['Aminoacid'], r['Position'] = key.split(' ')
                        r['Area_H_Diff'] = area_h['Diff'][key] if key in area_h['Diff'].keys() else 0
                        r['Area_H_Same'] = area_h['Same'][key] if key in area_h['Same'].keys() else 0
                        r['Area_H'] = r['Area_H_Diff'] + r['Area_H_Same']
                        r['Hydrophobic_Same'] = hydrophobic['Same'][key] if key in hydrophobic['Same'].keys() else 0
                        r['Hydrophobic_Diff'] = hydrophobic['Diff'][key] if key in hydrophobic['Diff'].keys() else 0
                        r['Hydrophobic'] = r['Hydrophobic_Same'] + r['Hydrophobic_Diff']
                        r['Cont_Diff'] = contacts['Diff'][key] if key in contacts['Diff'].keys() else 0
                        r['Cont_Same'] = contacts['Same'][key] if key in contacts['Same'].keys() else 0
                        r['Cont'] = r['Cont_Diff'] + r['Cont_Same']
                        bsahph = bsa['Hydrophobic'][key] if key in bsa['Hydrophobic'].keys() else 0
                        r['BSA_Hydrophobic'] = bsahph
                        bsanhph = bsa['Hydrophylic'][key] if key in bsa['Hydrophylic'].keys() else 0
                        r['BSA_Hydrophylic'] = bsanhph
                        r['BSA'] = r['BSA_Hydrophylic'] + r['BSA_Hydrophobic']
                        asahph = asa['Hydrophobic'][key] if key in asa['Hydrophobic'].keys() else 0
                        r['ASA_Hydrophobic'] = asahph
                        asanhph = asa['Hydrophylic'][key] if key in asa['Hydrophylic'].keys() else 0
                        r['ASA_Hydrophylic'] = asanhph
                        r['ASA'] = r['ASA_Hydrophylic'] = r['ASA_Hydrophobic']
                        r['Interface_Area'] = bsahph + bsanhph + asahph + asanhph
                        r['Shelling'] = shelling[key] if key in shelling.keys() else 0
                        r['hb_Diff'] = hb['Diff'][key] if key in hb['Diff'].keys() else 0
                        r['hb_Same'] = hb['Same'][key] if key in hb['Same'].keys() else 0
                        r['hb'] = r['hb_Diff'] + r['hb_Same']
                        r['hb_Diff_10'] = 1 if r['hb_Diff'] > 0 else 0
                        r['sb_Diff'] = sb['Diff'][key] if key in sb['Diff'].keys() else 0
                        r['sb_Same'] = sb['Same'][key] if key in sb['Same'].keys() else 0
                        r['sb'] = r['sb_Diff'] + r['sb_Same']
                        r['deltaVolume'] = vol_m[key] - vol_c[key]
                        r.update(types[key])
                        writer.writerow(r)
