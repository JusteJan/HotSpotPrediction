	 Amino Acid Conservation Scores
	=======================================

- POS: The position of the AA in the SEQRES derived sequence.
- SEQ: The SEQRES derived sequence in one letter code.
- 3LATOM: The ATOM derived sequence in three letter code, including the AA's positions as they appear in the PDB file and the chain identifier.
- SCORE: The normalized conservation scores.
- COLOR: The color scale representing the conservation scores (9 - conserved, 1 - variable).
- CONFIDENCE INTERVAL: When using the bayesian method for calculating rates, a confidence interval is assigned to each of the inferred evolutionary conservation scores.
- CONFIDENCE INTERVAL COLORS: When using the bayesian method for calculating rates. The color scale representing the lower and upper bounds of the confidence interval.
- MSA DATA: The number of aligned sequences having an amino acid (non-gapped) from the overall number of sequences at each position.
- RESIDUE VARIETY: The residues variety at each position of the multiple sequence alignment.

 POS	 SEQ	    3LATOM	SCORE		COLOR	CONFIDENCE INTERVAL	CONFIDENCE INTERVAL COLORS	MSA DATA	RESIDUE VARIETY
    	    	        	(normalized)	        	               
   1	   M	    MET1:A	-1.142		  9	-1.232,-1.109			    9,9			  24/150	M
   2	   T	    THR2:A	 0.556		  3	 0.061, 0.748			    5,2			  38/150	D,K,G,R,N,T,S,V,L,I,P
   3	   E	    GLU3:A	 0.405		  3	 0.061, 0.521			    5,3			  60/150	E,L,S,T,A,V,Q,R,K,D,N
   4	   Y	    TYR4:A	-0.266		  6	-0.469,-0.153			    7,6			 106/150	K,Y,C,R,H,W,V,T,L,Q,I,F
   5	   K	    LYS5:A	-0.776		  8	-0.888,-0.703			    8,8			 131/150	H,R,T,K,N,Q
   6	   L	    LEU6:A	-0.021		  5	-0.243, 0.061			    6,5			 131/150	C,Y,M,L,S,A,V,F,I
   7	   V	    VAL7:A	-0.807		  8	-0.929,-0.753			    8,8			 131/150	I,F,S,A,V,L,C
   8	   V	    VAL8:A	-0.746		  8	-0.845,-0.703			    8,8			 131/150	M,L,A,V,I
   9	   V	    VAL9:A	-0.671		  7	-0.800,-0.593			    8,7			 132/150	L,M,V,A,T,I
  10	   G	   GLY10:A	-1.203		  9	-1.246,-1.203			    9,9			 133/150	G
  11	   A	   ALA11:A	-0.128		  5	-0.325,-0.053			    6,5			 133/150	E,S,T,A,V,P,F,Q,C,R,G,Y,D,N
  12	   G	   GLY12:A	-0.486		  7	-0.650,-0.400			    7,6			 134/150	A,T,S,E,L,Q,P,Y,G,R
  13	   G	   GLY13:A	-0.177		  6	-0.400,-0.053			    6,5			 135/150	A,V,S,T,N,D,K,C,R,G,H
  14	   V	   VAL14:A	-1.157		  9	-1.203,-1.142			    9,9			 135/150	C,G,V,A,S
  15	   G	   GLY15:A	-1.203		  9	-1.246,-1.203			    9,9			 135/150	G
  16	   K	   LYS16:A	-1.205		  9	-1.246,-1.203			    9,9			 136/150	R,K
  17	   S	   SER17:A	-0.926		  8	-1.005,-0.888			    9,8			 137/150	N,S,T,A
  18	   A	   ALA18:A	-0.549		  7	-0.703,-0.469			    8,7			 137/150	N,C,F,I,S,T,A,V
  19	   L	   LEU19:A	-0.873		  8	-0.968,-0.800			    9,8			 138/150	L,M,V,F,I,Q
  20	   T	   THR20:A	-0.624		  7	-0.753,-0.534			    8,7			 138/150	N,C,I,F,L,M,V,A,S,T
  21	   I	   ILE21:A	-0.227		  6	-0.400,-0.153			    6,6			 138/150	N,G,R,H,Y,K,I,Q,L,M,A,V,S,T
  22	   Q	   GLN22:A	-0.623		  7	-0.753,-0.534			    8,7			 138/150	M,V,A,S,P,I,Q,C,R,K,Y
  23	   L	   LEU23:A	-0.404		  7	-0.593,-0.325			    7,6			 137/150	M,H,G,L,S,Y,F
  24	   I	   ILE24:A	-0.259		  6	-0.469,-0.153			    7,6			 137/150	K,R,C,N,S,T,V,A,M,L,Q,I,F
  25	   Q	   GLN25:A	 0.111		  5	-0.153, 0.189			    6,4			 137/150	H,C,Y,N,M,E,T,A,Q,R,G,K,D,W,L,S
  26	   N	   ASN26:A	 0.172		  4	-0.053, 0.339			    5,4			 136/150	A,S,T,L,E,Q,P,D,K,G,R,H,N
  27	   H	   HIS27:A	 0.743		  2	 0.339, 1.053			    4,1			 136/150	Y,C,H,N,A,V,T,E,M,Q,I,D,K,G,R,S,L,F
  28	   F	   PHE28:A	-1.011		  9	-1.109,-0.968			    9,9			 136/150	S,Y,W,F
  29	   V	   VAL29:A	 0.308		  4	 0.061, 0.521			    5,3			 135/150	K,D,R,P,S,L,N,Y,H,Q,I,T,V,A,M,E
  30	   D	   ASP30:A	 0.749		  2	 0.339, 1.053			    4,1			 133/150	K,D,G,R,N,W,S,T,V,M,L,E,Q,P
  31	   E	   GLU31:A	 0.397		  4	 0.061, 0.521			    5,3			 136/150	E,M,V,A,T,S,I,Q,R,H,D,K,Y,N
  32	   Y	   TYR32:A	-0.749		  8	-0.888,-0.650			    8,7			 137/150	C,G,H,Y,N,L,S,T,F,I,P
  33	   D	   ASP33:A	-0.133		  5	-0.325,-0.053			    6,5			 137/150	I,Q,E,T,V,A,N,C,Y,P,F,L,S,R,D,K
  34	   P	   PRO34:A	-0.476		  7	-0.650,-0.400			    7,6			 137/150	P,Q,L,M,V,A,T,S,R,H
  35	   T	   THR35:A	-1.125		  9	-1.203,-1.109			    9,9			 137/150	T,V,M,N,I,P
  36	   I	   ILE36:A	-0.943		  9	-1.042,-0.888			    9,8			 137/150	I,L,M,V,A,T,S,Y,K
  37	   E	   GLU37:A	-0.952		  9	-1.042,-0.888			    9,8			 137/150	F,Q,C,G,E,A
  38	   D	   ASP38:A	-0.676		  8	-0.800,-0.593			    8,7			 138/150	N,C,D,F,E,M,A,T,S
  39	   S	   SER39:A	-0.666		  7	-0.800,-0.593			    8,7			 138/150	L,E,M,A,V,T,S,I,F,Q,H,D,N
  40	   Y	   TYR40:A	-0.446		  7	-0.593,-0.325			    7,6			 138/150	Q,F,S,V,A,M,L,W,K,Y,H,R,C,G
  41	   R	   ARG41:A	-0.030		  5	-0.243, 0.061			    6,5			 138/150	N,W,K,R,H,Q,F,I,A,V,T,S,L,E,M
  42	   K	   LYS42:A	-0.706		  8	-0.845,-0.650			    8,7			 138/150	H,R,G,K,E,T,S,V,A,Q
  43	   Q	   GLN43:A	 0.457		  3	 0.189, 0.521			    4,3			 139/150	L,S,F,P,R,D,K,E,M,A,V,T,I,Q,C,H,Y,N
  44	   V	   VAL44:A	 0.182		  4	-0.053, 0.339			    5,4			 140/150	I,P,F,E,L,M,V,A,T,C,K,Y
  45	   V	   VAL45:A	 1.299		  1	 0.748, 1.526			    2,1			 141/150	E,M,V,A,T,I,Q,C,H,N,L,S,F,G,R,D,K
  46	   I	   ILE46:A	 0.093		  5	-0.153, 0.189			    6,4			 140/150	I,F,Q,M,L,T,S,A,V,D,Y
  47	   D	   ASP47:A	-0.228		  6	-0.400,-0.153			    6,6			 143/150	N,K,D,R,G,Q,P,F,S,T,A,E
  48	   G	   GLY48:A	 0.811		  2	 0.339, 1.053			    4,1			 144/150	H,C,G,D,Y,K,N,E,T,S,A,Q
  49	   E	   GLU49:A	 0.370		  4	 0.061, 0.521			    5,3			 144/150	P,F,L,S,G,R,K,D,I,Q,M,E,T,V,A,N,H
  50	   T	   THR50:A	 1.090		  1	 0.521, 1.526			    3,1			 144/150	N,Y,H,Q,I,V,A,T,E,M,W,D,K,R,G,P,S,L
  51	   C	   CYS51:A	 0.169		  4	-0.053, 0.339			    5,4			 145/150	S,T,A,V,L,F,I,Y,C,G
  52	   L	   LEU52:A	 0.273		  4	 0.061, 0.339			    5,4			 146/150	F,L,S,R,G,D,K,I,Q,E,M,A,V,T,N,H,Y
  53	   L	   LEU53:A	-0.625		  7	-0.753,-0.534			    8,7			 147/150	F,I,M,L,T,A,V,C,R
  54	   D	   ASP54:A	-0.642		  7	-0.753,-0.593			    8,7			 147/150	F,Q,L,E,T,A,N,H,G,R,K,D
  55	   I	   ILE55:A	-0.806		  8	-0.929,-0.753			    8,8			 148/150	T,V,L,I
  56	   L	   LEU56:A	-0.710		  8	-0.845,-0.650			    8,7			 148/150	W,M,L,T,S,A,V,I,F
  57	   D	   ASP57:A	-1.180		  9	-1.232,-1.173			    9,9			 149/150	V,D,E
  58	   T	   THR58:A	-1.151		  9	-1.203,-1.142			    9,9			 149/150	A,T,S,C
  59	   A	   ALA59:A	-1.049		  9	-1.109,-1.005			    9,9			 149/150	A,S,G,E
  60	   G	   GLY60:A	-1.059		  9	-1.142,-1.005			    9,9			 149/150	G,S,A,D
  61	   Q	   GLN61:A	-1.028		  9	-1.109,-1.005			    9,9			 149/150	H,G,K,D,N,M,E,S,T,A,V,Q
  62	   E	   GLU62:A	-0.730		  8	-0.845,-0.650			    8,7			 148/150	G,R,H,K,Y,D,F,Q,E,M,A,T
  63	   E	   GLU63:A	-0.634		  7	-0.753,-0.534			    8,7			 148/150	Q,I,P,T,S,V,E,K,D,R
  64	   Y	   TYR64:A	-0.577		  7	-0.703,-0.469			    8,7			 149/150	P,F,S,T,L,N,D,Y,H
  65	   S	   SER65:A	-0.363		  6	-0.534,-0.243			    7,6			 147/150	D,K,G,R,S,L,P,F,Y,H,A,V,T,E,M,Q
  66	   A	   ALA66:A	-0.642		  7	-0.753,-0.593			    8,7			 148/150	I,P,Q,L,A,V,S,T,N,C,G,R,D
  67	   M	   MET67:A	-0.707		  8	-0.845,-0.650			    8,7			 148/150	Q,I,F,V,L,M,N,K,G,H
  68	   R	   ARG68:A	-0.639		  7	-0.753,-0.534			    8,7			 148/150	H,C,G,R,N,W,T,S,A,V,L,Q,I,P
  69	   D	   ASP69:A	-0.065		  5	-0.243, 0.061			    6,5			 148/150	D,K,Y,R,H,N,A,V,S,T,E,Q,P
  70	   Q	   GLN70:A	-0.437		  7	-0.593,-0.325			    7,6			 148/150	N,R,G,H,K,P,F,Q,E,L,M,V,A,S
  71	   Y	   TYR71:A	-0.568		  7	-0.703,-0.469			    8,7			 149/150	W,H,R,C,D,K,Y,I,F,Q,S,V
  72	   M	   MET72:A	-0.396		  6	-0.534,-0.325			    7,6			 150/150	W,Y,I,F,T,S,A,V,M,L
  73	   R	   ARG73:A	-0.598		  7	-0.753,-0.534			    8,7			 150/150	P,I,M,E,L,S,T,V,A,N,H,R,K
  74	   T	   THR74:A	 0.067		  5	-0.153, 0.189			    6,4			 150/150	Q,I,F,A,T,S,E,N,W,D,K,G,R,H
  75	   G	   GLY75:A	-0.511		  7	-0.650,-0.400			    7,6			 150/150	N,K,C,G,R,I,V,A,S,T,L,M
  76	   E	   GLU76:A	-0.187		  6	-0.400,-0.053			    6,5			 150/150	L,E,M,V,A,S,F,Q,G,R,H,K,D,N
  77	   G	   GLY77:A	-0.463		  7	-0.650,-0.325			    7,6			 150/150	C,R,G,Y,I,M,L,V,A
  78	   F	   PHE78:A	-0.715		  8	-0.845,-0.650			    8,7			 150/150	C,H,Y,L,V,A,T,F,I
  79	   L	   LEU79:A	 0.170		  4	-0.053, 0.339			    5,4			 150/150	F,I,L,M,A,V,W,C
  80	   C	   CYS80:A	-0.331		  6	-0.534,-0.243			    7,6			 150/150	G,C,V,L,M,Q,F,I
  81	   V	   VAL81:A	-1.076		  9	-1.142,-1.042			    9,9			 150/150	L,C,M,V,A,T,I
  82	   F	   PHE82:A	-0.545		  7	-0.703,-0.469			    8,7			 150/150	Y,F,W
  83	   A	   ALA83:A	-1.018		  9	-1.109,-0.968			    9,9			 150/150	D,A,S
  84	   I	   ILE84:A	-0.343		  6	-0.534,-0.243			    7,6			 150/150	V,M,L,I,F
  85	   N	   ASN85:A	-0.235		  6	-0.400,-0.153			    6,6			 150/150	N,C,D,K,I,L,E,M,V,A,T,S
  86	   N	   ASN86:A	-0.235		  6	-0.400,-0.153			    6,6			 150/150	V,A,T,S,E,Q,K,D,C,R,G,N
  87	   T	   THR87:A	 1.038		  1	 0.521, 1.053			    3,1			 150/150	M,E,T,A,V,I,Q,H,N,L,S,F,P,R,G,K,D
  88	   K	   LYS88:A	 2.277		  1	 1.053, 2.771			    1,1			 150/150	E,T,A,V,I,Q,H,C,Y,N,L,S,P,G,R,D,K
  89	   S	   SER89:A	-0.945		  9	-1.042,-0.888			    9,8			 150/150	T,S,D,A,W
  90	   F	   PHE90:A	-0.799		  8	-0.929,-0.753			    8,8			 150/150	L,C,Y,F
  91	   E	   GLU91:A	 1.466		  1	 1.053, 1.526			    1,1			 150/150	Q,I,T,V,A,M,E,N,Y,H,C,S,L,W,D,K,R,G
  92	   D	   ASP92:A	-0.144		  6	-0.325,-0.053			    6,5			 150/150	E,M,V,A,T,I,Q,C,H,N,L,S,R,G,K,D
  93	   I	   ILE93:A	 0.024		  5	-0.153, 0.189			    6,4			 150/150	I,S,T,A,V,L,K,C
  94	   H	   HIS94:A	 2.734		  1	 1.526, 2.771			    1,1			 150/150	P,S,L,K,D,G,R,Q,I,T,V,A,M,E,N,Y,H
  95	   Q	   GLN95:A	 1.070		  1	 0.521, 1.526			    3,1			 150/150	I,Q,E,V,A,T,N,H,Y,P,L,S,R,G,K,D
  96	   Y	   TYR96:A	-0.171		  6	-0.400,-0.053			    6,5			 150/150	M,L,E,T,A,V,I,F,R,C,Y,W
  97	   R	   ARG97:A	 0.438		  3	 0.189, 0.521			    4,3			 150/150	Y,K,C,R,H,Q,F,I,V,L,M
  98	   E	   GLU98:A	 0.936		  2	 0.521, 1.053			    3,1			 150/150	A,V,T,S,E,L,M,Q,P,F,K,D,R,H,N
  99	   Q	   GLN99:A	-0.504		  7	-0.650,-0.400			    7,6			 150/150	I,Q,M,E,T,A,N,H,C,Y,L,S,W,R,D,K
 100	   I	  ILE100:A	-0.608		  7	-0.753,-0.534			    8,7			 150/150	I,F,V,A,L,M,W,C
 101	   K	  LYS101:A	 1.363		  1	 0.748, 1.526			    2,1			 150/150	L,S,F,R,G,D,K,E,M,A,V,T,I,Q,C,H,Y,N
 102	   R	  ARG102:A	 0.092		  5	-0.153, 0.189			    6,4			 150/150	P,S,L,D,G,R,Q,I,T,V,A,M,E,N,Y,H
 103	   V	  VAL103:A	-0.115		  5	-0.325,-0.053			    6,5			 150/150	E,L,M,A,V,S,T,F,I,Q,C,R,H,Y,N
 104	   K	  LYS104:A	-0.097		  5	-0.325, 0.061			    6,5			 150/150	L,S,G,R,K,I,Q,M,E,T,A,V,N,H,C,Y
 105	   D	  ASP105:A	 0.801		  2	 0.339, 1.053			    4,1			 133/150	M,E,T,A,I,H,C,N,L,S,F,P,G,R,K,D
 106	   S	  SER106:A	 1.416		  1	 0.748, 1.526			    2,1			  58/150	A,V,S,T,E,M,Q,I,K,D,R,G,N
 107	   D	  ASP107:A	 2.500		  1	 1.526, 2.771			    1,1			 145/150	K,D,R,G,F,P,S,L,N,H,Q,I,V,A,T,E,M
 108	   D	  ASP108:A	 1.655		  1	 1.053, 1.526			    1,1			 146/150	R,G,D,K,W,L,S,P,C,H,Y,N,E,A,V,T,Q
 109	   V	  VAL109:A	-0.124		  5	-0.325,-0.053			    6,5			 146/150	Q,F,P,I,S,T,V,A,M,L,D,Y,C
 110	   P	  PRO110:A	-0.028		  5	-0.243, 0.061			    6,5			 146/150	N,D,K,R,C,G,Q,P,I,V,A,T,S,L,E
 111	   M	  MET111:A	 0.747		  2	 0.339, 1.053			    4,1			 145/150	K,Y,R,C,F,I,T,S,A,V,M,L
 112	   V	  VAL112:A	-0.154		  6	-0.325,-0.053			    6,5			 146/150	A,V,M,L,G,I,F
 113	   L	  LEU113:A	-0.447		  7	-0.593,-0.325			    7,6			 146/150	M,L,V,I
 114	   V	  VAL114:A	-0.816		  8	-0.929,-0.753			    8,8			 146/150	I,C,L,A,V
 115	   G	  GLY115:A	-0.989		  9	-1.109,-0.929			    9,8			 146/150	G,S,A
 116	   N	  ASN116:A	-1.058		  9	-1.142,-1.042			    9,9			 146/150	C,H,N,L,A,T,S,Q
 117	   K	  LYS117:A	-1.209		  9	-1.246,-1.203			    9,9			 146/150	Q,K
 118	   C	  CYS118:A	 0.960		  1	 0.521, 1.053			    3,1			 146/150	C,R,G,K,N,L,V,A,S,T,I,F,Q
 119	   D	  ASP119:A	-1.153		  9	-1.203,-1.142			    9,9			 146/150	R,E,T,D
 120	   L	  LEU120:A	-0.595		  7	-0.753,-0.534			    8,7			 146/150	V,T,E,L,M,Q,I,F,K,C,R
 121	   A	  ALA121:A	 2.201		  1	 1.053, 2.771			    1,1			 144/150	L,S,P,G,R,K,D,M,E,T,A,V,Q,H,Y,N
 122	   A	  ALA122:A	 1.564		  1	 1.053, 1.526			    1,1			 140/150	D,K,R,G,S,L,N,Y,H,C,Q,I,T,A,V,M,E
 123	   R	  ARG123:A	-0.772		  8	-0.888,-0.703			    8,8			 139/150	W,C,R,K,P,I,Q,L,S,T,V,A
 124	   T	  THR124:A	 0.997		  1	 0.521, 1.053			    3,1			 137/150	G,R,D,K,P,F,L,S,N,C,I,Q,M,E,T,V,A
 125	   V	  VAL125:A	-0.846		  8	-0.929,-0.800			    8,8			 138/150	L,V,D,T,I,P
 126	   E	  GLU126:A	 0.557		  3	 0.189, 0.748			    4,2			 138/150	H,G,R,K,D,N,M,L,E,S,T,A,P,Q
 127	   S	  SER127:A	 2.728		  1	 1.526, 2.771			    1,1			 137/150	I,Q,E,M,A,V,T,H,Y,F,P,L,S,G,R,D,K
 128	   R	  ARG128:A	 2.430		  1	 1.053, 2.771			    1,1			 137/150	M,E,T,A,V,I,Q,H,C,Y,N,L,S,P,G,R,K,D
 129	   Q	  GLN129:A	 0.490		  3	 0.189, 0.748			    4,2			 136/150	A,V,S,T,L,E,M,Q,D,K,R,H,N
 130	   A	  ALA130:A	-0.060		  5	-0.325, 0.061			    6,5			 136/150	C,G,I,L,A,V,S,T
 131	   Q	  GLN131:A	 1.088		  1	 0.521, 1.526			    3,1			 136/150	K,D,R,G,S,L,F,H,N,T,A,V,M,E,Q,I
 132	   D	  ASP132:A	 2.762		  1	 1.526, 2.771			    1,1			 136/150	E,L,M,A,V,T,S,Q,G,C,R,H,K,D,N
 133	   L	  LEU133:A	-0.195		  6	-0.400,-0.053			    6,5			 134/150	M,L,V,F,I,R,K,Y,W
 134	   A	  ALA134:A	-0.829		  8	-0.929,-0.753			    8,8			 135/150	V,A,K,S,T,C,R
 135	   R	  ARG135:A	 2.647		  1	 1.526, 2.771			    1,1			 135/150	N,K,D,R,G,H,Q,I,A,S,T,L,E,M
 136	   S	  SER136:A	 2.150		  1	 1.053, 2.771			    1,1			 133/150	Q,V,A,S,T,E,M,N,K,Y,D,C,G,R,H
 137	   Y	  TYR137:A	 1.156		  1	 0.748, 1.526			    2,1			 133/150	K,D,Y,H,N,W,V,M,E,L,Q,I,F
 138	   G	  GLY138:A	 0.932		  2	 0.521, 1.053			    3,1			 134/150	A,S,T,E,M,Q,K,D,R,G,H,N
 139	   I	  ILE139:A	-0.083		  5	-0.325, 0.061			    6,5			 134/150	N,H,R,G,C,Y,F,I,M,L,S,A,V
 140	   P	  PRO140:A	 2.421		  1	 1.053, 2.771			    1,1			 134/150	D,K,G,R,S,L,F,P,Y,H,N,A,V,T,E,M,Q,I
 141	   Y	  TYR141:A	-0.316		  6	-0.534,-0.243			    7,6			 134/150	W,H,C,Y,F,I,L,V
 142	   I	  ILE142:A	 0.979		  1	 0.521, 1.053			    3,1			 133/150	I,F,S,T,A,V,M,L,Y,H
 143	   E	  GLU143:A	-1.121		  9	-1.203,-1.076			    9,9			 133/150	Q,S,K,E
 144	   T	  THR144:A	-0.608		  7	-0.753,-0.534			    8,7			 133/150	A,V,S,T,C,I
 145	   S	  SER145:A	-1.185		  9	-1.232,-1.173			    9,9			 133/150	I,S,T
 146	   A	  ALA146:A	-1.009		  9	-1.076,-0.968			    9,9			 133/150	G,C,V,A,T,S
 147	   K	  LYS147:A	-0.586		  7	-0.753,-0.469			    8,7			 133/150	L,S,V,A,R,C,K,Y
 148	   T	  THR148:A	 0.834		  2	 0.521, 1.053			    3,1			 133/150	E,L,A,V,S,T,I,F,Q,R,H,K,Y,D,N
 149	   R	  ARG149:A	 0.449		  3	 0.189, 0.521			    4,3			 133/150	Q,P,S,T,V,A,M,L,N,K,D,H,C,G,R
 150	   Q	  GLN150:A	 2.619		  1	 1.526, 2.771			    1,1			 132/150	T,V,A,M,E,Q,I,Y,H,C,N,S,L,F,K,D,G,R,W
 151	   G	  GLY151:A	-0.568		  7	-0.703,-0.469			    8,7			 132/150	Q,E,M,A,S,N,G,C,R,H,Y,D,K
 152	   V	  VAL152:A	-0.902		  8	-1.005,-0.845			    9,8			 131/150	V,Y,A,S,L,I
 153	   E	  GLU153:A	 0.934		  2	 0.521, 1.053			    3,1			 131/150	A,V,S,T,E,L,Q,K,D,R,C,G,H,N
 154	   D	  ASP154:A	 1.198		  1	 0.748, 1.526			    2,1			 131/150	N,D,K,R,G,H,Q,P,I,V,A,T,S,L,E
 155	   A	  ALA155:A	-0.009		  5	-0.243, 0.061			    6,5			 131/150	C,I,P,M,L,T,S,A,V
 156	   F	  PHE156:A	-1.116		  9	-1.203,-1.076			    9,9			 131/150	F,Y,V,M
 157	   Y	  TYR157:A	 2.704		  1	 1.526, 2.771			    1,1			 131/150	C,H,Y,N,E,M,A,V,T,I,Q,R,G,K,D,L,S,P,F
 158	   T	  THR158:A	 0.848		  2	 0.521, 1.053			    3,1			 131/150	Q,I,T,S,A,M,E,L,N,K,Y,D,R,C,G
 159	   L	  LEU159:A	-0.023		  5	-0.243, 0.061			    6,5			 130/150	F,I,T,V,A,M,L,C
 160	   V	  VAL160:A	-0.480		  7	-0.650,-0.400			    7,6			 129/150	L,M,A,V,S,T,I,F,C,H
 161	   R	  ARG161:A	 0.295		  4	 0.061, 0.521			    5,3			 125/150	K,D,Y,G,C,R,H,N,A,S,T,E,L,Q
 162	   E	  GLU162:A	 0.215		  4	-0.053, 0.339			    5,4			 116/150	S,A,V,M,L,E,Q,I,K,D,R,N
 163	   I	  ILE163:A	-0.596		  7	-0.753,-0.534			    8,7			 109/150	W,F,I,M,L,V
 164	   R	  ARG164:A	 0.039		  5	-0.243, 0.189			    6,4			  87/150	Y,D,K,H,R,N,S,A,L,E,Q,I
 165	   Q	  GLN165:A	 1.723		  1	 0.748, 2.771			    2,1			  43/150	A,S,E,Q,K,D,R,N
 166	   H	  HIS166:A	-0.756		  8	-1.042,-0.593			    9,7			   7/150	H,Y


*Below the confidence cut-off - The calculations for this site were performed on less than 6 non-gaped homologue sequences,
or the confidence interval for the estimated score is equal to- or larger than- 4 color grades.
