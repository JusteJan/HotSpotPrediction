	 Amino Acid Conservation Scores
	=======================================

- POS: The position of the AA in the SEQRES derived sequence.
- SEQ: The SEQRES derived sequence in one letter code.
- 3LATOM: The ATOM derived sequence in three letter code, including the AA's positions as they appear in the PDB file and the chain identifier.
- SCORE: The normalized conservation scores.
- COLOR: The color scale representing the conservation scores (9 - conserved, 1 - variable).
- CONFIDENCE INTERVAL: When using the bayesian method for calculating rates, a confidence interval is assigned to each of the inferred evolutionary conservation scores.
- CONFIDENCE INTERVAL COLORS: When using the bayesian method for calculating rates. The color scale representing the lower and upper bounds of the confidence interval.
- MSA DATA: The number of aligned sequences having an amino acid (non-gapped) from the overall number of sequences at each position.
- RESIDUE VARIETY: The residues variety at each position of the multiple sequence alignment.

 POS	 SEQ	    3LATOM	SCORE		COLOR	CONFIDENCE INTERVAL	CONFIDENCE INTERVAL COLORS	MSA DATA	RESIDUE VARIETY
    	    	        	(normalized)	        	               
   1	   R	    ARG1:I	-0.731		  7	-1.073,-0.546			    8,7			   8/150	K,R
   2	   P	    PRO2:I	-1.040		  8	-1.233,-0.915			    9,8			  22/150	Q,E,P
   3	   D	    ASP3:I	-0.191		  6	-0.451,-0.092			    6,5			  66/150	Q,D,K,G,S,P,A,N,E,V,H
   4	   F	    PHE4:I	 1.155		  1	 0.705, 1.474			    3,1			  91/150	T,V,F,E,A,M,I,P,S,K,L,R,C,W,Y
   5	   C	    CYS5:I	-1.333		  9	-1.380,-1.321			    9,9			 130/150	L,C
   6	   L	    LEU6:I	 1.435		  1	 0.705, 1.474			    3,1			 128/150	F,H,V,T,M,E,S,K,L,N,A,G,D,R,Y,Q
   7	   E	    GLU7:I	 0.177		  4	-0.092, 0.240			    5,4			 128/150	F,V,H,M,A,E,D,S,K,G,P,Y,R,L,Q
   8	   P	    PRO8:I	-0.333		  6	-0.546,-0.225			    7,6			 128/150	R,Y,Q,G,S,K,D,P,I,E,N,A,F,V
   9	   P	    PRO9:I	 1.301		  1	 0.705, 1.474			    3,1			 128/150	S,K,P,L,F,T,V,I,M,D,G,Y,R,Q,A,N
  10	   Y	   TYR10:I	 0.536		  3	 0.240, 0.705			    4,3			 130/150	I,M,A,N,E,F,T,V,H,Y,L,Q,D,K,S
  11	   T	   THR11:I	 0.926		  2	 0.449, 1.030			    4,2			 134/150	L,W,P,K,S,E,M,I,H,V,T,F,Q,R,Y,G,D,N,A
  12	   G	   GLY12:I	-1.337		  9	-1.380,-1.321			    9,9			 136/150	G,D
  13	   P	   PRO13:I	 0.247		  4	-0.092, 0.449			    5,4			 141/150	M,E,F,V,T,H,L,K,S,P,A,N,Y,R,Q,D
  14	   C	   CYS14:I	-1.189		  9	-1.294,-1.160			    9,9			 141/150	G,T,P,C
  15	   K	   LYS15:I	 0.697		  3	 0.240, 1.030			    4,2			 143/150	L,S,K,P,M,I,E,F,H,T,R,Y,Q,G,D,N,A
  16	   A	   ALA16:I	 0.065		  5	-0.225, 0.240			    6,4			 143/150	S,G,K,D,P,R,Y,Q,H,V,T,I,N,E,A
  17	   R	   ARG17:I	 1.447		  1	 0.705, 1.474			    3,1			 145/150	A,N,Y,R,Q,G,I,M,E,F,T,V,H,W,L,K,S,P
  18	   I	   ILE18:I	 0.855		  2	 0.449, 1.030			    4,2			 147/150	M,I,E,A,F,H,T,V,R,Y,Q,L,S,K,D
  19	   I	   ILE19:I	 1.737		  1	 1.030, 2.170			    2,1			 148/150	D,R,Y,Q,N,A,S,K,P,L,F,H,V,T,I,E
  20	   R	   ARG20:I	-0.780		  8	-0.915,-0.713			    8,7			 150/150	N,A,M,H,V,F,Q,L,R,K,S
  21	   Y	   TYR21:I	-0.201		  6	-0.451,-0.092			    6,5			 150/150	L,Y,W,T,V,F
  22	   F	   PHE22:I	 0.193		  4	-0.092, 0.240			    5,4			 150/150	S,G,Y,W,F,V,T,H,A,E,N
  23	   Y	   TYR23:I	-0.542		  7	-0.713,-0.451			    7,6			 150/150	F,W,Y
  24	   N	   ASN24:I	-0.810		  8	-0.915,-0.786			    8,8			 149/150	R,D,K,S,N,E,V,H
  25	   A	   ALA25:I	 0.674		  3	 0.240, 0.705			    4,3			 150/150	E,I,V,T,H,F,L,W,P,S,K,A,N,Q,Y,R,G
  26	   K	   LYS26:I	 1.549		  1	 1.030, 1.474			    2,1			 150/150	G,D,Q,R,Y,N,A,K,S,L,H,T,V,F,E,M,I
  27	   A	   ALA27:I	 0.292		  4	 0.061, 0.449			    5,4			 150/150	Y,R,C,Q,D,G,A,N,L,S,K,I,M,E,V,T,H
  28	   G	   GLY28:I	 0.741		  3	 0.240, 1.030			    4,2			 150/150	N,E,A,M,H,Q,L,R,K,S,G,D
  29	   L	   LEU29:I	 1.236		  1	 0.705, 1.474			    3,1			 150/150	L,K,S,M,I,E,F,H,V,T,R,Q,G,D,N,A
  30	   C	   CYS30:I	-1.372		  9	-1.394,-1.364			    9,9			 150/150	C
  31	   Q	   GLN31:I	-0.002		  5	-0.225, 0.061			    6,5			 150/150	L,K,S,E,I,T,V,H,F,Q,Y,R,D,G,A,N
  32	   T	   THR32:I	 0.794		  2	 0.449, 1.030			    4,2			 150/150	Q,R,Y,G,D,N,A,L,P,S,K,E,M,I,H,V,T,F
  33	   F	   PHE33:I	-1.334		  9	-1.380,-1.321			    9,9			 150/150	Y,F
  34	   V	   VAL34:I	 0.773		  2	 0.449, 1.030			    4,2			 150/150	G,D,R,Y,Q,N,A,K,S,P,W,L,F,H,T,V,M,I,E
  35	   Y	   TYR35:I	-1.177		  9	-1.265,-1.118			    9,9			 150/150	W,Y,F
  36	   G	   GLY36:I	-0.517		  7	-0.713,-0.451			    7,6			 150/150	S,G,D,L,Y,H,T,F,E,N,A
  37	   G	   GLY37:I	-1.206		  9	-1.294,-1.160			    9,9			 150/150	R,A,L,S,G,P
  38	   C	   CYS38:I	-1.021		  8	-1.160,-0.972			    9,8			 150/150	Q,L,R,C,K,D,E,A,M,I,V,F
  39	   R	   ARG39:I	 0.981		  2	 0.449, 1.030			    4,2			 150/150	M,E,N,A,F,H,T,R,Y,Q,L,K,G,D,P
  40	   A	   ALA40:I	-0.983		  8	-1.118,-0.915			    9,8			 150/150	P,V,S,G,Q,E,A
  41	   K	   LYS41:I	-1.282		  9	-1.321,-1.265			    9,9			 150/150	N,Q,R,Y,T,K,S
  42	   R	   ARG42:I	 1.131		  1	 0.705, 1.474			    3,1			 150/150	R,L,Q,D,K,S,G,P,I,A,E,N,T,H
  43	   N	   ASN43:I	-1.386		  9	-1.394,-1.380			    9,9			 149/150	N
  44	   N	   ASN44:I	-0.944		  8	-1.073,-0.915			    8,8			 150/150	V,H,N,P,S,K,Q,R
  45	   F	   PHE45:I	-1.265		  9	-1.321,-1.233			    9,9			 150/150	Y,H,F
  46	   K	   LYS46:I	 1.063		  2	 0.705, 1.474			    3,1			 150/150	Y,R,Q,D,G,A,N,L,S,K,P,I,M,E,F,V,T,H
  47	   S	   SER47:I	-0.622		  7	-0.786,-0.546			    8,7			 150/150	H,T,N,E,G,K,S,D,Q,Y
  48	   A	   ALA48:I	 0.328		  4	 0.061, 0.449			    5,4			 150/150	F,H,T,M,I,E,A,K,S,P,R,C,Q,L
  49	   E	   GLU49:I	 0.100		  5	-0.225, 0.240			    6,4			 150/150	I,A,E,N,F,T,V,H,R,L,Q,D,K,G,S
  50	   D	   ASP50:I	-0.039		  5	-0.225, 0.061			    6,5			 150/150	V,T,H,I,M,E,K,S,L,A,N,D,G,Y,R,Q
  51	   C	   CYS51:I	-1.372		  9	-1.394,-1.364			    9,9			 150/150	C
  52	   M	   MET52:I	 0.080		  5	-0.225, 0.240			    6,4			 150/150	Q,C,R,Y,D,A,L,W,S,K,E,M,I,H,T,V,F
  53	   R	   ARG53:I	 1.275		  1	 0.705, 1.474			    3,1			 149/150	R,Y,Q,G,D,N,A,L,K,S,M,I,E,H,T,V
  54	   T	   THR54:I	 1.412		  1	 0.705, 1.474			    3,1			 150/150	M,I,E,F,H,T,V,W,L,S,K,N,A,R,Y,Q,D
  55	   C	   CYS55:I	-1.372		  9	-1.394,-1.364			    9,9			 150/150	C
  56	   G	   GLY56:I	 1.837		  1	 1.030, 2.170			    2,1			  73/150	V,H,A,E,N,I,M,P,D,G,K,S,Q,R
  57	   G	   GLY57:I	-0.190		  6	-0.634, 0.061			    7,5			  26/150	D,G,S,P,T,A
  58	   A	   ALA58:I	-0.470		  7*	-1.073,-0.225			    8,6			   4/150	S,A


*Below the confidence cut-off - The calculations for this site were performed on less than 6 non-gaped homologue sequences,
or the confidence interval for the estimated score is equal to- or larger than- 4 color grades.
