	 Amino Acid Conservation Scores
	=======================================

- POS: The position of the AA in the SEQRES derived sequence.
- SEQ: The SEQRES derived sequence in one letter code.
- 3LATOM: The ATOM derived sequence in three letter code, including the AA's positions as they appear in the PDB file and the chain identifier.
- SCORE: The normalized conservation scores.
- COLOR: The color scale representing the conservation scores (9 - conserved, 1 - variable).
- CONFIDENCE INTERVAL: When using the bayesian method for calculating rates, a confidence interval is assigned to each of the inferred evolutionary conservation scores.
- CONFIDENCE INTERVAL COLORS: When using the bayesian method for calculating rates. The color scale representing the lower and upper bounds of the confidence interval.
- MSA DATA: The number of aligned sequences having an amino acid (non-gapped) from the overall number of sequences at each position.
- RESIDUE VARIETY: The residues variety at each position of the multiple sequence alignment.

 POS	 SEQ	    3LATOM	SCORE		COLOR	CONFIDENCE INTERVAL	CONFIDENCE INTERVAL COLORS	MSA DATA	RESIDUE VARIETY
    	    	        	(normalized)	        	               
   1	   V	   VAL16:A	-1.049		  8	-1.157,-0.981			    9,8			 123/150	F,V,I,M,C,N
   2	   V	   VAL17:A	-1.039		  8	-1.157,-0.981			    9,8			 130/150	L,F,S,M,I,Y,G,V,A
   3	   G	   GLY18:A	-1.007		  8	-1.115,-0.933			    9,8			 129/150	G,H,S,N,A
   4	   G	   GLY19:A	-1.329		  9	-1.408,-1.280			    9,9			 130/150	A,L,G
   5	   T	   THR20:A	 0.742		  3	 0.294, 1.000			    4,2			 130/150	V,A,K,D,M,Q,Y,N,G,H,E,L,T,S,F,R,C
   6	   D	   ASP21:A	 0.232		  4	 0.012, 0.294			    5,4			 131/150	V,A,P,I,M,N,Q,L,D,K,E,S,T
   7	   A	   ALA22:A	-0.631		  7	-0.778,-0.531			    7,7			 133/150	G,A,V,T,S,L,C,I
   8	   D	   ASP23:A	 1.399		  1	 0.700, 1.461			    3,1			 133/150	G,H,R,I,L,E,S,T,V,A,W,P,M,N,Y,Q,D,K
   9	   E	   GLU24:A	 0.773		  3	 0.294, 1.000			    4,2			 133/150	I,R,L,E,S,F,T,G,H,M,Q,D,K,V,A,P
  10	   G	   GLY25:A	-0.276		  6	-0.459,-0.102			    6,5			 134/150	G,H,X,A,E,K,D,T,S,M,R,Y,Q,N
  11	   E	   GLU26:A	-0.198		  6	-0.381,-0.102			    6,5			 136/150	H,G,A,V,S,T,D,E,K,N,Q,R,M
  12	   W	   TRP27:A	-0.063		  5	-0.296, 0.142			    6,5			 137/150	H,G,W,A,V,F,S,T,L,C,Q,Y,R,I
  13	   P	   PRO28:A	-1.294		  9	-1.362,-1.239			    9,9			 138/150	A,N,P,T,K
  14	   W	   TRP29:A	-0.944		  8	-1.071,-0.884			    8,8			 138/150	G,W,H,F,Y
  15	   Q	   GLN30:A	-1.046		  8	-1.157,-0.981			    9,8			 138/150	L,T,V,I,M,A,Q
  16	   V	   VAL31:A	-0.813		  8	-0.933,-0.721			    8,7			 138/150	C,A,V,I,S,G,L
  17	   S	   SER32:A	-1.029		  8	-1.115,-0.981			    9,8			 138/150	H,A,V,T,S,E,D,L,Y,Q,M,I,R
  18	   L	   LEU33:A	-0.776		  7	-0.933,-0.721			    8,7			 138/150	F,L,M,I,V
  19	   H	   HIS34:A	-0.200		  6	-0.381,-0.102			    6,5			 138/150	V,A,W,M,N,Y,Q,K,G,H,R,L,E,F,S,T
  20	   A	   ALA35:A	 2.517		  1	 1.461, 2.667			    1,1			 138/150	V,A,W,M,N,Y,Q,D,K,G,H,I,R,C,L,E,S,F,T
  21	   L	   LEU36:A	 1.844		  1	 1.000, 2.667			    2,1			  61/150	H,G,I,R,T,F,S,E,L,A,P,Q,Y,N,M,K,D
  22	   G	   GLY37:A	 1.471		  1	 1.000, 1.461			    2,1			 129/150	H,G,C,I,R,S,F,T,L,E,A,V,P,W,N,Y,Q,D,K
  23	   Q	   GLN38:A	 1.330		  1	 0.700, 1.461			    3,1			 142/150	D,K,N,Y,Q,W,P,V,L,E,S,F,T,R,G,H
  24	   G	   GLY39:A	 1.334		  1	 0.700, 1.461			    3,1			 142/150	Q,Y,N,M,K,D,A,V,P,W,I,R,T,F,S,E,L,H,G
  25	   H	   HIS40:A	-0.706		  7	-0.832,-0.598			    8,7			 141/150	R,I,N,Q,Y,L,K,E,F,S,A,G,H,P
  26	   I	   ILE41:A	 0.893		  2	 0.473, 1.000			    3,2			 143/150	M,I,R,Y,Q,N,K,L,T,S,F,V,A,W,H
  27	   C	   CYS42:A	-1.393		  9	-1.436,-1.408			    9,9			 143/150	C
  28	   G	   GLY43:A	-1.161		  9	-1.280,-1.115			    9,9			 143/150	A,G,S
  29	   A	   ALA44:A	-0.856		  8	-0.981,-0.778			    8,7			 143/150	G,S,R,A,C
  30	   S	   SER45:A	-0.898		  8	-1.027,-0.832			    8,8			 142/150	S,T,A,V,I
  31	   L	   LEU46:A	-1.052		  8	-1.157,-0.981			    9,8			 143/150	V,I,M,L
  32	   I	   ILE47:A	-0.825		  8	-0.933,-0.778			    8,7			 143/150	I,V,L,F
  33	   S	   SER48:A	-0.413		  6	-0.598,-0.296			    7,6			 143/150	D,K,S,T,R,N,G,H,A
  34	   P	   PRO49:A	 0.751		  3	 0.294, 1.000			    4,2			 144/150	A,G,P,H,R,N,Y,Q,D,E,K,S,T
  35	   N	   ASN50:A	 0.340		  4	 0.012, 0.473			    5,3			 144/150	H,C,R,I,F,S,T,L,E,W,N,Q,Y,M,D,K
  36	   W	   TRP51:A	-0.797		  8	-0.933,-0.721			    8,7			 144/150	V,Y,L,W,H,F,T
  37	   L	   LEU52:A	-0.933		  8	-1.027,-0.884			    8,8			 144/150	A,V,I,T,L
  38	   V	   VAL53:A	-0.557		  7	-0.721,-0.459			    7,6			 144/150	I,V,M,L
  39	   S	   SER54:A	-0.981		  8	-1.071,-0.933			    8,8			 144/150	T,S,H,W,D,N,C
  40	   A	   ALA55:A	-1.349		  9	-1.408,-1.321			    9,9			 144/150	V,R,A,T,H
  41	   A	   ALA56:A	-1.261		  9	-1.321,-1.239			    9,9			 144/150	T,S,G,A,C,R
  42	   H	   HIS57:A	-1.401		  9	-1.436,-1.408			    9,9			 144/150	P,H
  43	   C	   CYS58:A	-1.340		  9	-1.408,-1.321			    9,9			 144/150	C,S,L
  44	   Y	   TYR59:A	-0.171		  6	-0.381,-0.102			    6,5			 144/150	V,A,G,M,I,R,Y,Q,N,C,L,D,T,S,F
  45	   I	   ILE60:A	 1.412		  1	 1.000, 1.461			    2,1			 144/150	G,H,R,I,C,L,E,F,S,T,V,A,W,P,M,N,Y,Q,D,K
  46	   D	  ASP60A:A	 0.766		  3	 0.012, 1.000			    5,2			  12/150	G,D,E,S,R,N,Q,Y
  47	   D	  ASP60B:A	 1.307		  1	 0.473, 2.667			    3,1			  16/150	G,P,L,D,T,S,M,I,N
  48	   R	  ARG60C:A	 2.121		  1	 1.000, 2.667			    2,1			  17/150	V,G,H,P,R,Y,Q,N,L,D,T,S
  49	   G	  GLY60D:A	 1.122		  1	 0.294, 1.461			    4,1			  23/150	A,P,W,G,N,R,T,S,K,E,L,D
  50	   F	  PHE60E:A	 1.852		  1	 1.000, 2.667			    2,1			  39/150	P,G,T,F,S,E,K,L,D,Y,Q,N,I,R
  51	   R	  ARG60F:A	 0.965		  2	 0.473, 1.461			    3,1			  82/150	D,L,K,E,F,S,T,R,M,N,Q,Y,G,P,V
  52	   Y	  TYR60G:A	 2.279		  1	 1.000, 2.667			    2,1			 116/150	P,W,A,V,D,K,N,Y,Q,M,H,G,S,F,T,L,E,I,R
  53	   S	  SER60H:A	 1.201		  1	 0.700, 1.461			    3,1			 123/150	N,Y,Q,M,D,K,A,V,P,W,R,I,F,S,T,L,E,G
  54	   D	  ASP60I:A	 1.160		  1	 0.700, 1.461			    3,1			 134/150	I,R,F,S,T,L,E,H,G,N,Q,Y,D,K,A,P
  55	   P	   PRO61:A	 1.145		  1	 0.700, 1.461			    3,1			 139/150	M,R,Q,N,K,D,L,T,S,V,A,W,G,P
  56	   T	   THR62:A	 0.402		  4	 0.142, 0.473			    5,3			 143/150	L,E,F,S,T,I,R,G,H,D,K,N,Q,Y,P,V,A
  57	   Q	   GLN63:A	 1.460		  1	 1.000, 1.461			    2,1			 143/150	R,I,E,L,T,F,S,G,H,M,Y,Q,N,K,D,V,A,P
  58	   W	   TRP64:A	-0.007		  5	-0.204, 0.142			    6,5			 144/150	I,M,Y,L,D,F,T,V,A,G,W,H
  59	   T	   THR65:A	 0.458		  4	 0.142, 0.700			    5,3			 144/150	H,L,E,S,F,T,I,R,W,P,V,A,D,K,M,N,Q
  60	   A	   ALA66:A	-0.813		  8	-0.933,-0.721			    8,7			 144/150	G,L,A,I,V,M
  61	   F	   PHE67:A	 0.344		  4	 0.012, 0.473			    5,3			 144/150	Q,Y,N,M,K,D,A,V,W,R,I,T,S,F,L,H,G
  62	   L	   LEU68:A	-0.424		  6	-0.598,-0.296			    7,6			 144/150	G,V,A,L,F,T,I,M,Y
  63	   G	   GLY69:A	-1.220		  9	-1.321,-1.157			    9,9			 144/150	G,L,N,A,R
  64	   L	   LEU70:A	 0.146		  5	-0.102, 0.294			    5,4			 144/150	A,V,G,Q,N,M,R,I,T,S,K,E,D,L
  65	   H	   HIS71:A	-0.058		  5	-0.296, 0.012			    6,5			 144/150	R,I,E,L,T,F,S,G,H,M,Y,Q,N,K,D,V,A,W,P
  66	   D	   ASP72:A	 0.253		  4	 0.012, 0.473			    5,3			 144/150	Y,Q,N,K,D,A,V,I,R,T,F,S,E,L,H,G
  67	   Q	   GLN73:A	 0.089		  5	-0.204, 0.294			    6,4			 144/150	F,T,L,K,E,N,Q,Y,I,R,M,H,W,A,V
  68	   S	   SER74:A	 0.654		  3	 0.294, 0.700			    4,3			 142/150	G,F,S,T,L,E,R,I,P,A,V,D,K,N,Q,Y
  69	   Q	   GLN75:A	 0.868		  2	 0.473, 1.000			    3,2			 120/150	I,R,L,E,F,S,T,G,H,M,N,Y,Q,D,K,V,A,P
  70	   R	   ARG76:A	 1.562		  1	 1.000, 1.461			    2,1			 121/150	G,H,L,E,F,S,T,R,I,P,V,A,D,K,N,Q,Y
  71	   S	   SER77:A	 0.130		  5	-0.102, 0.294			    5,4			 107/150	I,C,E,L,T,S,G,H,M,Y,N,K,D,V,A,W,P
  72	   A	  ALA77A:A	 1.120		  1	 0.700, 1.461			    3,1			 118/150	W,P,A,K,D,Y,Q,N,G,H,E,L,T,F,S,R
  73	   P	   PRO78:A	 1.528		  1	 0.473, 2.667			    3,1			   8/150	R,Q,N,A,L,T,P
  74	   G	   GLY79:A	 0.666		  3	 0.294, 1.000			    4,2			 142/150	A,V,P,W,Y,Q,N,M,K,D,H,G,R,T,F,S,E,L
  75	   V	   VAL80:A	 0.274		  4	 0.012, 0.473			    5,3			 144/150	C,I,R,T,F,S,E,L,H,G,Y,Q,N,M,K,D,A,V,P
  76	   Q	   GLN81:A	-0.325		  6	-0.531,-0.204			    7,6			 145/150	T,S,E,L,I,R,H,G,K,Q,Y,N,M,P,A,V
  77	   E	   GLU82:A	 1.251		  1	 0.700, 1.461			    3,1			 146/150	W,P,V,A,D,K,M,N,Y,Q,G,H,L,E,S,F,T,R,I,C
  78	   R	   ARG83:A	 0.562		  3	 0.294, 0.700			    4,3			 146/150	R,I,M,N,Y,L,E,K,S,F,T,V,A,H
  79	   R	   ARG84:A	 2.451		  1	 1.461, 2.667			    1,1			 147/150	H,G,T,F,S,E,L,I,R,P,A,V,K,D,Y,Q,N,M
  80	   L	   LEU85:A	-0.756		  7	-0.884,-0.662			    8,7			 147/150	P,V,A,L,F,S,T,I,M
  81	   K	   LYS86:A	 0.824		  2	 0.473, 1.000			    3,2			 147/150	V,A,G,H,R,I,Q,N,K,E,D,L,T,S
  82	   R	   ARG87:A	 0.135		  5	-0.102, 0.294			    5,4			 147/150	G,H,A,D,L,K,E,S,T,R,I,N,Y,Q
  83	   I	   ILE88:A	-0.658		  7	-0.778,-0.598			    7,7			 148/150	H,V,A,K,E,L,T,F,S,M,I,Y,Q,N
  84	   I	   ILE89:A	-0.203		  6	-0.381,-0.102			    6,5			 148/150	A,V,W,N,Q,Y,M,K,H,I,R,S,F,T,L,E
  85	   S	   SER90:A	 1.125		  1	 0.700, 1.461			    3,1			 148/150	K,M,Q,Y,N,P,V,A,E,L,T,F,S,I,R,C,H
  86	   H	   HIS91:A	-1.026		  8	-1.115,-0.981			    9,8			 148/150	H,P,V,E,F,S,T,C,N,Q,Y
  87	   P	   PRO92:A	-0.189		  6	-0.381,-0.102			    6,5			 149/150	Y,Q,R,T,S,E,K,L,D,A,P,H
  88	   F	   PHE93:A	 1.235		  1	 0.700, 1.461			    3,1			 149/150	P,A,D,K,N,Y,Q,M,H,G,S,F,T,L,E,C,R
  89	   F	   PHE94:A	-0.564		  7	-0.721,-0.459			    7,6			 146/150	H,W,F,T,E,N,Q,Y
  90	   N	   ASN95:A	-0.300		  6	-0.459,-0.204			    6,6			 145/150	H,P,G,V,S,F,T,L,D,K,E,N,Y,Q,R
  91	   D	   ASP96:A	 1.624		  1	 1.000, 1.461			    2,1			 146/150	C,I,R,T,S,F,E,L,H,G,Y,Q,N,M,K,D,A,V,P
  92	   F	   PHE97:A	 2.556		  1	 1.461, 2.667			    1,1			 146/150	Y,Q,N,K,D,V,A,W,P,R,I,E,L,T,S,F,G,H
  93	   T	   THR98:A	 0.065		  5	-0.204, 0.142			    6,5			 143/150	A,V,P,Y,N,K,D,H,G,I,R,T,F,S,E,L
  94	   F	   PHE99:A	 1.655		  1	 1.000, 1.461			    2,1			 140/150	H,G,R,I,F,S,T,L,E,A,V,P,W,N,Y,Q,M,D,K
  95	   D	  ASP100:A	 0.285		  4	 0.012, 0.473			    5,3			 149/150	A,V,W,N,Y,Q,D,K,H,G,R,F,S,T,L,E
  96	   Y	  TYR101:A	-0.439		  6	-0.598,-0.381			    7,6			 149/150	X,A,G,H,R,N,Q,Y,D,K,S,F,T
  97	   D	  ASP102:A	-1.393		  9	-1.436,-1.362			    9,9			 150/150	D,N
  98	   I	  ILE103:A	-0.891		  8	-0.981,-0.832			    8,8			 150/150	Y,A,I,V,T,F,L
  99	   A	  ALA104:A	-1.156		  9	-1.239,-1.115			    9,9			 150/150	G,A,V,T,S,L,C,M
 100	   L	  LEU105:A	-1.081		  8	-1.198,-1.027			    9,8			 150/150	L,I,V,M
 101	   L	  LEU106:A	-0.161		  6	-0.381,-0.102			    6,5			 150/150	L,A,I,V,M
 102	   E	  GLU107:A	-0.354		  6	-0.531,-0.296			    7,6			 150/150	Y,Q,R,I,F,T,L,E,K,V,H
 103	   L	  LEU108:A	-1.246		  9	-1.321,-1.198			    9,9			 150/150	V,I,M,F,T,L
 104	   E	  GLU109:A	 0.674		  3	 0.294, 0.700			    4,3			 150/150	H,G,T,S,F,E,I,R,A,V,K,D,Y,Q,N,M
 105	   K	  LYS110:A	 0.769		  3	 0.473, 1.000			    3,2			 150/150	I,R,Q,N,E,K,D,T,F,S,V,A,G,H,P
 106	   P	  PRO111:A	-0.095		  5	-0.296, 0.012			    6,5			 150/150	R,I,Y,Q,C,N,K,E,D,T,S,V,A,G,P
 107	   A	  ALA112:A	-0.597		  7	-0.721,-0.531			    7,7			 149/150	V,A,L,T,S,F,M,I,Y,C
 108	   E	  GLU113:A	 1.008		  2	 0.473, 1.000			    3,2			 149/150	I,R,L,E,S,F,T,G,M,N,Q,Y,D,K,V,A,P
 109	   Y	  TYR114:A	 0.191		  4	-0.102, 0.294			    5,4			 149/150	M,N,Y,K,V,A,W,P,R,I,C,L,E,F,T,G
 110	   S	  SER115:A	-0.407		  6	-0.598,-0.296			    7,6			 146/150	G,A,K,D,T,S,M,R,Q,N
 111	   S	  SER116:A	 0.891		  2	 0.473, 1.000			    3,2			 149/150	P,G,A,V,S,T,D,L,K,E,N,Y,Q,R,M
 112	   M	  MET117:A	 1.006		  2	 0.473, 1.000			    3,2			 148/150	P,X,V,A,D,K,M,N,Q,Y,G,H,L,E,S,F,T,R
 113	   V	  VAL118:A	-0.718		  7	-0.832,-0.662			    8,7			 150/150	A,V,Q,M,I,T,S,K,L
 114	   R	  ARG119:A	 0.178		  4	-0.102, 0.294			    5,4			 149/150	W,A,V,K,N,Y,Q,M,H,G,S,T,L,E,R,I
 115	   P	  PRO120:A	-0.499		  7	-0.662,-0.381			    7,6			 149/150	R,I,M,Q,Y,L,K,T,V,A,H,P
 116	   I	  ILE121:A	-0.603		  7	-0.721,-0.531			    7,7			 150/150	S,T,G,L,A,I,V
 117	   C	  CYS122:A	-0.628		  7	-0.778,-0.531			    7,7			 150/150	P,V,A,K,D,L,T,F,S,R,Q,N,C
 118	   L	  LEU123:A	-1.142		  9	-1.239,-1.115			    9,9			 150/150	L,V,I,M
 119	   P	  PRO124:A	-0.777		  7	-0.933,-0.721			    8,7			 150/150	R,Q,Y,N,E,L,D,S,V,A,W,G,P
 120	   D	  ASP125:A	 0.780		  3	 0.473, 1.000			    3,2			 150/150	L,E,F,S,T,I,R,G,D,K,M,N,Q,Y,P,V,A
 121	   A	  ALA126:A	 1.581		  1	 1.000, 1.461			    2,1			 137/150	G,I,R,E,L,T,F,S,V,A,W,P,Q,N,K,D
 122	   S	  SER127:A	 0.739		  3	 0.294, 1.000			    4,2			 138/150	H,G,C,R,S,T,L,E,A,V,P,N,Q,Y,M,D,K
 123	   H	  HIS128:A	 0.763		  3	 0.473, 1.000			    3,2			 149/150	G,H,E,L,T,S,F,R,I,W,P,V,A,K,D,M,Y,Q,N
 124	   V	  VAL129:A	 1.339		  1	 0.700, 1.461			    3,1			 149/150	T,F,S,E,L,I,R,H,G,K,D,Q,Y,N,M,P,A,V
 125	   F	  PHE130:A	 0.533		  3	 0.142, 0.700			    5,3			 142/150	H,I,R,T,S,F,E,L,A,V,P,W,Y,Q,N,K
 126	   P	  PRO131:A	 1.267		  1	 0.700, 1.461			    3,1			 127/150	G,H,L,E,F,S,T,I,R,P,V,A,D,K,M,N,Y,Q
 127	   A	  ALA132:A	 1.177		  1	 0.700, 1.461			    3,1			 150/150	A,V,P,N,Y,Q,M,D,K,H,G,R,I,S,T,L,E
 128	   G	  GLY133:A	 0.436		  4	 0.142, 0.700			    5,3			 150/150	H,G,F,S,T,L,E,R,P,W,A,V,D,K,N,Y,M
 129	   K	  LYS134:A	 0.534		  3	 0.142, 0.700			    5,3			 150/150	W,P,V,A,K,D,M,Q,Y,N,G,H,E,L,T,S,I,R
 130	   A	  ALA135:A	 1.866		  1	 1.000, 2.667			    2,1			 150/150	V,A,P,M,Y,Q,N,K,D,H,R,I,C,E,L,T,S,F
 131	   I	  ILE136:A	-0.481		  7	-0.662,-0.381			    7,6			 150/150	A,V,P,G,C,I,T,F,S,L
 132	   W	  TRP137:A	 0.461		  4	 0.142, 0.700			    5,3			 150/150	I,R,M,Q,Y,L,K,E,S,F,T,V,A,W,H
 133	   V	  VAL138:A	-1.002		  8	-1.115,-0.933			    9,8			 150/150	A,C,I,V,S,T,L
 134	   T	  THR139:A	-0.891		  8	-0.981,-0.832			    8,8			 150/150	S,T,L,I,M,G,A,V
 135	   G	  GLY140:A	-1.394		  9	-1.436,-1.408			    9,9			 150/150	G
 136	   W	  TRP141:A	-1.371		  9	-1.436,-1.362			    9,9			 150/150	F,W
 137	   G	  GLY142:A	-1.375		  9	-1.436,-1.362			    9,9			 150/150	G,A
 138	   H	  HIS143:A	 0.865		  2	 0.473, 1.000			    3,2			 150/150	T,S,F,E,L,I,R,H,K,D,Q,Y,N,M,P,W,A,V
 139	   T	  THR144:A	-0.248		  6	-0.459,-0.102			    6,5			 150/150	V,P,N,Q,R,I,M,S,T,D,L,K
 140	   Q	  GLN145:A	 1.264		  1	 0.700, 1.461			    3,1			 150/150	G,H,E,L,T,F,S,R,I,V,A,K,D,M,Q,Y,N
 141	   Y	  TYR146:A	 0.121		  5	-0.102, 0.294			    5,4			 146/150	R,T,F,S,E,L,H,G,Q,Y,N,M,K,D,A,V,P
 142	   G	  GLY147:A	 0.917		  2	 0.473, 1.000			    3,2			 146/150	K,E,D,T,S,F,R,Q,Y,N,G,P,A
 143	   G	  GLY148:A	 0.481		  3	 0.142, 0.700			    5,3			 145/150	K,D,Q,Y,N,P,V,A,E,L,T,S,F,R,I,G,H
 144	   T	  THR150:A	 1.371		  1	 0.700, 1.461			    3,1			 140/150	S,F,T,L,E,I,R,H,G,D,K,N,Q,Y,M,P,A,V
 145	   G	  GLY151:A	 1.936		  1	 1.000, 2.667			    2,1			 148/150	Q,Y,N,M,K,D,A,V,P,R,I,T,F,S,E,L,H,G
 146	   A	  ALA152:A	-0.449		  6	-0.598,-0.381			    7,6			 150/150	P,A,V,S,T,D,L,E,N,Y,Q,R,M
 147	   L	  LEU153:A	 0.566		  3	 0.294, 0.700			    4,3			 149/150	K,D,Q,Y,N,P,W,A,V,T,F,S,E,L,I,R,G
 148	   I	  ILE154:A	 1.422		  1	 1.000, 1.461			    2,1			 150/150	Y,Q,N,K,D,A,V,P,I,R,T,S,F,E,L,H
 149	   L	  LEU155:A	-1.289		  9	-1.362,-1.239			    9,9			 150/150	V,I,Q,A,L,P
 150	   Q	  GLN156:A	-0.730		  7	-0.832,-0.662			    8,7			 150/150	H,N,Q,R,M,S,T,L,D,K,E
 151	   K	  LYS157:A	-0.619		  7	-0.778,-0.531			    7,7			 150/150	K,D,Q,Y,M,P,W,A,V,T,S,E,C,R,H,G
 152	   G	  GLY158:A	-0.553		  7	-0.721,-0.459			    7,6			 150/150	A,V,G,I,T,D,L,K
 153	   E	  GLU159:A	 1.049		  2	 0.700, 1.461			    3,1			 149/150	K,D,Q,N,M,P,W,A,V,X,T,S,F,E,L,R,I,H,G
 154	   I	  ILE160:A	-0.687		  7	-0.832,-0.598			    8,7			 150/150	V,A,H,M,I,Q,N,L,T,F
 155	   R	  ARG161:A	-0.213		  6	-0.459,-0.102			    6,5			 150/150	D,L,K,E,F,S,T,R,N,Q,G,H,P,A
 156	   V	  VAL162:A	-0.268		  6	-0.459,-0.204			    6,6			 150/150	Y,M,R,I,T,F,L,D,A,V
 157	   I	  ILE163:A	-0.511		  7	-0.662,-0.459			    7,6			 150/150	M,R,I,Y,L,V,A,W
 158	   N	  ASN164:A	-0.097		  5	-0.296, 0.012			    6,5			 150/150	E,D,T,S,M,R,N,G,H,P,A
 159	   Q	  GLN165:A	 0.071		  5	-0.204, 0.142			    6,5			 150/150	Q,Y,N,K,D,A,V,P,W,R,T,S,F,E,L,H,G
 160	   T	  THR166:A	 0.785		  2	 0.473, 1.000			    3,2			 150/150	Q,N,M,R,T,S,F,K,E,D,A,P,H,W,G
 161	   T	  THR167:A	 0.920		  2	 0.473, 1.000			    3,2			 150/150	A,V,D,K,N,Q,M,H,G,F,S,T,L,E,I,R
 162	   C	  CYS168:A	-1.343		  9	-1.408,-1.321			    9,9			 150/150	L,C,V
 163	   E	  GLU169:A	 0.130		  5	-0.102, 0.294			    5,4			 150/150	A,V,G,Q,N,I,R,T,S,E,K,D,L
 164	   N	  ASN170:A	 1.200		  1	 0.700, 1.461			    3,1			 150/150	G,H,E,L,T,S,R,C,W,V,A,K,D,M,Q,Y,N
 165	   L	  LEU171:A	 0.988		  2	 0.473, 1.000			    3,2			 149/150	A,V,W,Y,Q,M,D,K,H,G,R,I,S,F,T,L,E
 166	   L	  LEU172:A	-0.559		  7	-0.721,-0.459			    7,6			 150/150	L,K,S,F,T,R,M,N,Y,W,H
 167	   P	  PRO173:A	 0.962		  2	 0.473, 1.000			    3,2			 142/150	W,P,V,A,D,K,M,N,Q,G,H,E,F,S,T,R
 168	   Q	  GLN174:A	 1.834		  1	 1.000, 2.667			    2,1			 149/150	N,Q,Y,D,K,A,V,P,I,R,F,S,T,L,E,H,G
 169	   Q	  GLN175:A	 1.954		  1	 1.000, 2.667			    2,1			 150/150	T,S,F,E,L,I,R,H,G,K,D,Q,Y,N,M,P,W,A,V
 170	   I	  ILE176:A	-0.661		  7	-0.778,-0.598			    7,7			 150/150	I,R,V,M,F,S,L
 171	   T	  THR177:A	-0.305		  6	-0.459,-0.204			    6,6			 150/150	D,K,M,N,Y,Q,W,P,V,A,L,E,F,S,T,R,I,H
 172	   P	  PRO178:A	 0.977		  2	 0.473, 1.000			    3,2			 150/150	M,R,Q,Y,N,K,E,D,T,S,A,G,P
 173	   R	  ARG179:A	 0.003		  5	-0.204, 0.142			    6,5			 150/150	G,H,I,R,L,E,F,S,T,V,A,W,P,N,Q,Y,D,K
 174	   M	  MET180:A	-1.145		  9	-1.239,-1.115			    9,9			 150/150	D,L,E,S,F,T,R,M,N,Y,Q,V
 175	   M	  MET181:A	-0.149		  5	-0.381, 0.012			    6,5			 150/150	L,F,H,T,V,I,M
 176	   C	  CYS182:A	-1.339		  9	-1.408,-1.321			    9,9			 150/150	G,K,C
 177	   V	  VAL183:A	-1.308		  9	-1.362,-1.280			    9,9			 149/150	T,H,G,L,A,V
 178	   G	  GLY184:A	-1.130		  9	-1.239,-1.071			    9,8			 149/150	A,T,S,K,G
 179	   F	 PHE184A:A	 0.540		  3	 0.142, 0.700			    5,3			 128/150	R,I,T,S,F,E,L,H,G,Y,Q,N,K,D,A,V,P
 180	   L	  LEU185:A	 1.458		  1	 1.000, 1.461			    2,1			 133/150	R,I,E,L,T,F,S,G,H,M,Y,Q,N,K,D,V,A,P
 181	   S	  SER186:A	 0.971		  2	 0.473, 1.000			    3,2			 149/150	R,N,Q,L,D,K,E,S,T,V,A,G,W,P
 182	   G	 GLY186A:A	-0.555		  7	-0.721,-0.459			    7,6			 149/150	S,D,L,E,K,C,H,P,G,A,V
 183	   G	  GLY187:A	 0.105		  5	-0.204, 0.294			    6,4			 149/150	K,E,L,D,T,S,I,R,Q,C,N,G,H,V,A
 184	   V	  VAL188:A	-0.186		  6	-0.381,-0.102			    6,5			 148/150	S,T,K,Y,Q,R,I,M,P,H,G,A,V
 185	   D	  ASP189:A	-1.224		  9	-1.321,-1.198			    9,9			 148/150	G,D,T,F,S,A,N
 186	   S	  SER190:A	-0.554		  7	-0.721,-0.459			    7,6			 149/150	T,S,I,Q,N,G,P,H,V,A
 187	   C	  CYS191:A	-1.278		  9	-1.362,-1.239			    9,9			 148/150	G,C,A,Y,V,X
 188	   Q	  GLN192:A	-0.836		  8	-0.933,-0.778			    8,7			 150/150	G,L,E,S,F,T,R,W,V,A,D,K,M,N,Q,Y
 189	   G	  GLY193:A	-1.219		  9	-1.321,-1.157			    9,9			 149/150	G,L,Y,N,A
 190	   D	  ASP194:A	-1.354		  9	-1.408,-1.321			    9,9			 149/150	I,R,N,D,G
 191	   S	  SER195:A	-1.355		  9	-1.408,-1.321			    9,9			 148/150	H,P,S,L,A
 192	   G	  GLY196:A	-1.374		  9	-1.436,-1.362			    9,9			 148/150	G,S
 193	   G	  GLY197:A	-1.374		  9	-1.436,-1.362			    9,9			 147/150	A,G
 194	   P	  PRO198:A	-1.360		  9	-1.436,-1.321			    9,9			 147/150	G,S,P
 195	   L	  LEU199:A	-1.082		  8	-1.198,-1.027			    9,8			 147/150	A,V,H,Y,I,M,F,L
 196	   S	  SER200:A	-0.514		  7	-0.662,-0.459			    7,6			 147/150	L,T,S,F,M,I,Y,Q,N,H,V,A
 197	   S	  SER201:A	 0.092		  5	-0.204, 0.294			    6,4			 147/150	L,S,F,T,I,M,N,C,Y,Q,G,H,V,A
 198	   V	  VAL202:A	 0.348		  4	 0.012, 0.473			    5,3			 147/150	P,V,A,D,K,M,N,Q,Y,G,H,L,E,S,F,T,R,C
 199	   E	  GLU203:A	 0.700		  3	 0.294, 1.000			    4,2			 147/150	N,Q,Y,M,D,K,A,V,P,I,R,S,F,T,L,E,H,G
 200	   A	  ALA204:A	 1.437		  1	 0.700, 1.461			    3,1			  31/150	G,H,P,A,E,K,D,L,T,S,I,R
 201	   D	 ASP204A:A	 0.547		  3	 0.142, 0.700			    5,3			 116/150	E,K,D,T,S,R,Q,N,G,H,P,V,A
 202	   G	  GLY205:A	 0.419		  4	 0.142, 0.700			    5,3			 121/150	S,F,T,D,L,E,K,N,C,Q,R,P,G,A
 203	   R	  ARG206:A	 0.198		  4	-0.102, 0.294			    5,4			 122/150	L,E,S,F,T,I,R,H,D,K,M,N,Y,Q,V,A
 204	   I	  ILE207:A	-0.130		  5	-0.381, 0.012			    6,5			 123/150	K,D,L,F,S,M,I,Y,W,H,P,A
 205	   F	  PHE208:A	 0.877		  2	 0.473, 1.000			    3,2			 147/150	W,A,V,D,K,N,Y,Q,M,H,G,F,T,L,E,I,R
 206	   Q	  GLN209:A	-0.645		  7	-0.778,-0.531			    7,7			 147/150	L,S,I,V,N,Q
 207	   A	  ALA210:A	-0.303		  6	-0.459,-0.204			    6,6			 147/150	R,I,E,L,T,F,S,G,H,M,Q,Y,N,V,A,W
 208	   G	  GLY211:A	-1.374		  9	-1.436,-1.362			    9,9			 147/150	A,G
 209	   V	  VAL212:A	-0.563		  7	-0.721,-0.459			    7,6			 145/150	I,X,V,A,L,D
 210	   V	  VAL213:A	-1.025		  8	-1.115,-0.981			    9,8			 146/150	M,I,V,A,G,T,S
 211	   S	  SER214:A	-1.406		  9	-1.436,-1.408			    9,9			 146/150	S,T
 212	   W	  TRP215:A	-0.753		  7	-0.933,-0.662			    8,7			 146/150	A,Y,I,F,T,W,K
 213	   G	  GLY216:A	-1.219		  9	-1.321,-1.157			    9,9			 146/150	G,N,A,R
 214	   D	  ASP217:A	 1.838		  1	 1.000, 2.667			    2,1			 145/150	G,H,R,I,L,E,F,S,T,V,A,W,M,N,Y,Q,D,K
 215	   G	  GLY219:A	-0.363		  6	-0.531,-0.204			    7,6			 146/150	G,P,V,A,E,K,D,T,S,R,Y,Q,N
 216	   C	  CYS220:A	-1.160		  9	-1.280,-1.115			    9,9			 148/150	T,W,N,C,M,V,R
 217	   A	  ALA221:A	-0.852		  8	-0.981,-0.778			    8,7			 146/150	S,G,D,N,C,A,R
 218	   Q	 GLN221A:A	 0.451		  4	 0.142, 0.700			    5,3			 146/150	G,E,L,T,S,F,R,I,V,A,K,D,M,Q,Y,N
 219	   R	  ARG222:A	 0.422		  4	 0.142, 0.473			    5,3			 143/150	R,Y,Q,N,K,E,T,F,S,V,A,G,H,P
 220	   N	  ASN223:A	 0.045		  5	-0.204, 0.142			    6,5			 144/150	N,Q,Y,M,D,K,A,I,R,S,F,T,L,E,H,G
 221	   K	  LYS224:A	 0.514		  3	 0.142, 0.700			    5,3			 146/150	M,Y,Q,N,K,V,A,W,P,R,I,E,L,T,S,F,G,H
 222	   P	  PRO225:A	-1.124		  9	-1.239,-1.071			    9,8			 146/150	F,P,A,Y
 223	   G	  GLY226:A	-0.962		  8	-1.071,-0.884			    8,8			 146/150	T,S,E,G,A
 224	   V	  VAL227:A	-1.141		  9	-1.239,-1.115			    9,9			 146/150	A,M,V,I,F,G,L
 225	   Y	  TYR228:A	-1.285		  9	-1.362,-1.239			    9,9			 146/150	F,S,Y
 226	   T	  THR229:A	-0.854		  8	-0.981,-0.778			    8,7			 145/150	A,I,V,T,S,H,G
 227	   R	  ARG230:A	-0.584		  7	-0.721,-0.531			    7,7			 145/150	A,H,C,N,Q,R,S,D,L,K,E
 228	   L	  LEU231:A	-1.038		  8	-1.157,-0.981			    9,8			 145/150	V,I,M,A,L,T
 229	   P	  PRO232:A	-0.403		  6	-0.598,-0.296			    7,6			 144/150	Q,C,N,I,T,F,S,E,K,L,A,V,P
 230	   L	  LEU233:A	 1.348		  1	 0.700, 1.461			    3,1			 143/150	H,R,E,L,T,S,F,X,V,A,P,M,Q,Y,N,K,D
 231	   F	  PHE234:A	-0.054		  5	-0.296, 0.012			    6,5			 144/150	V,A,G,W,H,I,M,Y,L,S,F,T
 232	   R	  ARG235:A	 0.568		  3	 0.294, 0.700			    4,3			 144/150	R,I,M,N,Y,Q,L,E,K,F,S,T,V,A,H
 233	   D	  ASP236:A	 0.506		  3	 0.142, 0.700			    5,3			 144/150	N,Q,R,S,T,D,E,K,A,H,P,G
 234	   W	  TRP237:A	-1.335		  9	-1.408,-1.321			    9,9			 144/150	W,F
 235	   I	  ILE238:A	-1.246		  9	-1.321,-1.198			    9,9			 143/150	I,V,M,L
 236	   K	  LYS239:A	 0.707		  3	 0.294, 1.000			    4,2			 135/150	V,A,W,M,Y,Q,N,K,D,H,I,R,E,L,T,F,S
 237	   E	  GLU240:A	 0.478		  3	 0.142, 0.700			    5,3			 103/150	T,S,K,E,D,Q,N,R,G,A,V
 238	   N	  ASN241:A	-0.242		  6	-0.459,-0.102			    6,5			  58/150	Q,N,M,I,R,T,S,K,E,A,V,H
 239	   T	  THR242:A	-1.033		  8	-1.198,-0.933			    9,8			  32/150	V,S,T
 240	   G	  GLY243:A	-0.985		  8	-1.239,-0.832			    9,8			  17/150	D,G
 241	   V	  VAL244:A	-0.268		  6	-0.721, 0.012			    7,5			  13/150	V,I,L


*Below the confidence cut-off - The calculations for this site were performed on less than 6 non-gaped homologue sequences,
or the confidence interval for the estimated score is equal to- or larger than- 4 color grades.
