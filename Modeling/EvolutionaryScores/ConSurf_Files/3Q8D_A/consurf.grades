	 Amino Acid Conservation Scores
	=======================================

- POS: The position of the AA in the SEQRES derived sequence.
- SEQ: The SEQRES derived sequence in one letter code.
- 3LATOM: The ATOM derived sequence in three letter code, including the AA's positions as they appear in the PDB file and the chain identifier.
- SCORE: The normalized conservation scores.
- COLOR: The color scale representing the conservation scores (9 - conserved, 1 - variable).
- CONFIDENCE INTERVAL: When using the bayesian method for calculating rates, a confidence interval is assigned to each of the inferred evolutionary conservation scores.
- CONFIDENCE INTERVAL COLORS: When using the bayesian method for calculating rates. The color scale representing the lower and upper bounds of the confidence interval.
- MSA DATA: The number of aligned sequences having an amino acid (non-gapped) from the overall number of sequences at each position.
- RESIDUE VARIETY: The residues variety at each position of the multiple sequence alignment.

 POS	 SEQ	    3LATOM	SCORE		COLOR	CONFIDENCE INTERVAL	CONFIDENCE INTERVAL COLORS	MSA DATA	RESIDUE VARIETY
    	    	        	(normalized)	        	               
   1	   M	         -	-0.642		  7	-1.033,-0.435			    8,6			  14/150	Q,M,I
   2	   E	    GLU2:A	 0.095		  5	-0.352, 0.376			    6,4			  28/150	L,G,D,E,Q
   3	   G	    GLY3:A	-0.960		  8	-1.079,-0.885			    8,8			 112/150	E,Q,D,N,G,H,W,K
   4	   W	    TRP4:A	 0.341		  4*	-0.261, 0.819			    6,2			  26/150	L,F,W,A
   5	   Q	    GLN5:A	-0.539		  7	-0.885,-0.352			    8,6			  28/150	H,Y,L,Q,R
   6	   R	    ARG6:A	-0.018		  5	-0.261, 0.071			    6,5			 138/150	Q,E,H,S,N,A,R,P,T,L
   7	   A	    ALA7:A	-0.833		  8	-0.985,-0.774			    8,7			 144/150	V,C,S,G,A
   8	   F	    PHE8:A	-0.286		  6	-0.512,-0.163			    7,6			 145/150	F,I,L,Y,W,V
   9	   V	    VAL9:A	-0.759		  7	-0.885,-0.651			    8,7			 146/150	M,V,L,I
  10	   L	   LEU10:A	-1.212		  9	-1.326,-1.165			    9,9			 147/150	I,L,V,M
  11	   H	   HIS11:A	-1.314		  9	-1.405,-1.287			    9,9			 147/150	Q,R,N,H,K
  12	   S	   SER12:A	-0.239		  6	-0.435,-0.163			    6,6			 147/150	R,A,T,K,Q,S,G,H
  13	   R	   ARG13:A	-1.054		  8	-1.165,-0.985			    9,8			 147/150	R,T,W,K,L,Q,Y,H
  14	   P	   PRO14:A	-0.333		  6	-0.584,-0.163			    7,6			 147/150	K,P,L,R,A,H,G,S,D,Q,E
  15	   W	   TRP15:A	-0.616		  7	-0.774,-0.512			    7,7			 147/150	W,Y,H,F
  16	   S	   SER16:A	-0.764		  7	-0.885,-0.715			    8,7			 147/150	K,T,G,S,L,Q,R
  17	   E	   GLU17:A	-1.153		  9	-1.247,-1.123			    9,9			 147/150	D,E,N
  18	   T	   THR18:A	-0.879		  8	-0.985,-0.831			    8,8			 147/150	T,N,S,D,R
  19	   S	   SER19:A	-1.363		  9	-1.428,-1.326			    9,9			 147/150	A,Q,S,K
  20	   L	   LEU20:A	-0.475		  7	-0.651,-0.352			    7,6			 147/150	M,Y,Q,E,V,L,F,R,A
  21	   M	   MET21:A	-0.570		  7	-0.715,-0.435			    7,6			 149/150	V,M,W,L,I
  22	   L	   LEU22:A	-0.370		  6	-0.584,-0.261			    7,6			 149/150	A,V,H,F,I,L
  23	   D	   ASP23:A	-0.767		  7	-0.885,-0.715			    8,7			 149/150	E,Q,D,S,N,T
  24	   V	   VAL24:A	 0.386		  4	 0.071, 0.572			    5,3			 149/150	A,T,C,V,L,F,M,I
  25	   F	   PHE25:A	-0.680		  7	-0.831,-0.584			    8,7			 149/150	I,L,F,Y
  26	   T	   THR26:A	-0.656		  7	-0.831,-0.584			    8,7			 149/150	L,S,V,C,T,A
  27	   E	   GLU27:A	-0.242		  6	-0.435,-0.163			    6,6			 148/150	A,R,F,L,K,P,Q,E,D,I
  28	   E	   GLU28:A	 0.157		  4	-0.163, 0.376			    6,4			 149/150	T,K,R,A,H,S,N,G,D,E,Q
  29	   S	   SER29:A	 0.639		  3	 0.212, 0.819			    4,2			 134/150	F,V,K,C,R,N,S,I,Y,D,L,A,G,H,Q,E
  30	   G	   GLY30:A	-1.377		  9	-1.428,-1.365			    9,9			 149/150	G
  31	   R	   ARG31:A	-1.120		  9	-1.207,-1.079			    9,8			 149/150	Q,R,L,V,H,K,C
  32	   V	   VAL32:A	 0.377		  4	 0.071, 0.572			    5,3			 149/150	L,F,V,T,Q,S,I,M
  33	   R	   ARG33:A	-0.348		  6	-0.512,-0.261			    7,6			 149/150	G,S,N,M,D,P,C,T,A,R
  34	   L	   LEU34:A	-0.167		  6	-0.435,-0.052			    6,5			 149/150	A,C,V,T,L,M,G,I
  35	   V	   VAL35:A	-0.302		  6	-0.512,-0.163			    7,6			 149/150	V,M,T,L,I,A
  36	   A	   ALA36:A	-1.071		  8	-1.165,-1.033			    9,8			 149/150	Y,M,S,I,G,W,V,F,L,A
  37	   K	   LYS37:A	-0.847		  8	-0.985,-0.774			    8,7			 149/150	N,K,Q,R
  38	   G	   GLY38:A	-0.852		  8	-1.033,-0.774			    8,7			 148/150	S,I,G,L,T,V,A,R
  39	   A	   ALA39:A	-0.830		  8	-0.985,-0.774			    8,7			 148/150	A,S,L,G,V,K
  40	   R	   ARG40:A	-0.932		  8	-1.079,-0.885			    8,8			 143/150	H,G,S,N,Q,K,L,R
  41	   S	   SER41:A	-0.371		  6	-0.584,-0.261			    7,6			 149/150	Q,A,R,K,G,N,S
  42	   K	   LYS42:A	 0.077		  5	-0.163, 0.212			    6,4			 149/150	E,Q,D,S,N,G,R,A,L,T,P,K
  43	   R	   ARG43:A	-0.329		  6	-0.512,-0.261			    7,6			 149/150	A,R,K,T,L,Q,H,G,N,S
  44	   S	   SER44:A	-0.674		  7	-0.831,-0.584			    8,7			 148/150	R,A,T,K,V,L,Q,S,N,G
  45	   T	   THR45:A	 0.671		  3	 0.376, 0.819			    4,2			 147/150	D,E,Q,M,H,S,N,G,A,R,T,V,K,P
  46	   L	   LEU46:A	 0.209		  4	-0.052, 0.376			    5,4			 127/150	Q,H,M,Y,G,S,N,I,R,K,V,W,T,F,L
  47	   K	   LYS47:A	-0.695		  7	-0.831,-0.584			    8,7			 127/150	Q,H,G,S,N,A,R,P,K,F
  48	   G	   GLY48:A	-0.265		  6	-0.512,-0.163			    7,6			 129/150	H,S,G,Q,T,P,L,A,R
  49	   A	   ALA49:A	 0.568		  3	 0.212, 0.819			    4,2			 148/150	Y,M,H,I,S,D,Q,W,T,K,V,C,L,A,R
  50	   L	   LEU50:A	-1.029		  8	-1.165,-0.985			    9,8			 149/150	P,V,M,I,L,A
  51	   Q	   GLN51:A	-1.011		  8	-1.123,-0.936			    9,8			 149/150	N,M,Q,E,L,K,V,R
  52	   P	   PRO52:A	-0.489		  7	-0.651,-0.352			    7,6			 149/150	A,L,F,T,V,P,Q,S,N,G,H
  53	   F	   PHE53:A	-1.171		  9	-1.287,-1.123			    9,9			 149/150	M,Y,G,L,F
  54	   T	   THR54:A	-0.540		  7	-0.715,-0.435			    7,6			 150/150	N,S,I,H,M,Q,L,C,K,V,T,R,A
  55	   P	   PRO55:A	-0.383		  6	-0.584,-0.261			    7,6			 150/150	Q,S,H,A,R,L,C,V,P
  56	   L	   LEU56:A	-1.013		  8	-1.123,-0.936			    9,8			 150/150	L,F,I,Y,V,M
  57	   L	   LEU57:A	 0.135		  5	-0.163, 0.212			    6,4			 150/150	T,K,V,R,I,S,N,Y,M,L,W,A,G,H,E,Q
  58	   L	   LEU58:A	 0.557		  3	 0.212, 0.819			    4,2			 150/150	C,V,F,L,A,M,G,S,I
  59	   R	   ARG59:A	-0.481		  7	-0.651,-0.352			    7,6			 150/150	R,A,V,K,T,D,Q,E,G,N,S
  60	   F	   PHE60:A	-0.036		  5	-0.352, 0.071			    6,5			 150/150	V,T,W,F,L,A,H,M,Y
  61	   G	   GLY61:A	 0.648		  3	 0.212, 0.819			    4,2			 150/150	R,A,F,L,K,V,T,Q,G,I,S,H,M
  62	   G	   GLY62:A	-1.035		  8	-1.165,-0.936			    9,8			 150/150	L,P,S,G,M,E,Q,D
  63	   R	   ARG63:A	-0.392		  6	-0.584,-0.261			    7,6			 149/150	A,R,K,E,Q,D,N,S,G
  64	   G	   GLY64:A	 0.344		  4	 0.071, 0.572			    5,3			 150/150	Q,H,Y,G,N,S,R,A,T,L
  65	   E	   GLU65:A	-0.773		  7	-0.885,-0.715			    8,7			 150/150	S,N,G,T,E,A,D
  66	   V	   VAL66:A	-0.911		  8	-1.033,-0.831			    8,8			 150/150	I,L,W,M,V
  67	   K	   LYS67:A	-0.583		  7	-0.774,-0.512			    7,7			 150/150	Q,Y,H,M,I,G,A,R,T,K,P,V,L
  68	   T	   THR68:A	-0.719		  7	-0.885,-0.651			    8,7			 150/150	N,I,S,Y,H,Q,L,T,V,A,R
  69	   L	   LEU69:A	-1.124		  9	-1.247,-1.079			    9,8			 150/150	V,P,M,L,I
  70	   R	   ARG70:A	 0.541		  3	 0.212, 0.819			    4,2			 150/150	Y,M,S,N,I,R,T,C,K,V,F,Q,H,G,A,L
  71	   S	   SER71:A	 0.888		  2	 0.376, 1.148			    4,1			 150/150	A,R,L,T,K,P,Q,D,N,S,G,H
  72	   A	   ALA72:A	-0.239		  6	-0.435,-0.163			    6,6			 150/150	W,T,C,V,L,F,A,M,I,S
  73	   E	   GLU73:A	-1.147		  9	-1.247,-1.123			    9,9			 150/150	D,A,E,Q
  74	   A	   ALA74:A	 0.519		  3	 0.212, 0.819			    4,2			 150/150	A,T,W,P,V,L,F,E,Y,H,M,N,S,I
  75	   V	   VAL75:A	 0.734		  3	 0.376, 0.819			    4,2			 150/150	A,P,L,E,Q,H,G,R,T,K,C,V,D,Y,M,I,S,N
  76	   S	   SER76:A	 0.096		  5	-0.163, 0.212			    6,4			 146/150	T,P,A,Y,N,S,G,D,E,Q
  77	   L	   LEU77:A	 1.305		  1	 0.819, 1.655			    2,1			 150/150	A,L,P,E,Q,G,H,R,F,T,K,V,S,N,I,Y,M
  78	   A	   ALA78:A	 0.718		  3	 0.376, 0.819			    4,2			 150/150	Q,M,Y,G,N,I,S,A,R,V,C,P,T,L,F
  79	   L	   LEU79:A	 1.159		  1	 0.572, 1.655			    3,1			 149/150	R,T,K,V,F,Y,M,S,N,I,A,P,L,E,Q,H,G
  80	   P	   PRO80:A	 0.856		  2	 0.376, 1.148			    4,1			 150/150	G,H,Q,L,W,P,A,I,N,S,Y,M,T,V,K,R
  81	   L	   LEU81:A	-0.953		  8	-1.079,-0.885			    8,8			 150/150	A,R,M,P,I,L,F
  82	   S	   SER82:A	 1.218		  1	 0.819, 1.655			    2,1			 150/150	R,F,T,C,K,V,D,N,I,S,A,P,E,Q,G,H
  83	   G	   GLY83:A	-0.631		  7	-0.831,-0.512			    8,7			 150/150	Q,E,H,G,S,A,R,K,T
  84	   I	   ILE84:A	 1.445		  1	 0.819, 1.655			    2,1			 150/150	P,L,A,H,G,E,Q,T,K,V,F,R,M,N,S,I,D
  85	   T	   THR85:A	 0.217		  4	-0.052, 0.376			    5,4			 150/150	R,V,K,C,T,F,D,M,Y,S,I,N,A,P,L,Q,E,H,G
  86	   L	   LEU86:A	-1.064		  8	-1.165,-0.985			    9,8			 150/150	V,L,F,Q,M,S,N,G
  87	   Y	   TYR87:A	-0.053		  5	-0.261, 0.071			    6,5			 150/150	A,V,F,L,M,Y,S,I
  88	   S	   SER88:A	-0.566		  7	-0.774,-0.435			    7,6			 150/150	C,T,L,A,M,G,S,N
  89	   G	   GLY89:A	-0.577		  7	-0.774,-0.435			    7,6			 150/150	G,S,V,M,A
  90	   L	   LEU90:A	-0.557		  7	-0.715,-0.435			    7,6			 150/150	M,Y,I,L,F
  91	   Y	   TYR91:A	-1.390		  9	-1.428,-1.365			    9,9			 150/150	Y
  92	   I	   ILE92:A	-0.599		  7	-0.774,-0.512			    7,7			 150/150	A,I,L,M,V
  93	   N	   ASN93:A	-1.385		  9	-1.428,-1.365			    9,9			 150/150	S,N
  94	   E	   GLU94:A	-1.401		  9	-1.428,-1.405			    9,9			 150/150	E
  95	   L	   LEU95:A	-1.337		  9	-1.405,-1.326			    9,9			 150/150	L,V,M
  96	   L	   LEU96:A	-0.594		  7	-0.774,-0.512			    7,7			 150/150	T,C,M,V,S,L,I
  97	   S	   SER97:A	 0.711		  3	 0.376, 0.819			    4,2			 150/150	G,N,S,I,M,Y,Q,E,D,L,C,V,T,A
  98	   R	   ARG98:A	-1.050		  8	-1.165,-0.985			    9,8			 150/150	Y,H,N,A,R,K,V,L
  99	   V	   VAL99:A	-0.670		  7	-0.831,-0.584			    8,7			 150/150	S,I,A,F,L,V,C,T
 100	   L	  LEU100:A	-0.685		  7	-0.831,-0.584			    8,7			 150/150	L,V,C,T,W,A,S,I,M
 101	   E	  GLU101:A	 0.279		  4	-0.052, 0.376			    5,4			 150/150	V,P,L,R,A,Y,H,M,S,G,D,E,Q
 102	   Y	  TYR102:A	-0.119		  5	-0.352,-0.052			    6,5			 150/150	S,I,N,Y,D,F,K,V,T,R,G,H,Q,E,P,A
 103	   E	  GLU103:A	 0.091		  5	-0.163, 0.212			    6,4			 150/150	Q,E,D,G,S,N,H,M,Y,R,A,F,C,W
 104	   T	  THR104:A	-0.746		  7	-0.885,-0.651			    8,7			 150/150	M,H,S,I,D,Q,E,V,T,L,F,A
 105	   R	  ARG105:A	 0.397		  4	 0.071, 0.572			    5,3			 150/150	E,D,G,S,N,H,A,R,V,P,T
 106	   F	  PHE106:A	-0.030		  5	-0.261, 0.071			    6,5			 150/150	A,V,C,F,L,Q,H,M,Y,I,N,S
 107	   S	  SER107:A	-0.123		  5	-0.352, 0.071			    6,5			 150/150	Y,G,N,S,D,Q,E,V,P,T,F,L,R,A
 108	   E	  GLU108:A	 1.292		  1	 0.819, 1.655			    2,1			 150/150	T,V,C,K,R,I,S,N,Y,D,L,P,A,G,H,E,Q
 109	   L	  LEU109:A	-0.626		  7	-0.774,-0.512			    7,7			 150/150	I,F,L,V,M
 110	   F	  PHE110:A	-0.803		  8	-0.936,-0.715			    8,7			 150/150	M,H,Y,F
 111	   F	  PHE111:A	 1.019		  2	 0.572, 1.148			    3,1			 150/150	H,G,Q,E,P,L,A,M,Y,N,S,D,V,K,T,F,R
 112	   D	  ASP112:A	 1.041		  2	 0.572, 1.148			    3,1			 150/150	D,M,Y,I,S,N,R,V,C,T,F,Q,E,H,G,A,P,L
 113	   Y	  TYR113:A	-1.367		  9	-1.428,-1.365			    9,9			 150/150	Y,L
 114	   L	  LEU114:A	 0.373		  4	 0.071, 0.572			    5,3			 150/150	H,G,Q,E,L,A,M,I,S,D,V,K,C,T,F,R
 115	   H	  HIS115:A	 1.343		  1	 0.819, 1.655			    2,1			 150/150	L,A,H,G,E,Q,T,C,V,K,F,R,Y,M,N,I,S,D
 116	   C	  CYS116:A	-0.074		  5	-0.352, 0.071			    6,5			 150/150	H,M,I,S,T,C,V,F,L,A
 117	   I	  ILE117:A	-0.842		  8	-0.985,-0.774			    8,7			 150/150	L,I,M,V,R
 118	   Q	  GLN118:A	 1.254		  1	 0.819, 1.655			    2,1			 150/150	R,F,V,C,K,T,D,N,I,S,M,Y,A,L,P,Q,E,G,H
 119	   S	  SER119:A	 1.130		  1	 0.572, 1.148			    3,1			 150/150	N,S,I,Y,D,T,C,K,R,G,H,E,Q,L,W,A
 120	   L	  LEU120:A	-1.157		  9	-1.247,-1.123			    9,9			 150/150	I,F,L,M
 121	   A	  ALA121:A	-0.164		  6	-0.352,-0.052			    6,5			 150/150	Q,D,G,S,I,N,H,A,R,L,P,C,V,T,W
 122	   G	  GLY122:A	 1.232		  1	 0.819, 1.655			    2,1			 149/150	L,K,V,T,R,A,G,N,S,M,H,Q,E,D
 123	   V	  VAL123:A	 2.982		  1	 1.655, 2.986			    1,1			 148/150	D,S,I,N,Y,R,T,C,K,V,E,Q,G,H,A,L,P
 124	   T	  THR124:A	 2.846		  1	 1.655, 2.986			    1,1			  25/150	A,T,V,P,Q,D,S,G,H
 125	   G	  GLY125:A	 1.746		  1	 1.148, 1.655			    1,1			 147/150	A,R,L,F,T,V,K,P,E,Q,D,S,N,G
 126	   T	  THR126:A	 1.442		  1	 0.819, 1.655			    2,1			 147/150	A,R,P,C,V,T,L,D,Q,E,H,G,S,N
 127	   P	  PRO127:A	 0.943		  2	 0.572, 1.148			    3,1			 145/150	M,H,Y,I,S,D,Q,E,V,P,F,L,R,A
 128	   E	  GLU128:A	-0.619		  7	-0.774,-0.512			    7,7			 150/150	L,K,V,P,A,N,S,I,G,E,Q,D
 129	   P	  PRO129:A	 1.034		  2	 0.572, 1.148			    3,1			 150/150	A,W,P,L,E,Q,H,G,R,T,K,V,F,D,Y,M,N,S,I
 130	   A	  ALA130:A	 0.411		  4	 0.071, 0.572			    5,3			 150/150	Y,H,S,I,G,D,Q,T,P,C,V,L,A
 131	   L	  LEU131:A	-1.368		  9	-1.428,-1.365			    9,9			 150/150	L,M
 132	   R	  ARG132:A	-1.397		  9	-1.428,-1.405			    9,9			 150/150	R
 133	   R	  ARG133:A	 0.411		  4	 0.071, 0.572			    5,3			 150/150	T,C,V,K,F,R,Y,S,I,N,L,A,H,G,E,Q
 134	   F	  PHE134:A	-1.180		  9	-1.287,-1.123			    9,9			 150/150	Y,I,F,L
 135	   E	  GLU135:A	-1.401		  9	-1.428,-1.405			    9,9			 150/150	E
 136	   L	  LEU136:A	 0.077		  5	-0.163, 0.212			    6,4			 150/150	Q,Y,M,H,R,L,F,T,W,C,V,K
 137	   A	  ALA137:A	 0.052		  5	-0.163, 0.212			    6,4			 150/150	T,V,C,K,L,A,R,Y,H,M,N,S,D,E,Q
 138	   L	  LEU138:A	-1.059		  8	-1.165,-0.985			    9,8			 150/150	T,M,F,I,L
 139	   L	  LEU139:A	-1.086		  8	-1.207,-1.033			    9,8			 150/150	M,C,I,F,L,A
 140	   G	  GLY140:A	 0.651		  3	 0.376, 0.819			    4,2			 150/150	A,R,T,V,K,P,E,Q,D,N,S,G,H
 141	   H	  HIS141:A	 0.039		  5	-0.163, 0.212			    6,4			 150/150	G,S,I,M,H,Y,Q,E,D,L,K,C,V,T,A
 142	   L	  LEU142:A	-0.727		  7	-0.885,-0.651			    8,7			 150/150	A,T,M,S,I,L
 143	   G	  GLY143:A	-1.251		  9	-1.365,-1.207			    9,9			 150/150	G,E,D
 144	   Y	  TYR144:A	-0.724		  7	-0.885,-0.651			    8,7			 150/150	Y,S,N,G,A,T,V,L,F
 145	   G	  GLY145:A	-0.194		  6	-0.435,-0.052			    6,5			 150/150	A,L,P,C,T,E,D,G,S,I,M
 146	   V	  VAL146:A	-0.082		  5	-0.352, 0.071			    6,5			 150/150	F,L,V,P,A,I,G,Y,M
 147	   N	  ASN147:A	 0.488		  3	 0.212, 0.572			    4,3			 150/150	T,P,V,L,R,A,Y,M,S,I,N,G,D,E,Q
 148	   F	  PHE148:A	-0.620		  7	-0.774,-0.512			    7,7			 150/150	Y,M,S,W,P,C,F,L
 149	   T	  THR149:A	 1.741		  1	 1.148, 1.655			    1,1			 146/150	M,I,N,S,D,V,C,K,T,F,R,H,G,Q,E,P,L,A
 150	   H	  HIS150:A	 1.818		  1	 1.148, 1.655			    1,1			 146/150	F,T,C,K,V,R,S,N,I,Y,M,D,L,A,G,H,E,Q
 151	   C	  CYS151:A	-0.236		  6	-0.435,-0.163			    6,6			 146/150	D,Q,E,H,S,N,A,P,C,V,T,L
 152	   A	  ALA152:A	 0.231		  4	-0.052, 0.376			    5,4			 142/150	L,W,P,A,G,E,Q,F,T,V,C,R,I,N,S,M
 153	   G	  GLY153:A	 2.392		  1	 1.148, 2.986			    1,1			  82/150	L,K,V,T,R,A,G,S,I,H,M,Y,Q,E,D
 154	   S	  SER154:A	 0.840		  2	 0.376, 1.148			    4,1			 145/150	I,S,N,G,Y,H,E,Q,D,T,C,V,K,R,A
 155	   G	  GLY155:A	-0.040		  5	-0.352, 0.071			    6,5			 146/150	M,H,S,N,G,D,E,Q,K,V,A,R
 156	   E	  GLU156:A	 1.380		  1	 0.819, 1.655			    2,1			 149/150	D,E,Q,M,H,N,S,G,R,A,T,V,C,K,L
 157	   P	  PRO157:A	 1.388		  1	 0.819, 1.655			    2,1			 149/150	D,Q,E,M,G,S,I,R,A,V,P,K,T,L
 158	   V	  VAL158:A	-0.171		  6	-0.352,-0.052			    6,5			 149/150	I,F,L,V,T
 159	   D	  ASP159:A	 2.187		  1	 1.148, 2.986			    1,1			 150/150	H,E,Q,L,A,I,S,Y,M,D,F,T,C,K,V,R
 160	   D	  ASP160:A	 1.037		  2	 0.572, 1.148			    3,1			 150/150	A,T,P,V,C,L,D,E,I,S,G
 161	   T	  THR161:A	 1.491		  1	 0.819, 1.655			    2,1			 150/150	A,R,L,T,K,E,Q,D,S,N,G,H
 162	   M	  MET162:A	 1.360		  1	 0.819, 1.655			    2,1			 150/150	L,A,H,G,Q,E,C,V,K,T,F,R,M,Y,I,N,S,D
 163	   T	  THR163:A	 1.872		  1	 1.148, 1.655			    1,1			 150/150	H,Q,E,L,W,A,I,N,S,M,Y,D,F,C,K,V,T,R
 164	   Y	  TYR164:A	-1.390		  9	-1.428,-1.365			    9,9			 150/150	Y
 165	   R	  ARG165:A	 0.580		  3	 0.212, 0.819			    4,2			 150/150	A,L,Q,E,H,G,R,V,C,K,T,F,D,Y,I,N,S
 166	   Y	  TYR166:A	-0.256		  6	-0.512,-0.163			    7,6			 150/150	C,V,W,Y,L,F
 167	   R	  ARG167:A	 1.115		  1	 0.572, 1.148			    3,1			 150/150	H,G,Q,E,L,A,M,S,I,N,D,V,K,T,F,R
 168	   E	  GLU168:A	 0.450		  4	 0.071, 0.572			    5,3			 150/150	L,F,W,V,P,R,A,S,I,M,H,E,Q
 169	   E	  GLU169:A	-0.543		  7	-0.715,-0.435			    7,6			 150/150	H,G,N,S,D,Q,E,T,A,R
 170	   K	  LYS170:A	 0.594		  3	 0.212, 0.819			    4,2			 150/150	D,M,Y,S,I,R,C,K,V,T,F,Q,E,H,G,A,L
 171	   G	  GLY171:A	-1.161		  9	-1.287,-1.079			    9,8			 150/150	R,G,S,L,C,W
 172	   F	  PHE172:A	 0.075		  5	-0.163, 0.212			    6,4			 150/150	A,T,W,P,V,L,F,Q,M,I
 173	   I	  ILE173:A	 0.121		  5	-0.163, 0.212			    6,4			 150/150	H,Q,E,L,W,A,S,I,M,Y,F,C,K,V,T,R
 174	   A	  ALA174:A	 0.752		  3	 0.376, 0.819			    4,2			 150/150	N,S,G,H,E,F,L,T,K,P,V,R,A
 175	   S	  SER175:A	 0.290		  4	-0.052, 0.376			    5,4			 149/150	T,C,V,K,F,R,Y,I,N,S,D,W,P,L,A,H,G,E,Q
 176	   V	  VAL176:A	 2.837		  1	 1.655, 2.986			    1,1			 147/150	D,M,Y,S,N,I,R,V,T,F,Q,E,H,G,A,P,L
 177	   V	  VAL177:A	 2.879		  1	 1.655, 2.986			    1,1			 126/150	F,T,K,V,R,S,N,I,Y,D,L,P,A,G,H,E,Q
 178	   I	  ILE178:A	 2.984		  1	 1.655, 2.986			    1,1			 131/150	R,T,K,V,F,D,M,S,I,N,A,W,P,L,E,Q,H,G
 179	   D	  ASP179:A	 1.339		  1	 0.819, 1.655			    2,1			 131/150	R,T,V,K,D,M,N,I,S,A,P,L,E,Q,H,G
 180	   N	  ASN180:A	 1.307		  1	 0.819, 1.655			    2,1			 146/150	G,N,S,H,Q,E,D,L,P,K,W,T,R,A
 181	   K	  LYS181:A	 2.285		  1	 1.148, 2.986			    1,1			 146/150	A,L,P,W,Q,E,G,H,R,F,K,C,V,T,D,S,I,N,M,Y
 182	   T	  THR182:A	 1.024		  2	 0.572, 1.148			    3,1			 150/150	Q,E,H,G,A,P,W,L,D,Y,N,S,I,R,K,C,V,T,F
 183	   F	  PHE183:A	-0.136		  5	-0.352,-0.052			    6,5			 150/150	A,T,W,V,F,L,Y,M,I
 184	   T	  THR184:A	 0.622		  3	 0.212, 0.819			    4,2			 150/150	Q,G,H,A,L,P,D,N,I,S,M,Y,R,F,C,K,T
 185	   G	  GLY185:A	-1.341		  9	-1.428,-1.326			    9,9			 150/150	G,A
 186	   R	  ARG186:A	 0.848		  2	 0.376, 1.148			    4,1			 150/150	H,Y,G,S,I,N,D,Q,E,V,C,K,T,R,A
 187	   Q	  GLN187:A	 0.023		  5	-0.261, 0.212			    6,4			 150/150	D,Q,E,H,M,S,N,A,C,V,W,T,L
 188	   L	  LEU188:A	-1.186		  9	-1.287,-1.123			    9,9			 150/150	I,L,M
 189	   K	  LYS189:A	 0.137		  5	-0.163, 0.376			    6,4			 150/150	G,S,I,N,H,Q,L,K,V,T,R,A
 190	   A	  ALA190:A	-0.692		  7	-0.831,-0.584			    8,7			 150/150	D,E,Q,N,I,S,G,A,R,T,C,K,L
 191	   L	  LEU191:A	-0.552		  7	-0.715,-0.435			    7,6			 150/150	F,L,I,W,M,D
 192	   N	  ASN192:A	-0.386		  6	-0.584,-0.261			    7,6			 150/150	F,T,C,V,K,R,N,S,D,L,W,A,G,H,E,Q
 193	   A	  ALA193:A	 1.823		  1	 1.148, 1.655			    1,1			 150/150	N,I,S,Y,D,K,C,V,T,R,G,H,Q,E,L,A
 194	   R	  ARG194:A	 0.442		  4	 0.071, 0.572			    5,3			 150/150	K,F,L,R,A,H,G,S,N,D,Q,E
 195	   E	  GLU195:A	 0.548		  3	 0.212, 0.819			    4,2			 149/150	G,I,S,N,H,Q,E,D,L,V,K,C,T,R,A
 196	   F	  PHE196:A	 0.123		  5	-0.163, 0.212			    6,4			 150/150	Q,E,M,Y,I,A,V,K,P,C,W,T,L,F
 197	   P	  PRO197:A	 1.450		  1	 0.819, 1.655			    2,1			 150/150	R,C,V,T,F,D,Y,S,I,N,A,P,L,Q,E,H,G
 198	   D	  ASP198:A	 0.851		  2	 0.376, 1.148			    4,1			 139/150	A,R,K,V,P,T,F,L,D,Q,E,M,G,N,S
 199	   A	  ALA199:A	 2.666		  1	 1.655, 2.986			    1,1			 149/150	M,G,S,N,I,D,Q,E,P,V,K,T,L,R,A
 200	   D	  ASP200:A	 1.512		  1	 0.819, 1.655			    2,1			 150/150	A,P,L,E,Q,H,G,R,T,V,C,K,D,M,N,S,I
 201	   T	  THR201:A	 0.245		  4	-0.052, 0.376			    5,4			 150/150	A,V,C,T,L,D,Q,E,H,M,Y,G,I,S,N
 202	   L	  LEU202:A	-0.248		  6	-0.435,-0.163			    6,6			 150/150	I,S,M,Y,Q,L,F,K,V,A,R
 203	   R	  ARG203:A	 0.781		  2	 0.376, 0.819			    4,2			 149/150	A,R,T,P,K,V,L,D,Q,Y,M,H,S,N,G
 204	   A	  ALA204:A	-0.426		  6	-0.584,-0.352			    7,6			 150/150	D,Q,E,M,G,I,S,A,R,V,C,P,T,L
 205	   A	  ALA205:A	-0.937		  8	-1.079,-0.885			    8,8			 149/150	A,C,V,W,T,L,F,M,Y,G,S,I
 206	   K	  LYS206:A	-1.147		  9	-1.247,-1.079			    9,8			 149/150	N,K,R
 207	   R	  ARG207:A	-0.071		  5	-0.261, 0.071			    6,5			 149/150	P,V,K,T,L,F,R,A,M,H,Y,G,D,Q
 208	   F	  PHE208:A	-0.919		  8	-1.079,-0.831			    8,8			 149/150	A,F,L,V,P,T,Q,I,M
 209	   T	  THR209:A	-0.757		  7	-0.885,-0.651			    8,7			 149/150	S,N,I,M,L,F,C,V,T,A
 210	   R	  ARG210:A	-1.360		  9	-1.428,-1.326			    9,9			 149/150	T,Q,R
 211	   M	  MET211:A	 0.463		  4	 0.071, 0.572			    5,3			 149/150	E,Q,H,G,A,W,L,D,Y,M,I,S,R,T,K,V,F
 212	   A	  ALA212:A	-0.515		  7	-0.715,-0.435			    7,6			 149/150	S,I,Y,M,R,A,L,T,V
 213	   L	  LEU213:A	-0.862		  8	-0.985,-0.774			    8,7			 149/150	F,I,L,T,M,V
 214	   K	  LYS214:A	-0.013		  5	-0.261, 0.071			    6,5			 149/150	Q,D,S,N,G,H,R,A,L,T,K
 215	   P	  PRO215:A	 0.314		  4	-0.052, 0.572			    5,3			 149/150	D,E,Q,Y,H,S,N,G,A,R,T,V,P,L,F
 216	   Y	  TYR216:A	-0.199		  6	-0.435,-0.052			    6,5			 149/150	Q,H,Y,I,A,R,C,V,T,W,L,F
 217	   L	  LEU217:A	-1.336		  9	-1.405,-1.326			    9,9			 149/150	I,L
 218	   G	  GLY218:A	-0.314		  6	-0.512,-0.163			    7,6			 149/150	D,E,Q,H,S,N,G,A,K
 219	   G	  GLY219:A	 0.811		  2	 0.376, 1.148			    4,1			 149/150	P,K,T,A,H,G,N,S,D,Q,E
 220	   K	  LYS220:A	-0.233		  6	-0.435,-0.163			    6,6			 149/150	Q,E,G,H,A,R,V,K,T
 221	   P	  PRO221:A	-0.372		  6	-0.584,-0.261			    7,6			 149/150	E,Q,M,I,S,G,A,R,T,V,P,K
 222	   L	  LEU222:A	-1.213		  9	-1.326,-1.165			    9,9			 149/150	M,V,L,I
 223	   K	  LYS223:A	-0.160		  6	-0.352,-0.052			    6,5			 149/150	K,V,L,R,A,H,Y,N,I,S,Q,E
 224	   S	  SER224:A	-1.293		  9	-1.365,-1.247			    9,9			 149/150	A,T,S
 225	   R	  ARG225:A	-1.285		  9	-1.365,-1.247			    9,9			 149/150	V,W,T,S,Q,R
 226	   E	  GLU226:A	-0.362		  6	-0.584,-0.261			    7,6			 149/150	N,S,G,D,E,Q,T,K,L,F,R,A
 227	   L	  LEU227:A	-0.926		  8	-1.079,-0.831			    8,8			 148/150	I,L,F,T,M,V
 228	   F	  PHE228:A	-1.046		  8	-1.165,-0.985			    9,8			 145/150	A,M,Y,F,I,L
 229	   R	  ARG229:A	 0.594		  3	 0.212, 0.819			    4,2			 119/150	M,G,S,N,I,Q,E,K,V,T,L,R,A
 230	   Q	  GLN230:A	-0.064		  5	-0.352, 0.071			    6,5			  95/150	Y,H,S,N,G,D,E,Q,T,P,K,A,R
 231	   F	  PHE231:A	 0.916		  2	 0.376, 1.148			    4,1			  62/150	R,A,F,L,P,V,T,Q,G,N,I,M,H,Y
 232	   M	  MET232:A	 2.474		  1	 1.148, 2.986			    1,1			  36/150	M,I,S,Q,P,V,K,T,L,A,R
 233	   P	         -	 1.689		  1	 0.819, 2.986			    2,1			  28/150	S,G,H,E,L,T,V,P,K,R,A
 234	   K	         -	 0.556		  3	-0.052, 0.819			    5,2			  25/150	A,R,P,K,N,S,L
 235	   R	         -	 0.179		  4	-0.352, 0.572			    6,3			  20/150	K,Q,A,R
 236	   T	         -	 1.685		  1	 0.572, 2.986			    3,1			   9/150	N,P,Y,T,R
 237	   V	         -	 1.049		  2*	-0.052, 1.655			    5,1			   3/150	D,A,V
 238	   K	         -	-0.392		  6*	-1.123,-0.052			    9,5			   1/150	K
 239	   T	         -	-0.392		  6*	-1.123,-0.052			    9,5			   1/150	T
 240	   H	         -	-0.392		  6*	-1.165,-0.052			    9,5			   1/150	H
 241	   Y	         -	-0.392		  6*	-1.123, 0.071			    9,5			   1/150	Y
 242	   E	         -	-0.392		  6*	-1.165,-0.052			    9,5			   1/150	E


*Below the confidence cut-off - The calculations for this site were performed on less than 6 non-gaped homologue sequences,
or the confidence interval for the estimated score is equal to- or larger than- 4 color grades.
