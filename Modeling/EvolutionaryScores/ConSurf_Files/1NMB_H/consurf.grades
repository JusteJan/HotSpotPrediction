	 Amino Acid Conservation Scores
	=======================================

- POS: The position of the AA in the SEQRES derived sequence.
- SEQ: The SEQRES derived sequence in one letter code.
- 3LATOM: The ATOM derived sequence in three letter code, including the AA's positions as they appear in the PDB file and the chain identifier.
- SCORE: The normalized conservation scores.
- COLOR: The color scale representing the conservation scores (9 - conserved, 1 - variable).
- CONFIDENCE INTERVAL: When using the bayesian method for calculating rates, a confidence interval is assigned to each of the inferred evolutionary conservation scores.
- CONFIDENCE INTERVAL COLORS: When using the bayesian method for calculating rates. The color scale representing the lower and upper bounds of the confidence interval.
- MSA DATA: The number of aligned sequences having an amino acid (non-gapped) from the overall number of sequences at each position.
- RESIDUE VARIETY: The residues variety at each position of the multiple sequence alignment.

 POS	 SEQ	    3LATOM	SCORE		COLOR	CONFIDENCE INTERVAL	CONFIDENCE INTERVAL COLORS	MSA DATA	RESIDUE VARIETY
    	    	        	(normalized)	        	               
   1	   Q	    GLN1:H	-0.489		  8	-0.559,-0.463			    8,8			  96/150	E,Q
   2	   V	    VAL2:H	-0.602		  9	-0.654,-0.559			    9,8			 113/150	E,V,L,T,M,I
   3	   Q	    GLN3:H	-0.443		  8	-0.515,-0.401			    8,7			 121/150	N,K,T,D,R,I,H,Q,L,V,E
   4	   L	    LEU4:H	-0.669		  9	-0.711,-0.654			    9,9			 124/150	L,P,Q,M
   5	   Q	    GLN5:H	-0.293		  7	-0.401,-0.237			    7,6			 124/150	K,R,M,T,I,H,A,V,E,Q,L
   6	   Q	    GLN6:H	-0.580		  8	-0.654,-0.559			    9,8			 125/150	Q,D,E,S,A
   7	   P	    PRO7:H	-0.587		  8	-0.654,-0.559			    9,8			 128/150	W,T,E,Q,P,L,S,A
   8	   G	    GLY8:H	-0.140		  6	-0.326, 0.009			    7,5			 131/150	E,V,P,Q,A,G,R,D,T,N
   9	   A	    ALA9:H	-0.327		  7	-0.463,-0.237			    8,6			 140/150	T,E,Q,P,A,H,S,G
  10	   E	   GLU10:H	-0.175		  6	-0.326,-0.127			    7,6			 143/150	M,D,R,G,S,I,A,Q,L,V,E
  11	   L	   LEU11:H	-0.150		  6	-0.326,-0.127			    7,6			 146/150	M,T,N,V,F,C,Q,L,I,S,A
  12	   V	   VAL12:H	-0.233		  6	-0.401,-0.127			    7,6			 147/150	K,R,M,T,I,A,G,V,E,F,Q,L
  13	   K	   LYS13:H	-0.310		  7	-0.401,-0.237			    7,6			 148/150	Q,A,H,S,R,T,M,N,K
  14	   P	   PRO14:H	-0.601		  9	-0.676,-0.559			    9,8			 148/150	Y,S,A,P,T
  15	   G	   GLY15:H	-0.502		  8	-0.596,-0.463			    9,8			 148/150	E,R,T,Q,K,S,G
  16	   A	   ALA16:H	-0.138		  6	-0.326,-0.127			    7,6			 148/150	Q,V,E,G,S,H,A,T,D,R,K
  17	   S	   SER17:H	-0.521		  8	-0.596,-0.463			    9,8			 149/150	K,N,T,R,I,S,A,P,L,V,F
  18	   V	   VAL18:H	-0.548		  8	-0.628,-0.515			    9,8			 148/150	T,R,X,L,V,Y,H,I
  19	   R	   ARG19:H	-0.288		  7	-0.401,-0.237			    7,6			 147/150	H,A,S,R,T,M,X,N,K
  20	   M	   MET20:H	-0.368		  7	-0.463,-0.326			    8,7			 150/150	I,M,Q,L,V
  21	   S	   SER21:H	-0.622		  9	-0.676,-0.596			    9,9			 150/150	W,L,T,C,P,S
  22	   C	   CYS22:H	-0.756		  9	-0.764,-0.753			    9,9			 150/150	C
  23	   K	   LYS23:H	-0.074		  5	-0.237, 0.009			    6,5			 150/150	R,M,T,K,N,V,E,Q,P,L,I,S,A,G
  24	   A	   ALA24:H	-0.341		  7	-0.463,-0.326			    8,7			 150/150	W,T,I,S,A,G,V,F,C,L
  25	   S	   SER25:H	-0.680		  9	-0.711,-0.654			    9,9			 150/150	T,P,R,F,Y,S
  26	   G	   GLY26:H	-0.700		  9	-0.742,-0.676			    9,9			 147/150	D,E,G
  27	   Y	   TYR27:H	-0.232		  6	-0.401,-0.127			    7,6			 149/150	D,R,T,V,E,F,P,L,S,I,A,G,Y
  28	   T	   THR28:H	-0.362		  7	-0.463,-0.326			    8,7			 149/150	S,I,A,E,P,L,N,D,M,T
  29	   F	   PHE29:H	-0.216		  6	-0.401,-0.127			    7,6			 149/150	I,S,L,M,F,V,D
  30	   T	   THR30:H	-0.281		  7	-0.401,-0.237			    7,6			 149/150	P,S,I,A,G,D,R,T,N,K
  31	   N	   ASN31:H	 0.243		  4	 0.009, 0.417			    5,3			 150/150	K,N,M,T,D,R,G,Y,S,Q,E
  32	   Y	   TYR32:H	 0.043		  5	-0.127, 0.184			    6,4			 150/150	N,T,R,Y,G,H,A,S,C,Q,E,F
  33	   N	   ASN33:H	 2.919		  1	 1.302, 2.921			    1,1			 150/150	H,C,E,T,D,Y,G,A,S,I,P,F,V,K,N,W,R
  34	   M	   MET34:H	-0.487		  8	-0.559,-0.463			    8,8			 150/150	R,V,W,T,L,M,I
  35	   Y	   TYR35:H	 0.632		  1	 0.184, 0.752			    4,1			 150/150	R,D,T,N,F,E,V,Q,C,A,H,S,I,Y,G
  36	   W	   TRP36:H	-0.669		  9	-0.723,-0.628			    9,9			 150/150	V,F,L,W
  37	   V	   VAL37:H	-0.338		  7	-0.463,-0.326			    8,7			 150/150	W,M,N,L,F,V,Y,I
  38	   K	   LYS38:H	-0.588		  8	-0.654,-0.559			    9,8			 150/150	R,P,C,Q,S,K,H
  39	   Q	   GLN39:H	-0.700		  9	-0.734,-0.676			    9,9			 150/150	R,Q,L,K,H
  40	   S	   SER40:H	-0.189		  6	-0.326,-0.127			    7,6			 150/150	V,F,E,Q,P,I,S,H,A,R,M,T,K
  41	   P	   PRO41:H	-0.340		  7	-0.463,-0.237			    8,6			 150/150	V,E,Q,P,L,S,A,H,T
  42	   G	   GLY42:H	-0.481		  8	-0.596,-0.401			    9,7			 149/150	N,R,D,G,S,Q,E,V
  43	   Q	   GLN43:H	-0.440		  8	-0.515,-0.401			    8,7			 149/150	E,Q,H,S,G,R,N,K
  44	   G	   GLY44:H	 0.004		  5	-0.237, 0.184			    6,4			 149/150	K,T,R,D,G,A,H,S,I,Q,P,E,V
  45	   L	   LEU45:H	-0.529		  8	-0.596,-0.463			    9,8			 149/150	I,F,R,L,P,Q,M
  46	   E	   GLU46:H	-0.680		  9	-0.711,-0.654			    9,9			 150/150	E,D,V,L,Q
  47	   W	   TRP47:H	-0.438		  8	-0.559,-0.401			    8,7			 149/150	Q,L,V,F,G,Y,S,W
  48	   I	   ILE48:H	-0.238		  6	-0.401,-0.237			    7,6			 149/150	V,F,M,W,L,I
  49	   G	   GLY49:H	-0.204		  6	-0.326,-0.127			    7,6			 149/150	V,L,C,A,S,G,T,N
  50	   I	   ILE50:H	 2.921		  1	 1.302, 2.921			    1,1			 148/150	T,M,D,Q,C,E,H,W,R,N,L,F,V,Y,G,A,S,I
  51	   F	   PHE51:H	-0.433		  8	-0.515,-0.401			    8,7			 149/150	F,V,L,A,I,S,Y,G,R,T,M
  52	   Y	   TYR52:H	 0.839		  1	 0.417, 0.752			    3,1			 148/150	V,F,L,S,A,G,Y,R,W,K,N,E,C,H,D,T
  53	   P	  PRO52A:H	 0.687		  1	 0.417, 0.752			    3,1			 145/150	N,R,W,A,I,S,Y,G,F,V,P,D,T,M,H,Q,C
  54	   G	   GLY53:H	 0.879		  1	 0.417, 1.302			    3,1			 146/150	Q,P,V,E,G,Y,S,A,M,T,D,R,K,N
  55	   N	   ASN54:H	 0.413		  3	 0.184, 0.417			    4,3			 145/150	R,D,W,T,K,N,F,H,A,I,S,G
  56	   G	   GLY55:H	 0.203		  4	-0.127, 0.417			    6,3			  99/150	Y,G,A,S,Q,E,N,K,T,R,D
  57	   D	   ASP56:H	 1.256		  1	 0.752, 1.302			    1,1			 150/150	E,V,Q,A,S,I,Y,G,R,D,T,N,K
  58	   T	   THR57:H	-0.105		  6	-0.237, 0.009			    6,5			 150/150	M,T,R,N,K,P,Q,L,V,Y,S,I,A
  59	   S	   SER58:H	 0.698		  1	 0.417, 0.752			    3,1			 150/150	L,V,F,G,Y,I,S,A,W,R,N,K,Q,E,H,M,T,D
  60	   Y	   TYR59:H	-0.560		  8	-0.628,-0.515			    9,8			 150/150	F,L,H,I,S,Y,G,D,N
  61	   N	   ASN60:H	-0.081		  5	-0.237, 0.009			    6,5			 150/150	L,P,Q,V,G,A,H,I,S,T,D,N,K
  62	   Q	   GLN61:H	 0.163		  4	-0.127, 0.184			    6,4			 150/150	V,E,P,Q,L,S,A,G,D,R,T,N,K
  63	   K	   LYS62:H	-0.497		  8	-0.559,-0.463			    8,8			 150/150	T,W,M,R,K,L,Q,G,A,S
  64	   F	   PHE63:H	-0.467		  8	-0.559,-0.401			    8,7			 150/150	I,A,M,L,V,F
  65	   K	   LYS64:H	-0.364		  7	-0.463,-0.326			    8,7			 150/150	S,E,P,Q,N,K,D,R,T
  66	   D	   ASP65:H	-0.255		  7	-0.401,-0.237			    7,6			 150/150	T,R,D,G,N,K,S
  67	   K	   LYS66:H	-0.562		  8	-0.628,-0.515			    9,8			 150/150	G,K,H,Q,R
  68	   A	   ALA67:H	-0.300		  7	-0.401,-0.237			    7,6			 149/150	V,F,L,I,S,A,T,X
  69	   T	   THR68:H	-0.328		  7	-0.463,-0.237			    8,6			 150/150	H,A,I,S,Q,F,E,V,N,K,T,M
  70	   L	   LEU69:H	-0.480		  8	-0.559,-0.463			    8,8			 150/150	M,T,L,V,F,I,A
  71	   T	   THR70:H	-0.526		  8	-0.596,-0.515			    9,8			 149/150	L,C,A,S,R,D,T,N,X
  72	   A	   ALA71:H	 0.011		  5	-0.237, 0.009			    6,5			 150/150	K,R,M,W,T,S,I,A,G,V,E,P,Q,L
  73	   D	   ASP72:H	-0.631		  9	-0.676,-0.596			    9,9			 149/150	N,G,E,D,Q
  74	   K	   LYS73:H	-0.328		  7	-0.463,-0.237			    8,6			 149/150	K,N,D,M,T,I,S,A,V,E,Q,L
  75	   S	   SER74:H	-0.487		  8	-0.559,-0.463			    8,8			 150/150	P,V,Y,G,A,I,S,T,N
  76	   S	   SER75:H	-0.254		  7	-0.401,-0.237			    7,6			 147/150	V,E,Q,L,S,I,A,R,M,T,N,K
  77	   N	   ASN76:H	-0.426		  8	-0.515,-0.401			    8,7			 147/150	Q,E,A,S,T,R,D,K,N
  78	   T	   THR77:H	-0.373		  7	-0.463,-0.326			    8,7			 150/150	A,S,I,E,V,L,Q,N,R,W,T,M
  79	   A	   ALA78:H	-0.293		  7	-0.401,-0.237			    7,6			 150/150	T,M,Y,G,A,I,L,Q,F,V
  80	   Y	   TYR79:H	-0.266		  7	-0.401,-0.237			    7,6			 150/150	N,D,T,S,I,H,Y,V,F,L
  81	   M	   MET80:H	-0.638		  9	-0.695,-0.596			    9,9			 150/150	F,L,M,I
  82	   Q	   GLN81:H	-0.368		  7	-0.463,-0.326			    8,7			 150/150	T,R,D,N,K,L,Q,E,H
  83	   L	   LEU82:H	-0.476		  8	-0.559,-0.463			    8,8			 150/150	I,G,F,V,W,L,M
  84	   S	  SER82A:H	-0.283		  7	-0.401,-0.237			    7,6			 149/150	K,N,X,D,R,T,I,S,A,Q
  85	   S	  SER82B:H	-0.397		  7	-0.515,-0.326			    8,7			 150/150	G,A,I,S,Q,N,K,T,R
  86	   L	  LEU82C:H	-0.384		  7	-0.515,-0.326			    8,7			 150/150	V,L,M,P,A,S
  87	   T	   THR83:H	-0.260		  7	-0.401,-0.237			    7,6			 150/150	G,A,S,Q,P,E,N,K,T,R,D
  88	   S	   SER84:H	-0.062		  5	-0.237, 0.009			    6,5			 149/150	S,A,Q,P,L,V,E,F,X,T,D
  89	   E	   GLU85:H	-0.521		  8	-0.596,-0.463			    9,8			 149/150	D,R,K,V,E,S,A,G
  90	   D	   ASP86:H	-0.704		  9	-0.734,-0.695			    9,9			 149/150	D,E,N,G
  91	   S	   SER87:H	-0.533		  8	-0.596,-0.515			    9,8			 149/150	F,T,M,A,S
  92	   A	   ALA88:H	-0.684		  9	-0.723,-0.676			    9,9			 149/150	T,V,G,A,S
  93	   V	   VAL89:H	-0.125		  6	-0.326, 0.009			    7,5			 148/150	R,M,T,I,H,V,E,F,L
  94	   Y	   TYR90:H	-0.701		  9	-0.734,-0.676			    9,9			 148/150	C,F,Y
  95	   Y	   TYR91:H	-0.468		  8	-0.559,-0.401			    8,7			 148/150	T,F,Y
  96	   C	   CYS92:H	-0.700		  9	-0.742,-0.676			    9,9			 148/150	F,L,C
  97	   A	   ALA93:H	-0.435		  8	-0.515,-0.401			    8,7			 147/150	V,T,N,I,A,G,Y
  98	   R	   ARG94:H	-0.266		  7	-0.401,-0.237			    7,6			 142/150	N,K,D,R,T,S,A,Y,E,L
  99	   S	   SER95:H	 1.412		  1	 0.752, 1.302			    1,1			 109/150	D,T,H,E,Q,C,N,K,R,W,A,S,I,Y,G,F,V,L,P
 100	   G	   GLY96:H	 2.919		  1	 1.302, 2.921			    1,1			  90/150	A,I,S,Y,G,F,V,L,P,K,N,R,W,H,E,Q,D,T
 101	   G	   GLY97:H	 2.921		  1	 1.302, 2.921			    1,1			  83/150	T,D,H,Q,E,N,K,W,R,Y,G,A,S,I,L,P,F,V
 102	   S	   SER98:H	 2.920		  1	 1.302, 2.921			    1,1			  78/150	V,E,C,P,Q,L,S,H,G,Y,D,R,W,T,N
 103	   Y	   TYR99:H	 2.921		  1	 1.302, 2.921			    1,1			  69/150	T,M,D,H,Q,C,E,N,W,R,Y,G,A,I,S,L,P,F,V
 104	   R	  ARG100:H	 2.921		  1	 1.302, 2.921			    1,1			  65/150	R,W,K,N,V,F,P,L,S,A,G,Y,D,M,T,E,C,Q
 105	   Y	 TYR100A:H	 2.814		  1	 1.302, 2.921			    1,1			  48/150	Y,G,H,A,I,S,L,Q,P,N,T,R,D
 106	   D	 ASP100B:H	 2.692		  1	 1.302, 2.921			    1,1			  32/150	S,A,G,Y,V,F,E,P,K,N,D,T
 107	   G	 GLY100C:H	 2.915		  1	 1.302, 2.921			    1,1			  49/150	M,W,D,R,N,K,Q,L,V,F,G,Y,I,A
 108	   G	 GLY100D:H	 2.920		  1	 1.302, 2.921			    1,1			  65/150	Y,G,A,S,I,L,P,V,N,W,R,H,C,Q,E,T,D
 109	   F	 PHE100E:H	 0.687		  1	 0.184, 0.752			    4,1			  72/150	Y,A,S,I,L,Q,P,F,V,W,M,R
 110	   D	  ASP101:H	 0.205		  4	-0.127, 0.417			    6,3			  72/150	N,D,R,T,W,I,S,A,H,G,Y,V,P,Q,L
 111	   Y	  TYR102:H	 0.705		  1	 0.184, 0.752			    4,1			  72/150	K,N,R,A,H,I,Y,F,V,L,P
 112	   W	  TRP103:H	-0.696		  9	-0.748,-0.676			    9,9			  71/150	Y,X,W
 113	   G	  GLY104:H	-0.592		  9	-0.676,-0.559			    9,8			  72/150	S,G,T,L
 114	   Q	  GLN105:H	-0.552		  8	-0.628,-0.515			    9,8			  72/150	R,Q,P,K,A,G
 115	   G	  GLY106:H	-0.699		  9	-0.748,-0.676			    9,9			  71/150	R,G
 116	   T	  THR107:H	-0.497		  8	-0.596,-0.463			    9,8			  70/150	A,I,N,S,T,V
 117	   T	  THR108:H	 0.215		  4	-0.127, 0.417			    6,3			  68/150	T,L,Q,M,A,S
 118	   L	  LEU109:H	-0.600		  9	-0.676,-0.559			    9,8			  68/150	Y,V,L,T
 119	   T	  THR110:H	-0.688		  9	-0.734,-0.676			    9,9			  67/150	X,I,S,V,T
 120	   V	  VAL111:H	-0.705		  9	-0.742,-0.676			    9,9			  67/150	V,I,N
 121	   S	  SER112:H	-0.717		  9	-0.748,-0.695			    9,9			  66/150	T,S
 122	   S	  SER113:H	-0.757		  9	-0.764,-0.757			    9,9			  59/150	S


*Below the confidence cut-off - The calculations for this site were performed on less than 6 non-gaped homologue sequences,
or the confidence interval for the estimated score is equal to- or larger than- 4 color grades.
