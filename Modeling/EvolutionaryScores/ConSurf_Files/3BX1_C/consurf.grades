	 Amino Acid Conservation Scores
	=======================================

- POS: The position of the AA in the SEQRES derived sequence.
- SEQ: The SEQRES derived sequence in one letter code.
- 3LATOM: The ATOM derived sequence in three letter code, including the AA's positions as they appear in the PDB file and the chain identifier.
- SCORE: The normalized conservation scores.
- COLOR: The color scale representing the conservation scores (9 - conserved, 1 - variable).
- CONFIDENCE INTERVAL: When using the bayesian method for calculating rates, a confidence interval is assigned to each of the inferred evolutionary conservation scores.
- CONFIDENCE INTERVAL COLORS: When using the bayesian method for calculating rates. The color scale representing the lower and upper bounds of the confidence interval.
- MSA DATA: The number of aligned sequences having an amino acid (non-gapped) from the overall number of sequences at each position.
- RESIDUE VARIETY: The residues variety at each position of the multiple sequence alignment.

 POS	 SEQ	    3LATOM	SCORE		COLOR	CONFIDENCE INTERVAL	CONFIDENCE INTERVAL COLORS	MSA DATA	RESIDUE VARIETY
    	    	        	(normalized)	        	               
   1	   A	    ALA1:C	-1.076		  8	-1.419,-0.867			    9,7			  12/119	S,A
   2	   D	    ASP2:C	 0.568		  3	-0.007, 0.968			    5,2			  39/119	E,L,Q,N,D,A,S,P
   3	   P	    PRO3:C	-0.029		  5	-0.459, 0.232			    6,4			  58/119	T,S,P,E,Q,L,N,G
   4	   P	    PRO4:C	 0.676		  3	 0.232, 0.968			    4,2			  84/119	N,K,E,Q,L,T,A,D,S,P
   5	   P	    PRO5:C	-0.143		  5	-0.459, 0.105			    6,5			  89/119	S,A,T,K,P,E,Q
   6	   V	    VAL6:C	-1.567		  9	-1.694,-1.596			    9,9			 110/119	V,A
   7	   H	    HIS7:C	-0.163		  5	-0.459,-0.007			    6,5			 113/119	Y,H,R,V,W,F,I,L
   8	   D	    ASP8:C	-1.561		  9	-1.694,-1.495			    9,9			 113/119	D,G
   9	   T	    THR9:C	-0.178		  6	-0.459,-0.007			    6,5			 113/119	L,I,M,V,R,S,T,A
  10	   D	   ASP10:C	 0.159		  5	-0.207, 0.373			    6,4			 113/119	E,Q,L,K,N,G,R,S,A,D,T
  11	   G	   GLY11:C	-1.113		  8	-1.351,-0.988			    9,8			 113/119	R,N,G
  12	   H	   HIS12:C	 0.328		  4	-0.007, 0.537			    5,3			 113/119	D,S,H,R,N,K,E,Q
  13	   E	   GLU13:C	-0.026		  5	-0.296, 0.232			    6,4			 114/119	I,N,K,E,Q,L,T,D,A,S,P,V
  14	   L	   LEU14:C	-0.278		  6	-0.605,-0.112			    7,5			 115/119	L,V,M,I
  15	   R	   ARG15:C	 0.275		  4	-0.112, 0.537			    5,3			 115/119	S,H,T,A,R,M,V,K,N,I,L,Q,E
  16	   A	   ALA16:C	 0.150		  5	-0.207, 0.373			    6,4			 115/119	R,V,P,S,T,A,Q,N
  17	   D	   ASP17:C	 0.180		  4	-0.207, 0.373			    6,4			 115/119	R,Y,T,A,D,S,Q,E,G,N,K
  18	   A	   ALA18:C	 0.692		  3	 0.232, 0.968			    4,2			 115/119	V,R,T,A,S,H,Y,E,Q,L,I,G,K
  19	   N	   ASN19:C	 0.041		  5	-0.296, 0.232			    6,4			 116/119	F,N,G,K,E,Q,T,D,S,P,R
  20	   Y	   TYR20:C	-1.478		  9	-1.694,-1.419			    9,9			 117/119	H,Y,C
  21	   Y	   TYR21:C	-0.846		  7	-1.047,-0.673			    8,7			 117/119	Y,N,F,H,I,L,W
  22	   V	   VAL22:C	-0.898		  8	-1.107,-0.804			    8,7			 117/119	V,M,I,A
  23	   L	   LEU23:C	-0.547		  7	-0.804,-0.379			    7,6			 117/119	Y,T,D,S,R,V,N,K,I,L,E
  24	   S	   SER24:C	-1.135		  8	-1.351,-1.047			    9,8			 117/119	P,S,A,T
  25	   A	   ALA25:C	-0.483		  6	-0.740,-0.296			    7,6			 117/119	G,E,L,T,A,S,V,R
  26	   N	   ASN26:C	 0.714		  3	 0.232, 0.968			    4,2			 116/119	Q,L,E,N,F,K,I,V,P,Y,D,T,A,S
  27	   R	   ARG27:C	-0.122		  5	-0.379, 0.105			    6,5			 116/119	K,N,F,Q,C,P,H,S,A,T,D,R,W
  28	   A	   ALA28:C	-0.090		  5	-0.379, 0.105			    6,5			 118/119	N,G,E,Q,S,A,D,P,R
  29	   H	   HIS29:C	 0.760		  3	 0.373, 0.968			    4,2			 117/119	R,M,V,H,S,T,D,A,L,Q,K,G,F,N,I
  30	   G	   GLY30:C	-0.658		  7	-0.928,-0.459			    8,6			 118/119	R,L,E,G,T,A
  31	   G	   GLY31:C	-1.390		  9	-1.596,-1.287			    9,9			 118/119	R,S,G
  32	   G	   GLY32:C	-1.057		  8	-1.287,-0.928			    9,8			 118/119	R,S,P,N,G,Y
  33	   L	   LEU33:C	-1.213		  8	-1.419,-1.107			    9,8			 118/119	L,V,F,I,A
  34	   T	   THR34:C	-0.546		  7	-0.804,-0.379			    7,6			 118/119	R,Y,T,D,A,S,G,K,I
  35	   M	   MET35:C	-0.146		  5	-0.459, 0.105			    6,5			 118/119	A,P,V,M,I,N,K,L
  36	   A	   ALA36:C	-0.331		  6	-0.605,-0.207			    7,6			 118/119	R,V,Y,D,A,T,S,L,E,G,N,F,K,I
  37	   P	   PRO37:C	 0.480		  4	 0.105, 0.730			    5,3			 118/119	N,G,Q,L,Y,P,S,H,T,D,A,R,M,W
  38	   G	   GLY38:C	 1.361		  1	 0.373, 1.759			    4,1			  10/119	G,S,I,A,T
  39	   H	   HIS39:C	-0.004		  5	-0.296, 0.232			    6,4			 115/119	L,Q,E,G,N,K,R,D,T,A,H,S
  40	   G	   GLY40:C	 2.635		  1	 1.284, 2.975			    1,1			 118/119	S,H,A,T,D,P,W,V,R,K,N,F,G,E,Q
  41	   R	   ARG41:C	 1.254		  1	 0.373, 1.759			    4,1			  21/119	Q,L,R,A,D,I,F
  42	   H	   HIS42:C	 0.596		  3	 0.232, 0.730			    4,3			 118/119	K,N,E,Q,H,A,T,D,Y,P,M,V,R,I,G,L,S
  43	   C	   CYS43:C	-1.377		  9	-1.596,-1.287			    9,9			 118/119	L,C,V
  44	   P	   PRO44:C	-1.213		  8	-1.419,-1.107			    9,8			 118/119	Q,L,A,N,P
  45	   L	   LEU45:C	 0.009		  5	-0.296, 0.232			    6,4			 118/119	L,E,N,F,K,R,V,M,Y,D,A,H,S
  46	   F	   PHE46:C	-0.065		  5	-0.379, 0.105			    6,5			 118/119	L,F,N,I,V,Y,T,D,A,S,H
  47	   V	   VAL47:C	-1.584		  9	-1.694,-1.596			    9,9			 118/119	V
  48	   S	   SER48:C	-0.551		  7	-0.804,-0.379			    7,6			 118/119	V,M,A,T,S,Y,L,I,F,G
  49	   Q	   GLN49:C	-1.287		  9	-1.419,-1.226			    9,8			 118/119	V,E,L,Q,A,K
  50	   D	   ASP50:C	-0.551		  7	-0.804,-0.379			    7,6			 116/119	P,D,A,H,S,R,V,M,N,K,Q,E
  51	   P	   PRO51:C	 1.462		  1	 0.968, 1.759			    2,1			 116/119	P,H,S,D,A,T,R,K,N,G,I,L,Q,E
  52	   N	   ASN52:C	 2.717		  1	 1.759, 2.975			    1,1			 116/119	I,F,N,G,Q,L,H,S,A,T,D,P,V,R
  53	   G	   GLY53:C	-0.074		  5	-0.379, 0.105			    6,5			 114/119	E,L,Q,I,N,F,G,K,W,R,T,A,D,S,P
  54	   Q	   GLN54:C	 1.090		  2	 0.537, 1.284			    3,1			 109/119	V,R,S,H,A,T,Y,E,L,Q,I,G,F
  55	   H	   HIS55:C	 0.315		  4	-0.007, 0.537			    5,3			 118/119	L,Q,E,F,N,K,R,V,P,Y,A,D,H,S
  56	   D	   ASP56:C	 1.315		  1	 0.730, 1.759			    3,1			 118/119	Q,E,N,K,R,V,M,P,Y,A,D,T,H,L,F,I,S
  57	   G	   GLY57:C	-1.207		  8	-1.419,-1.107			    9,8			 118/119	K,N,G,S,D
  58	   F	   PHE58:C	 0.852		  3	 0.373, 1.284			    4,1			 118/119	H,S,T,D,Y,M,V,R,I,F,E,Q,L
  59	   P	   PRO59:C	-0.726		  7	-0.988,-0.533			    8,7			 118/119	R,S,A,T,K,P
  60	   V	   VAL60:C	-0.656		  7	-0.867,-0.533			    7,7			 118/119	I,L,V,M
  61	   R	   ARG61:C	-0.130		  5	-0.379,-0.007			    6,5			 118/119	S,A,T,Y,M,V,R,I,K,G,F,Q
  62	   I	   ILE62:C	-0.825		  7	-1.047,-0.673			    8,7			 118/119	L,I,F
  63	   T	   THR63:C	 0.205		  4	-0.112, 0.373			    5,4			 118/119	T,A,S,H,Y,V,M,W,R,I,F,K,E,L,Q
  64	   P	   PRO64:C	-0.876		  7	-1.107,-0.740			    8,7			 118/119	A,T,S,N,P,M
  65	   Y	   TYR65:C	 0.424		  4	-0.007, 0.730			    5,3			 118/119	R,W,V,Y,S,H,A,T,L,Q,E,F,I
  66	   G	   GLY66:C	 0.748		  3*	-0.007, 1.284			    5,1			  21/119	N,G,E,Q,H,A,P,V
  67	   V	   VAL67:C	 0.452		  4	 0.105, 0.730			    5,3			 115/119	K,G,N,I,Q,E,Y,P,D,T,A,R,V
  68	   A	   ALA68:C	 0.244		  4	-0.379, 0.730			    6,3			  17/119	S,H,A,I,Y,G
  69	   P	   PRO69:C	 1.592		  1	 0.968, 1.759			    2,1			 117/119	F,G,K,I,L,Q,E,P,Y,A,D,T,S,R,V
  70	   S	   SER70:C	 0.739		  3	 0.373, 0.968			    4,2			 118/119	T,D,A,S,R,N,G,K,L,Q,E
  71	   D	   ASP71:C	 0.075		  5	-0.207, 0.232			    6,4			 118/119	P,S,T,A,D,K,G,N,I,Q,E
  72	   K	   LYS72:C	 0.838		  3*	-0.007, 1.284			    5,1			  14/119	K,T,A,R,L
  73	   I	   ILE73:C	 0.223		  4	-0.112, 0.373			    5,4			 119/119	M,V,R,T,A,Y,P,L,I,K,N,F
  74	   I	   ILE74:C	-0.412		  6	-0.673,-0.296			    7,6			 119/119	T,A,I,V
  75	   R	   ARG75:C	-0.263		  6	-0.533,-0.112			    7,5			 119/119	L,Q,E,K,N,G,F,R,Y,P,H,S,T,D
  76	   L	   LEU76:C	-0.193		  6	-0.459,-0.007			    6,5			 119/119	T,A,R,V,M,I,L,Q,E
  77	   S	   SER77:C	-0.969		  8	-1.166,-0.867			    8,7			 119/119	N,F,G,P,S,A,T,D
  78	   T	   THR78:C	-0.612		  7	-0.804,-0.459			    7,6			 119/119	Y,T,A,H,S,R,V,M,W,G,F,I,E
  79	   D	   ASP79:C	-1.215		  8	-1.419,-1.107			    9,8			 119/119	E,R,D,A,S,N
  80	   V	   VAL80:C	 0.548		  3	 0.105, 0.730			    5,3			 119/119	F,I,L,Q,H,T,A,M,V
  81	   R	   ARG81:C	-0.954		  8	-1.107,-0.867			    8,7			 119/119	W,R,S,A,T,Q,I,K,G,N
  82	   I	   ILE82:C	-0.788		  7	-0.988,-0.673			    8,7			 118/119	A,I,F,V
  83	   S	   SER83:C	 0.469		  4	 0.105, 0.730			    5,3			 119/119	E,Q,I,G,K,V,M,W,R,A,T,H,S,Y
  84	   F	   PHE84:C	-1.471		  9	-1.694,-1.419			    9,9			 119/119	T,F,V
  85	   R	   ARG85:C	 0.539		  3	 0.105, 0.730			    5,3			 119/119	I,G,L,S,N,K,E,Q,A,T,D,H,P,Y,V,R
  86	   A	   ALA86:C	-0.010		  5	-0.296, 0.232			    6,4			 119/119	F,N,G,I,L,E,Y,P,T,A,D,V
  87	   Y	   TYR87:C	 2.763		  1	 1.759, 2.975			    1,1			 119/119	I,K,F,N,G,E,L,S,A,D,T,Y,P,V,R
  88	   T	   THR88:C	-0.523		  6	-0.740,-0.379			    7,6			 119/119	N,K,T,A,D,S,P,R
  89	   T	   THR89:C	 0.416		  4	 0.105, 0.730			    5,3			 119/119	S,A,T,D,Y,M,V,I,K,N,F,C,L
  90	   C	   CYS90:C	-1.176		  8	-1.419,-1.047			    9,8			 119/119	R,L,C,I,S
  91	   L	   LEU91:C	 1.145		  2	 0.537, 1.284			    3,1			 118/119	I,K,N,G,Q,L,S,T,A,P,V,R
  92	   Q	   GLN92:C	-0.963		  8	-1.166,-0.867			    8,7			 119/119	R,S,D,T,A,E,Q,K,N,G
  93	   S	   SER93:C	-0.670		  7	-0.867,-0.533			    7,7			 119/119	Q,L,K,G,N,V,R,S,A,T,P
  94	   T	   THR94:C	-0.854		  7	-1.047,-0.740			    8,7			 119/119	G,N,K,L,Q,A,T,H,S,V,M
  95	   E	   GLU95:C	-0.363		  6	-0.605,-0.207			    7,6			 119/119	K,F,G,E,L,Q,A,D,T,Y,P,M,V
  96	   W	   TRP96:C	-1.451		  9	-1.694,-1.351			    9,9			 119/119	I,W
  97	   H	   HIS97:C	-0.154		  5	-0.459,-0.007			    6,5			 119/119	Y,H,S,A,T,R,M,K,N,G,Q,E
  98	   I	   ILE98:C	-0.184		  6	-0.459,-0.007			    6,5			 119/119	V,M,L,R,I,A,F
  99	   D	   ASP99:C	 0.209		  4	-0.112, 0.373			    5,4			 111/119	K,G,Q,E,P,S,A,D,T
 100	   S	  SER100:C	 1.054		  2	 0.537, 1.284			    3,1			 107/119	K,F,G,N,Q,E,Y,P,S,H,D,T,A,V
 101	   E	  GLU101:C	-0.314		  6	-0.605,-0.112			    7,5			 113/119	D,A,S,P,V,M,N,K,E
 102	   L	  LEU102:C	 1.593		  1	 0.968, 1.759			    2,1			 108/119	V,M,D,A,T,H,P,Y,E,Q,N,K,S,L,I,G,F
 103	   A	  ALA103:C	 0.765		  3	 0.373, 0.968			    4,2			 109/119	F,G,K,I,L,Q,E,P,Y,T,D,A,S,R,V
 104	   A	  ALA104:C	 0.256		  4	-0.112, 0.537			    5,3			 111/119	A,T,S,P,V,W,M,R,F,N,G,K,E,L
 105	   G	  GLY105:C	 0.332		  4	-0.112, 0.537			    5,3			 118/119	I,G,N,K,Q,T,D,A,S,P,V,R
 106	   R	  ARG106:C	-0.314		  6	-0.605,-0.112			    7,5			 116/119	P,S,T,A,R,V,K,G,I,Q,L,E
 107	   R	  ARG107:C	 0.196		  4	-0.112, 0.373			    5,4			 118/119	M,W,R,T,D,S,P,Y,Q,L,G,F,K
 108	   H	  HIS108:C	 0.349		  4	-0.007, 0.537			    5,3			 118/119	Q,L,N,F,I,R,W,V,Y,P,H,S
 109	   V	  VAL109:C	-0.877		  7	-1.047,-0.740			    8,7			 118/119	L,V,C,F,I
 110	   I	  ILE110:C	-0.085		  5	-0.379, 0.105			    6,5			 118/119	I,K,N,G,L,S,A,T,V
 111	   T	  THR111:C	-0.180		  6	-0.459,-0.007			    6,5			 118/119	L,F,G,I,V,P,T,A,S
 112	   G	  GLY112:C	-0.385		  6	-0.673,-0.207			    7,6			 117/119	N,G,E,S,H,D,A,P,R
 113	   P	  PRO113:C	-0.526		  6	-0.804,-0.296			    7,6			  99/119	R,W,V,P,S,A,E,K,N,G
 114	   V	  VAL114:C	-0.573		  7	-0.804,-0.379			    7,6			  99/119	L,E,I,V,M,Y,D,A,T,S
 115	   K	  LYS115:C	 1.458		  1	 0.730, 1.759			    3,1			  97/119	S,A,D,T,R,W,V,K,F,I,L,Q,E
 116	   D	  ASP116:C	-0.160		  5	-0.459,-0.007			    6,5			 111/119	P,S,H,T,D,A,R,K,N,G,L,E
 117	   P	  PRO117:C	-0.322		  6	-0.605,-0.112			    7,5			 108/119	K,I,Q,L,E,P,S,A,D,R
 118	   S	  SER118:C	-0.313		  6	-0.605,-0.112			    7,5			 107/119	M,V,R,S,D,T,Y,P,I,G
 119	   P	  PRO119:C	 2.911		  1	 1.759, 2.975			    1,1			 101/119	D,A,T,H,P,Y,V,M,R,K,Q,S,W,I,G,F,L
 120	   S	  SER120:C	 1.796		  1	 0.968, 1.759			    2,1			 102/119	Y,P,H,S,T,A,D,R,K,G,N,Q,L,E
 121	   G	  GLY121:C	-1.191		  8	-1.351,-1.107			    9,8			 101/119	R,G,P,K,T,S
 122	   R	  ARG122:C	 1.001		  2	 0.537, 1.284			    3,1			 102/119	L,I,N,G,F,V,M,R,T,A,S,P
 123	   E	  GLU123:C	 0.322		  4	-0.007, 0.537			    5,3			 104/119	K,N,F,G,E,C,L,S,H,T,A,D,P,M,R
 124	   N	  ASN124:C	-0.798		  7	-0.988,-0.673			    8,7			 104/119	K,G,N,F,L,C,S,A,D,R,W,M
 125	   A	  ALA125:C	-0.625		  7	-0.928,-0.459			    8,6			 105/119	H,A,Y,W,V,F,E,L,Q
 126	   F	  PHE126:C	-1.554		  9	-1.694,-1.495			    9,9			 105/119	F
 127	   R	  ARG127:C	-0.797		  7	-0.988,-0.673			    8,7			 105/119	M,R,H,T,A,Q,K,N
 128	   I	  ILE128:C	-1.310		  9	-1.495,-1.226			    9,8			 105/119	F,I,V
 129	   E	  GLU129:C	-1.146		  8	-1.351,-1.047			    9,8			 104/119	D,A,G,V,E,Q,R
 130	   K	  LYS130:C	-0.584		  7	-0.804,-0.459			    7,6			 100/119	H,S,A,T,D,R,K,N,G,E
 131	   Y	  TYR131:C	 2.167		  1	 1.284, 2.975			    1,1			  98/119	E,L,Q,G,F,N,V,M,T,A,D,S,H,Y
 132	   S	  SER132:C	 2.759		  1	 1.759, 2.975			    1,1			 100/119	R,P,S,A,T,D,Q,L,E,K,N,G,F,I
 133	   G	  GLY133:C	 2.707		  1	 1.759, 2.975			    1,1			  98/119	S,H,D,T,A,Y,P,V,R,K,G,N,E
 134	   A	  ALA134:C	 1.359		  1	 0.730, 1.759			    3,1			  91/119	V,R,S,H,T,D,A,P,E,L,I,G,F,N
 135	   E	  GLU135:C	 0.927		  2*	 0.105, 1.759			    5,1			  10/119	M,E,D,A,N,G
 136	   V	  VAL136:C	 0.565		  3	-0.007, 0.968			    5,2			  27/119	V,M,A,D,T,S,P,L,G,N,K
 137	   H	  HIS137:C	 0.208		  4	-0.207, 0.537			    6,3			  30/119	G,N,K,E,Q,D,T,S,H,Y,R
 138	   E	  GLU138:C	 0.730		  3	 0.232, 0.968			    4,2			  56/119	I,K,G,E,S,T,A,D,W,V,R
 139	   Y	  TYR139:C	-1.500		  9	-1.694,-1.419			    9,9			  96/119	N,Y
 140	   K	  LYS140:C	-0.975		  8	-1.166,-0.867			    8,7			  96/119	I,K,N,G,E,Q,T,A,V,R
 141	   L	  LEU141:C	-0.272		  6	-0.605,-0.112			    7,5			  95/119	F,I,L,V
 142	   M	  MET142:C	-0.308		  6	-0.605,-0.112			    7,5			  94/119	S,T,A,M,V,R,I,K,E,L,Q
 143	   S	  SER143:C	-0.074		  5	-0.379, 0.105			    6,5			  94/119	W,F,Y,A,H,S
 144	   C	  CYS144:C	-1.496		  9	-1.694,-1.419			    9,9			  93/119	C
 145	   G	  GLY145:C	 0.541		  3	 0.105, 0.730			    5,3			  65/119	Q,E,K,G,N,I,R,M,Y,P,S,T
 146	   D	  ASP146:C	 0.819		  3	 0.232, 1.284			    4,1			  64/119	V,S,H,D,A,T,Y,P,E,I,K,G
 147	   W	  TRP147:C	 0.799		  3	 0.232, 1.284			    4,1			  64/119	P,A,S,R,V,W,M,N,F,I,Q,L,E
 148	   C	  CYS148:C	-1.456		  9	-1.694,-1.351			    9,9			  63/119	C
 149	   Q	  GLN149:C	-0.064		  5	-0.459, 0.232			    6,4			  63/119	E,Q,K,G,N,R,S,A
 150	   D	  ASP150:C	-0.808		  7	-1.047,-0.673			    8,7			  63/119	E,S,D,A,Y,N
 151	   L	  LEU151:C	 0.266		  4	-0.112, 0.537			    5,3			  63/119	V,M,L,I
 152	   G	  GLY152:C	-1.458		  9	-1.694,-1.351			    9,9			  63/119	G
 153	   V	  VAL153:C	-0.123		  5	-0.459, 0.105			    6,5			  63/119	L,F,I,R,V,Y,S,A
 154	   F	  PHE154:C	 0.861		  3	 0.373, 1.284			    4,1			  63/119	F,L,S,H,T,A,Y,V
 155	   R	  ARG155:C	 1.279		  1	 0.730, 1.759			    3,1			  60/119	R,M,V,Y,S,T,A,D,L,Q,E,N,F,I
 156	   D	  ASP156:C	-0.102		  5	-0.459, 0.105			    6,5			  59/119	Q,E,G,F,N,K,V,Y,A,D,S,H
 157	   L	  LEU157:C	 1.816		  1	 0.968, 2.975			    2,1			  38/119	N,G,K,E,L,Q,D,S,H,R
 158	   K	  LYS158:C	 2.026		  1	 0.968, 2.975			    2,1			  59/119	E,G,N,K,I,R,V,W,Y,A,D,S
 159	   G	  GLY159:C	-0.128		  5	-0.533, 0.105			    7,5			  59/119	Q,L,N,G,K,M,R,D,H
 160	   G	  GLY160:C	 1.987		  1	 0.968, 2.975			    2,1			  56/119	I,F,N,G,K,V,M,R,T,A,D,Y
 161	   A	  ALA161:C	-0.231		  6	-0.605,-0.007			    7,5			  56/119	K,F,N,A,Q,R,W
 162	   W	  TRP162:C	 0.350		  4	-0.112, 0.730			    5,3			  56/119	F,I,L,T,A,H,S,R,V,W
 163	   F	  PHE163:C	-0.518		  6	-0.867,-0.296			    7,6			  50/119	A,T,F,G,V,W,L
 164	   L	  LEU164:C	-0.075		  5	-0.459, 0.232			    6,4			  47/119	L,V,G,K,A,T,S
 165	   G	  GLY165:C	 0.847		  3	 0.232, 1.284			    4,1			  50/119	S,D,A,T,E,Q,N,G
 166	   A	  ALA166:C	 0.798		  3	 0.232, 1.284			    4,1			  47/119	P,A,D,S,R,V,G,K,Q,E
 167	   T	  THR167:C	 0.926		  2	 0.232, 1.284			    4,1			  29/119	N,K,I,L,E,A,D,T,S,V,W
 168	   E	  GLU168:C	-0.295		  6	-0.804,-0.007			    7,5			  16/119	E,Q,D,P
 169	   P	  PRO169:C	 1.863		  1	 0.730, 2.975			    3,1			  13/119	W,E,Q,R,F,P
 170	   Y	  TYR170:C	-0.197		  6	-0.605, 0.105			    7,5			  49/119	V,Y,P,S,I,A
 171	   H	  HIS171:C	-0.566		  7	-0.867,-0.379			    7,6			  49/119	L,H,Y,F
 172	   V	  VAL172:C	 0.259		  4	-0.207, 0.537			    6,3			  49/119	K,I,L,Q,P,T,A,S,R,V,M
 173	   V	  VAL173:C	-1.307		  9	-1.495,-1.226			    9,8			  49/119	L,V,K,I
 174	   V	  VAL174:C	 0.164		  5	-0.296, 0.373			    6,4			  47/119	Q,K,F,N,R,M,V,A
 175	   F	  PHE175:C	-1.216		  8	-1.495,-1.107			    9,8			  45/119	K,F,S
 176	   K	  LYS176:C	-0.997		  8	-1.226,-0.867			    8,7			  43/119	K,N,Q,R,E
 177	   K	  LYS177:C	-1.171		  8	-1.419,-1.047			    9,8			  41/119	R,N,K
 178	   A	  ALA178:C	-1.233		  9	-1.495,-1.107			    9,8			  29/119	R,K,A
 179	   P	  PRO179:C	 1.282		  1	 0.232, 1.759			    4,1			   7/119	R,L,A,S,P
 180	   P	  PRO180:C	-0.223		  6*	-0.928, 0.232			    8,4			   5/119	P,S
 181	   A	  ALA181:C	-0.323		  6*	-1.107, 0.232			    8,4			   1/119	A


*Below the confidence cut-off - The calculations for this site were performed on less than 6 non-gaped homologue sequences,
or the confidence interval for the estimated score is equal to- or larger than- 4 color grades.
