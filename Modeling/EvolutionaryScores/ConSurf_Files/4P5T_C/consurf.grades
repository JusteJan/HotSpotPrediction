	 Amino Acid Conservation Scores
	=======================================

- POS: The position of the AA in the SEQRES derived sequence.
- SEQ: The SEQRES derived sequence in one letter code.
- 3LATOM: The ATOM derived sequence in three letter code, including the AA's positions as they appear in the PDB file and the chain identifier.
- SCORE: The normalized conservation scores.
- COLOR: The color scale representing the conservation scores (9 - conserved, 1 - variable).
- CONFIDENCE INTERVAL: When using the bayesian method for calculating rates, a confidence interval is assigned to each of the inferred evolutionary conservation scores.
- CONFIDENCE INTERVAL COLORS: When using the bayesian method for calculating rates. The color scale representing the lower and upper bounds of the confidence interval.
- MSA DATA: The number of aligned sequences having an amino acid (non-gapped) from the overall number of sequences at each position.
- RESIDUE VARIETY: The residues variety at each position of the multiple sequence alignment.

 POS	 SEQ	    3LATOM	SCORE		COLOR	CONFIDENCE INTERVAL	CONFIDENCE INTERVAL COLORS	MSA DATA	RESIDUE VARIETY
    	    	        	(normalized)	        	               
   1	   I	    ILE0:C	-0.199		  6	-0.578, 0.068			    7,5			  66/150	I,S,E,Q,V,A
   2	   E	    GLU1:C	 1.985		  1	 1.012, 2.943			    2,1			  48/150	Q,T,E,R,K,G
   3	   A	    ALA2:C	-0.927		  8	-1.179,-0.766			    9,7			  80/150	G,S,V,A
   4	   D	    ASP3:C	-0.782		  7	-1.059,-0.643			    8,7			  87/150	D,G,N,E
   5	   H	    HIS4:C	-1.138		  8	-1.385,-1.001			    9,8			  88/150	Y,D,H,R
   6	   V	    VAL5:C	-0.254		  6	-0.578,-0.031			    7,5			  92/150	H,I,V,F,A,M
   7	   G	    GLY6:C	 0.269		  4	-0.124, 0.595			    5,3			  92/150	R,L,V,A,I,G,S
   8	   T	    THR7:C	 1.308		  1	 0.780, 1.774			    3,1			  93/150	C,M,V,T,R,S,N,Y,I,A,F,E
   9	   Y	    TYR8:C	 0.249		  4	-0.212, 0.595			    6,3			  93/150	Y,I,F,C
  10	   G	    GLY9:C	-0.911		  8	-1.242,-0.705			    9,7			  69/150	Q,G,S
  11	   I	   ILE10:C	 1.002		  2	 0.437, 1.314			    4,1			  97/150	M,V,L,T,P,N,I,A,F,D,G
  12	   S	   SER11:C	 1.052		  2	 0.595, 1.314			    3,1			 107/150	L,T,M,C,I,K,S,N,Q,E,A,G,H
  13	   V	   VAL12:C	 0.584		  3	 0.068, 1.012			    5,2			 110/150	Y,I,L,T,M,V,F
  14	   Y	   TYR13:C	 0.182		  4	-0.212, 0.437			    6,4			 112/150	V,T,S,I,Y,F,A,D
  15	   Q	   GLN14:C	-0.253		  6	-0.578,-0.031			    7,5			 116/150	L,M,K,S,P,Q,E,H
  16	   S	   SER15:C	 0.097		  5	-0.212, 0.300			    6,4			 118/150	T,L,M,I,Y,K,P,S,N,A,G,H
  17	   P	   PRO16:C	 0.865		  2	 0.437, 1.314			    4,1			 119/150	R,T,L,Y,N,P,S,Q,F,H,G,D
  18	   G	   GLY17:C	 1.813		  1	 1.012, 1.774			    2,1			 119/150	H,G,D,E,Q,A,N,S,K,R,V,C
  19	   D	   ASP18:C	 1.254		  1	 0.780, 1.774			    3,1			 127/150	L,T,I,S,K,P,Q,E,A,F,D,G
  20	   I	   ILE19:C	 0.542		  3	 0.178, 0.780			    4,3			 134/150	T,L,I,N,Q,F,G,R,M,V,Y,S,P,E,A,D
  21	   G	   GLY20:C	-0.749		  7	-1.001,-0.578			    8,7			 136/150	X,G,S,A
  22	   Q	   GLN21:C	-0.459		  6	-0.705,-0.292			    7,6			 140/150	V,Q,E,D,N,Y
  23	   Y	   TYR22:C	 1.765		  1	 1.012, 1.774			    2,1			 143/150	D,H,F,N,Y,C,V,L
  24	   T	   THR23:C	-0.193		  6	-0.442,-0.031			    6,5			 143/150	A,E,Q,G,D,V,M,L,T,N,S,I
  25	   F	   PHE24:C	 1.643		  1	 1.012, 1.774			    2,1			 144/150	S,N,I,Y,M,C,V,T,L,H,A,F,Q
  26	   E	   GLU25:C	-0.288		  6	-0.578,-0.124			    7,5			 146/150	H,G,D,E,Q,N,S,M,T,L
  27	   F	   PHE26:C	 0.043		  5	-0.292, 0.300			    6,4			 146/150	L,C,M,V,Y,I,S,F
  28	   D	   ASP27:C	-1.146		  8	-1.309,-1.059			    9,8			 147/150	N,D,A,E
  29	   G	   GLY28:C	 0.119		  5	-0.212, 0.300			    6,4			 147/150	N,K,S,A,E,Q,G,D
  30	   D	   ASP29:C	-0.880		  8	-1.059,-0.766			    8,7			 147/150	E,D,N,H
  31	   E	   GLU30:C	-1.394		  9	-1.590,-1.309			    9,9			 147/150	E,Q
  32	   L	   LEU31:C	 0.997		  2	 0.595, 1.314			    3,1			 146/150	M,V,T,L,R,I,A,Q,E
  33	   F	   PHE32:C	-0.278		  6	-0.578,-0.031			    7,5			 146/150	F,L,W,Y
  34	   Y	   TYR33:C	-0.612		  7	-0.826,-0.442			    8,6			 146/150	X,Y,H,N,S
  35	   V	   VAL34:C	-1.112		  8	-1.309,-1.001			    9,8			 147/150	V,A,M,L,T,S
  36	   D	   ASP35:C	-0.941		  8	-1.118,-0.826			    8,8			 146/150	E,T,N,D
  37	   L	   LEU36:C	 0.301		  4	-0.031, 0.595			    5,3			 146/150	L,T,R,M,V,I,P,S,W,Q,F,H
  38	   D	   ASP37:C	 1.723		  1	 1.012, 1.774			    2,1			 147/150	S,K,N,I,M,D,G,A,F,Q,E
  39	   K	   LYS38:C	 0.911		  2	 0.437, 1.314			    4,1			 147/150	T,L,R,I,S,K,N,Q,E,A
  40	   K	   LYS39:C	 0.680		  3	 0.300, 1.012			    4,2			 147/150	K,S,Y,M,T,R,D,H,G,A,Q,E
  41	   E	   GLU40:C	 0.261		  4	-0.124, 0.437			    5,4			 147/150	D,Q,E,A,Y,K,S,T,R,M
  42	   T	   THR41:C	-0.380		  6	-0.643,-0.212			    7,6			 147/150	A,F,E,D,G,X,M,V,L,T,K,P,I
  43	   V	   VAL42:C	-0.213		  6	-0.442,-0.031			    6,5			 147/150	I,N,L,T,V,X,G,A,F
  44	   W	   TRP43:C	 0.049		  5	-0.369, 0.300			    6,4			 147/150	P,N,W,I,C,M,V,L,X,A,F,Q
  45	   M	   MET44:C	 0.092		  5	-0.212, 0.300			    6,4			 147/150	Y,K,P,S,W,N,T,R,M,X,D,G,H,Q,A
  46	   L	   LEU45:C	-0.764		  7	-1.001,-0.643			    8,7			 147/150	M,V,L,K,W,I,Q,X
  47	   P	   PRO46:C	-0.479		  6	-0.766,-0.292			    7,6			 148/150	K,P,S,A,Q,R,E
  48	   E	   GLU47:C	 0.975		  2	 0.437, 1.314			    4,1			 148/150	A,E,Q,G,D,V,M,R,L,N,K,S,P,I
  49	   F	   PHE48:C	-0.833		  8	-1.059,-0.705			    8,7			 148/150	S,L,F
  50	   G	   GLY49:C	 0.907		  2	 0.437, 1.314			    4,1			 148/150	E,Q,A,G,R,T,L,V,C,I,N,K,S
  51	   Q	   GLN50:C	 2.487		  1	 1.314, 2.943			    1,1			 148/150	K,S,N,T,R,D,G,H,Q,E
  52	   L	   LEU51:C	 0.976		  2	 0.437, 1.314			    4,1			 148/150	S,P,I,Y,V,M,T,L,G,H,D,F,A,E,Q
  53	   A	   ALA52:C	 2.034		  1	 1.314, 2.943			    1,1			 147/150	M,V,L,T,R,K,S,W,Y,I,A,F,G
  54	   S	   SER53:C	 1.404		  1	 0.780, 1.774			    3,1			 148/150	N,W,I,T,L,G,F,Q,K,S,Y,M,R,H,D,A
  55	   F	   PHE54:C	 0.106		  5	-0.212, 0.300			    6,4			 148/150	L,Q,F,V,Y,S,P
  56	   D	   ASP55:C	 0.067		  5	-0.212, 0.300			    6,4			 148/150	G,H,D,E,Q,A,N,P,S,K
  57	   P	   PRO56:C	-0.222		  6	-0.511,-0.031			    7,5			 148/150	F,A,G,D,L,T,V,I,P,S
  58	   Q	   GLN57:C	-0.329		  6	-0.578,-0.124			    7,5			 148/150	H,D,A,E,Q,N,W,P,K,R
  59	   G	   GLY58:C	 1.808		  1	 1.012, 1.774			    2,1			 148/150	W,N,I,T,G,F,Q,S,P,K,Y,V,R,H,D,A,E
  60	   G	   GLY59:C	-0.489		  6	-0.705,-0.369			    7,6			 148/150	W,S,I,T,G,H,D,A
  61	   L	   LEU60:C	-0.410		  6	-0.705,-0.212			    7,6			 148/150	L,V,I,Y,W,K,S,E,Q
  62	   Q	   GLN61:C	 1.072		  2	 0.595, 1.314			    3,1			 148/150	V,R,L,T,N,P,K,S,I,A,E,Q,G
  63	   N	   ASN62:C	 0.155		  5	-0.124, 0.300			    5,4			 148/150	D,H,G,E,A,Y,S,K,N,M
  64	   I	   ILE63:C	 0.461		  4	 0.068, 0.595			    5,3			 148/150	Q,F,A,H,R,L,V,M,I,K
  65	   A	   ALA64:C	-0.412		  6	-0.643,-0.292			    7,6			 148/150	N,K,P,I,V,L,G,D,A,E,Q
  66	   V	   VAL65:C	 1.555		  1	 1.012, 1.774			    2,1			 148/150	G,A,I,S,K,T,L,R,M,V
  67	   V	   VAL66:C	 2.902		  1	 1.774, 2.943			    1,1			 148/150	D,G,Q,E,A,I,S,N,T,L,M,C,V
  68	   K	   LYS67:C	 0.466		  4	 0.068, 0.780			    5,3			 150/150	R,L,M,I,Y,N,K,P,E,Q,G
  69	   H	   HIS68:C	 1.706		  1	 1.012, 1.774			    2,1			 148/150	A,X,D,H,R,M,V,Y,K,S,P,Q,F,G,T,L,N
  70	   N	   ASN69:C	-0.770		  7	-0.943,-0.643			    8,7			 150/150	D,H,G,A,Y,I,S,N,T,R
  71	   L	   LEU70:C	-1.023		  8	-1.242,-0.885			    9,8			 150/150	I,V,M,L
  72	   G	   GLY71:C	 1.635		  1	 1.012, 1.774			    2,1			 149/150	A,E,H,G,D,R,T,N,K,P,Y
  73	   V	   VAL72:C	 1.381		  1	 0.780, 1.774			    3,1			 148/150	Q,A,F,X,T,L,R,C,M,V,I,Y,K,S,N
  74	   L	   LEU73:C	 1.452		  1	 0.780, 1.774			    3,1			 149/150	L,R,M,C,V,I,Y,S,N,W,A,F,G
  75	   T	   THR74:C	 0.481		  4	 0.178, 0.780			    4,3			 149/150	I,K,R,T,L,V,M,F,A
  76	   K	   LYS75:C	 0.616		  3	 0.178, 0.780			    4,3			 149/150	A,E,Q,G,R,L,N,K,S,I
  77	   R	   ARG76:C	 1.186		  1	 0.595, 1.314			    3,1			 148/150	R,L,T,V,C,M,N,W,S,E,Q,A,G,H,D
  78	   S	   SER77:C	-0.371		  6	-0.578,-0.212			    7,6			 149/150	Y,P,S,L,T,R,V,H,E,A,F
  79	   N	   ASN78:C	-1.129		  8	-1.309,-1.059			    9,8			 150/150	S,K,N,G,V,T
  80	   S	   SER79:C	 1.332		  1	 0.780, 1.774			    3,1			 150/150	E,F,H,G,D,R,V,C,Y,I,N,W,S,K
  81	   T	   THR80:C	-0.939		  8	-1.118,-0.826			    8,8			 150/150	V,T,L,K,S,P,I,G
  82	   P	   PRO81:C	 0.313		  4	-0.031, 0.595			    5,3			 150/150	S,P,K,C,L,T,R,D,H,A,Q,E
  83	   A	   ALA82:C	 0.032		  5	-0.292, 0.178			    6,4			 150/150	G,D,E,Q,A,I,N,S,P,T,V
  84	   T	   THR83:C	 2.585		  1	 1.314, 2.943			    1,1			 150/150	I,N,K,S,P,R,L,T,V,M,D,E,Q,A
  85	   N	   ASN84:C	 0.220		  4	-0.124, 0.437			    5,4			 150/150	V,M,R,T,L,N,S,P,K,I,F,Q,D
  86	   E	   GLU85:C	 0.285		  4	-0.031, 0.437			    5,4			 149/150	E,Q,V,A,I,D,K
  87	   A	   ALA86:C	 0.013		  5	-0.292, 0.178			    6,4			 149/150	I,P,S,R,T,V,A
  88	   P	   PRO87:C	-1.337		  9	-1.590,-1.242			    9,9			 150/150	P,S,G
  89	   Q	   GLN88:C	-0.419		  6	-0.643,-0.292			    7,6			 150/150	D,H,Q,E,A,S,K,L,T,R,V
  90	   A	   ALA89:C	-0.576		  7	-0.766,-0.442			    7,6			 150/150	S,N,W,I,C,M,V,T,L,G,A
  91	   T	   THR90:C	-0.288		  6	-0.511,-0.124			    7,5			 150/150	A,M,R,L,T,S,I
  92	   V	   VAL91:C	-0.636		  7	-0.826,-0.511			    8,7			 150/150	I,G,L,V,A,M
  93	   F	   PHE92:C	-0.757		  7	-1.001,-0.578			    8,7			 150/150	H,I,Y,F,L
  94	   P	   PRO93:C	 0.754		  3	 0.300, 1.012			    4,2			 150/150	T,L,S,P
  95	   K	   LYS94:C	 0.131		  5	-0.212, 0.300			    6,4			 150/150	A,Q,E,D,H,M,T,R,S,K,N,I
  96	   S	   SER95:C	 1.202		  1	 0.780, 1.314			    3,1			 150/150	R,T,Y,N,K,S,E,Q,F,A,H,G,D
  97	   P	   PRO96:C	 0.646		  3	 0.178, 1.012			    4,2			 150/150	K,S,P,Y,R,T,L,H,D,A,E,Q
  98	   V	   VAL97:C	-1.084		  8	-1.242,-1.001			    9,8			 150/150	T,M,A,V,I,G
  99	   L	   LEU98:C	 0.278		  4	-0.031, 0.437			    5,4			 150/150	D,E,Q,A,N,K,S,L,T,V,M
 100	   L	   LEU99:C	-0.476		  6	-0.766,-0.292			    7,6			 150/150	L,V,M,I,W,P,F,D
 101	   G	  GLY100:C	-0.541		  7	-0.826,-0.369			    8,6			 150/150	V,E,R,L,G,S,D
 102	   Q	  GLN101:C	 0.292		  4	-0.031, 0.437			    5,4			 150/150	A,Q,E,D,H,V,L,R,K,I
 103	   P	  PRO102:C	-0.883		  8	-1.118,-0.766			    8,7			 150/150	L,R,K,S,P,Q,E,A
 104	   N	  ASN103:C	-1.443		  9	-1.590,-1.385			    9,9			 150/150	H,N,K
 105	   T	  THR104:C	 0.306		  4	-0.031, 0.437			    5,4			 150/150	I,K,T,L,V,M
 106	   L	  LEU105:C	-1.438		  9	-1.590,-1.385			    9,9			 150/150	L
 107	   I	  ILE106:C	-1.231		  9	-1.385,-1.179			    9,9			 150/150	I,L,V
 108	   C	  CYS107:C	-1.357		  9	-1.590,-1.309			    9,9			 149/150	S,X,C
 109	   F	  PHE108:C	 0.509		  3	 0.178, 0.780			    4,3			 150/150	F,A,Q,H,V,C,M,L,I,Y
 110	   V	  VAL109:C	-0.561		  7	-0.766,-0.442			    7,6			 150/150	A,M,F,V,I
 111	   D	  ASP110:C	-0.750		  7	-0.943,-0.643			    8,7			 150/150	D,K,S,N,T,E
 112	   N	  ASN111:C	-0.237		  6	-0.511,-0.031			    7,5			 149/150	R,N,K,S,E,Q,A,H,G,D
 113	   I	  ILE112:C	-1.120		  8	-1.309,-1.001			    9,8			 149/150	F,L,I
 114	   F	  PHE113:C	-0.661		  7	-0.885,-0.511			    8,7			 150/150	Y,S,H,W,L,T,F
 115	   P	  PRO114:C	-1.402		  9	-1.590,-1.385			    9,9			 150/150	P,L
 116	   P	  PRO115:C	-1.214		  9	-1.385,-1.118			    9,8			 150/150	A,L,P,S
 117	   V	  VAL116:C	-0.868		  8	-1.059,-0.766			    8,7			 150/150	Y,P,T,M,V,H,E,A
 118	   I	  ILE117:C	 0.045		  5	-0.292, 0.300			    6,4			 150/150	I,L,M,A,V
 119	   N	  ASN118:C	-0.340		  6	-0.578,-0.212			    7,6			 150/150	K,S,D,N,T,R,E
 120	   I	  ILE119:C	-0.562		  7	-0.766,-0.442			    7,6			 150/150	I,V,M,L,T
 121	   T	  THR120:C	-0.605		  7	-0.826,-0.442			    8,6			 150/150	Q,E,H,M,L,T,R,S,K,I
 122	   W	  TRP121:C	-1.210		  9	-1.488,-1.059			    9,8			 150/150	W,R
 123	   L	  LEU122:C	-0.702		  7	-0.943,-0.578			    8,7			 150/150	A,F,M,V,L,T,R,Y
 124	   R	  ARG123:C	 1.178		  1	 0.780, 1.314			    3,1			 149/150	Y,I,S,K,N,L,T,R,C,X,H,G,Q
 125	   N	  ASN124:C	-1.477		  9	-1.590,-1.488			    9,9			 150/150	N
 126	   S	  SER125:C	-0.254		  6	-0.578,-0.031			    7,5			 150/150	R,N,K,S,E,Q,G,D
 127	   K	  LYS126:C	 0.520		  3	 0.178, 0.780			    4,3			 149/150	X,H,Q,E,A,I,Y,K,N,L,R,V
 128	   S	  SER127:C	 2.642		  1	 1.774, 2.943			    1,1			 150/150	E,F,A,D,R,T,L,V,M,I,N,P,K,S
 129	   V	  VAL128:C	-1.066		  8	-1.242,-1.001			    9,8			 150/150	V,E,L,N,I
 130	   A	  ALA129:C	-0.419		  6	-0.643,-0.292			    7,6			 149/150	V,M,R,T,N,K,P,S,I,F,A,D
 131	   D	  ASP130:C	 1.051		  2	 0.595, 1.314			    3,1			 150/150	D,E,Q,K,I,V,M,T,L
 132	   G	  GLY131:C	-0.564		  7	-0.826,-0.369			    8,6			 148/150	S,K,M,R,D,G,X,A,Q
 133	   V	  VAL132:C	-0.028		  5	-0.292, 0.178			    6,4			 150/150	S,I,V,M,R,T,G,F,A,E
 134	   Y	  TYR133:C	 0.166		  4	-0.124, 0.300			    5,4			 150/150	F,A,E,Q,G,M,R,T,L,N,S,Y
 135	   E	  GLU134:C	-0.661		  7	-0.885,-0.511			    8,7			 150/150	S,I,R,L,T,G,D,E,Q
 136	   T	  THR135:C	-1.038		  8	-1.179,-0.943			    9,8			 150/150	G,S,I,M,T
 137	   S	  SER136:C	 0.080		  5	-0.212, 0.300			    6,4			 150/150	G,D,E,Q,A,I,N,S,P,R,L,T,V
 138	   F	  PHE137:C	-0.649		  7	-0.885,-0.511			    8,7			 150/150	F,L,S,P,I,Y
 139	   F	  PHE138:C	-0.552		  7	-0.826,-0.369			    8,6			 150/150	F,M,R,L,Q,Y,I
 140	   V	  VAL139:C	 0.012		  5	-0.292, 0.178			    6,4			 150/150	V,R,L,T,P,S,I,A,G,H
 141	   N	  ASN140:C	-0.346		  6	-0.578,-0.212			    7,6			 150/150	L,T,R,M,S,K,N,Q,A,D,G
 142	   R	  ARG141:C	 2.521		  1	 1.314, 2.943			    1,1			 150/150	R,T,V,M,N,S,P,K,E,Q,A,G,D
 143	   D	  ASP142:C	-1.051		  8	-1.242,-0.943			    9,8			 150/150	N,G,K,D,Y,E
 144	   Y	  TYR143:C	 1.280		  1	 0.780, 1.774			    3,1			 150/150	Q,F,A,G,H,D,R,L,V,Y,N
 145	   S	  SER144:C	 0.067		  5	-0.212, 0.300			    6,4			 150/150	P,S,K,N,M,T,L,R,G,H,A,F
 146	   F	  PHE145:C	-0.991		  8	-1.179,-0.885			    9,8			 150/150	L,F,Y
 147	   H	  HIS146:C	 0.453		  4	 0.068, 0.595			    5,3			 150/150	L,R,C,Y,S,N,Q,A,F,G,H
 148	   K	  LYS147:C	-0.690		  7	-0.885,-0.578			    8,7			 150/150	K,I,R,T,Q,L
 149	   L	  LEU148:C	-0.382		  6	-0.643,-0.212			    7,6			 150/150	Y,I,S,L,M,A,F
 150	   S	  SER149:C	-0.662		  7	-0.885,-0.511			    8,7			 150/150	T,L,C,Y,N,S,Q,A,H,G
 151	   Y	  TYR150:C	-1.159		  9	-1.385,-1.059			    9,8			 150/150	T,Y,D,S,H
 152	   L	  LEU151:C	-1.316		  9	-1.488,-1.242			    9,9			 150/150	V,L,P
 153	   T	  THR152:C	-0.184		  6	-0.442,-0.031			    6,5			 150/150	A,E,D,V,T,R,K,S,P,N,I
 154	   F	  PHE153:C	-1.223		  9	-1.385,-1.118			    9,8			 150/150	L,F,I
 155	   I	  ILE154:C	 0.105		  5	-0.212, 0.300			    6,4			 149/150	M,F,V,L,T,I
 156	   P	  PRO155:C	-1.445		  9	-1.590,-1.385			    9,9			 149/150	P
 157	   S	  SER156:C	-0.172		  6	-0.442,-0.031			    6,5			 149/150	T,L,M,W,N,S,K,E,Q,A
 158	   D	  ASP157:C	 1.025		  2	 0.595, 1.314			    3,1			 149/150	D,G,Q,E,A,I,P,K,S,N,T,L,R,M,V
 159	   D	  ASP158:C	 0.213		  4	-0.124, 0.437			    5,4			 149/150	K,N,T,R,D,G,H,A,Q,E
 160	   D	  ASP159:C	-1.307		  9	-1.488,-1.242			    9,9			 149/150	N,D,E,V
 161	   I	  ILE160:C	 1.030		  2	 0.595, 1.314			    3,1			 147/150	D,F,A,Y,I,S,T,L,V,M
 162	   Y	  TYR161:C	-1.119		  8	-1.309,-1.001			    9,8			 147/150	N,S,Y,C
 163	   D	  ASP162:C	-0.850		  8	-1.059,-0.705			    8,7			 147/150	A,D,H,G,T,S,N,Y
 164	   C	  CYS163:C	-1.264		  9	-1.488,-1.179			    9,9			 147/150	Y,L,C
 165	   K	  LYS164:C	 0.983		  2	 0.595, 1.314			    3,1			 147/150	E,Q,A,G,H,R,T,V,M,K,S
 166	   V	  VAL165:C	-1.396		  9	-1.590,-1.309			    9,9			 147/150	G,I,V,M
 167	   E	  GLU166:C	-0.778		  7	-1.001,-0.643			    8,7			 147/150	K,R,V,D,G,Q,E,F
 168	   H	  HIS167:C	-1.425		  9	-1.590,-1.385			    9,9			 147/150	Q,H
 169	   W	  TRP168:C	 0.502		  3	 0.068, 0.780			    5,3			 147/150	I,K,N,W,L,R,M,E
 170	   G	  GLY169:C	-0.620		  7	-0.885,-0.442			    8,6			 147/150	S,D,G,H,C,A,V
 171	   L	  LEU170:C	-1.072		  8	-1.309,-0.943			    9,8			 147/150	Q,L,F,A,W
 172	   E	  GLU171:C	 1.363		  1	 0.780, 1.774			    3,1			 147/150	Q,E,A,D,G,H,T,R,S,K,P,N
 173	   E	  GLU172:C	 2.356		  1	 1.314, 2.943			    1,1			 147/150	E,Q,A,H,G,D,R,T,N,S,K
 174	   P	  PRO173:C	-0.426		  6	-0.705,-0.212			    7,6			 147/150	L,E,A,P,S,G,H
 175	   V	  VAL174:C	 1.148		  2	 0.595, 1.314			    3,1			 147/150	D,A,F,E,S,K,Y,I,V,T,L,R
 176	   L	  LEU175:C	-0.023		  5	-0.369, 0.178			    6,4			 147/150	N,S,I,V,M,R,L,T,G,F,A,Q
 177	   K	  LYS176:C	 0.732		  3	 0.300, 1.012			    4,2			 147/150	A,Q,E,G,M,V,T,R,S,K,W,N
 178	   H	  HIS177:C	-0.738		  7	-0.943,-0.578			    8,7			 147/150	H,F,E,Y,I,M,V,L,T,R
 179	   W	  TRP178:C	-1.048		  8	-1.309,-0.885			    9,8			 146/150	W,L,C
 180	   E	         -	-0.617		  7	-0.826,-0.442			    8,6			 136/150	G,H,D,E,Q,A,Y,N,R
 181	   P	         -	-0.806		  7	-1.059,-0.643			    8,7			  80/150	P,A,V,T
 182	   E	         -	-0.186		  6	-0.511, 0.068			    7,5			 109/150	M,L,K,Y,F,A,E,Q,H,D


*Below the confidence cut-off - The calculations for this site were performed on less than 6 non-gaped homologue sequences,
or the confidence interval for the estimated score is equal to- or larger than- 4 color grades.
