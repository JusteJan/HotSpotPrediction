	 Amino Acid Conservation Scores
	=======================================

- POS: The position of the AA in the SEQRES derived sequence.
- SEQ: The SEQRES derived sequence in one letter code.
- 3LATOM: The ATOM derived sequence in three letter code, including the AA's positions as they appear in the PDB file and the chain identifier.
- SCORE: The normalized conservation scores.
- COLOR: The color scale representing the conservation scores (9 - conserved, 1 - variable).
- CONFIDENCE INTERVAL: When using the bayesian method for calculating rates, a confidence interval is assigned to each of the inferred evolutionary conservation scores.
- CONFIDENCE INTERVAL COLORS: When using the bayesian method for calculating rates. The color scale representing the lower and upper bounds of the confidence interval.
- MSA DATA: The number of aligned sequences having an amino acid (non-gapped) from the overall number of sequences at each position.
- RESIDUE VARIETY: The residues variety at each position of the multiple sequence alignment.

 POS	 SEQ	    3LATOM	SCORE		COLOR	CONFIDENCE INTERVAL	CONFIDENCE INTERVAL COLORS	MSA DATA	RESIDUE VARIETY
    	    	        	(normalized)	        	               
   1	   G	   GLY14:A	-0.982		  9	-1.269,-0.837			    9,8			  38/150	G
   2	   D	   ASP15:A	-0.731		  8	-0.932,-0.632			    8,7			 127/150	S,E,D,G,N
   3	   C	   CYS16:A	 0.770		  2	 0.361, 0.918			    4,2			 144/150	A,Y,R,Q,M,G,T,L,S,C,N,H
   4	   C	   CYS17:A	-0.509		  7	-0.738,-0.320			    8,6			 147/150	A,Y,C,Q,R
   5	   I	   ILE18:A	-0.658		  7	-0.837,-0.517			    8,7			 147/150	A,V,F,L,I
   6	   I	   ILE19:A	-0.316		  6	-0.517,-0.166			    7,6			 147/150	A,V,L,I
   7	   R	   ARG20:A	-0.487		  7	-0.686,-0.320			    8,6			 147/150	K,H,R,C,Q
   8	   V	   VAL21:A	-0.529		  7	-0.738,-0.389			    8,6			 147/150	V,A,S,I,M,T,L
   9	   S	   SER22:A	-0.387		  6	-0.576,-0.247			    7,6			 147/150	A,I,Q,R,T,G,V,S
  10	   L	   LEU23:A	 0.480		  3	 0.116, 0.693			    5,2			 147/150	I,M,L,T,V,W,K
  11	   D	   ASP24:A	 0.279		  4	-0.082, 0.514			    5,3			 147/150	P,T,D,Q,E,A,N,S
  12	   V	   VAL25:A	 1.380		  1	 0.918, 1.671			    2,1			 148/150	K,S,V,C,H,N,A,E,G,M,L,T,D,I,Q
  13	   D	   ASP26:A	 1.963		  1	 1.217, 2.858			    1,1			 148/150	F,E,A,Q,I,D,T,G,V,S,K,N,H
  14	   N	   ASN27:A	 0.054		  5	-0.247, 0.231			    6,4			 148/150	P,T,D,G,R,Q,I,S,K,N,H
  15	   G	   GLY28:A	-0.394		  6	-0.632,-0.247			    7,6			 147/150	C,D,G,V,S,A,K
  16	   N	   ASN29:A	-0.649		  7	-0.837,-0.517			    8,7			 149/150	R,Q,T,Y,N,H,V,S,K
  17	   M	   MET30:A	 0.963		  1	 0.514, 1.217			    3,1			 148/150	V,K,H,N,F,E,I,L,D,M
  18	   Y	   TYR31:A	-0.680		  8	-0.885,-0.517			    8,7			 149/150	F,V,S,Y,C
  19	   K	   LYS32:A	-0.749		  8	-0.932,-0.632			    8,7			 149/150	Q,R,T,K
  20	   S	   SER33:A	-1.182		  9	-1.269,-1.161			    9,9			 149/150	E,S,T
  21	   I	   ILE34:A	-0.740		  8	-0.885,-0.632			    8,7			 150/150	I,M,L,V
  22	   L	   LEU35:A	-0.156		  6	-0.455, 0.013			    7,5			 150/150	M,L,V,W,K
  23	   V	   VAL36:A	-0.508		  7	-0.738,-0.389			    8,6			 150/150	I,L,V
  24	   T	   THR37:A	-0.814		  8	-0.978,-0.738			    9,8			 150/150	S,T,C,N
  25	   S	   SER38:A	-0.462		  7	-0.632,-0.320			    7,6			 150/150	S,C,H,N,D
  26	   Q	   GLN39:A	-0.848		  8	-1.023,-0.738			    9,8			 150/150	Q,H,N,S,E
  27	   D	   ASP40:A	-0.615		  7	-0.788,-0.517			    8,7			 150/150	E,Q,D
  28	   K	   LYS41:A	-0.270		  6	-0.517,-0.082			    7,5			 150/150	K,M,R,H,N
  29	   A	   ALA42:A	-0.577		  7	-0.738,-0.455			    8,7			 150/150	T,I,S,A,V
  30	   P	   PRO43:A	 0.270		  4	-0.082, 0.514			    5,3			 150/150	S,A,Q,R,T,L,P
  31	   T	   THR44:A	 0.722		  2	 0.361, 0.918			    4,2			 149/150	I,X,Q,P,D,T,G,E,A,H,V,S,K
  32	   V	   VAL45:A	-0.869		  8	-1.023,-0.788			    9,8			 150/150	V,A,I,M
  33	   I	   ILE46:A	-0.699		  8	-0.885,-0.576			    8,7			 150/150	T,L,M,I,F,V
  34	   R	   ARG47:A	 0.993		  1	 0.514, 1.217			    3,1			 150/150	S,K,H,N,C,E,A,L,T,G,R,Q
  35	   K	   LYS48:A	-0.393		  6	-0.632,-0.247			    7,6			 150/150	K,S,C,N,A,G,T,Q,R
  36	   A	   ALA49:A	-0.967		  9	-1.114,-0.885			    9,8			 150/150	V,S,A,I,T,L
  37	   M	   MET50:A	-0.568		  7	-0.788,-0.455			    8,7			 150/150	L,M,V
  38	   D	   ASP51:A	 1.640		  1	 0.918, 1.671			    2,1			 150/150	R,Q,I,L,T,D,G,M,E,A,H,C,V,S,K
  39	   K	   LYS52:A	-0.973		  9	-1.114,-0.885			    9,8			 150/150	N,D,V,E,K
  40	   H	   HIS53:A	-0.536		  7	-0.738,-0.389			    8,6			 150/150	Y,F,L,Q,R,N,H
  41	   N	   ASN54:A	 0.510		  3	 0.116, 0.693			    5,2			 150/150	A,E,F,G,T,D,R,Q,K,S,N,H
  42	   L	   LEU55:A	 0.049		  5	-0.247, 0.231			    6,4			 150/150	K,S,V,M,T,L,R,I,Q,F
  43	   D	   ASP56:A	 0.714		  2	 0.361, 0.918			    4,2			 150/150	N,H,V,K,S,Q,G,D,P,A,E
  44	   E	   GLU57:A	 2.834		  1	 1.671, 2.858			    1,1			 150/150	I,R,Q,L,M,G,Y,E,A,N,H,S,P,D,T,F,V,K
  45	   D	   ASP58:A	 2.699		  1	 1.671, 2.858			    1,1			 146/150	K,V,F,D,P,T,S,C,N,A,E,Y,G,M,Q,I,R
  46	   E	   GLU59:A	 2.811		  1	 1.671, 2.858			    1,1			 148/150	K,S,W,V,N,A,E,G,T,D,P,I,X,R,Q
  47	   P	   PRO60:A	 1.746		  1	 0.918, 1.671			    2,1			 150/150	A,E,Y,F,G,D,T,P,L,I,R,S,V,W,C
  48	   E	   GLU61:A	 2.554		  1	 1.671, 2.858			    1,1			 150/150	S,K,H,N,C,E,A,R,Q,T,P,D,L,G
  49	   D	   ASP62:A	 1.367		  1	 0.918, 1.671			    2,1			 150/150	S,K,H,N,C,Y,E,A,Q,R,D,G
  50	   Y	   TYR63:A	-0.072		  5	-0.389, 0.116			    6,5			 150/150	C,N,L,F,Y
  51	   E	   GLU64:A	 0.037		  5	-0.247, 0.231			    6,4			 150/150	S,K,N,C,F,E,Q,R,T,D
  52	   L	   LEU65:A	-1.201		  9	-1.309,-1.161			    9,9			 150/150	L
  53	   L	   LEU66:A	 0.231		  4	-0.082, 0.361			    5,4			 150/150	I,T,L,M,F,A,C,V,S,K
  54	   Q	   GLN67:A	-1.192		  9	-1.309,-1.161			    9,9			 150/150	A,Q
  55	   I	   ILE68:A	 0.477		  3	 0.116, 0.693			    5,2			 150/150	A,K,V,M,L,T,I
  56	   I	   ILE69:A	-0.398		  6	-0.632,-0.247			    7,6			 150/150	Q,I,L,V
  57	   S	   SER70:A	-0.220		  6	-0.455,-0.082			    7,5			 150/150	H,N,S,R,G,D,T,P,A,E
  58	   E	   GLU71:A	 1.962		  1	 1.217, 2.858			    1,1			 147/150	N,H,C,S,K,Q,R,D,G,E,A
  59	   D	   ASP72:A	 1.049		  1	 0.514, 1.217			    3,1			 150/150	D,G,Q,E,A,N,S,K
  60	   H	   HIS73:A	 1.121		  1	 0.693, 1.217			    2,1			 148/150	V,K,C,H,N,E,X,R,Q,G,T,P
  61	   K	   LYS74:A	-0.555		  7	-0.738,-0.455			    8,7			 148/150	E,G,M,D,T,R,K,V
  62	   L	   LEU75:A	-0.561		  7	-0.788,-0.389			    8,6			 148/150	M,L,F
  63	   K	   LYS76:A	 0.986		  1	 0.514, 1.217			    3,1			 148/150	A,E,Q,R,I,T,P,L,V,K,H
  64	   I	   ILE77:A	-0.825		  8	-0.978,-0.738			    9,8			 148/150	V,F,L,M,I
  65	   P	   PRO78:A	-1.063		  9	-1.211,-0.978			    9,9			 148/150	I,L,P
  66	   E	   GLU79:A	 0.426		  3	 0.116, 0.693			    5,2			 148/150	F,E,A,R,P,D,G,S
  67	   N	   ASN80:A	 0.295		  4	 0.013, 0.514			    5,3			 148/150	T,D,G,R,Q,H,N,S,K
  68	   A	   ALA81:A	-0.789		  8	-0.932,-0.686			    8,8			 147/150	F,S,A,C,T,G
  69	   N	   ASN82:A	-1.114		  9	-1.211,-1.069			    9,9			 147/150	K,I,N,T
  70	   V	   VAL83:A	-0.913		  8	-1.069,-0.837			    9,8			 147/150	L,M,I,C,V
  71	   F	   PHE84:A	-0.429		  7	-0.686,-0.247			    8,6			 147/150	F,Y
  72	   Y	   TYR85:A	-1.190		  9	-1.309,-1.161			    9,9			 147/150	Y
  73	   A	   ALA86:A	-1.015		  9	-1.161,-0.932			    9,8			 147/150	G,T,V,A
  74	   M	   MET87:A	-1.053		  9	-1.161,-0.978			    9,9			 147/150	V,M,L,I
  75	   N	   ASN88:A	 0.037		  5	-0.247, 0.231			    6,4			 147/150	A,R,G,P,T,D,V,K,S,C,N,H
  76	   S	   SER89:A	-0.369		  6	-0.576,-0.247			    7,6			 147/150	S,K,T,P,G,Q,R
  77	   A	   ALA90:A	 0.919		  2	 0.514, 1.217			    3,1			 147/150	C,N,H,V,K,S,R,Q,M,G,L,T,A
  78	   A	   ALA91:A	 0.072		  5	-0.247, 0.231			    6,4			 128/150	E,Y,A,T,P,G,M,V,S
  79	   N	   ASN92:A	-0.117		  5	-0.389, 0.013			    6,5			 145/150	D,P,Q,E,A,F,N,C,S,K
  80	   Y	   TYR93:A	 0.858		  2	 0.361, 1.217			    4,1			 145/150	F,Y,R,Q,P,L,T,D,G,V,S,K,N,H
  81	   D	   ASP94:A	-0.118		  5	-0.389, 0.013			    6,5			 145/150	W,K,H,N,E,Q,R,G,D,T
  82	   F	   PHE95:A	-1.026		  9	-1.211,-0.932			    9,8			 145/150	V,F,L
  83	   I	   ILE96:A	 0.430		  3	 0.116, 0.693			    5,2			 145/150	V,E,A,I,L,T,M
  84	   L	   LEU97:A	-1.198		  9	-1.309,-1.161			    9,9			 145/150	L
  85	   K	   LYS98:A	-0.488		  7	-0.686,-0.320			    8,6			 146/150	T,L,M,Q,R,H,K,V
  86	   K	   LYS99:A	 1.009		  1	 0.514, 1.217			    3,1			 140/150	H,V,K,Q,R,P,T,A
  87	   R	  ARG100:A	-0.553		  7	-0.788,-0.389			    8,6			  75/150	K,R


*Below the confidence cut-off - The calculations for this site were performed on less than 6 non-gaped homologue sequences,
or the confidence interval for the estimated score is equal to- or larger than- 4 color grades.
