	 Amino Acid Conservation Scores
	=======================================

- POS: The position of the AA in the SEQRES derived sequence.
- SEQ: The SEQRES derived sequence in one letter code.
- 3LATOM: The ATOM derived sequence in three letter code, including the AA's positions as they appear in the PDB file and the chain identifier.
- SCORE: The normalized conservation scores.
- COLOR: The color scale representing the conservation scores (9 - conserved, 1 - variable).
- CONFIDENCE INTERVAL: When using the bayesian method for calculating rates, a confidence interval is assigned to each of the inferred evolutionary conservation scores.
- CONFIDENCE INTERVAL COLORS: When using the bayesian method for calculating rates. The color scale representing the lower and upper bounds of the confidence interval.
- MSA DATA: The number of aligned sequences having an amino acid (non-gapped) from the overall number of sequences at each position.
- RESIDUE VARIETY: The residues variety at each position of the multiple sequence alignment.

 POS	 SEQ	    3LATOM	SCORE		COLOR	CONFIDENCE INTERVAL	CONFIDENCE INTERVAL COLORS	MSA DATA	RESIDUE VARIETY
    	    	        	(normalized)	        	               
   1	   M	         -	-1.354		  9	-1.537,-1.285			    9,9			  21/150	M
   2	   A	         -	-0.141		  5	-0.531, 0.074			    7,5			  26/150	S,M,T,G,A
   3	   A	         -	-0.552		  7	-0.775,-0.390			    7,6			  60/150	A,P,Q,T,G,R,S
   4	   S	    SER4:D	 0.788		  3	 0.345, 1.013			    4,2			  69/150	K,E,S,N,V,Q,T,M,A,P,L,W,G
   5	   R	    ARG5:D	-0.253		  6	-0.531,-0.037			    7,5			  81/150	A,L,H,G,R,M,T,I,K,N,S
   6	   R	    ARG6:D	-1.373		  9	-1.537,-1.339			    9,9			  87/150	W,S,R
   7	   L	    LEU7:D	-0.496		  7	-0.718,-0.312			    7,6			  88/150	I,F,P,A,L,V
   8	   M	    MET8:D	 1.244		  1	 0.730, 1.440			    3,1			  89/150	L,H,A,G,R,M,Q,V,T,I,E,K,N,S
   9	   K	    LYS9:D	-0.848		  8	-1.036,-0.718			    8,7			  90/150	I,A,Q,N,S,K,R
  10	   E	   GLU10:D	-1.410		  9	-1.537,-1.399			    9,9			  90/150	E,D
  11	   L	   LEU11:D	-0.452		  6	-0.718,-0.312			    7,6			  90/150	M,Y,L,A,F,S,I,T
  12	   E	   GLU12:D	 1.289		  1	 0.730, 1.440			    3,1			  91/150	K,E,S,N,D,V,Q,T,Y,R,M,A,L,G
  13	   E	   GLU13:D	 0.048		  5	-0.229, 0.200			    6,4			  91/150	R,A,E,S,N,D,Q,T,I
  14	   I	   ILE14:D	 0.238		  4	-0.137, 0.516			    5,3			  92/150	V,I,A,C,L,F,Y,M
  15	   R	   ARG15:D	 2.060		  1	 1.013, 2.543			    2,1			  92/150	R,M,A,C,H,L,G,K,E,N,S,V,Q,I,T
  16	   K	   LYS16:D	 1.149		  1	 0.516, 1.440			    3,1			  93/150	E,K,N,S,V,Q,D,T,R,M,L,A,G
  17	   C	   CYS17:D	 0.838		  2	 0.345, 1.013			    4,2			  93/150	K,E,N,S,D,Q,V,R,P,C,A,H,G
  18	   G	   GLY18:D	 0.450		  4	 0.074, 0.730			    5,3			  90/150	A,P,L,G,D,V,T,I,E,S,N
  19	   M	   MET19:D	 1.716		  1	 1.013, 2.543			    2,1			  90/150	D,Q,V,I,K,E,S,N,A,P,H,L,G,F,M
  20	   K	   LYS20:D	 2.323		  1	 1.440, 2.543			    1,1			  90/150	L,A,C,P,G,R,Q,D,T,E,K,N,S
  21	   N	   ASN21:D	 0.601		  3	 0.200, 0.730			    4,3			  93/150	F,L,H,A,Y,T,N,K,W,G,R,I,Q,D,S,E
  22	   F	   PHE22:D	 0.022		  5	-0.312, 0.200			    6,4			  93/150	T,I,V,Y,M,W,F,C,A,L
  23	   R	   ARG23:D	 0.045		  5	-0.229, 0.200			    6,4			  94/150	S,N,K,E,I,T,D,Q,Y,R,M,F,C,H
  24	   N	   ASN24:D	 1.386		  1	 0.730, 1.440			    3,1			  98/150	C,A,H,F,V,T,K,N,W,G,R,D,Q,I,E,S
  25	   I	   ILE25:D	 0.472		  4	 0.074, 0.730			    5,3			 100/150	E,K,S,V,D,I,T,M,L,P,A,F
  26	   Q	   GLN26:D	 1.454		  1	 0.730, 2.543			    3,1			  45/150	D,V,Q,T,K,E,S,N,C,A,P,L,F,R
  27	   V	   VAL27:D	 1.306		  1	 0.730, 1.440			    3,1			 103/150	Y,F,H,L,A,C,P,N,K,T,V,M,S,E,I,D
  28	   D	   ASP28:D	 1.046		  2	 0.516, 1.440			    3,1			 104/150	E,S,N,Q,D,R,C,A,G
  29	   E	   GLU29:D	 0.881		  2	 0.516, 1.013			    3,2			 105/150	D,V,Q,T,K,E,S,N,P,A,F,M,R
  30	   A	   ALA30:D	 1.091		  2	 0.345, 1.440			    4,1			  31/150	G,A,S,N,E,K,T,D
  31	   N	   ASN31:D	-0.807		  7	-0.986,-0.718			    8,7			 117/150	S,N,R,T,H,D
  32	   L	   LEU32:D	 0.992		  2	 0.516, 1.440			    3,1			 121/150	K,S,V,I,Y,M,R,C,L,W
  33	   L	   LEU33:D	 1.203		  1	 0.730, 1.440			    3,1			 121/150	T,D,Q,N,S,K,E,F,W,C,H,L,Y,M,R
  34	   T	   THR34:D	 1.153		  1	 0.730, 1.440			    3,1			 122/150	Y,R,M,C,L,H,K,E,S,N,D,V,Q,I,T
  35	   W	   TRP35:D	-0.825		  8	-1.036,-0.718			    8,7			 122/150	M,Y,L,A,W,F,I
  36	   Q	   GLN36:D	 1.024		  2	 0.516, 1.440			    3,1			 123/150	M,R,E,S,Q,D,I,Y,H,L,A,F,K,N,V,T
  37	   G	   GLY37:D	-0.226		  6	-0.464,-0.037			    6,5			 124/150	M,G,A,C,L,S,I,T,V
  38	   L	   LEU38:D	 0.780		  3	 0.345, 1.013			    4,2			 128/150	N,K,T,V,Y,F,L,H,A,S,E,I,D,R,M,W,G
  39	   I	   ILE39:D	-0.693		  7	-0.883,-0.597			    8,7			 130/150	I,F,L,V,M
  40	   V	   VAL40:D	 1.627		  1	 1.013, 1.440			    2,1			 131/150	M,R,I,D,Q,S,E,F,P,C,A,L,T,V,N,K
  41	   P	   PRO41:D	-0.901		  8	-1.085,-0.775			    8,7			 145/150	T,Q,S,K,G,A,P,R
  42	   D	   ASP42:D	 2.465		  1	 1.440, 2.543			    1,1			 144/150	P,A,L,H,G,D,Q,V,T,I,K,E,N,S
  43	   N	   ASN43:D	 1.496		  1	 1.013, 1.440			    2,1			 148/150	T,D,Q,N,S,K,E,G,F,A,P,C,H,R
  44	   P	   PRO44:D	-0.608		  7	-0.775,-0.531			    7,7			 148/150	A,P,G,F,T,E,S
  45	   P	   PRO45:D	-0.311		  6	-0.531,-0.137			    7,5			 149/150	N,V,I,M,Y,L,C,P,A,F,G
  46	   Y	   TYR46:D	-1.002		  8	-1.134,-0.935			    8,8			 149/150	Y,N,C,F,W
  47	   D	   ASP47:D	 0.042		  5	-0.229, 0.200			    6,4			 149/150	K,E,N,S,D,Q,V,T,Y,R,A,H
  48	   K	   LYS48:D	 0.312		  4	-0.037, 0.516			    5,3			 148/150	R,P,A,H,L,G,K,X,N,S,D,Q
  49	   G	   GLY49:D	-1.175		  9	-1.339,-1.085			    9,8			 149/150	C,A,G,R,K
  50	   A	   ALA50:D	 1.162		  1	 0.730, 1.440			    3,1			 149/150	M,R,G,S,I,D,Q,Y,F,A,C,H,L,N,K,T,V
  51	   F	   PHE51:D	-1.153		  9	-1.285,-1.085			    9,8			 149/150	Y,F,W
  52	   R	   ARG52:D	 1.195		  1	 0.730, 1.440			    3,1			 149/150	G,R,I,Q,D,S,E,F,L,H,P,A,Y,T,V,N,K
  53	   I	   ILE53:D	-0.494		  7	-0.718,-0.390			    7,6			 149/150	L,V,A,F,I
  54	   E	   GLU54:D	 0.714		  3	 0.345, 1.013			    4,2			 150/150	V,T,K,N,A,C,L,H,F,D,Q,I,E,S,G,R
  55	   I	   ILE55:D	-0.549		  7	-0.718,-0.464			    7,6			 150/150	F,I,T,L,V,M
  56	   N	   ASN56:D	 2.543		  1	 1.440, 2.543			    1,1			 150/150	F,A,H,L,Y,T,V,N,K,W,R,I,D,Q,S,E
  57	   F	   PHE57:D	-0.410		  6	-0.597,-0.312			    7,6			 150/150	Y,S,L,V,F,I
  58	   P	   PRO58:D	-0.824		  8	-0.986,-0.718			    8,7			 150/150	D,Q,T,K,N,S,C,A,P,G
  59	   A	   ALA59:D	 2.455		  1	 1.440, 2.543			    1,1			 150/150	S,E,I,D,Q,R,M,G,N,K,T,V,A,P,H,L
  60	   E	   GLU60:D	 0.680		  3	 0.345, 1.013			    4,2			 150/150	E,K,N,S,Q,V,D,T,R,L,H,A,P,G
  61	   Y	   TYR61:D	-1.244		  9	-1.399,-1.184			    9,9			 150/150	H,F,Y
  62	   P	   PRO62:D	-1.423		  9	-1.537,-1.399			    9,9			 150/150	P,S
  63	   F	   PHE63:D	-0.691		  7	-0.883,-0.597			    8,7			 149/150	Y,M,H,L,F,S,N,I,T
  64	   K	   LYS64:D	-0.141		  5	-0.390,-0.037			    6,5			 150/150	G,H,L,A,R,Y,T,Q,D,N,S,E,K
  65	   P	   PRO65:D	-0.660		  7	-0.830,-0.531			    8,7			 150/150	S,P,A,H
  66	   P	   PRO66:D	-1.330		  9	-1.474,-1.285			    9,9			 150/150	L,A,P,R
  67	   K	   LYS67:D	-0.191		  6	-0.390,-0.037			    6,5			 150/150	M,R,L,H,A,E,K,S,N,V,Q,D,I,T
  68	   I	   ILE68:D	 0.023		  5	-0.229, 0.200			    6,4			 150/150	V,I,S,C,A,L,F,M
  69	   T	   THR69:D	 1.335		  1	 0.730, 1.440			    3,1			 150/150	R,M,G,S,E,I,Q,Y,F,A,H,L,N,K,T,V
  70	   F	   PHE70:D	-1.225		  9	-1.339,-1.134			    9,8			 150/150	F,I,L,A,C,M
  71	   K	   LYS71:D	 2.343		  1	 1.440, 2.543			    1,1			 149/150	R,M,G,A,L,S,N,K,E,I,T,D,Q,V
  72	   T	   THR72:D	-1.318		  9	-1.474,-1.285			    9,9			 150/150	T,Q,V,P,D,N,S
  73	   K	   LYS73:D	-0.523		  7	-0.718,-0.390			    7,6			 150/150	E,K,N,S,Q,T,R,P
  74	   I	   ILE74:D	-1.146		  9	-1.285,-1.085			    9,8			 150/150	I,T,V,L,M
  75	   Y	   TYR75:D	-0.834		  8	-0.986,-0.718			    8,7			 150/150	Y,W,F,C,V
  76	   H	   HIS76:D	-1.466		  9	-1.537,-1.474			    9,9			 150/150	H
  77	   P	   PRO77:D	-1.327		  9	-1.474,-1.285			    9,9			 150/150	C,P,S
  78	   N	   ASN78:D	-1.463		  9	-1.537,-1.474			    9,9			 150/150	G,S,N
  79	   I	   ILE79:D	-0.775		  7	-0.935,-0.659			    8,7			 150/150	N,I,T,F,V
  80	   D	   ASP80:D	-0.573		  7	-0.775,-0.464			    7,6			 149/150	Y,N,S,H,D,G,T
  81	   E	   GLU81:D	 0.769		  3	 0.345, 1.013			    4,2			 150/150	E,S,D,Q,I,R,G,W,K,N,V,T,Y,P,C,A,H,L
  82	   K	   LYS82:D	 0.517		  3	 0.200, 0.730			    4,3			 150/150	S,N,E,K,T,I,Q,D,M,R,G,L,H,A
  83	   G	   GLY83:D	-1.345		  9	-1.474,-1.285			    9,9			 150/150	R,S,G
  84	   Q	   GLN84:D	 0.004		  5	-0.229, 0.200			    6,4			 150/150	G,C,A,L,H,R,T,D,V,Q,S,N,K,E
  85	   V	   VAL85:D	-1.023		  8	-1.134,-0.935			    8,8			 150/150	V,I,T,F,M
  86	   C	   CYS86:D	-0.598		  7	-0.830,-0.464			    8,6			 150/150	F,G,C,L,H,R,D,V,N,S
  87	   L	   LEU87:D	-0.809		  7	-0.986,-0.718			    8,7			 150/150	I,V,L,A,M,Y
  88	   P	   PRO88:D	-0.742		  7	-0.935,-0.659			    8,7			 150/150	G,A,P,R,T,D,N,S,E
  89	   V	   VAL89:D	-0.168		  6	-0.390,-0.037			    6,5			 150/150	R,M,F,G,A,C,L,S,E,I,T,V,Q
  90	   I	   ILE90:D	-0.924		  8	-1.085,-0.830			    8,8			 150/150	M,T,I,F,V,L
  91	   S	   SER91:D	-0.297		  6	-0.531,-0.137			    7,5			 149/150	R,A,C,W,G,K,N,S,Q,T
  92	   A	   ALA92:D	 0.794		  3	 0.200, 1.013			    4,2			  36/150	N,S,K,T,V,D,R,G,A,P
  93	   E	   GLU93:D	-0.020		  5	-0.229, 0.074			    6,5			 150/150	T,Q,D,S,N,E,K,G,H,A
  94	   N	   ASN94:D	 0.296		  4	-0.037, 0.516			    5,3			 150/150	A,C,H,L,N,K,T,V,R,M,G,S,E,I,D,Q
  95	   W	   TRP95:D	-1.434		  9	-1.537,-1.399			    9,9			 149/150	X,W
  96	   K	   LYS96:D	-0.892		  8	-1.036,-0.830			    8,8			 149/150	N,S,X,K,T,Q,R,M,G,A
  97	   P	   PRO97:D	-1.139		  8	-1.285,-1.036			    9,8			 150/150	L,A,P,I,S
  98	   A	   ALA98:D	-0.509		  7	-0.718,-0.390			    7,6			 150/150	G,A,P,C,H,L,M,T,I,Q,V,S,N
  99	   T	   THR99:D	-0.009		  5	-0.312, 0.200			    6,4			 150/150	S,N,V,T,I,M,R,Y,L,H,A,F,W
 100	   K	  LYS100:D	-0.646		  7	-0.830,-0.531			    8,7			 150/150	R,G,C,A,H,S,N,K,T,D,Q
 101	   T	  THR101:D	-0.146		  5	-0.390,-0.037			    6,5			 150/150	M,A,P,L,V,T,I
 102	   D	  ASP102:D	 0.071		  5	-0.229, 0.200			    6,4			 150/150	P,C,A,L,Y,T,V,N,K,G,R,M,I,D,Q,S,E
 103	   Q	  GLN103:D	-0.573		  7	-0.775,-0.464			    7,6			 149/150	R,A,G,E,K,N,S,Q,D,T
 104	   V	  VAL104:D	-0.796		  7	-0.935,-0.718			    8,7			 149/150	I,T,A,L,V,S,M
 105	   I	  ILE105:D	-0.886		  8	-1.036,-0.775			    8,7			 149/150	C,P,V,L,I,F,M
 106	   Q	  GLN106:D	 0.077		  5	-0.229, 0.200			    6,4			 149/150	T,I,D,Q,V,S,N,K,E,A,L,Y,R,M
 107	   S	  SER107:D	-0.855		  8	-0.986,-0.775			    8,7			 149/150	V,D,T,K,S,L,C,A,G,F
 108	   L	  LEU108:D	-0.623		  7	-0.775,-0.531			    7,7			 149/150	A,L,V,I,F,Y
 109	   I	  ILE109:D	 0.686		  3	 0.345, 1.013			    4,2			 149/150	N,K,T,V,Y,F,P,A,C,L,H,S,I,Q,R,G
 110	   A	  ALA110:D	-0.133		  5	-0.390,-0.037			    6,5			 149/150	R,F,W,H,L,C,A,S,N,E,K,T,Q,V,D
 111	   L	  LEU111:D	-1.008		  8	-1.184,-0.935			    9,8			 149/150	M,K,L,V,P,F,I
 112	   V	  VAL112:D	-0.688		  7	-0.883,-0.597			    8,7			 149/150	I,F,P,V,L,M
 113	   N	  ASN113:D	 0.313		  4	-0.037, 0.516			    5,3			 149/150	F,C,A,L,H,T,V,N,G,R,M,I,D,Q,S,E
 114	   D	  ASP114:D	 0.263		  4	-0.037, 0.345			    5,4			 149/150	R,Y,H,A,N,S,E,K,T,Q,V,D
 115	   P	  PRO115:D	-0.902		  8	-1.085,-0.775			    8,7			 149/150	A,C,P
 116	   Q	  GLN116:D	-0.911		  8	-1.036,-0.830			    8,8			 149/150	M,F,G,L,N,S,E,Q,V,D
 117	   P	  PRO117:D	-0.316		  6	-0.531,-0.137			    7,5			 149/150	M,W,L,H,A,C,P,S,I,T,V,Q,D
 118	   E	  GLU118:D	-0.045		  5	-0.312, 0.074			    6,5			 149/150	N,S,K,E,T,D,Q,V,Y,R,G,A,H
 119	   H	  HIS119:D	-0.866		  8	-1.036,-0.775			    8,7			 149/150	Y,C,L,H,G,E,S,N,D,V,Q
 120	   P	  PRO120:D	-0.793		  7	-0.986,-0.659			    8,7			 149/150	S,Q,V,G,F,A,P,L
 121	   L	  LEU121:D	-0.985		  8	-1.134,-0.883			    8,8			 148/150	R,M,F,A,L,I,V,Q
 122	   R	  ARG122:D	-0.643		  7	-0.830,-0.531			    8,7			 148/150	F,A,R,M,I,D,V,Q,N,S,E
 123	   A	  ALA123:D	 0.216		  4	-0.037, 0.345			    5,4			 148/150	M,R,G,S,E,I,Q,D,H,L,C,P,A,N,K,T,V
 124	   D	  ASP124:D	 0.041		  5	-0.229, 0.200			    6,4			 148/150	E,K,N,S,Q,V,D,T,I,R,L,C,P,A,G
 125	   L	  LEU125:D	-0.765		  7	-0.935,-0.659			    8,7			 148/150	A,V,L,I,M,S
 126	   A	  ALA126:D	-1.161		  9	-1.285,-1.085			    9,8			 148/150	S,Y,W,G,T,V,A
 127	   E	  GLU127:D	 1.190		  1	 0.730, 1.440			    3,1			 147/150	V,Q,D,T,E,K,S,N,H,A,G,F,M,R,Y
 128	   E	  GLU128:D	 0.583		  3	 0.200, 0.730			    4,3			 146/150	H,L,A,M,Q,V,D,I,T,E,K,N
 129	   Y	  TYR129:D	-0.040		  5	-0.312, 0.074			    6,5			 146/150	M,Y,F,W,L,C
 130	   S	  SER130:D	 0.280		  4	-0.037, 0.516			    5,3			 146/150	E,K,N,S,V,Q,I,T,M,R,H,L,A
 131	   K	  LYS131:D	 1.474		  1	 1.013, 1.440			    2,1			 145/150	F,A,P,L,H,Y,T,N,K,G,R,I,D,Q,S,E
 132	   D	  ASP132:D	-0.254		  6	-0.464,-0.137			    6,5			 143/150	G,H,Y,D,Q,N,S,K,E
 133	   R	  ARG133:D	 0.462		  4	 0.074, 0.730			    5,3			 143/150	Y,R,P,A,L,H,W,K,E,S,Q,V,I
 134	   K	  LYS134:D	 1.925		  1	 1.013, 2.543			    2,1			 143/150	G,H,L,P,A,R,M,T,Q,V,D,N,S,E,K
 135	   K	  LYS135:D	 2.168		  1	 1.013, 2.543			    2,1			 143/150	K,E,N,S,D,Q,V,T,I,M,R,A,H,L,G
 136	   F	  PHE136:D	-0.827		  8	-0.986,-0.718			    8,7			 143/150	Y,A,H,F
 137	   C	  CYS137:D	 1.498		  1	 1.013, 1.440			    2,1			 140/150	M,R,G,W,E,D,Q,I,A,C,H,L,F,K,N,V,T
 138	   K	  LYS138:D	 1.250		  1	 0.730, 1.440			    3,1			 139/150	P,A,H,L,F,V,T,K,N,G,R,M,D,Q,E,S
 139	   N	  ASN139:D	 0.531		  3	 0.200, 0.730			    4,3			 139/150	H,L,R,M,T,I,V,Q,N,S,E,K
 140	   A	  ALA140:D	-1.293		  9	-1.399,-1.234			    9,9			 137/150	V,A,I,S
 141	   E	  GLU141:D	 0.397		  4	 0.074, 0.516			    5,3			 133/150	H,L,A,M,R,Q,V,I,T,E,K,S
 142	   E	  GLU142:D	 0.542		  3	 0.200, 0.730			    4,3			 129/150	K,E,N,S,D,Q,T,Y,R,M,A,H,L,F,G
 143	   F	  PHE143:D	 0.379		  4	-0.037, 0.516			    5,3			 129/150	Y,M,H,W,F,K,S,D,V
 144	   T	  THR144:D	-1.010		  8	-1.134,-0.935			    8,8			 128/150	T,I,V,P,C,A,N
 145	   K	  LYS145:D	 1.213		  1	 0.730, 1.440			    3,1			 127/150	I,D,Q,S,E,G,M,R,T,V,N,K,C,A,L,H
 146	   K	  LYS146:D	 0.051		  5	-0.229, 0.200			    6,4			 124/150	K,E,N,S,V,Q,T,I,M,R,A,L
 147	   Y	  TYR147:D	-0.201		  6	-0.464,-0.037			    6,5			 122/150	G,W,F,T,H,N,Y
 148	   G	         -	-1.002		  8	-1.134,-0.935			    8,8			 104/150	S,E,G,A,C
 149	   E	         -	 0.815		  2	 0.345, 1.013			    4,2			  50/150	M,R,L,A,N,S,E,K,T,I,V,Q,D
 150	   K	         -	-0.311		  6	-0.659,-0.037			    7,5			  33/150	G,P,R,I,Q,D,S,E,K
 151	   R	         -	-0.499		  7	-0.830,-0.312			    8,6			  29/150	N,K,R,E,T,L
 152	   P	         -	-0.903		  8	-1.234,-0.718			    9,7			  20/150	T,P
 153	   V	         -	 1.214		  1	 0.200, 2.543			    4,1			  10/150	M,V,P,A,F,I
 154	   D	         -	-0.836		  8	-1.285,-0.597			    9,7			   9/150	D


*Below the confidence cut-off - The calculations for this site were performed on less than 6 non-gaped homologue sequences,
or the confidence interval for the estimated score is equal to- or larger than- 4 color grades.
