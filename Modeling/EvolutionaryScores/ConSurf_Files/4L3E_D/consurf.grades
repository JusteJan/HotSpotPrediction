	 Amino Acid Conservation Scores
	=======================================

- POS: The position of the AA in the SEQRES derived sequence.
- SEQ: The SEQRES derived sequence in one letter code.
- 3LATOM: The ATOM derived sequence in three letter code, including the AA's positions as they appear in the PDB file and the chain identifier.
- SCORE: The normalized conservation scores.
- COLOR: The color scale representing the conservation scores (9 - conserved, 1 - variable).
- CONFIDENCE INTERVAL: When using the bayesian method for calculating rates, a confidence interval is assigned to each of the inferred evolutionary conservation scores.
- CONFIDENCE INTERVAL COLORS: When using the bayesian method for calculating rates. The color scale representing the lower and upper bounds of the confidence interval.
- MSA DATA: The number of aligned sequences having an amino acid (non-gapped) from the overall number of sequences at each position.
- RESIDUE VARIETY: The residues variety at each position of the multiple sequence alignment.

 POS	 SEQ	    3LATOM	SCORE		COLOR	CONFIDENCE INTERVAL	CONFIDENCE INTERVAL COLORS	MSA DATA	RESIDUE VARIETY
    	    	        	(normalized)	        	               
   1	   K	    LYS1:D	-0.750		  8*	-0.982,-0.657			    9,8			   5/109	K
   2	   E	    GLU2:D	 0.207		  4	-0.115, 0.428			    6,3			  32/109	K,Q,N,D,E
   3	   V	    VAL3:D	-0.932		  9	-0.982,-0.916			    9,9			  67/109	V,L
   4	   E	    GLU4:D	 0.002		  5	-0.210, 0.116			    6,4			  75/109	S,T,R,L,N,D,K,Q,A,V,E
   5	   Q	    GLN5:D	-0.952		  9	-0.991,-0.936			    9,9			  80/109	E,Q
   6	   N	    ASN6:D	 0.227		  4	-0.008, 0.428			    5,3			  81/109	A,H,P,I,E,Y,N,D,T,M,S,L
   7	   S	    SER7:D	 0.001		  5	-0.294, 0.116			    6,4			  81/109	G,K,Q,P,I,E,S,L,D
   8	   G	    GLY8:D	 2.780		  1	 1.269, 3.416			    1,1			  85/109	G,Q,P,A,V,E,I,S,T,R,L,D
   9	   P	    PRO9:D	 1.249		  1	 0.637, 1.269			    2,1			  89/109	D,N,L,R,F,T,S,E,V,A,P,H,Q,G,Y
  10	   L	   LEU10:D	 0.098		  5	-0.210, 0.259			    6,4			  94/109	T,M,Q,F,L,V,I
  11	   S	   SER11:D	 0.199		  4	-0.115, 0.259			    6,4			  95/109	L,F,R,T,S,D,N,I,V,A
  12	   V	   VAL12:D	 0.220		  4	-0.115, 0.428			    6,3			  99/109	S,C,T,M,L,P,A,V,I,E,Y
  13	   P	   PRO13:D	 0.430		  3	 0.116, 0.637			    4,2			 102/109	S,T,M,F,N,D,Y,Q,H,P,A,V,I,R,L,W,G
  14	   E	   GLU14:D	-0.649		  8	-0.740,-0.609			    8,8			 104/109	Y,K,Q,A,V,I,E
  15	   G	   GLY15:D	-0.180		  6	-0.439,-0.008			    7,5			 105/109	K,R,G,T,E,N,A
  16	   A	   ALA16:D	 0.881		  1	 0.428, 0.903			    3,1			 105/109	G,K,Q,A,E,V,T,M,S,L,R,N,D
  17	   I	   ILE17:D	 0.970		  1	 0.428, 1.269			    3,1			 105/109	D,N,F,S,M,T,I,E,P,H,A,K,Q,R,L,G
  18	   A	   ALA18:D	 0.619		  2	 0.259, 0.903			    4,1			 106/109	L,S,M,C,T,V,I,P,H,A,Y,G
  19	   S	   SER19:D	 0.404		  3	 0.116, 0.637			    4,2			 106/109	L,F,T,M,S,N,K,Q,G,E,I,V,A,H
  20	   L	   LEU20:D	 0.065		  5	-0.210, 0.259			    6,4			 106/109	F,L,M,V,I
  21	   N	   ASN21:D	 0.401		  3	 0.116, 0.637			    4,2			 106/109	A,P,H,E,G,Y,Q,K,N,D,T,S,W,R
  22	   C	   CYS22:D	-0.962		  9	-0.997,-0.954			    9,9			 106/109	C
  23	   T	   THR23:D	 0.327		  3	-0.008, 0.428			    5,3			 106/109	R,F,L,S,M,T,D,N,K,V,E,I
  24	   Y	   TYR24:D	-0.360		  7	-0.557,-0.294			    8,6			 106/109	P,H,M,Y,S,F
  25	   S	   SER25:D	 0.066		  5	-0.210, 0.116			    6,4			 106/109	K,Q,I,E,A,P,L,T,S,D,N
  26	   Y	   TYR26:D	 0.977		  1	 0.637, 1.269			    2,1			 105/109	N,D,S,T,R,P,A,V,I,E,Y,G,K
  27	   R	   ARG27:D	 0.260		  4	-0.008, 0.428			    5,3			 102/109	G,A,P,I,V,T,S,W,L,R
  28	   G	   GLY28:D	 2.169		  1	 1.269, 3.416			    1,1			  96/109	V,I,E,P,A,Q,Y,D,N,S,M,T,G,R,W,L
  29	   S	   SER29:D	 1.444		  1	 0.903, 1.841			    1,1			  95/109	S,T,M,F,L,N,D,Y,G,H,P,A,V,I,E
  30	   Q	   GLN30:D	 1.145		  1	 0.637, 1.269			    2,1			 106/109	A,H,P,I,E,V,Y,K,Q,N,D,T,S,F,R
  31	   S	   SER31:D	 0.861		  1	 0.428, 0.903			    3,1			 107/109	A,H,V,G,Y,Q,N,D,T,S,R,F
  32	   F	   PHE32:D	 0.204		  4	-0.115, 0.428			    6,3			 107/109	M,Y,W,L,F,I,V
  33	   F	   PHE33:D	 0.647		  2	 0.259, 0.903			    4,1			 107/109	Q,Y,H,P,A,F,R,W,L,M,T,N
  34	   W	   TRP34:D	-0.961		  9	-0.997,-0.954			    9,9			 107/109	W
  35	   Y	   TYR35:D	-0.495		  7	-0.657,-0.370			    8,7			 107/109	F,L,Y,D
  36	   R	   ARG36:D	 0.126		  4	-0.115, 0.259			    6,4			 107/109	Q,K,Y,V,I,H,R,W,L
  37	   Q	   GLN37:D	-0.919		  9	-0.969,-0.893			    9,9			 107/109	H,K,Q
  38	   Y	   TYR38:D	 1.580		  1	 0.903, 1.841			    1,1			 107/109	Q,K,Y,V,E,I,P,H,F,R,L,S,T,D,N
  39	   S	   SER39:D	 0.151		  4	-0.115, 0.259			    6,4			 107/109	P,H,A,Q,K,S,T,L
  40	   G	   GLY40:D	 0.647		  2	 0.259, 0.903			    4,1			 106/109	K,Q,G,E,A,L,W,R,S,D,N
  41	   K	   LYS41:D	 0.455		  3	 0.116, 0.637			    4,2			 107/109	A,E,G,K,Q,N,S,R
  42	   S	   SER42:D	 0.830		  1	 0.428, 0.903			    3,1			 107/109	R,S,T,K,Q,G,E,P,H,A
  43	   P	   PRO43:D	 0.071		  5	-0.210, 0.259			    6,4			 107/109	P,I,M,L
  44	   E	   GLU44:D	 0.557		  2	 0.259, 0.637			    4,2			 107/109	V,I,E,H,A,Q,K,D,R,L,T
  45	   L	   LEU45:D	 1.088		  1	 0.637, 1.269			    2,1			 107/109	V,A,H,Y,N,L,R,F,M,T,S
  46	   I	   ILE46:D	-0.354		  7	-0.557,-0.210			    8,6			 107/109	L,P,V,I
  47	   M	   MET47:D	 0.333		  3	-0.008, 0.428			    5,3			 107/109	V,I,Y,F,L,S,M,T
  48	   F	   PHE48:D	 1.239		  1	 0.637, 1.269			    2,1			 107/109	F,S,M,T,D,N,K,Q,Y,V,I,H,A,R,L,G
  49	   I	   ILE49:D	 0.629		  2	 0.259, 0.903			    4,1			 107/109	A,H,E,I,V,Y,Q,K,N,D,T,S,F,G,L,R
  50	   Y	   TYR50:D	 3.296		  1	 1.841, 3.416			    1,1			 108/109	K,Q,Y,V,E,I,P,H,A,F,S,T,D,N,G,R,W,L
  51	   S	   SER51:D	 0.451		  3	 0.116, 0.637			    4,2			 107/109	P,A,I,E,Y,G,K,Q,D,S,M,T,R,L,W
  52	   N	   ASN52:D	 2.733		  1	 1.269, 3.416			    1,1			 107/109	L,W,R,G,F,T,S,D,N,Q,K,Y,I,E,V,A
  53	   G	   GLY53:D	 1.193		  1	 0.637, 1.269			    2,1			 107/109	N,D,M,T,S,A,E,I,V,G,Y,K,Q
  54	   D	   ASP54:D	 1.437		  1	 0.903, 1.841			    1,1			  99/109	D,N,F,R,L,S,T,V,E,I,P,A,K,Q,G
  55	   K	   LYS55:D	 1.071		  1	 0.637, 1.269			    2,1			 103/109	T,M,S,D,N,Q,K,Y,I,E,V,A,P,H,L,R,G
  56	   E	   GLU56:D	 0.716		  2	 0.259, 0.903			    4,1			 104/109	D,N,L,T,M,S,E,V,A,P,H,K,Q,G
  57	   D	   ASP57:D	 1.464		  1	 0.903, 1.841			    1,1			  91/109	L,R,G,M,T,S,F,N,D,Q,K,A,H,P,E,V
  58	   G	   GLY58:D	 0.885		  1	 0.428, 1.269			    3,1			 108/109	N,D,T,S,R,A,P,H,E,G,Y,Q,K
  59	   R	   ARG59:D	-0.373		  7	-0.557,-0.294			    8,6			 108/109	T,M,S,R,F,N,D,G,K,V
  60	   F	   PHE60:D	 0.381		  3	 0.116, 0.637			    4,2			 108/109	Y,A,P,I,V,M,L,F
  61	   T	   THR61:D	 0.068		  5	-0.210, 0.259			    6,4			 108/109	K,Q,Y,E,I,V,A,H,R,M,T,S,N
  62	   A	   ALA62:D	 0.025		  5	-0.210, 0.116			    6,4			 108/109	G,A,V,I,S,T,F,L,W
  63	   Q	   GLN63:D	 0.293		  4	-0.008, 0.428			    5,3			 108/109	S,T,F,L,N,D,K,Q,H,A,V,E,I
  64	   L	   LEU64:D	 0.907		  1	 0.428, 1.269			    3,1			 107/109	E,I,V,A,G,Y,L,F,T,S
  65	   N	   ASN65:D	 0.656		  2	 0.259, 0.903			    4,1			 107/109	R,L,S,D,N,Q,K,G,V,I,E,P,H,A
  66	   K	   LYS66:D	 0.266		  4	-0.008, 0.428			    5,3			 107/109	S,T,M,R,N,D,G,Q,K,P,A,E,I
  67	   A	   ALA67:D	 0.936		  1	 0.428, 1.269			    3,1			 103/109	G,K,Q,P,A,E,S,T,M,R,N,D
  68	   S	   SER68:D	 1.072		  1	 0.637, 1.269			    2,1			 103/109	N,D,S,T,R,A,V,E,G,Q,K
  69	   Q	   GLN69:D	 0.084		  5	-0.115, 0.259			    6,4			 108/109	K,Q,G,A,L,R,T,S,D,N
  70	   Y	   TYR70:D	 0.258		  4	-0.008, 0.428			    5,3			 108/109	E,I,H,K,Q,Y,D,N,L,F,R,T,S
  71	   V	   VAL71:D	-0.095		  5	-0.294,-0.008			    6,5			 108/109	N,F,L,S,T,V,I,A,Y
  72	   S	   SER72:D	 0.094		  5	-0.115, 0.259			    6,4			 108/109	G,Y,A,P,H,I,E,V,T,S,L,F,N,D
  73	   L	   LEU73:D	-0.933		  9	-0.982,-0.916			    9,9			 108/109	F,L
  74	   L	   LEU74:D	 0.873		  1	 0.428, 0.903			    3,1			 108/109	N,L,F,R,M,T,S,E,I,V,H,Q,K,G,Y
  75	   I	   ILE75:D	-0.773		  9	-0.868,-0.740			    9,8			 108/109	K,F,L,I
  76	   R	   ARG76:D	 0.748		  2	 0.428, 0.903			    3,1			 108/109	V,I,E,P,H,A,Q,K,G,R,L,S,T
  77	   D	   ASP77:D	 0.044		  5	-0.210, 0.116			    6,4			 108/109	R,S,T,D,Q,G,E,P,H,A
  78	   S	   SER78:D	 0.224		  4	-0.008, 0.428			    5,3			 108/109	V,A,P,L,C,T,S
  79	   Q	   GLN79:D	-0.295		  6	-0.439,-0.210			    7,6			 109/109	V,I,E,H,A,K,Q,Y,N,R,L,S,T
  80	   P	   PRO80:D	 1.419		  1	 0.903, 1.841			    1,1			 109/109	M,T,L,W,H,P,A,V,I,E
  81	   S	   SER81:D	 0.412		  3	 0.116, 0.637			    4,2			 109/109	A,I,E,V,G,K,D,T,S,L,R
  82	   D	   ASP82:D	-0.919		  9	-0.969,-0.893			    9,9			 109/109	D,H,L
  83	   S	   SER83:D	-0.585		  8	-0.700,-0.501			    8,7			 109/109	A,E,V,T,S
  84	   A	   ALA84:D	-0.688		  8	-0.776,-0.609			    9,8			 109/109	V,A,G,T
  85	   T	   THR85:D	 0.283		  4	-0.008, 0.428			    5,3			 109/109	K,Q,V,I,E,L,S,T,M
  86	   Y	   TYR86:D	-0.892		  9	-0.954,-0.868			    9,9			 109/109	Y,W,D
  87	   L	   LEU87:D	 0.118		  4	-0.115, 0.259			    6,4			 109/109	H,I,Y,S,L,F
  88	   C	   CYS88:D	-0.903		  9	-0.969,-0.868			    9,9			 109/109	C,N
  89	   A	   ALA89:D	-0.631		  8	-0.740,-0.557			    8,8			 108/109	L,G,T,V,I,P,A
  90	   V	   VAL90:D	 1.294		  1	 0.637, 1.841			    2,1			 107/109	M,C,T,S,W,L,R,A,P,I,V,G,Y
  91	   N	   ASN91:D	 3.168		  1	 1.841, 3.416			    1,1			 105/109	L,W,R,G,N,D,C,M,T,S,F,A,P,I,E,V,Y,K,Q
  92	   F	   PHE92:D	 3.409		  1	 1.841, 3.416			    1,1			  96/109	Q,G,Y,E,V,A,W,R,F,C,T,S,D,N
  93	   G	   GLY93:D	 3.403		  1	 1.841, 3.416			    1,1			  98/109	G,Y,A,H,W,F,R,T,S,D,N
  94	   G	   GLY94:D	 3.400		  1	 1.841, 3.416			    1,1			 100/109	K,Q,Y,G,V,I,P,A,F,L,W,S,T,N
  95	   G	   GLY95:D	 3.410		  1	 1.841, 3.416			    1,1			 107/109	T,S,W,F,R,N,D,G,Y,Q,A,P,E,V
  96	   K	   LYS96:D	 0.574		  2	 0.259, 0.637			    4,2			 107/109	A,P,E,G,Q,K,N,T,M,S,F,R
  97	   L	   LEU97:D	 0.342		  3	-0.008, 0.428			    5,3			 108/109	M,T,F,L,W,Y,P,A,V,I
  98	   I	   ILE98:D	 1.291		  1	 0.637, 1.269			    2,1			 108/109	N,L,F,R,M,T,S,I,E,V,H,Q,K,Y
  99	   F	   PHE99:D	-0.577		  8	-0.700,-0.501			    8,7			 109/109	F,W,C
 100	   G	  GLY100:D	-0.795		  9	-0.893,-0.740			    9,8			 109/109	A,S,G
 101	   Q	  GLN101:D	 1.668		  1	 0.903, 1.841			    1,1			 109/109	D,R,L,S,T,I,E,H,P,A,K,Q,G
 102	   G	  GLY102:D	-0.903		  9	-0.969,-0.868			    9,9			 109/109	V,G
 103	   T	  THR103:D	-0.904		  9	-0.954,-0.868			    9,9			 109/109	A,I,T
 104	   E	  GLU104:D	 1.548		  1	 0.903, 1.841			    1,1			 107/109	Q,K,H,I,E,V,T,M,S,L,R,N
 105	   L	  LEU105:D	-0.397		  7	-0.557,-0.294			    8,6			 109/109	C,L,V
 106	   S	  SER106:D	 1.700		  1	 0.903, 1.841			    1,1			 109/109	R,F,L,S,T,M,N,K,Q,V,I,H,A
 107	   V	  VAL107:D	-0.382		  7	-0.557,-0.294			    8,6			 109/109	V,I,L
 108	   K	  LYS108:D	 3.227		  1	 1.841, 3.416			    1,1			 109/109	W,L,F,R,T,M,S,N,Q,K,Y,E,I,V,H
 109	   P	  PRO109:D	 0.191		  4	-0.115, 0.428			    6,3			 109/109	P,A,D,S,G,T,L
 110	   N	  ASN110:D	 0.016		  5	-0.210, 0.116			    6,4			 108/109	N,D,S,R,H,I,E,Y,Q,K
 111	   I	  ILE111:D	-0.543		  7	-0.657,-0.439			    8,7			 108/109	G,V,I,A,L,S,T,D
 112	   Q	  GLN112:D	-0.555		  8	-0.700,-0.501			    8,7			 104/109	Q,R,K,L,S,T,E
 113	   N	  ASN113:D	-0.431		  7	-0.557,-0.370			    8,7			 109/109	H,V,E,G,K,N,D,R
 114	   P	  PRO114:D	-0.556		  8	-0.700,-0.439			    8,7			 109/109	V,I,P,H,R,S,M,T
 115	   D	  ASP115:D	-0.509		  7	-0.657,-0.439			    8,7			 108/109	R,D,N,Q,K,E,A,H
 116	   P	  PRO116:D	-0.919		  9	-0.982,-0.893			    9,9			 108/109	Q,P
 117	   A	  ALA117:D	-0.748		  8	-0.841,-0.700			    9,8			 107/109	A,P,E,C,T,S,L,N
 118	   V	  VAL118:D	-0.826		  9	-0.893,-0.776			    9,9			 106/109	I,V,L,K
 119	   Y	  TYR119:D	-0.788		  9	-0.893,-0.740			    9,8			 106/109	Q,F,Y,V,I
 120	   Q	  GLN120:D	-0.832		  9	-0.893,-0.810			    9,9			 106/109	R,K,Q,E,V,A
 121	   L	  LEU121:D	-0.737		  8	-0.841,-0.657			    9,8			 106/109	N,V,M,S,L
 122	   R	  ARG122:D	-0.527		  7	-0.657,-0.439			    8,7			 104/109	L,R,F,T,S,P,K,G
 123	   D	  ASP123:D	-0.511		  7	-0.657,-0.439			    8,7			 104/109	E,D,A,N,K,G,S
 124	   S	  SER124:D	-0.545		  8	-0.700,-0.439			    8,7			 104/109	D,P,T,C,S
 125	   K	  LYS125:D	-0.533		  7	-0.657,-0.439			    8,7			 103/109	E,D,S,Q,K,R
 126	   S	  SER126:D	-0.846		  9	-0.916,-0.810			    9,9			 103/109	S,K,Q,N,A,E
 127	   S	  SER127:D	-0.421		  7	-0.557,-0.370			    8,7			 102/109	S,N,D,G,K,Q,A,V,E
 128	   D	  ASP128:D	-0.460		  7	-0.609,-0.370			    8,7			  97/109	Q,T,E,I,D,N
 129	   K	  LYS129:D	-0.426		  7	-0.557,-0.370			    8,7			 100/109	N,I,S,T,K
 130	   S	  SER130:D	-0.742		  8	-0.841,-0.700			    9,8			 100/109	I,V,T,S,Q
 131	   V	  VAL131:D	-0.763		  9	-0.868,-0.700			    9,8			 101/109	L,Y,V,I,A
 132	   C	  CYS132:D	-0.958		  9	-0.997,-0.936			    9,9			 103/109	C
 133	   L	  LEU133:D	-0.970		  9	-0.997,-0.954			    9,9			 103/109	L
 134	   F	  PHE134:D	-0.707		  8	-0.810,-0.657			    9,8			 103/109	A,V,Y,F
 135	   T	  THR135:D	-0.951		  9	-0.991,-0.936			    9,9			 103/109	K,T
 136	   D	  ASP136:D	-0.829		  9	-0.916,-0.776			    9,9			 103/109	R,G,E,D
 137	   F	  PHE137:D	-0.970		  9	-0.997,-0.954			    9,9			 103/109	F
 138	   D	  ASP138:D	-0.580		  8	-0.700,-0.501			    8,7			 102/109	A,P,E,G,K,Q,D,T,S
 139	   S	  SER139:D	-0.837		  9	-0.916,-0.810			    9,9			 102/109	K,R,S,M,V,I
 140	   Q	  GLN140:D	-0.595		  8	-0.700,-0.501			    8,7			  96/109	L,R,S,D,N,K,Q,E,V
 141	   T	  THR141:D	-0.155		  6	-0.370,-0.008			    7,5			 102/109	K,V,I,H,A,S,T,M,D
 142	   N	  ASN142:D	-0.932		  9	-0.982,-0.916			    9,9			 101/109	T,G,N
 143	   V	  VAL143:D	-0.582		  8	-0.700,-0.501			    8,7			 101/109	M,T,L,Q,K,E,I,V
 144	   S	  SER144:D	-0.375		  7	-0.557,-0.294			    8,6			  98/109	F,L,S,T,D,Q,V,E,P
 145	   Q	  GLN145:D	-0.396		  7	-0.557,-0.294			    8,6			  97/109	Q,K,E,T,S,R,N,D
 146	   S	  SER146:D	-0.425		  7	-0.557,-0.370			    8,7			  96/109	S,T,L,D,Y,K,P,A,V,I
 147	   K	  LYS147:D	-0.372		  7	-0.557,-0.294			    8,6			  96/109	L,S,T,M,E,P,A,Q,K
 148	   D	  ASP148:D	 0.041		  5	-0.210, 0.259			    6,4			  95/109	E,A,Q,K,G,D,N,R,W,S
 149	   S	  SER149:D	-0.711		  8	-0.810,-0.657			    9,8			  94/109	D,N,P,L,R,T,S
 150	   D	  ASP150:D	-0.068		  5	-0.294, 0.116			    6,4			  93/109	K,G,V,E,P,A,M,T,D
 151	   V	  VAL151:D	-0.635		  8	-0.776,-0.557			    9,8			  92/109	V,A,T
 152	   Y	  TYR152:D	-0.435		  7	-0.609,-0.294			    8,6			  92/109	I,H,F,L,Y
 153	   I	  ILE153:D	-0.414		  7	-0.557,-0.294			    8,6			  92/109	N,I,V,G,S,Q,K
 154	   T	  THR154:D	-0.609		  8	-0.740,-0.557			    8,8			  92/109	T,S,L,K
 155	   D	  ASP155:D	-0.527		  7	-0.657,-0.439			    8,7			  92/109	D,N,G,T
 156	   K	  LYS156:D	-0.501		  7	-0.657,-0.439			    8,7			  92/109	N,A,Q,K,S,G,T
 157	   C	  CYS157:D	-0.189		  6	-0.439,-0.008			    7,5			  92/109	C,T
 158	   V	  VAL158:D	-0.850		  9	-0.936,-0.810			    9,9			  92/109	V,A,T
 159	   L	  LEU159:D	-0.731		  8	-0.841,-0.657			    9,8			  92/109	M,L
 160	   D	  ASP160:D	-0.846		  9	-0.916,-0.810			    9,9			  92/109	T,N,E,D
 161	   M	  MET161:D	-0.978		  9	-0.999,-0.969			    9,9			  92/109	M
 162	   R	  ARG162:D	-0.600		  8	-0.740,-0.501			    8,7			  92/109	G,K,R,E
 163	   S	  SER163:D	-0.674		  8	-0.776,-0.609			    9,8			  92/109	T,S,I,V,A
 164	   M	  MET164:D	-0.649		  8	-0.776,-0.557			    9,8			  92/109	V,I,M,T,L
 165	   D	  ASP165:D	-0.699		  8	-0.810,-0.609			    9,8			  92/109	N,E,D,G
 166	   F	  PHE166:D	-0.858		  9	-0.936,-0.810			    9,9			  92/109	S,F
 167	   K	  LYS167:D	-0.974		  9	-0.999,-0.969			    9,9			  92/109	K
 168	   S	  SER168:D	-0.979		  9	-0.999,-0.969			    9,9			  92/109	S
 169	   N	  ASN169:D	-0.980		  9	-0.999,-0.969			    9,9			  92/109	N
 170	   S	  SER170:D	-0.694		  8	-0.810,-0.609			    9,8			  92/109	A,G,S
 171	   A	  ALA171:D	-0.665		  8	-0.776,-0.609			    9,8			  92/109	L,A,I,V
 172	   V	  VAL172:D	-0.708		  8	-0.810,-0.657			    9,8			  92/109	V,I,L
 173	   A	  ALA173:D	-0.728		  8	-0.841,-0.657			    9,8			  92/109	H,A,V,S,T,F,R
 174	   W	  TRP174:D	-0.853		  9	-0.954,-0.810			    9,9			  92/109	C,W
 175	   S	  SER175:D	-0.829		  9	-0.916,-0.776			    9,9			  92/109	N,D,G,S
 176	   N	  ASN176:D	-0.586		  8	-0.700,-0.501			    8,7			  91/109	N,A,D,S,K,Q
 177	   K	  LYS177:D	-0.469		  7	-0.609,-0.370			    8,7			  92/109	H,K,Q,G,N,R,T,S
 178	   S	  SER178:D	-0.576		  8	-0.700,-0.501			    8,7			  92/109	K,S,T,I,P,N,A
 179	   D	  ASP179:D	-0.590		  8	-0.740,-0.501			    8,7			  92/109	N,D,G,S
 180	   F	  PHE180:D	-0.679		  8	-0.810,-0.609			    9,8			  92/109	S,F,L,A
 181	   A	  ALA181:D	-0.167		  6	-0.370,-0.008			    7,5			  92/109	A,I,E,G,K,Q,N,S,T,L
 182	   C	  CYS182:D	-0.945		  9	-0.997,-0.916			    9,9			  92/109	C
 183	   A	  ALA183:D	-0.189		  6	-0.370,-0.115			    7,6			  92/109	T,Q,R,K,A,N,E
 184	   N	  ASN184:D	-0.390		  7	-0.557,-0.294			    8,6			  92/109	Y,G,K,A,E,S,N,D
 185	   A	  ALA185:D	-0.387		  7	-0.557,-0.294			    8,6			  92/109	D,I,V,A,N,T
 186	   F	  PHE186:D	-0.900		  9	-0.969,-0.868			    9,9			  92/109	S,F
 187	   N	  ASN187:D	-0.602		  8	-0.740,-0.557			    8,8			  91/109	D,N,R,Q,K,S
 188	   N	  ASN188:D	-0.384		  7	-0.557,-0.294			    8,6			  89/109	V,E,D,N,K,Q,G
 189	   S	  SER189:D	-0.534		  7	-0.657,-0.439			    8,7			  82/109	T,S,K,N,E
 190	   I	  ILE190:D	-0.614		  8	-0.776,-0.501			    9,7			  69/109	F,S,V,I
 191	   I	  ILE191:D	-0.533		  7	-0.700,-0.439			    8,7			  51/109	Y,F,L,I
 192	   P	  PRO192:D	-0.661		  8	-0.841,-0.557			    9,8			  50/109	S,P,A
 193	   E	  GLU193:D	-0.551		  8	-0.740,-0.439			    8,7			  48/109	E,N,A,S
 194	   D	  ASP194:D	-0.609		  8	-0.810,-0.501			    9,7			  44/109	N,E,D
 195	   T	  THR195:D	-0.843		  9	-0.954,-0.776			    9,9			  44/109	E,T
 196	   F	  PHE196:D	-0.905		  9	-0.991,-0.868			    9,9			  43/109	F
 197	   F	  PHE197:D	-0.905		  9	-0.991,-0.868			    9,9			  43/109	F
 198	   P	  PRO198:D	-0.883		  9	-0.991,-0.841			    9,9			  40/109	P
 199	   S	  SER199:D	-0.945		  9	-0.997,-0.916			    9,9			  39/109	S


*Below the confidence cut-off - The calculations for this site were performed on less than 6 non-gaped homologue sequences,
or the confidence interval for the estimated score is equal to- or larger than- 4 color grades.
