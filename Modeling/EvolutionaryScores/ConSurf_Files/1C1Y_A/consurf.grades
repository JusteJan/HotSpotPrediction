	 Amino Acid Conservation Scores
	=======================================

- POS: The position of the AA in the SEQRES derived sequence.
- SEQ: The SEQRES derived sequence in one letter code.
- 3LATOM: The ATOM derived sequence in three letter code, including the AA's positions as they appear in the PDB file and the chain identifier.
- SCORE: The normalized conservation scores.
- COLOR: The color scale representing the conservation scores (9 - conserved, 1 - variable).
- CONFIDENCE INTERVAL: When using the bayesian method for calculating rates, a confidence interval is assigned to each of the inferred evolutionary conservation scores.
- CONFIDENCE INTERVAL COLORS: When using the bayesian method for calculating rates. The color scale representing the lower and upper bounds of the confidence interval.
- MSA DATA: The number of aligned sequences having an amino acid (non-gapped) from the overall number of sequences at each position.
- RESIDUE VARIETY: The residues variety at each position of the multiple sequence alignment.

 POS	 SEQ	    3LATOM	SCORE		COLOR	CONFIDENCE INTERVAL	CONFIDENCE INTERVAL COLORS	MSA DATA	RESIDUE VARIETY
    	    	        	(normalized)	        	               
   1	   M	    MET1:A	-1.010		  9	-1.187,-0.899			    9,8			  20/150	L,M,I
   2	   R	    ARG2:A	 0.330		  4	-0.039, 0.527			    5,3			  40/150	N,M,K,L,T,Q,P,H,R
   3	   E	    GLU3:A	 0.317		  4	-0.039, 0.527			    5,3			  70/150	Q,T,K,L,R,H,E,A,S,N,I,D
   4	   Y	    TYR4:A	-0.068		  5	-0.313, 0.072			    6,5			 115/150	H,R,V,F,A,C,T,L,M,I,Y
   5	   K	    LYS5:A	-0.858		  8	-0.987,-0.804			    8,8			 132/150	N,I,K,C,Q,V,R,H
   6	   L	    LEU6:A	-0.216		  6	-0.390,-0.140			    6,5			 134/150	M,I,Y,F,V,A,C,L
   7	   V	    VAL7:A	-0.797		  8	-0.899,-0.754			    8,8			 135/150	A,G,V,L,C,I,M,S
   8	   V	    VAL8:A	-0.334		  6	-0.526,-0.230			    7,6			 135/150	F,V,I,M,L
   9	   L	    LEU9:A	-0.825		  8	-0.943,-0.754			    8,8			 136/150	L,M,F,I,V,A
  10	   G	   GLY10:A	-1.249		  9	-1.333,-1.225			    9,9			 136/150	G,D
  11	   S	   SER11:A	-0.031		  5	-0.230, 0.072			    6,5			 136/150	D,Y,S,N,G,A,R,E,F,P,C,T,K
  12	   G	   GLY12:A	-0.362		  6	-0.526,-0.230			    7,6			 136/150	M,D,S,E,R,H,P,V,G,A,Q,T,L
  13	   G	   GLY13:A	-0.135		  5	-0.390,-0.039			    6,5			 136/150	N,S,K,C,V,R,G,A
  14	   V	   VAL14:A	-1.251		  9	-1.307,-1.225			    9,9			 136/150	A,V,C,T
  15	   G	   GLY15:A	-1.249		  9	-1.333,-1.225			    9,9			 136/150	D,G
  16	   K	   LYS16:A	-1.269		  9	-1.333,-1.265			    9,9			 137/150	K,N,S
  17	   S	   SER17:A	-1.031		  9	-1.109,-0.987			    9,8			 138/150	A,T,S
  18	   A	   ALA18:A	-0.577		  7	-0.702,-0.526			    7,7			 138/150	G,A,V,C,T,S,N
  19	   L	   LEU19:A	-0.494		  7	-0.646,-0.390			    7,6			 139/150	M,I,V,C,L,K
  20	   T	   THR20:A	-0.617		  7	-0.754,-0.526			    8,7			 139/150	V,A,L,C,T,I,M,S
  21	   V	   VAL21:A	 0.015		  5	-0.230, 0.201			    6,4			 140/150	Y,S,N,M,I,T,L,K,Q,A,G,R,H,V,F
  22	   Q	   GLN22:A	-0.792		  8	-0.899,-0.702			    8,7			 140/150	F,I,R,K,C,S,Q
  23	   F	   PHE23:A	-0.523		  7	-0.702,-0.460			    7,7			 139/150	A,F,L,Y,C
  24	   V	   VAL24:A	-0.265		  6	-0.460,-0.140			    7,5			 139/150	M,I,S,N,A,G,H,V,F,T,C,L
  25	   Q	   GLN25:A	 0.254		  4	-0.039, 0.349			    5,4			 139/150	F,E,R,H,G,Q,L,K,T,I,D,N,S,Y
  26	   G	   GLY26:A	 0.292		  4	-0.039, 0.527			    5,3			 138/150	S,N,Y,I,D,Q,K,H,E,R,P,G,A
  27	   I	   ILE27:A	 1.194		  1	 0.750, 1.500			    2,1			 138/150	V,E,R,K,L,T,I,Y,A,P,H,C,Q,D,N,S
  28	   F	   PHE28:A	-0.973		  8	-1.069,-0.899			    9,8			 139/150	F,P,A,C,L,M,I,D,W,Y
  29	   V	   VAL29:A	 0.835		  2	 0.349, 1.045			    4,1			 139/150	N,S,D,C,Q,A,F,P,H,Y,I,M,L,K,T,G,V,R,E
  30	   E	   GLU30:A	 0.953		  2	 0.527, 1.045			    3,1			 134/150	S,N,W,D,Q,A,H,P,F,Y,M,T,K,L,G,E,R,V
  31	   K	   LYS31:A	 1.090		  1	 0.527, 1.500			    3,1			 138/150	Y,I,K,L,T,V,R,E,G,N,S,D,Q,F,H,A
  32	   Y	   TYR32:A	-0.316		  6	-0.526,-0.230			    7,6			 137/150	D,I,M,Y,W,S,A,F,V,H,L,C,T,Q
  33	   D	   ASP33:A	-0.055		  5	-0.313, 0.072			    6,5			 139/150	N,S,Y,I,D,Q,K,L,T,F,V,E,R,G,A
  34	   P	   PRO34:A	-0.285		  6	-0.460,-0.140			    7,5			 139/150	Q,T,L,R,H,V,P,A,S,N
  35	   T	   THR35:A	-1.140		  9	-1.225,-1.109			    9,9			 139/150	G,P,R,K,T,N,S
  36	   I	   ILE36:A	-0.905		  8	-1.028,-0.853			    9,8			 139/150	V,G,T,C,L,K,M,I,S
  37	   E	   GLU37:A	-0.791		  8	-0.943,-0.702			    8,7			 139/150	N,D,M,I,T,Q,G,A,E,V
  38	   D	   ASP38:A	-0.829		  8	-0.943,-0.754			    8,8			 141/150	V,E,R,G,A,K,D,N,S
  39	   S	   SER39:A	-0.548		  7	-0.702,-0.460			    7,7			 142/150	I,M,Y,W,S,A,V,F,E,L,T,C,Q
  40	   Y	   TYR40:A	-0.631		  7	-0.804,-0.526			    8,7			 142/150	M,I,Y,R,H,F,V,G,A,Q,T,K,L
  41	   R	   ARG41:A	 0.093		  5	-0.140, 0.201			    5,4			 142/150	T,L,K,R,E,V,G,Y,M,I,Q,H,A,S,W,N
  42	   K	   LYS42:A	-0.409		  6	-0.588,-0.313			    7,6			 143/150	A,G,P,V,R,H,L,K,T,C,Q,M,Y,N,S
  43	   Q	   GLN43:A	 0.222		  4	-0.039, 0.349			    5,4			 143/150	P,F,H,A,Q,D,N,S,V,E,R,G,K,L,T,I,M,Y
  44	   V	   VAL44:A	 0.039		  5	-0.230, 0.201			    6,4			 143/150	D,I,M,Y,A,V,F,H,L,K,C,T
  45	   E	   GLU45:A	 1.861		  1	 1.045, 2.693			    1,1			 143/150	C,Q,A,F,H,N,S,D,K,L,T,G,V,E,R,Y,I,M
  46	   V	   VAL46:A	 0.072		  5	-0.140, 0.201			    5,4			 145/150	F,V,H,E,R,A,Q,L,T,C,I,M,S,Y
  47	   D	   ASP47:A	-0.055		  5	-0.313, 0.072			    6,5			 146/150	E,H,R,V,G,Q,T,K,I,D,S,N
  48	   C	   CYS48:A	 0.798		  2	 0.349, 1.045			    4,1			 146/150	K,T,C,Q,G,A,E,R,H,N,S,D
  49	   Q	   GLN49:A	 0.714		  3	 0.349, 0.750			    4,2			 146/150	F,H,A,Q,C,D,N,S,V,E,R,G,K,L,T,I
  50	   Q	   GLN50:A	 2.301		  1	 1.045, 2.693			    1,1			 146/150	I,M,L,K,T,G,V,R,E,W,N,S,D,C,Q,A,P,H
  51	   C	   CYS51:A	 0.252		  4	-0.039, 0.349			    5,4			 146/150	T,C,L,R,F,V,A,G,S,Y,M,I
  52	   M	   MET52:A	 0.342		  4	 0.072, 0.527			    5,3			 146/150	A,E,R,P,V,T,L,K,Q,D,M,I,Y,S,N
  53	   L	   LEU53:A	-0.674		  7	-0.804,-0.588			    8,7			 146/150	C,T,L,A,R,F,P,V,M,I
  54	   E	   GLU54:A	-0.400		  6	-0.588,-0.313			    7,6			 148/150	T,K,L,Q,A,G,E,H,V,Y,S,N,D,I
  55	   I	   ILE55:A	-0.902		  8	-0.987,-0.853			    8,8			 148/150	L,T,V,I
  56	   L	   LEU56:A	-0.455		  7	-0.646,-0.313			    7,6			 149/150	V,F,R,Q,L,K,T,I,M,W,Y
  57	   D	   ASP57:A	-1.287		  9	-1.333,-1.265			    9,9			 149/150	N,D
  58	   T	   THR58:A	-1.250		  9	-1.307,-1.225			    9,9			 149/150	S,T,P,R
  59	   A	   ALA59:A	-1.042		  9	-1.148,-0.987			    9,8			 149/150	S,N,W,P,V,G,A,T,C
  60	   G	   GLY60:A	-1.103		  9	-1.225,-1.069			    9,9			 149/150	E,G,N,S
  61	   T	   THR61:A	-0.983		  8	-1.069,-0.943			    9,8			 149/150	N,S,D,M,K,T,Q,A,G,V
  62	   E	   GLU62:A	-0.762		  8	-0.899,-0.702			    8,7			 147/150	N,Y,D,Q,K,F,E,H,G,A
  63	   Q	   GLN63:A	-0.672		  7	-0.804,-0.588			    8,7			 148/150	N,S,D,Q,K,T,P,E,R,G,A
  64	   F	   PHE64:A	-0.594		  7	-0.754,-0.526			    8,7			 148/150	D,Y,H,E,F,A,G,L
  65	   T	   THR65:A	-0.343		  6	-0.526,-0.230			    7,6			 148/150	A,E,H,R,P,V,T,K,L,D,I,Y,S,N
  66	   A	   ALA66:A	-0.358		  6	-0.526,-0.230			    7,6			 148/150	V,P,G,A,Q,L,T,C,I,D,N,W,S,Y
  67	   M	   MET67:A	-0.640		  7	-0.754,-0.588			    8,7			 148/150	R,V,F,Q,T,C,L,M,I,S
  68	   R	   ARG68:A	-0.530		  7	-0.702,-0.460			    7,7			 148/150	R,V,P,A,G,Q,T,C,K,L,M,I,S,N,Y
  69	   D	   ASP69:A	-0.151		  6	-0.390,-0.039			    6,5			 150/150	S,D,M,K,T,Q,A,G,P,E,H,R
  70	   L	   LEU70:A	-0.379		  6	-0.526,-0.313			    7,6			 150/150	A,H,P,Q,D,S,N,W,G,E,R,V,T,K,L,M,I,Y
  71	   Y	   TYR71:A	-0.650		  7	-0.804,-0.588			    8,7			 150/150	Q,L,C,F,P,H,A,N,W,S,Y
  72	   M	   MET72:A	-0.608		  7	-0.754,-0.526			    8,7			 149/150	Y,S,I,M,L,A,V,F
  73	   K	   LYS73:A	-0.524		  7	-0.702,-0.460			    7,7			 149/150	M,I,D,S,R,V,A,Q,T,L,K
  74	   N	   ASN74:A	 0.110		  5	-0.140, 0.201			    5,4			 149/150	V,R,E,G,K,T,I,M,Y,F,H,A,Q,D,W,N,S
  75	   G	   GLY75:A	-0.602		  7	-0.754,-0.526			    8,7			 149/150	V,F,E,G,A,K,C,T,I,N,S
  76	   Q	   GLN76:A	-0.184		  6	-0.390,-0.039			    6,5			 149/150	S,N,D,M,L,K,Q,A,R,E,H
  77	   G	   GLY77:A	-0.586		  7	-0.754,-0.460			    8,7			 149/150	S,G,A,V,I
  78	   F	   PHE78:A	-0.839		  8	-0.943,-0.754			    8,8			 150/150	V,F,I,A,C,Y,L
  79	   A	   ALA79:A	-0.006		  5	-0.230, 0.072			    6,5			 150/150	A,V,I,M,L
  80	   L	   LEU80:A	-0.446		  7	-0.646,-0.313			    7,6			 150/150	F,V,I,M,L,C
  81	   V	   VAL81:A	-1.125		  9	-1.187,-1.109			    9,9			 150/150	C,T,L,A,M,I,V
  82	   Y	   TYR82:A	-0.951		  8	-1.069,-0.899			    9,8			 150/150	Y,F
  83	   S	   SER83:A	-0.940		  8	-1.028,-0.899			    9,8			 150/150	T,S,N,A,G,D,E
  84	   I	   ILE84:A	-0.534		  7	-0.702,-0.460			    7,7			 150/150	V,I,M,A,L,C,T
  85	   T	   THR85:A	-0.797		  8	-0.899,-0.754			    8,8			 150/150	G,A,V,H,T,C,Q,D,I,N,S
  86	   A	   ALA86:A	-0.094		  5	-0.313, 0.072			    6,5			 150/150	D,N,S,A,P,V,E,R,K,L,T,C,Q
  87	   Q	   GLN87:A	 1.022		  1	 0.527, 1.045			    3,1			 150/150	V,E,R,G,K,L,T,I,M,Y,F,P,H,A,Q,D,W,S
  88	   S	   SER88:A	 1.015		  1	 0.527, 1.045			    3,1			 150/150	N,S,I,D,Q,K,T,P,V,R,E,H,G,A
  89	   T	   THR89:A	-1.028		  9	-1.109,-0.987			    9,8			 150/150	S,T
  90	   F	   PHE90:A	-0.742		  8	-0.899,-0.646			    8,7			 150/150	Y,L,W,M,I,V,F
  91	   N	   ASN91:A	 2.526		  1	 1.500, 2.693			    1,1			 150/150	Y,I,K,L,T,G,V,E,R,N,S,D,C,Q,A,H
  92	   D	   ASP92:A	-0.043		  5	-0.230, 0.072			    6,5			 150/150	A,V,R,E,H,K,C,Q,D,I,M,Y,N,S
  93	   L	   LEU93:A	-0.275		  6	-0.460,-0.140			    7,5			 150/150	M,V,I,A,T,L
  94	   Q	   GLN94:A	 1.568		  1	 1.045, 1.500			    1,1			 150/150	A,P,H,C,Q,D,N,S,V,E,R,K,L,T,I,M,Y
  95	   D	   ASP95:A	 2.300		  1	 1.045, 2.693			    1,1			 150/150	E,R,V,G,T,L,K,M,I,Y,H,P,F,A,Q,D,S,N
  96	   L	   LEU96:A	-0.212		  6	-0.390,-0.039			    6,5			 150/150	L,C,V,F,E,Y,W,S,I,M
  97	   R	   ARG97:A	 0.596		  3	 0.201, 0.750			    4,2			 149/150	Y,N,I,M,K,L,Q,A,F,V,H,R
  98	   E	   GLU98:A	 1.056		  1	 0.527, 1.045			    3,1			 149/150	N,S,D,I,L,K,T,Q,G,A,R,E,H
  99	   Q	   GLN99:A	-0.303		  6	-0.460,-0.230			    7,6			 149/150	Q,K,L,T,F,E,H,N,S,Y,M,D
 100	   I	  ILE100:A	-0.752		  8	-0.899,-0.702			    8,7			 149/150	L,C,V,F,H,A,G,W,S,I
 101	   L	  LEU101:A	 1.443		  1	 0.750, 1.500			    2,1			 149/150	Y,M,I,T,L,K,G,R,E,V,S,N,W,D,C,Q,A,H,F
 102	   R	  ARG102:A	 0.481		  3	 0.201, 0.750			    4,2			 150/150	G,A,E,H,R,P,T,L,K,Q,D,S,N
 103	   V	  VAL103:A	 0.714		  3	 0.349, 0.750			    4,2			 150/150	K,L,T,V,R,E,G,Y,I,M,Q,F,H,A,N,W,S,D
 104	   K	  LYS104:A	-0.188		  6	-0.390,-0.039			    6,5			 150/150	G,A,F,V,R,H,L,K,C,T,I,M,Y,N,S
 105	   D	  ASP105:A	 0.496		  3	 0.072, 0.750			    5,2			  86/150	I,D,S,N,H,R,E,G,A,Q,K
 106	   T	  THR106:A	 1.099		  1	 0.527, 1.500			    3,1			  92/150	D,I,S,N,A,G,E,R,V,P,C,T,L,K,Q
 107	   E	  GLU107:A	 1.923		  1	 1.045, 2.693			    1,1			  92/150	S,N,D,Q,H,F,P,A,I,T,L,K,R,E,V,G
 108	   D	  ASP108:A	 1.560		  1	 1.045, 1.500			    1,1			 148/150	L,K,T,V,R,E,G,Y,I,Q,C,P,F,H,A,N,S,D
 109	   V	  VAL109:A	 0.043		  5	-0.230, 0.201			    6,4			 149/150	P,F,V,R,E,A,L,T,C,I,M,S,Y
 110	   P	  PRO110:A	-0.044		  5	-0.313, 0.072			    6,5			 149/150	P,V,E,H,R,K,L,C,T,Q,D,I,M,N,S
 111	   M	  MET111:A	-0.068		  5	-0.313, 0.072			    6,5			 149/150	M,I,Y,A,R,V,F,C,T,L,K
 112	   I	  ILE112:A	-0.197		  6	-0.390,-0.039			    6,5			 149/150	T,C,L,V,F,A,Y,M,I
 113	   L	  LEU113:A	-0.593		  7	-0.754,-0.526			    8,7			 149/150	M,V,I,L
 114	   V	  VAL114:A	-0.822		  8	-0.943,-0.754			    8,8			 148/150	V,I,M,A,L,C
 115	   G	  GLY115:A	-0.821		  8	-0.943,-0.754			    8,8			 149/150	A,G
 116	   N	  ASN116:A	-1.212		  9	-1.265,-1.187			    9,9			 149/150	T,S,N,A
 117	   K	  LYS117:A	-1.272		  9	-1.333,-1.265			    9,9			 148/150	H,K,Q
 118	   C	  CYS118:A	 0.994		  2	 0.527, 1.045			    3,1			 148/150	Q,K,L,C,T,V,F,G,A,N,S,Y,I,M
 119	   D	  ASP119:A	-1.263		  9	-1.333,-1.225			    9,9			 148/150	D,E,R
 120	   L	  LEU120:A	-0.444		  7	-0.646,-0.313			    7,6			 145/150	W,D,M,I,K,L,A,H,R,E,V
 121	   E	  GLU121:A	 1.984		  1	 1.045, 2.693			    1,1			 143/150	I,Y,G,R,E,V,T,L,K,D,S,N,A,H,P,C,Q
 122	   D	  ASP122:A	 1.743		  1	 1.045, 1.500			    1,1			 145/150	Y,M,T,K,L,G,E,R,V,S,N,D,Q,A,H,P,F
 123	   E	  GLU123:A	 1.534		  1	 1.045, 1.500			    1,1			 146/150	H,A,Q,C,D,S,N,R,E,V,G,T,K,L,M,Y
 124	   R	  ARG124:A	-1.019		  9	-1.109,-0.987			    9,8			 148/150	K,V,E,R,Y,D,I,M
 125	   V	  VAL125:A	 0.811		  2	 0.349, 1.045			    4,1			 146/150	D,I,M,S,A,G,V,E,R,K,L,T,Q
 126	   V	  VAL126:A	-1.085		  9	-1.187,-1.028			    9,9			 147/150	D,G,V,I,M,L
 127	   G	  GLY127:A	 0.445		  3	 0.201, 0.527			    4,3			 146/150	A,F,P,C,Q,D,N,S,G,V,R,E,K,L,T,I
 128	   K	  LYS128:A	 2.624		  1	 1.500, 2.693			    1,1			 146/150	M,I,Y,R,E,V,T,K,L,D,S,W,N,H,P,F,A,Q,C
 129	   E	  GLU129:A	 0.795		  2	 0.349, 1.045			    4,1			 145/150	Q,L,K,T,E,R,G,A,N,S,Y,I,M,D
 130	   Q	  GLN130:A	 0.308		  4	 0.072, 0.527			    5,3			 145/150	D,I,M,Y,S,A,V,R,E,H,L,K,T,Q
 131	   G	  GLY131:A	-0.445		  7	-0.646,-0.313			    7,6			 144/150	L,T,V,R,G,A,S,I,M
 132	   Q	  GLN132:A	 1.082		  1	 0.527, 1.500			    3,1			 144/150	I,M,Y,G,V,R,E,L,K,N,W,S,A,F,H,C,Q
 133	   N	  ASN133:A	 2.595		  1	 1.500, 2.693			    1,1			 144/150	I,M,D,N,S,V,R,E,H,A,G,Q,K,L,T
 134	   L	  LEU134:A	 0.181		  4	-0.140, 0.349			    5,4			 143/150	I,M,Y,W,A,V,F,R,L,K,T,C
 135	   A	  ALA135:A	-0.949		  8	-1.069,-0.899			    9,8			 142/150	T,C,L,V,G,A,S,I
 136	   R	  ARG136:A	 2.316		  1	 1.045, 2.693			    1,1			 143/150	S,N,W,D,Q,H,P,A,I,T,L,K,R,E,V,G
 137	   Q	  GLN137:A	 1.193		  1	 0.750, 1.500			    2,1			 142/150	Y,N,S,D,L,K,T,Q,G,A,F,V,H,R,E
 138	   W	  TRP138:A	 1.070		  1	 0.527, 1.500			    3,1			 142/150	L,K,Q,G,A,F,R,E,H,Y,W,N,I,M
 139	   C	  CYS139:A	 1.204		  1	 0.349, 1.500			    4,1			  11/150	S,N,C,V,P,G
 140	   N	  ASN140:A	 0.912		  2	 0.527, 1.045			    3,1			 142/150	M,D,S,N,H,E,R,A,G,Q,K
 141	   C	  CYS141:A	 0.060		  5	-0.140, 0.201			    5,4			 142/150	A,G,V,P,F,H,L,C,T,I,M,N,S
 142	   A	  ALA142:A	 2.454		  1	 1.500, 2.693			    1,1			 143/150	L,K,G,V,E,R,Y,I,M,C,Q,A,P,F,N,S,D
 143	   F	  PHE143:A	-0.405		  6	-0.588,-0.313			    7,6			 143/150	I,Y,W,A,F,V,H,L,C
 144	   L	  LEU144:A	 1.146		  1	 0.750, 1.500			    2,1			 143/150	Y,I,M,K,L,T,Q,F,V,E
 145	   E	  GLU145:A	-1.259		  9	-1.333,-1.225			    9,9			 143/150	P,E,N
 146	   S	  SER146:A	-0.455		  7	-0.588,-0.390			    7,6			 143/150	S,I,L,C,T,P,V,A
 147	   S	  SER147:A	-1.259		  9	-1.307,-1.225			    9,9			 143/150	T,S,D,A,F
 148	   A	  ALA148:A	-1.197		  9	-1.265,-1.187			    9,9			 141/150	A,G,V,T,S
 149	   K	  LYS149:A	-0.713		  7	-0.853,-0.646			    8,7			 141/150	A,R,E,V,C,T,K,L,M,S
 150	   S	  SER150:A	 1.343		  1	 0.750, 1.500			    2,1			 141/150	S,N,D,Q,H,F,A,Y,M,I,T,K,L,R,E,V
 151	   K	  LYS151:A	 0.502		  3	 0.201, 0.750			    4,2			 140/150	S,X,N,D,Q,C,K,R,H,E,P,A,G
 152	   I	  ILE152:A	 2.693		  1	 1.500, 2.693			    1,1			 139/150	Y,I,M,L,K,T,V,E,R,G,W,N,S,D,Q,C,H,A
 153	   N	  ASN153:A	-0.804		  8	-0.943,-0.754			    8,8			 140/150	G,A,E,R,H,L,T,C,Q,D,M,Y,N,S
 154	   V	  VAL154:A	-0.847		  8	-0.943,-0.804			    8,8			 139/150	N,T,L,M,V,I,A
 155	   N	  ASN155:A	 1.191		  1	 0.750, 1.500			    2,1			 139/150	I,M,Y,G,V,R,E,K,L,T,D,N,S,A,F,H,C,Q
 156	   E	  GLU156:A	 0.574		  3	 0.201, 0.750			    4,2			 139/150	A,G,E,R,V,T,K,L,Q,D,S,N
 157	   I	  ILE157:A	 0.026		  5	-0.230, 0.201			    6,4			 139/150	T,C,L,V,G,A,S,M,I
 158	   F	  PHE158:A	-1.070		  9	-1.187,-1.028			    9,9			 138/150	F,V,W,L,Y
 159	   Y	  TYR159:A	 2.619		  1	 1.500, 2.693			    1,1			 136/150	V,R,E,K,L,T,I,M,Y,A,F,H,C,Q,D,N,W,S
 160	   D	  ASP160:A	 1.648		  1	 1.045, 1.500			    1,1			 133/150	I,M,Y,G,V,E,R,L,K,T,D,N,S,A,H,C,Q
 161	   L	  LEU161:A	-0.376		  6	-0.588,-0.230			    7,6			 132/150	N,I,M,L,C,A,F,V
 162	   V	  VAL162:A	-0.420		  6	-0.588,-0.313			    7,6			 127/150	A,G,P,V,F,L,T,C,I,M,S
 163	   R	  ARG163:A	 0.276		  4	-0.039, 0.527			    5,3			 117/150	H,A,Q,D,N,S,V,R,E,G,K,L,T,I,M,Y
 164	   Q	  GLN164:A	 0.298		  4	-0.039, 0.527			    5,3			 108/150	A,H,E,R,K,L,C,Q,D,I,M,S
 165	   I	  ILE165:A	-0.722		  8	-0.853,-0.646			    8,7			  97/150	L,Y,T,I,V,M,A
 166	   N	  ASN166:A	 0.261		  4	-0.140, 0.527			    5,3			  54/150	P,E,H,R,L,K,Q,D,I,M,N,S
 167	   R	  ARG167:A	-1.198		  9	-1.333,-1.148			    9,9			  19/150	R


*Below the confidence cut-off - The calculations for this site were performed on less than 6 non-gaped homologue sequences,
or the confidence interval for the estimated score is equal to- or larger than- 4 color grades.
