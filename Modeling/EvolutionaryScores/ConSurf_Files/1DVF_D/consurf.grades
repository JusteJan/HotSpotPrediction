	 Amino Acid Conservation Scores
	=======================================

- POS: The position of the AA in the SEQRES derived sequence.
- SEQ: The SEQRES derived sequence in one letter code.
- 3LATOM: The ATOM derived sequence in three letter code, including the AA's positions as they appear in the PDB file and the chain identifier.
- SCORE: The normalized conservation scores.
- COLOR: The color scale representing the conservation scores (9 - conserved, 1 - variable).
- CONFIDENCE INTERVAL: When using the bayesian method for calculating rates, a confidence interval is assigned to each of the inferred evolutionary conservation scores.
- CONFIDENCE INTERVAL COLORS: When using the bayesian method for calculating rates. The color scale representing the lower and upper bounds of the confidence interval.
- MSA DATA: The number of aligned sequences having an amino acid (non-gapped) from the overall number of sequences at each position.
- RESIDUE VARIETY: The residues variety at each position of the multiple sequence alignment.

 POS	 SEQ	    3LATOM	SCORE		COLOR	CONFIDENCE INTERVAL	CONFIDENCE INTERVAL COLORS	MSA DATA	RESIDUE VARIETY
    	    	        	(normalized)	        	               
   1	   Q	    GLN1:D	-0.618		  8	-0.704,-0.552			    9,8			  90/150	E,Q
   2	   V	    VAL2:D	-0.537		  8	-0.638,-0.500			    8,8			 112/150	V,I,K,M,A,L
   3	   Q	    GLN3:D	-0.490		  8	-0.598,-0.440			    8,7			 118/150	V,A,P,K,N,E,H,Q,R,L,T
   4	   L	    LEU4:D	-0.771		  9	-0.818,-0.754			    9,9			 123/150	L,P,F
   5	   Q	    GLN5:D	-0.266		  6	-0.440,-0.193			    7,6			 124/150	E,I,K,N,T,L,D,H,Q,V
   6	   Q	    GLN6:D	-0.645		  8	-0.704,-0.598			    9,8			 130/150	L,T,Q,K,S,E,V
   7	   S	    SER7:D	-0.789		  9	-0.818,-0.774			    9,9			 132/150	S,T,P,A
   8	   G	    GLY8:D	-0.437		  7	-0.552,-0.370			    8,7			 132/150	R,D,N,G,E,S,A,P,V
   9	   T	    THR9:D	-0.274		  6	-0.440,-0.193			    7,6			 133/150	A,P,N,S,E,G,L,R,T
  10	   E	   GLU10:D	-0.237		  6	-0.370,-0.193			    7,6			 138/150	V,A,G,E,S,R,D,Q,H
  11	   L	   LEU11:D	-0.172		  6	-0.370,-0.078			    7,5			 145/150	A,M,V,Q,T,L,S,I
  12	   V	   VAL12:D	-0.359		  7	-0.500,-0.289			    8,7			 145/150	V,A,E,I,G,K,T,L,R,D,Q
  13	   K	   LYS13:D	-0.245		  6	-0.370,-0.193			    7,6			 145/150	A,V,L,R,T,Q,H,K,N,S
  14	   S	   SER14:D	-0.533		  8	-0.638,-0.500			    8,8			 146/150	P,A,L,R,S,F
  15	   G	   GLY15:D	-0.551		  8	-0.638,-0.500			    8,8			 147/150	Q,T,R,L,K,S,E,G,A
  16	   A	   ALA16:D	-0.349		  7	-0.500,-0.289			    8,7			 147/150	A,D,Q,L,T,R,G,S,E,N
  17	   S	   SER17:D	-0.684		  9	-0.730,-0.673			    9,9			 147/150	T,L,S,G,N,K,Y
  18	   V	   VAL18:D	-0.578		  8	-0.673,-0.552			    9,8			 147/150	H,L,T,R,I,M,V,F
  19	   K	   LYS19:D	-0.319		  7	-0.440,-0.289			    7,7			 147/150	X,A,S,I,E,K,N,Q,H,C,T,R
  20	   L	   LEU20:D	-0.502		  8	-0.598,-0.440			    8,7			 148/150	I,V,L,M,A
  21	   S	   SER21:D	-0.651		  8	-0.704,-0.638			    9,8			 148/150	V,F,A,K,G,S,Q,H,T,R,C
  22	   C	   CYS22:D	-0.847		  9	-0.859,-0.844			    9,9			 148/150	C
  23	   T	   THR23:D	-0.270		  6	-0.440,-0.193			    7,6			 149/150	S,E,K,Q,L,R,T,V,A
  24	   A	   ALA24:D	-0.330		  7	-0.440,-0.289			    7,7			 149/150	S,G,I,Y,C,T,Q,F,V,P,A
  25	   S	   SER25:D	-0.727		  9	-0.774,-0.704			    9,9			 150/150	Y,S,T,A,Q,P,D
  26	   G	   GLY26:D	-0.625		  8	-0.704,-0.552			    9,8			 150/150	E,G,S,V,T,R
  27	   F	   PHE27:D	 0.024		  5	-0.193, 0.062			    6,5			 149/150	F,V,Y,I,E,S,G,L,R,Q
  28	   N	   ASN28:D	-0.343		  7	-0.440,-0.289			    7,7			 149/150	P,M,A,D,T,I,S,N,K
  29	   I	   ILE29:D	-0.210		  6	-0.370,-0.078			    7,5			 148/150	I,Y,T,L,D,F,V,M,P,A
  30	   K	   LYS30:D	-0.190		  6	-0.370,-0.078			    7,5			 150/150	V,A,P,K,N,G,I,E,S,Q,D,T,R,C
  31	   D	   ASP31:D	 0.257		  4	 0.062, 0.238			    5,4			 149/150	F,A,Y,K,N,E,G,S,D,L,T,R,C
  32	   T	   THR32:D	 0.626		  2	 0.238, 0.790			    4,1			 149/150	P,M,F,C,L,D,H,S,N,A,V,R,T,Q,G,E,I,K,Y
  33	   H	   HIS33:D	 2.802		  1	 1.312, 2.805			    1,1			 150/150	T,E,G,Y,K,A,V,D,H,C,L,S,N,P,W,F
  34	   M	   MET34:D	-0.377		  7	-0.500,-0.289			    8,7			 150/150	V,W,I,M,L,T
  35	   N	   ASN35:D	 1.055		  1	 0.468, 1.312			    3,1			 150/150	F,M,S,N,D,H,C,L,V,A,I,G,E,Y,Q,T,R
  36	   W	   TRP36:D	-0.815		  9	-0.849,-0.806			    9,9			 150/150	R,W
  37	   V	   VAL37:D	-0.382		  7	-0.500,-0.289			    8,7			 150/150	A,L,V,F,Y,I
  38	   K	   LYS38:D	-0.649		  8	-0.730,-0.598			    9,8			 150/150	P,W,Q,H,L,R,C,K,S
  39	   Q	   GLN39:D	-0.821		  9	-0.844,-0.806			    9,9			 150/150	L,R,Q,H
  40	   R	   ARG40:D	-0.213		  6	-0.370,-0.193			    7,6			 150/150	N,K,G,E,I,S,R,T,H,V,A,P,M
  41	   P	   PRO41:D	-0.369		  7	-0.500,-0.289			    8,7			 150/150	N,K,S,E,Q,H,L,T,A,P
  42	   E	   GLU42:D	-0.509		  8	-0.638,-0.440			    8,7			 149/150	F,S,E,G,V,R,D
  43	   Q	   GLN43:D	-0.630		  8	-0.704,-0.598			    9,8			 150/150	K,N,E,G,T,R,H,Q
  44	   G	   GLY44:D	-0.391		  7	-0.552,-0.289			    8,7			 150/150	G,S,E,R,Q,D,V,A,P
  45	   L	   LEU45:D	-0.550		  8	-0.638,-0.500			    8,8			 150/150	A,P,V,F,H,R,L,I
  46	   E	   GLU46:D	-0.711		  9	-0.754,-0.673			    9,9			 150/150	V,M,E,K,R,L,D,Q
  47	   W	   TRP47:D	-0.254		  6	-0.440,-0.193			    7,6			 150/150	E,G,Y,K,C,L,V,F,W
  48	   I	   ILE48:D	-0.219		  6	-0.370,-0.193			    7,6			 150/150	V,I,M,L
  49	   G	   GLY49:D	-0.254		  6	-0.440,-0.193			    7,6			 150/150	V,A,S,G,E,C,T,H
  50	   R	   ARG50:D	 2.804		  1	 1.312, 2.805			    1,1			 150/150	M,W,F,L,C,H,D,N,S,A,V,R,T,Q,Y,I,G,E
  51	   I	   ILE51:D	-0.395		  7	-0.500,-0.370			    8,7			 149/150	V,F,M,Y,K,G,I,S,L,T
  52	   D	   ASP52:D	 0.855		  1	 0.468, 0.790			    3,1			 148/150	V,A,E,G,I,K,Y,R,T,Q,F,W,S,N,C,L,D,H
  53	   P	  PRO52A:D	 0.827		  1	 0.468, 0.790			    3,1			 105/150	W,F,V,P,A,S,E,G,Y,K,N,R,T,D,Q
  54	   A	   ALA53:D	 1.385		  1	 0.790, 1.312			    1,1			 144/150	W,F,H,D,L,N,S,A,V,Q,T,R,Y,K,I,E,G
  55	   N	   ASN54:D	 0.628		  2	 0.238, 0.790			    4,1			 148/150	E,S,G,K,Y,N,T,L,R,D,F,W,P,A
  56	   G	   GLY55:D	 0.470		  3	 0.062, 0.468			    5,3			 150/150	H,Q,D,T,R,N,K,Y,S,I,G,E,A,V,W
  57	   N	   ASN56:D	 0.618		  2	 0.238, 0.790			    4,1			 150/150	F,V,A,P,N,K,Y,G,S,E,R,L,T,Q,D
  58	   I	   ILE57:D	-0.099		  6	-0.289,-0.078			    7,5			 150/150	P,A,V,C,L,R,T,D,Q,I,G,E,S,N,K
  59	   Q	   GLN58:D	 1.284		  1	 0.790, 1.312			    1,1			 150/150	F,W,M,S,N,D,H,L,V,A,G,I,E,Y,K,Q,T,R
  60	   Y	   TYR59:D	-0.596		  8	-0.673,-0.552			    9,8			 150/150	R,T,L,H,S,F,Y
  61	   D	   ASP60:D	-0.206		  6	-0.370,-0.193			    7,6			 150/150	A,P,V,H,D,L,T,N,E,S,G
  62	   P	   PRO61:D	 0.246		  4	-0.078, 0.238			    5,4			 150/150	P,A,V,L,R,T,D,Q,S,E,G,K,N
  63	   K	   LYS62:D	-0.383		  7	-0.500,-0.370			    8,7			 149/150	A,P,W,F,R,T,Q,D,N,K,E,G,S
  64	   F	   PHE63:D	-0.450		  7	-0.552,-0.370			    8,7			 150/150	V,F,Y,I,A,M,L
  65	   R	   ARG64:D	-0.409		  7	-0.500,-0.370			    8,7			 150/150	M,Q,T,R,E,S,K,N
  66	   G	   GLY65:D	-0.351		  7	-0.500,-0.289			    8,7			 150/150	N,E,S,G,R,T,D,A
  67	   K	   LYS66:D	-0.714		  9	-0.774,-0.673			    9,9			 150/150	G,K,Q,R,L
  68	   A	   ALA67:D	-0.341		  7	-0.500,-0.289			    8,7			 150/150	S,I,T,L,C,V,F,A,M
  69	   T	   THR68:D	-0.474		  8	-0.552,-0.440			    8,7			 150/150	A,V,H,L,T,N,K,I,E,S
  70	   I	   ILE69:D	-0.562		  8	-0.638,-0.500			    8,8			 150/150	A,M,F,V,L,T,S,I
  71	   T	   THR70:D	-0.685		  9	-0.730,-0.673			    9,9			 149/150	X,S,A,T
  72	   A	   ALA71:D	-0.031		  5	-0.193, 0.062			    6,5			 150/150	V,A,P,M,K,E,G,S,I,R,L,T
  73	   D	   ASP72:D	-0.602		  8	-0.673,-0.552			    9,8			 147/150	A,V,D,Q,G,E,K,N
  74	   T	   THR73:D	-0.157		  6	-0.289,-0.078			    7,5			 148/150	P,M,A,V,L,T,R,D,I,S,E,N,K
  75	   S	   SER74:D	-0.487		  8	-0.598,-0.440			    8,7			 150/150	D,T,Y,N,S,E,G,A,P,F
  76	   S	   SER75:D	-0.181		  6	-0.370,-0.078			    7,5			 146/150	F,A,K,N,S,E,I,Q,R,T
  77	   N	   ASN76:D	-0.406		  7	-0.500,-0.370			    8,7			 146/150	N,K,E,G,S,R,T,Q,D,A
  78	   T	   THR77:D	-0.431		  7	-0.552,-0.370			    8,7			 148/150	A,M,V,Q,L,T,K,N,E,I,S
  79	   A	   ALA78:D	-0.225		  6	-0.370,-0.193			    7,6			 149/150	I,G,S,Y,Q,T,L,V,F,M,A
  80	   Y	   TYR79:D	-0.440		  7	-0.552,-0.370			    8,7			 149/150	F,S,N,Y,D,H,T,R
  81	   L	   LEU80:D	-0.759		  9	-0.806,-0.730			    9,9			 150/150	M,L,F,I
  82	   Q	   GLN81:D	-0.421		  7	-0.552,-0.370			    8,7			 149/150	D,H,Q,C,R,L,T,E,S,K,N,M,X
  83	   L	   LEU82:D	-0.515		  8	-0.598,-0.440			    8,7			 150/150	F,W,V,M,A,S,I,L
  84	   S	  SER82A:D	-0.301		  7	-0.440,-0.289			    7,7			 149/150	D,Q,H,R,T,G,S,K,N,A,X
  85	   S	  SER82B:D	-0.451		  7	-0.552,-0.440			    8,7			 150/150	G,I,S,N,R,L,T,D,Q,H,F,A
  86	   L	  LEU82C:D	-0.583		  8	-0.673,-0.552			    9,8			 150/150	S,V,L,T,A,M,P
  87	   T	   THR83:D	-0.289		  7	-0.440,-0.193			    7,6			 150/150	S,E,N,K,T,L,R,D,Q,M,A
  88	   S	   SER84:D	 0.004		  5	-0.193, 0.062			    6,5			 150/150	N,S,I,T,L,D,F,V,A,P
  89	   E	   GLU85:D	-0.445		  7	-0.552,-0.370			    8,7			 149/150	A,Q,D,T,R,K,E,S,G
  90	   D	   ASP86:D	-0.853		  9	-0.859,-0.853			    9,9			 149/150	D
  91	   T	   THR87:D	-0.612		  8	-0.673,-0.552			    9,8			 149/150	T,M,A,S
  92	   A	   ALA88:D	-0.741		  9	-0.791,-0.704			    9,9			 148/150	A,T,V,G
  93	   V	   VAL89:D	-0.039		  5	-0.193, 0.062			    6,5			 148/150	T,R,L,H,I,E,Y,M,A,V
  94	   Y	   TYR90:D	-0.723		  9	-0.774,-0.704			    9,9			 148/150	H,L,T,Y,F
  95	   Y	   TYR91:D	-0.520		  8	-0.638,-0.440			    8,7			 148/150	L,Y,F
  96	   C	   CYS92:D	-0.724		  9	-0.791,-0.673			    9,9			 147/150	S,G,Y,V,C
  97	   A	   ALA93:D	-0.321		  7	-0.440,-0.289			    7,7			 145/150	M,A,V,T,G,I,K,N
  98	   T	   THR94:D	-0.093		  5	-0.289,-0.078			    7,5			 125/150	R,L,T,Q,H,G,E,S,Y,K,P,A,V
  99	   K	   LYS95:D	 2.785		  1	 1.312, 2.805			    1,1			  30/150	P,F,W,V,C,R,L,D,H,S,G,E,K,Y
 100	   V	   VAL96:D	 2.801		  1	 1.312, 2.805			    1,1			  42/150	A,V,Q,R,T,E,I,G,Y,M,P,W,F,D,L,S,N
 101	   I	   ILE97:D	 2.799		  1	 1.312, 2.805			    1,1			  53/150	V,W,F,P,A,G,S,I,N,K,Y,D,H,T,L
 102	   Y	   TYR98:D	 2.785		  1	 1.312, 2.805			    1,1			  50/150	R,T,C,D,Y,K,N,I,E,G,S,A,P,W,V
 103	   Y	   TYR99:D	 2.788		  1	 1.312, 2.805			    1,1			  52/150	H,D,L,C,S,P,F,Q,R,T,Y,G,E,I,A,V
 104	   Q	  GLN100:D	 2.801		  1	 1.312, 2.805			    1,1			  54/150	V,A,I,E,G,Y,K,Q,R,T,F,P,S,N,D,C,L
 105	   G	 GLY100A:D	 2.803		  1	 1.312, 2.805			    1,1			  60/150	A,K,Y,E,G,Q,R,T,F,W,P,M,S,H,D,L,C
 106	   R	 ARG100B:D	 2.778		  1	 1.312, 2.805			    1,1			  59/150	A,R,Q,Y,G,I,P,M,F,W,L,C,H,D,N,S
 107	   G	 GLY100C:D	 1.293		  1	 0.790, 1.312			    1,1			  53/150	H,D,L,T,C,N,Y,E,I,G,S,A,P,F,W
 108	   A	 ALA100D:D	 2.788		  1	 1.312, 2.805			    1,1			  36/150	E,S,G,N,Y,H,R,L,V,F,W,P,A
 109	   M	 MET100E:D	 0.685		  1	 0.238, 0.790			    4,1			  43/150	Y,I,E,R,L,C,W,F,V,P,M
 110	   D	  ASP101:D	 0.151		  4	-0.193, 0.238			    6,4			  43/150	R,L,H,Q,D,S,E,I,A,P,V
 111	   Y	  TYR102:D	 0.656		  2	 0.238, 0.790			    4,1			  68/150	S,I,N,Y,D,H,L,V,W,F,P,A
 112	   W	  TRP103:D	-0.703		  9	-0.791,-0.638			    9,8			  69/150	W,D,R
 113	   G	  GLY104:D	-0.770		  9	-0.837,-0.730			    9,9			  69/150	G,T
 114	   Q	  GLN105:D	-0.470		  7	-0.598,-0.440			    8,7			  69/150	K,V,R,Q,A,P
 115	   G	  GLY106:D	-0.770		  9	-0.837,-0.730			    9,9			  69/150	G,T
 116	   T	  THR107:D	-0.684		  9	-0.754,-0.638			    9,8			  70/150	Q,A,L,T,S,I
 117	   T	  THR108:D	 0.686		  1	 0.238, 0.790			    4,1			  70/150	A,M,P,V,T,R,L,Q,D,K,S
 118	   L	  LEU109:D	-0.388		  7	-0.552,-0.289			    8,7			  71/150	L,A,P,D,F,V
 119	   T	  THR110:D	-0.387		  7	-0.500,-0.289			    8,7			  81/150	N,Y,D,T,R,V,F,A
 120	   V	  VAL111:D	-0.287		  7	-0.440,-0.193			    7,6			  88/150	W,V,P,A,G,S,T,L,D
 121	   S	  SER112:D	-0.320		  7	-0.440,-0.289			    7,7			  93/150	E,G,S,I,D,H,Q,C,T,L,V,A


*Below the confidence cut-off - The calculations for this site were performed on less than 6 non-gaped homologue sequences,
or the confidence interval for the estimated score is equal to- or larger than- 4 color grades.
