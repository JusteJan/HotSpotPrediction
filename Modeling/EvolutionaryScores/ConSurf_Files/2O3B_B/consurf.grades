	 Amino Acid Conservation Scores
	=======================================

- POS: The position of the AA in the SEQRES derived sequence.
- SEQ: The SEQRES derived sequence in one letter code.
- 3LATOM: The ATOM derived sequence in three letter code, including the AA's positions as they appear in the PDB file and the chain identifier.
- SCORE: The normalized conservation scores.
- COLOR: The color scale representing the conservation scores (9 - conserved, 1 - variable).
- CONFIDENCE INTERVAL: When using the bayesian method for calculating rates, a confidence interval is assigned to each of the inferred evolutionary conservation scores.
- CONFIDENCE INTERVAL COLORS: When using the bayesian method for calculating rates. The color scale representing the lower and upper bounds of the confidence interval.
- MSA DATA: The number of aligned sequences having an amino acid (non-gapped) from the overall number of sequences at each position.
- RESIDUE VARIETY: The residues variety at each position of the multiple sequence alignment.

 POS	 SEQ	    3LATOM	SCORE		COLOR	CONFIDENCE INTERVAL	CONFIDENCE INTERVAL COLORS	MSA DATA	RESIDUE VARIETY
    	    	        	(normalized)	        	               
   1	   G	         -	-0.348		  6*	-1.225, 0.071			    9,5			    1/72	G
   2	   S	    SER1:B	-0.348		  6*	-1.225, 0.071			    9,5			    1/72	S
   3	   T	    THR2:B	-0.890		  8	-1.336,-0.672			    9,7			   10/72	T,I
   4	   K	    LYS3:B	-0.202		  6	-0.742, 0.207			    7,4			   13/72	S,Q,K,N
   5	   T	    THR4:B	-0.483		  6	-0.874,-0.260			    8,6			   25/72	S,M,I,D,T,N
   6	   N	    ASN5:B	-0.027		  5	-0.441, 0.207			    6,4			   33/72	K,N,D,T,M,Q,S
   7	   S	    SER6:B	 0.744		  3	 0.207, 1.006			    4,2			   47/72	N,K,E,Q,F,S,L,A,G,T,D
   8	   E	    GLU7:B	 0.195		  4	-0.159, 0.358			    5,4			   57/72	N,E,K,S,Q,A,T,P,D
   9	   I	    ILE8:B	-0.157		  5	-0.521, 0.071			    6,5			   63/72	V,L,A,F,I
  10	   L	    LEU9:B	 1.653		  1	 1.006, 1.881			    2,1			   67/72	L,A,V,I,M,F,Q,S,E,R,T,P
  11	   E	   GLU10:B	 1.226		  1	 0.536, 1.352			    3,1			   67/72	A,L,Q,S,K,E,R,N,T,P,D
  12	   Q	   GLN11:B	 0.249		  4	-0.159, 0.536			    5,3			   69/72	T,N,E,K,R,H,I,S,Q,F,A,L,V
  13	   L	   LEU12:B	-1.228		  9	-1.393,-1.113			    9,8			   69/72	A,L,I
  14	   K	   LYS13:B	-0.028		  5	-0.353, 0.207			    6,4			   69/72	A,Q,S,R,H,K,E,N,D,T,G
  15	   Q	   GLN14:B	 0.487		  4	 0.071, 0.745			    5,3			   69/72	T,D,P,I,Q,S,L,A,N,E,K,H,R
  16	   A	   ALA15:B	-0.814		  7	-1.055,-0.672			    8,7			   69/72	I,S,T,L,A,V
  17	   S	   SER16:B	-0.154		  5	-0.441, 0.071			    6,5			   69/72	S,F,V,C,L,A,T
  18	   D	   ASP17:B	 1.397		  1	 0.745, 1.881			    3,1			   69/72	T,P,D,Y,A,S,Q,K,E,H,N
  19	   G	   GLY18:B	 0.493		  4	-0.049, 0.745			    5,3			   69/72	D,G,S,K,N
  20	   L	   LEU19:B	-1.532		  9	-1.648,-1.515			    9,9			   69/72	L
  21	   L	   LEU20:B	-0.772		  7	-1.055,-0.600			    8,7			   69/72	V,L,M,F,Q,I,R,T
  22	   F	   PHE21:B	-0.565		  7	-0.809,-0.353			    7,6			   69/72	Y,M,F,W
  23	   M	   MET22:B	 0.013		  5	-0.353, 0.207			    6,4			   70/72	P,T,Q,S,M,I,V,L,N,R
  24	   S	   SER23:B	-1.551		  9	-1.648,-1.515			    9,9			   70/72	S,T
  25	   E	   GLU24:B	-1.568		  9	-1.648,-1.591			    9,9			   70/72	E
  26	   S	   SER25:B	-1.029		  8	-1.225,-0.936			    9,8			   70/72	T,A,S,I,R
  27	   E	   GLU26:B	-0.873		  8	-1.055,-0.742			    8,7			   70/72	E,N,D
  28	   Y	   TYR27:B	-0.310		  6	-0.672,-0.049			    7,5			   70/72	S,Y,F,A,H,E
  29	   P	   PRO28:B	-1.251		  9	-1.452,-1.169			    9,8			   70/72	P,A,S,E
  30	   F	   PHE29:B	-0.708		  7	-0.997,-0.521			    8,6			   70/72	W,L,C,I,F
  31	   E	   GLU30:B	-0.709		  7	-0.936,-0.600			    8,7			   70/72	Q,L,A,V,N,E,K,R,T,D
  32	   V	   VAL31:B	 0.406		  4	-0.049, 0.745			    5,3			   70/72	Y,G,T,P,I,F,S,L,A,V
  33	   F	   PHE32:B	 0.820		  3	 0.358, 1.006			    4,2			   70/72	Y,F,I,V,T,A,L
  34	   L	   LEU33:B	 2.516		  1	 1.352, 3.244			    1,1			   70/72	Q,S,F,I,V,L,A,H,R,E,Y,D,C,T
  35	   W	   TRP34:B	-0.680		  7	-0.997,-0.441			    8,6			   71/72	Y,A,C,W,L,D
  36	   E	   GLU35:B	 0.787		  3	 0.358, 1.006			    4,2			   71/72	G,Y,P,D,T,S,Q,V,L,H,K,E
  37	   G	   GLY36:B	 2.871		  1	 1.352, 3.244			    1,1			   70/72	G,D,C,T,F,S,I,V,W,L,A,N,H,R,E
  38	   S	   SER37:B	 3.128		  1	 1.881, 3.244			    1,1			   70/72	R,E,K,V,W,L,Q,P,N,A,F,S,M,I,D,T,G
  39	   A	   ALA38:B	 1.187		  2	 0.536, 1.352			    3,1			   61/72	E,K,R,N,A,V,Q,S,T,D,G
  40	   P	   PRO39:B	 1.044		  2	 0.358, 1.352			    4,1			   40/72	N,E,K,Q,S,L,A,Y,G,T,P,D
  41	   P	   PRO40:B	 3.184		  1	 1.881, 3.244			    1,1			   71/72	T,C,D,P,N,E,K,H,R,I,Q,S,A,V
  42	   V	   VAL41:B	 0.503		  4	 0.071, 0.745			    5,3			   71/72	E,I,F,M,A,L,V,P
  43	   T	   THR42:B	-0.519		  6	-0.742,-0.353			    7,6			   72/72	T,L,D,S,K,N
  44	   H	   HIS43:B	 1.498		  1	 0.745, 1.881			    3,1			   72/72	S,Q,V,A,L,N,H,E,K,P,D,T
  45	   E	   GLU44:B	 0.405		  4	-0.049, 0.745			    5,3			   72/72	T,P,D,E,K,R,N,A,S,Q
  46	   I	   ILE45:B	 0.219		  4	-0.159, 0.536			    5,3			   72/72	T,G,A,L,V,I,S,Q,F,E,K,N
  47	   V	   VAL46:B	-0.288		  6	-0.600,-0.049			    7,5			   72/72	F,I,V,L
  48	   L	   LEU47:B	-0.502		  6	-0.809,-0.353			    7,6			   70/72	Q,F,I,V,L,R,K
  49	   Q	   GLN48:B	 0.036		  5	-0.260, 0.207			    6,4			   70/72	S,Q,K,E,H,R,T,D
  50	   Q	   GLN49:B	 0.615		  3	 0.207, 1.006			    4,2			   69/72	H,R,K,N,W,A,L,Q,M,F,I,T,Y
  51	   T	   THR50:B	-0.479		  6	-0.742,-0.260			    7,6			   69/72	Q,M,S,I,L,A,K,G,Y,T
  52	   G	   GLY51:B	 0.815		  3	 0.358, 1.006			    4,2			   70/72	D,G,H,E,N,A,M,S
  53	   H	   HIS52:B	-0.327		  6	-0.600,-0.159			    7,5			   68/72	H,K,E,N,L,Q,M,I,Y
  54	   G	   GLY53:B	 1.410		  1	 0.745, 1.881			    3,1			   69/72	G,D,P,N,H,K,E,S,Q,M,I,A
  55	   Q	   GLN54:B	 2.494		  1	 1.352, 3.244			    1,1			   72/72	D,P,T,G,V,A,L,Q,S,M,I,E,N
  56	   D	   ASP55:B	 0.112		  5	-0.260, 0.358			    6,4			   72/72	E,K,N,A,S,I,D,T,G
  57	   A	   ALA56:B	-0.253		  6	-0.521,-0.049			    6,5			   70/72	Y,G,T,I,Q,M,S,L,A,V
  58	   P	   PRO57:B	 0.516		  4	 0.071, 0.745			    5,3			   70/72	Y,P,T,R,K,S,Q,I,V,A,L
  59	   F	   PHE58:B	-0.291		  6	-0.600,-0.049			    7,5			   72/72	A,T,L,V,I,F
  60	   K	   LYS59:B	-0.180		  6	-0.521, 0.071			    6,5			   72/72	E,K,R,Q,S,A,L,V,T
  61	   V	   VAL60:B	 0.973		  2	 0.358, 1.352			    4,1			   72/72	T,Y,E,K,V,A,L,F,Q,M,S,I
  62	   V	   VAL61:B	-0.044		  5	-0.353, 0.207			    6,4			   72/72	T,D,K,E,R,L,V,I,Q,M
  63	   D	   ASP62:B	 1.399		  1	 0.745, 1.881			    3,1			   72/72	G,T,D,P,E,K,S,A
  64	   I	   ILE63:B	 0.100		  5	-0.260, 0.358			    6,4			   72/72	E,V,L,M,F,I
  65	   D	   ASP64:B	 0.138		  5	-0.260, 0.358			    6,4			   72/72	P,D,T,Y,A,S,Q,R,E,N
  66	   S	   SER65:B	 0.447		  4	 0.071, 0.745			    5,3			   72/72	S,Q,A,N,E,K,R,Y,T,D
  67	   F	   PHE66:B	-0.748		  7	-0.997,-0.600			    8,7			   72/72	F,L
  68	   F	   PHE67:B	-1.063		  8	-1.281,-0.936			    9,8			   72/72	F,W,L
  69	   S	   SER68:B	 0.324		  4	-0.049, 0.536			    5,3			   72/72	Y,G,D,N,R,K,E,Q,S,A
  70	   R	   ARG69:B	-0.510		  6	-0.742,-0.353			    7,6			   72/72	P,G,R,N,A,V,I,Q,S,F
  71	   A	   ALA70:B	-0.663		  7	-0.874,-0.521			    8,6			   72/72	S,M,V,C,L,A
  72	   T	   THR71:B	-0.441		  6	-0.742,-0.260			    7,6			   72/72	I,L,C,A,T,V
  73	   T	   THR72:B	 0.695		  3	 0.207, 1.006			    4,2			   72/72	T,D,Y,A,L,V,S,F,Q,K,E,R
  74	   P	   PRO73:B	-0.139		  5	-0.441, 0.071			    6,5			   72/72	S,Q,L,A,V,E,G,T,P,D
  75	   Q	   GLN74:B	-0.491		  6	-0.742,-0.353			    7,6			   72/72	R,E,K,N,A,S,Q
  76	   D	   ASP75:B	-0.677		  7	-0.936,-0.521			    8,6			   72/72	N,E,S,D,P,A
  77	   W	   TRP76:B	-1.180		  8	-1.393,-1.055			    9,8			   71/72	E,G,W
  78	   Y	   TYR77:B	-0.214		  6	-0.521,-0.049			    6,5			   71/72	A,S,F,Q,Y,H,N
  79	   E	   GLU78:B	 0.739		  3	 0.207, 1.006			    4,2			   72/72	G,S,T,D,N,E,H
  80	   D	   ASP79:B	 0.712		  3	 0.207, 1.006			    4,2			   72/72	T,P,D,K,E,R,S,Q,A
  81	   E	   GLU80:B	-0.125		  5	-0.441, 0.071			    6,5			   72/72	Q,S,M,I,L,A,R,K,E,G,D,P,T
  82	   E	   GLU81:B	-1.137		  8	-1.281,-1.055			    9,8			   72/72	E,G,M,Q,A,L,D
  83	   N	   ASN82:B	 0.368		  4	-0.049, 0.536			    5,3			   71/72	T,V,A,L,F,Q,I,R,K,E,N
  84	   A	   ALA83:B	 0.447		  4	 0.071, 0.745			    5,3			   71/72	D,T,Q,S,M,I,L,A,N,R,K,E
  85	   V	   VAL84:B	 0.846		  3	 0.358, 1.006			    4,2			   72/72	V,L,Q,M,S,I,R,E,N,D,T
  86	   V	   VAL85:B	-0.898		  8	-1.113,-0.742			    8,7			   72/72	R,T,C,A,V,M
  87	   A	   ALA86:B	 0.854		  3	 0.358, 1.006			    4,2			   72/72	A,Q,S,H,R,K,E,N,P,D,T,G
  88	   K	   LYS87:B	-0.411		  6	-0.672,-0.260			    7,6			   72/72	Q,S,T,R,K
  89	   F	   PHE88:B	-0.485		  6	-0.809,-0.260			    7,6			   72/72	F,Y
  90	   Q	   GLN89:B	-0.777		  7	-0.997,-0.672			    8,7			   72/72	K,E,H,R,T,Q
  91	   K	   LYS90:B	 1.352		  1	 0.745, 1.881			    3,1			   72/72	N,E,K,R,H,I,Q,F,S,A,L,G,T,D
  92	   L	   LEU91:B	-1.239		  9	-1.452,-1.113			    9,8			   72/72	I,F,M,L
  93	   L	   LEU92:B	 0.467		  4	 0.071, 0.745			    5,3			   72/72	R,K,E,F,Q,I,V,L
  94	   E	   GLU93:B	 0.783		  3	 0.358, 1.006			    4,2			   72/72	L,A,Q,F,M,S,E,K,H,R,N,T,D,G
  95	   V	   VAL94:B	 1.188		  2	 0.536, 1.352			    3,1			   72/72	T,E,K,I,F,Q,S,W,A,L,V
  96	   I	   ILE95:B	-0.594		  7	-0.874,-0.441			    8,6			   72/72	V,L,F,I
  97	   K	   LYS96:B	-0.360		  6	-0.672,-0.159			    7,5			   71/72	D,T,R,K,E,N,V,L,Q,S
  98	   S	   SER97:B	 1.368		  1	 0.745, 1.881			    3,1			   72/72	A,L,V,S,Q,E,K,H,R,N,T,D,Y
  99	   N	   ASN98:B	-0.193		  6	-0.521,-0.049			    6,5			   72/72	C,T,S,Q,F,A,L,N,R,H,E
 100	   L	   LEU99:B	-1.470		  9	-1.648,-1.393			    9,9			   72/72	L,V
 101	   K	  LYS100:B	 0.683		  3	 0.207, 1.006			    4,2			   72/72	R,H,K,E,N,A,S,Q,I,D,T
 102	   N	  ASN101:B	 0.108		  5	-0.260, 0.358			    6,4			   72/72	D,T,H,K,E,N,V,S
 103	   P	  PRO102:B	-0.286		  6	-0.600,-0.049			    7,5			   72/72	R,S,I,P,V,A,L
 104	   Q	  GLN103:B	-0.214		  6	-0.521,-0.049			    6,5			   72/72	H,R,K,S,Q,I,T
 105	   V	  VAL104:B	-1.476		  9	-1.591,-1.452			    9,9			   72/72	V,I
 106	   Y	  TYR105:B	-0.552		  7	-0.809,-0.353			    7,6			   72/72	V,A,F,Y,M,I,N
 107	   R	  ARG106:B	-1.151		  8	-1.336,-1.055			    9,8			   72/72	V,E,K,R
 108	   L	  LEU107:B	-0.388		  6	-0.672,-0.159			    7,5			   72/72	L,V,I,F
 109	   G	  GLY108:B	-1.290		  9	-1.515,-1.169			    9,8			   72/72	S,G,D
 110	   E	  GLU109:B	 0.751		  3	 0.358, 1.006			    4,2			   72/72	F,Q,S,A,N,R,H,K,E,G,D,T
 111	   V	  VAL110:B	 0.687		  3	 0.207, 1.006			    4,2			   72/72	T,D,G,K,R,A,L,V,I,S
 112	   E	  GLU111:B	-0.094		  5	-0.441, 0.071			    6,5			   72/72	A,S,Q,K,E,R,N,T,D
 113	   L	  LEU112:B	-0.527		  7	-0.809,-0.353			    7,6			   72/72	A,L,V,I,Q,M,K
 114	   D	  ASP113:B	-0.924		  8	-1.113,-0.809			    8,7			   72/72	N,E,H,Q,S,L,A,D
 115	   V	  VAL114:B	-0.670		  7	-0.936,-0.521			    8,6			   72/72	V,A,L,I
 116	   Y	  TYR115:B	-1.151		  8	-1.336,-1.055			    9,8			   72/72	L,Y,F
 117	   V	  VAL116:B	-1.122		  8	-1.281,-0.997			    9,8			   72/72	L,V,I
 118	   I	  ILE117:B	 0.220		  4	-0.159, 0.536			    5,3			   72/72	I,A,L,T,V
 119	   G	  GLY118:B	-1.495		  9	-1.648,-1.452			    9,9			   72/72	G
 120	   E	  GLU119:B	 0.097		  5	-0.260, 0.358			    6,4			   70/72	R,E,K,N,A,L,S,Q,I,T
 121	   T	  THR120:B	-0.344		  6	-0.600,-0.159			    7,5			   72/72	D,P,T,N,V,A,L,Q,S,M,I
 122	   P	  PRO121:B	 2.490		  1	 1.352, 3.244			    1,1			   70/72	T,P,G,K,E,R,A,L,V,Q,S
 123	   A	  ALA122:B	 1.088		  2	 0.536, 1.352			    3,1			   72/72	H,E,K,N,A,Q,F,S,P,D,T
 124	   G	  GLY123:B	 1.238		  1	 0.536, 1.881			    3,1			   72/72	G,D,T,S,Q,L,N,R,E,K
 125	   N	  ASN124:B	 1.018		  2	 0.536, 1.352			    3,1			   69/72	Y,G,T,D,S,Q,A,N,E,K,R,H
 126	   L	  LEU125:B	 0.534		  3	 0.071, 0.745			    5,3			   72/72	R,W,L,A,V,I,S,T,P,Y
 127	   A	  ALA126:B	-0.612		  7	-0.874,-0.441			    8,6			   72/72	I,G,S,A,L,V,E
 128	   G	  GLY127:B	-1.495		  9	-1.648,-1.452			    9,9			   72/72	G
 129	   I	  ILE128:B	-0.345		  6	-0.672,-0.159			    7,5			   72/72	L,V,I
 130	   S	  SER129:B	-0.348		  6	-0.600,-0.159			    7,5			   72/72	K,R,Q,F,S,A,V,C,T
 131	   T	  THR130:B	-1.535		  9	-1.648,-1.515			    9,9			   72/72	V,T
 132	   K	  LYS131:B	-0.236		  6	-0.521,-0.049			    6,5			   72/72	L,V,Q,M,K,H,R,T
 133	   V	  VAL132:B	-0.049		  5	-0.353, 0.207			    6,4			   72/72	V,L,A,I
 134	   V	  VAL133:B	-0.901		  8	-1.113,-0.742			    8,7			   72/72	V,I
 135	   E	  GLU134:B	-1.441		  9	-1.591,-1.393			    9,9			   72/72	R,E,Q
 136	   T	  THR135:B	-1.564		  9	-1.648,-1.515			    9,9			   70/72	T


*Below the confidence cut-off - The calculations for this site were performed on less than 6 non-gaped homologue sequences,
or the confidence interval for the estimated score is equal to- or larger than- 4 color grades.
