	 Amino Acid Conservation Scores
	=======================================

- POS: The position of the AA in the SEQRES derived sequence.
- SEQ: The SEQRES derived sequence in one letter code.
- 3LATOM: The ATOM derived sequence in three letter code, including the AA's positions as they appear in the PDB file and the chain identifier.
- SCORE: The normalized conservation scores.
- COLOR: The color scale representing the conservation scores (9 - conserved, 1 - variable).
- CONFIDENCE INTERVAL: When using the bayesian method for calculating rates, a confidence interval is assigned to each of the inferred evolutionary conservation scores.
- CONFIDENCE INTERVAL COLORS: When using the bayesian method for calculating rates. The color scale representing the lower and upper bounds of the confidence interval.
- MSA DATA: The number of aligned sequences having an amino acid (non-gapped) from the overall number of sequences at each position.
- RESIDUE VARIETY: The residues variety at each position of the multiple sequence alignment.

 POS	 SEQ	    3LATOM	SCORE		COLOR	CONFIDENCE INTERVAL	CONFIDENCE INTERVAL COLORS	MSA DATA	RESIDUE VARIETY
    	    	        	(normalized)	        	               
   1	   P	  PRO401:D	-0.845		  9	-0.921,-0.823			    9,9			 122/150	P
   2	   I	  ILE402:D	-0.111		  6	-0.363, 0.073			    7,5			 122/150	I,V,X
   3	   V	  VAL403:D	-0.026		  5	-0.294, 0.198			    7,4			 129/150	I,L,A,V,R,M
   4	   Q	  GLN404:D	 0.127		  4	-0.217, 0.342			    6,3			 129/150	T,R,Q,G,N
   5	   N	  ASN405:D	-0.213		  6	-0.425,-0.036			    7,5			 129/150	A,N,D,T,Q,K,S,X,G
   6	   L	  LEU406:D	 1.802		  1	 0.994, 1.942			    1,1			 130/150	P,T,V,X,M,I,L,A
   7	   Q	  GLN407:D	-0.504		  8	-0.704,-0.363			    9,7			 135/150	N,G,Q,R
   8	   G	  GLY408:D	-0.451		  7	-0.667,-0.294			    8,7			 138/150	V,N,G
   9	   Q	  GLN409:D	-0.334		  7	-0.535,-0.217			    8,6			 140/150	V,N,I,Q,R,T,P
  10	   M	  MET410:D	 0.838		  1	 0.342, 0.994			    3,1			 138/150	Y,M,R,I,H,L,A,F,P,W,T
  11	   V	  VAL411:D	 1.361		  1	 0.725, 1.942			    1,1			 142/150	T,V,Q,S,M,R,L,A,I
  12	   H	  HIS412:D	 0.227		  4	-0.131, 0.342			    6,3			 140/150	X,L,H,P,Y,W
  13	   Q	  GLN413:D	 0.162		  4	-0.131, 0.342			    6,3			 142/150	F,P,D,Q,S,V,E,R,H,L
  14	   A	  ALA414:D	 3.534		  1	 1.942, 3.534			    1,1			 138/150	R,N,A,P,T,S,G,X,V
  15	   I	  ILE415:D	 3.533		  1	 1.942, 3.534			    1,1			 138/150	F,M,I,V,X,L
  16	   S	  SER416:D	-0.375		  7	-0.583,-0.294			    8,7			 142/150	T,Q,S,A,N,E
  17	   P	  PRO417:D	 0.193		  4	-0.131, 0.342			    6,3			 142/150	R,L,A,I,T,P,X,S
  18	   R	  ARG418:D	-0.291		  7	-0.535,-0.131			    8,6			 144/150	A,K,T,R
  19	   T	  THR419:D	-0.410		  7	-0.583,-0.294			    8,7			 144/150	T,D,P,A,V,I,S
  20	   L	  LEU420:D	-0.608		  8	-0.797,-0.483			    9,8			 144/150	T,X,L,I
  21	   N	  ASN421:D	-0.080		  5	-0.363, 0.073			    7,5			 146/150	D,K,Q,S,E,G,Y,N
  22	   A	  ALA422:D	-0.482		  8	-0.667,-0.363			    8,7			 146/150	V,E,G,A,S,T
  23	   W	  TRP423:D	-0.108		  6	-0.483, 0.073			    8,5			 145/150	X,A,G,W,M
  24	   V	  VAL424:D	-0.684		  9	-0.797,-0.627			    9,8			 146/150	G,L,E,V
  25	   K	  LYS425:D	-0.391		  7	-0.583,-0.294			    8,7			 146/150	N,E,I,S,K,D
  26	   V	  VAL426:D	 0.302		  3	-0.036, 0.514			    5,2			 146/150	I,L,A,H,K,G,V,T,W
  27	   V	  VAL427:D	 1.252		  1	 0.725, 1.363			    1,1			 145/150	E,V,X,I,P
  28	   E	  GLU428:D	-0.704		  9	-0.823,-0.627			    9,8			 145/150	E,X,K
  29	   E	  GLU429:D	-0.262		  6	-0.483,-0.131			    8,6			 145/150	D,X,E,K
  30	   K	  LYS430:D	 0.038		  5	-0.294, 0.198			    7,4			 145/150	K,I,X,E,R
  31	   A	  ALA431:D	 0.868		  1	 0.342, 0.994			    3,1			 145/150	N,A,R,Q,K,S,X,G
  32	   F	  PHE432:D	-0.853		  9	-0.927,-0.823			    9,9			 145/150	X,F
  33	   S	  SER433:D	 0.683		  1	 0.342, 0.994			    3,1			 147/150	T,S,K,G,X,C,I,N,A
  34	   P	  PRO434:D	-0.473		  7	-0.667,-0.363			    8,7			 148/150	P,A,S
  35	   E	  GLU435:D	-0.621		  8	-0.769,-0.535			    9,8			 146/150	D,K,X,E
  36	   V	  VAL436:D	 0.248		  4	-0.036, 0.514			    5,2			 147/150	C,T,V,X,L,I
  37	   I	  ILE437:D	-0.694		  9	-0.823,-0.627			    9,8			 148/150	I,V,T
  38	   P	  PRO438:D	-0.654		  8	-0.797,-0.583			    9,8			 148/150	A,P,R
  39	   M	  MET439:D	-0.504		  8	-0.667,-0.425			    8,7			 148/150	G,L,M,F
  40	   F	  PHE440:D	-0.733		  9	-0.866,-0.667			    9,8			 147/150	F,X,V
  41	   S	  SER441:D	 1.677		  1	 0.994, 1.942			    1,1			 146/150	A,I,M,X,V,Q,S,T
  42	   A	  ALA442:D	-0.759		  9	-0.866,-0.704			    9,9			 149/150	A,G,T
  43	   L	  LEU443:D	-0.742		  9	-0.866,-0.667			    9,8			 149/150	V,L
  44	   S	  SER444:D	-0.243		  6	-0.425,-0.131			    7,6			 150/150	T,S,A
  45	   E	  GLU445:D	-0.367		  7	-0.583,-0.217			    8,6			 149/150	D,X,A,E,K
  46	   G	  GLY446:D	-0.384		  7	-0.627,-0.217			    8,6			 150/150	G,E,R
  47	   A	  ALA447:D	-0.229		  6	-0.425,-0.131			    7,6			 150/150	L,A,G,S,T,C,P
  48	   T	  THR448:D	-0.351		  7	-0.535,-0.217			    8,6			 150/150	P,T,I,V,A,L
  49	   P	  PRO449:D	-0.113		  6	-0.363, 0.073			    7,5			 149/150	S,X,A,P,T
  50	   Q	  GLN450:D	 1.483		  1	 0.994, 1.942			    1,1			 149/150	D,T,S,Q,V,E,G,X,Y,H,A
  51	   D	  ASP451:D	-0.555		  8	-0.704,-0.483			    9,8			 149/150	B,D,Y,R,X,N
  52	   L	  LEU452:D	 0.014		  5	-0.294, 0.198			    7,4			 149/150	M,F,X,L,V,I
  53	   N	  ASN453:D	-0.762		  9	-0.866,-0.704			    9,9			 149/150	Y,K,N,X
  54	   T	  THR454:D	 0.849		  1	 0.342, 0.994			    3,1			 147/150	M,H,L,A,T,S,Q,V,G,X
  55	   M	  MET455:D	-0.452		  7	-0.627,-0.363			    8,7			 150/150	L,F,M,D
  56	   L	  LEU456:D	-0.378		  7	-0.627,-0.217			    8,6			 149/150	A,L,X,F,M
  57	   N	  ASN457:D	-0.759		  9	-0.866,-0.704			    9,9			 150/150	N,E,S
  58	   T	  THR458:D	 0.383		  3	 0.073, 0.514			    5,2			 147/150	C,N,A,H,I,T,X,V,S
  59	   V	  VAL459:D	 0.280		  4	-0.036, 0.514			    5,2			 149/150	M,L,X,G,V,S,I
  60	   G	  GLY460:D	-0.856		  9	-0.927,-0.823			    9,9			 150/150	G
  61	   G	  GLY461:D	-0.092		  5	-0.363, 0.073			    7,5			 150/150	E,G,D,R
  62	   H	  HIS462:D	-0.667		  8	-0.797,-0.583			    9,8			 150/150	T,Y,L,H
  63	   Q	  GLN463:D	-0.727		  9	-0.845,-0.667			    9,8			 150/150	P,H,Q
  64	   A	  ALA464:D	-0.514		  8	-0.667,-0.425			    8,7			 150/150	A,G
  65	   A	  ALA465:D	-0.759		  9	-0.866,-0.704			    9,9			 150/150	A,G,S
  66	   M	  MET466:D	-0.329		  7	-0.535,-0.217			    8,6			 150/150	I,L,V,M,R,Y
  67	   Q	  GLN467:D	-0.727		  9	-0.845,-0.667			    9,8			 150/150	R,A,Q
  68	   M	  MET468:D	-0.011		  5	-0.294, 0.198			    7,4			 147/150	M,T,I,V,L,N,X
  69	   L	  LEU469:D	-0.228		  6	-0.483,-0.036			    8,5			 150/150	V,L,I
  70	   K	  LYS470:D	-0.700		  9	-0.823,-0.627			    9,8			 150/150	K,N,R
  71	   E	  GLU471:D	 0.759		  1	 0.342, 0.994			    3,1			 148/150	D,X,G,E
  72	   T	  THR472:D	-0.108		  6	-0.363, 0.073			    7,5			 150/150	T,I,S,V,A
  73	   I	  ILE473:D	-0.689		  9	-0.797,-0.627			    9,8			 149/150	I,X,V,P
  74	   N	  ASN474:D	-0.701		  9	-0.823,-0.627			    9,8			 150/150	K,S,I,N
  75	   E	  GLU475:D	-0.178		  6	-0.425,-0.036			    7,5			 148/150	E,X,K,D,M
  76	   E	  GLU476:D	-0.152		  6	-0.425,-0.036			    7,5			 149/150	G,X,E,Q,K,R
  77	   A	  ALA477:D	-0.590		  8	-0.738,-0.483			    9,8			 150/150	K,I,G,A
  78	   A	  ALA478:D	-0.411		  7	-0.583,-0.294			    8,7			 149/150	S,Q,V,X,L,A,T
  79	   E	  GLU479:D	 0.389		  3	 0.073, 0.514			    5,2			 148/150	D,X,N,E,Q
  80	   W	  TRP480:D	-0.549		  8	-0.797,-0.425			    9,7			 147/150	N,X,W
  81	   D	  ASP481:D	-0.649		  8	-0.797,-0.583			    9,8			 150/150	A,N,G,D
  82	   R	  ARG482:D	-0.280		  6	-0.483,-0.131			    8,6			 149/150	T,K,Q,V,X,R,M,I,L
  83	   L	  LEU483:D	 3.534		  1	 1.942, 3.534			    1,1			 146/150	I,A,L,M,R,Q,S,X,V,T
  84	   H	  HIS484:D	-0.815		  9	-0.899,-0.769			    9,9			 148/150	X,N,H
  85	   P	  PRO485:D	-0.756		  9	-0.866,-0.704			    9,9			 150/150	P,T
  86	   V	  VAL486:D	 1.299		  1	 0.725, 1.363			    1,1			 149/150	L,N,A,I,T,P,X,E,V,Q
  87	   H	  HIS487:D	 1.629		  1	 0.994, 1.942			    1,1			 146/150	P,X,A,V,H,Q
  88	   A	  ALA488:D	-0.451		  7	-0.627,-0.363			    8,7			 146/150	A,V,Q,M
  89	   G	  GLY489:D	-0.624		  8	-0.797,-0.535			    9,8			 150/150	G,A,Q
  90	   P	  PRO490:D	-0.863		  9	-0.927,-0.845			    9,9			 149/150	P,X
  91	   I	  ILE491:D	 3.527		  1	 1.942, 3.534			    1,1			 145/150	V,X,Q,T,F,P,H,N,L,A,I,Y,M
  92	   A	  ALA492:D	 1.451		  1	 0.725, 1.942			    1,1			 149/150	P,X,A,L,V,S,Q
  93	   P	  PRO493:D	 0.588		  2	 0.198, 0.725			    4,1			 149/150	V,N,X,A,Q,P
  94	   G	  GLY494:D	-0.625		  8	-0.797,-0.535			    9,8			 148/150	G,A,N,X
  95	   Q	  GLN495:D	-0.543		  8	-0.704,-0.425			    9,7			 150/150	Q,V,G,A,M
  96	   M	  MET496:D	 2.171		  1	 1.363, 1.942			    1,1			 146/150	X,L,V,I,M
  97	   R	  ARG497:D	-0.480		  8	-0.667,-0.363			    8,7			 149/150	T,R,X,K,I
  98	   E	  GLU498:D	 0.455		  3*	 0.073, 0.725			    5,1			 148/150	K,S,Q,E,X,N,D
  99	   P	  PRO499:D	-0.649		  8	-0.797,-0.535			    9,8			 149/150	Q,X,A,P
 100	   R	  ARG500:D	-0.071		  5	-0.363, 0.073			    7,5			 148/150	G,X,Q,S,K,R,T
 101	   G	  GLY501:D	-0.736		  9	-0.866,-0.667			    9,8			 149/150	G,R
 102	   S	  SER502:D	-0.543		  8	-0.704,-0.483			    9,8			 149/150	N,A,G,H,S,R
 103	   D	  ASP503:D	-0.726		  9	-0.845,-0.667			    9,8			 148/150	D,N,G,X
 104	   I	  ILE504:D	-0.760		  9	-0.866,-0.704			    9,9			 149/150	I,V,M
 105	   A	  ALA505:D	-0.824		  9	-0.899,-0.797			    9,9			 148/150	S,A,X
 106	   G	  GLY506:D	-0.495		  8	-0.704,-0.363			    9,7			 149/150	R,G
 107	   T	  THR507:D	-0.627		  8	-0.769,-0.535			    9,8			 146/150	S,V,X,T
 108	   T	  THR508:D	-0.705		  9	-0.823,-0.627			    9,8			 149/150	P,T,S,I
 109	   S	  SER509:D	-0.719		  9	-0.823,-0.667			    9,8			 149/150	C,A,S,I
 110	   T	  THR510:D	 0.645		  2	 0.198, 0.725			    4,1			 149/150	T,N,S
 111	   L	  LEU511:D	 1.453		  1	 0.725, 1.942			    1,1			 149/150	T,P,V,Q,M,A,L,I
 112	   Q	  GLN512:D	-0.122		  6	-0.363, 0.073			    7,5			 149/150	Q,N,A,E,V,P,D
 113	   E	  GLU513:D	-0.433		  7	-0.627,-0.294			    8,7			 147/150	D,E,V,X,A,K
 114	   Q	  GLN514:D	-0.726		  9	-0.845,-0.667			    9,8			 149/150	P,Q,E
 115	   I	  ILE515:D	 0.103		  4	-0.217, 0.342			    6,3			 148/150	M,X,L,V,I
 116	   G	  GLY516:D	 3.534		  1	 1.942, 3.534			    1,1			 144/150	L,N,A,I,R,E,X,G,Q,T
 117	   W	  TRP517:D	-0.559		  8	-0.797,-0.425			    9,7			 144/150	W,S,X
 118	   M	  MET518:D	-0.211		  6	-0.425,-0.036			    7,5			 147/150	L,I,T,M
 119	   T	  THR519:D	-0.627		  8	-0.769,-0.535			    9,8			 147/150	Y,T,P,N,A
 120	   H	  HIS520:D	 3.526		  1	 1.942, 3.534			    1,1			 142/150	N,A,H,R,Q,K,S,X,G,T
 121	   N	  ASN521:D	-0.101		  6	-0.363, 0.073			    7,5			 146/150	A,N,Y,G,X,E,Q,T
 122	   P	  PRO522:D	 0.003		  5	-0.294, 0.198			    7,4			 145/150	N,G,X,D,P
 123	   P	  PRO523:D	 0.286		  4	-0.036, 0.514			    5,2			 146/150	T,R,P,G,A,S
 124	   I	  ILE524:D	 1.425		  1	 0.725, 1.942			    1,1			 143/150	I,N,L,M,Y,X,V,T
 125	   P	  PRO525:D	-0.292		  7	-0.535,-0.131			    8,6			 145/150	P,D,Q,S,X,N
 126	   V	  VAL526:D	-0.679		  9	-0.797,-0.627			    9,8			 146/150	I,S,V,L
 127	   G	  GLY527:D	-0.374		  7	-0.627,-0.217			    8,6			 145/150	K,S,G,A,X,E
 128	   E	  GLU528:D	 2.196		  1	 1.363, 1.942			    1,1			 142/150	D,K,Q,N,X,A,E
 129	   I	  ILE529:D	-0.753		  9	-0.845,-0.704			    9,9			 142/150	I,V,L
 130	   Y	  TYR530:D	-0.848		  9	-0.921,-0.823			    9,9			 142/150	Y
 131	   K	  LYS531:D	 0.504		  2*	 0.073, 0.725			    5,1			 142/150	R,M,K,N
 132	   R	  ARG532:D	 0.412		  3*	 0.073, 0.725			    5,1			 142/150	R,G,K,S,Q
 133	   W	  TRP533:D	-0.319		  7	-0.627,-0.131			    8,6			 141/150	W,M,R,X
 134	   I	  ILE534:D	-0.612		  8	-0.738,-0.535			    9,8			 141/150	I,V
 135	   I	  ILE535:D	 0.224		  4	-0.131, 0.342			    6,3			 139/150	N,L,V,Q,I,S
 136	   L	  LEU536:D	 0.847		  1	 0.342, 0.994			    3,1			 139/150	L,A,V,I,R,M
 137	   G	  GLY537:D	-0.455		  7	-0.667,-0.294			    8,7			 139/150	V,L,G,R
 138	   L	  LEU538:D	-0.353		  7	-0.583,-0.217			    8,6			 139/150	L,V,W,F
 139	   N	  ASN539:D	-0.598		  8	-0.738,-0.535			    9,8			 139/150	T,N,H,Q
 140	   K	  LYS540:D	-0.684		  9	-0.823,-0.627			    9,8			 139/150	R,T,K
 141	   I	  ILE541:D	-0.329		  7	-0.535,-0.217			    8,6			 137/150	C,M,T,I,V,L
 142	   V	  VAL542:D	-0.597		  8	-0.738,-0.535			    9,8			 137/150	I,E,V,L
 143	   R	  ARG543:D	-0.052		  5	-0.294, 0.073			    7,5			 136/150	G,X,K,Q,S,R
 144	   M	  MET544:D	-0.808		  9	-0.899,-0.769			    9,9			 136/150	M,I
 145	   Y	  TYR545:D	-0.844		  9	-0.921,-0.823			    9,9			 136/150	Y


*Below the confidence cut-off - The calculations for this site were performed on less than 6 non-gaped homologue sequences,
or the confidence interval for the estimated score is equal to- or larger than- 4 color grades.
