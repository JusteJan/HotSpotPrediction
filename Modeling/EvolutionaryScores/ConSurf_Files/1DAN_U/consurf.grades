	 Amino Acid Conservation Scores
	=======================================

- POS: The position of the AA in the SEQRES derived sequence.
- SEQ: The SEQRES derived sequence in one letter code.
- 3LATOM: The ATOM derived sequence in three letter code, including the AA's positions as they appear in the PDB file and the chain identifier.
- SCORE: The normalized conservation scores.
- COLOR: The color scale representing the conservation scores (9 - conserved, 1 - variable).
- CONFIDENCE INTERVAL: When using the bayesian method for calculating rates, a confidence interval is assigned to each of the inferred evolutionary conservation scores.
- CONFIDENCE INTERVAL COLORS: When using the bayesian method for calculating rates. The color scale representing the lower and upper bounds of the confidence interval.
- MSA DATA: The number of aligned sequences having an amino acid (non-gapped) from the overall number of sequences at each position.
- RESIDUE VARIETY: The residues variety at each position of the multiple sequence alignment.

 POS	 SEQ	    3LATOM	SCORE		COLOR	CONFIDENCE INTERVAL	CONFIDENCE INTERVAL COLORS	MSA DATA	RESIDUE VARIETY
    	    	        	(normalized)	        	               
   1	   G	         -	-0.898		  8	-1.277,-0.709			    9,7			  30/150	G
   2	   E	   GLU91:U	-0.800		  8	-0.985,-0.646			    8,7			  96/150	Q,D,N,E
   3	   P	   PRO92:U	-0.139		  5	-0.511, 0.131			    7,5			 105/150	L,P,H,A,K,S
   4	   L	   LEU93:U	 3.060		  1	 1.915, 3.290			    1,1			 113/150	P,S,M,G,H,V,L,R,Q
   5	   Y	   TYR94:U	 1.265		  1	 0.790, 1.392			    2,1			 130/150	L,N,H,F,Y,S,R
   6	   E	   GLU95:U	 0.925		  2	 0.410, 1.046			    4,1			 140/150	S,M,G,A,T,N,V,E,R,K,Q
   7	   N	   ASN96:U	 0.186		  4	-0.093, 0.410			    5,4			 145/150	K,R,Q,A,L,E,T,N,V,D,S,I,Y
   8	   S	   SER97:U	-0.961		  8	-1.085,-0.880			    9,8			 147/150	S,A,G,T
   9	   P	   PRO98:U	 0.112		  5	-0.188, 0.261			    6,4			 147/150	P,S,D,E,L,A,R,K
  10	   E	   GLU99:U	 1.224		  1	 0.790, 1.392			    2,1			 148/150	P,S,W,I,Y,M,A,N,L,T,E,D,K,R,Q
  11	   F	  PHE100:U	-1.187		  9	-1.327,-1.133			    9,9			 148/150	I,F,L
  12	   T	  THR101:U	 0.113		  5	-0.188, 0.261			    6,4			 148/150	I,S,Q,R,K,E,N,V,L,T,C,M
  13	   P	  PRO102:U	-1.313		  9	-1.417,-1.277			    9,9			 148/150	P
  14	   Y	  TYR103:U	-0.903		  8	-1.085,-0.768			    9,8			 149/150	L,H,Y,F,S
  15	   L	  LEU104:U	 1.536		  1	 1.046, 1.915			    1,1			 150/150	S,I,K,R,Q,M,N,E,T,V,L,D
  16	   E	  GLU105:U	-0.307		  6	-0.511,-0.188			    7,6			 150/150	R,Q,E,L,N,D
  17	   T	  THR106:U	-1.140		  9	-1.277,-1.085			    9,9			 150/150	S,A,T
  18	   N	  ASN107:U	 1.427		  1	 0.790, 1.915			    2,1			 150/150	K,Q,H,M,D,V,E,L,T,N,I
  19	   L	  LEU108:U	-0.528		  7	-0.768,-0.361			    8,6			 150/150	A,L,I,F
  20	   G	  GLY109:U	-0.452		  7	-0.768,-0.278			    8,6			 150/150	C,G,D,L,S,I
  21	   Q	  GLN110:U	 0.287		  4	 0.014, 0.410			    5,4			 150/150	S,K,R,Q,A,M,L,E,T
  22	   P	  PRO111:U	-0.759		  8	-0.985,-0.646			    8,7			 150/150	S,P,A,V,L
  23	   T	  THR112:U	 1.451		  1	 0.790, 1.915			    2,1			 150/150	S,I,R,K,Q,G,M,A,D,N,T,E,V
  24	   I	  ILE113:U	-0.744		  8	-0.934,-0.646			    8,7			 150/150	V,L,A,F,I
  25	   Q	  GLN114:U	 0.485		  3	 0.131, 0.790			    5,2			 150/150	S,A,H,L,N,E,T,D,K,R,Q
  26	   S	  SER115:U	 0.652		  3	 0.261, 0.790			    4,2			 150/150	Q,K,R,N,E,V,T,D,G,H,I,Y,S
  27	   F	  PHE116:U	 0.321		  4	-0.093, 0.584			    5,3			 150/150	F,I,Y,S,V,L,G,A,M,H,C,K,R
  28	   E	  GLU117:U	-0.142		  5	-0.361, 0.014			    6,5			 150/150	D,E,N,T,L,H,M,R,K,I,S
  29	   Q	  GLN118:U	-0.014		  5	-0.278, 0.131			    6,5			 147/150	I,P,Q,K,R,V,E,L,D,A,G,H
  30	   V	  VAL119:U	 0.325		  4	 0.014, 0.584			    5,3			 147/150	T,E,L,N,V,D,A,G,K,R,I,S
  31	   G	  GLY120:U	 0.824		  2	 0.410, 1.046			    4,1			 148/150	K,Q,A,G,H,E,N,D,S,I
  32	   T	  THR121:U	 0.127		  5	-0.188, 0.261			    6,4			 150/150	S,D,T,E,V,A,G,Q,R,K
  33	   K	  LYS122:U	-0.136		  5	-0.361, 0.014			    6,5			 150/150	S,I,E,L,T,N,R,K,Q
  34	   V	  VAL123:U	-0.417		  6	-0.646,-0.278			    7,6			 150/150	I,F,M,A,T,V,L
  35	   N	  ASN124:U	-0.052		  5	-0.278, 0.131			    6,5			 150/150	I,S,Q,K,R,L,V,N,E,T,M,A,H
  36	   V	  VAL125:U	-0.810		  8	-0.985,-0.709			    8,7			 150/150	V,L,Q,I
  37	   T	  THR126:U	 0.804		  2	 0.410, 1.046			    4,1			 150/150	S,F,I,Y,G,M,H,N,E,L,V,T,K,Q
  38	   V	  VAL127:U	-0.467		  7	-0.709,-0.361			    7,6			 150/150	L,V,F,I,R
  39	   E	  GLU128:U	 0.892		  2	 0.410, 1.046			    4,1			 150/150	S,F,I,K,R,Q,A,H,T,E,N
  40	   D	  ASP129:U	-1.041		  9	-1.181,-0.934			    9,8			 150/150	D,E,N,P,A
  41	   E	  GLU130:U	 0.119		  5	-0.188, 0.261			    6,4			 150/150	A,M,T,E,Q,P,S,I
  42	   R	  ARG131:U	 0.992		  2	 0.584, 1.392			    3,1			 150/150	H,C,M,V,L,R,Q,P,W,Y,F,I
  43	   T	  THR132:U	-1.184		  9	-1.327,-1.133			    9,9			 150/150	T,S,I
  44	   L	  LEU133:U	-0.305		  6	-0.581,-0.093			    7,5			 150/150	F,S,L,H,P,A,G
  45	   V	  VAL134:U	 0.150		  4	-0.188, 0.410			    6,4			 150/150	A,L,V,S,F,I,Y
  46	   R	  ARG135:U	 0.073		  5	-0.188, 0.261			    6,4			 150/150	S,I,F,Y,K,R,A,M,H,L,V,T
  47	   R	  ARG136:U	 2.757		  1	 1.392, 3.290			    1,1			 144/150	P,Y,I,S,E,V,N,G,M,A,Q,R,K
  48	   N	  ASN137:U	-0.684		  7	-0.880,-0.581			    8,7			 150/150	Q,K,S,D,E,N,G
  49	   N	  ASN138:U	 1.333		  1	 0.790, 1.915			    2,1			 150/150	R,K,Q,H,G,D,N,E,S
  50	   T	  THR139:U	-0.074		  5	-0.361, 0.131			    6,5			 150/150	G,A,H,N,E,T,K,R,Q,S
  51	   F	  PHE140:U	-0.084		  5	-0.361, 0.131			    6,5			 150/150	R,Q,T,L,S,W,F,Y,P
  52	   L	  LEU141:U	 0.221		  4	-0.093, 0.410			    5,4			 150/150	Q,R,K,E,L,V,M
  53	   S	  SER142:U	-0.270		  6	-0.511,-0.093			    7,5			 150/150	S,K,R,G,H,N,T
  54	   L	  LEU143:U	-0.521		  7	-0.709,-0.361			    7,6			 150/150	I,V,L,M
  55	   R	  ARG144:U	-0.933		  8	-1.085,-0.825			    9,8			 150/150	Q,R,H
  56	   D	  ASP145:U	-0.413		  6	-0.646,-0.278			    7,6			 150/150	K,Q,G,A,E,D
  57	   V	  VAL146:U	-0.645		  7	-0.825,-0.511			    8,7			 150/150	I,F,V
  58	   F	  PHE147:U	-0.903		  8	-1.085,-0.768			    9,8			 150/150	Y,F,L
  59	   G	  GLY148:U	 0.324		  4	 0.014, 0.584			    5,3			 150/150	S,N,T,A,G,M,H,Q,K,R
  60	   K	  LYS149:U	 0.746		  2	 0.410, 1.046			    4,1			 150/150	D,E,T,N,H,G,A,Q,R,K,Y
  61	   D	  ASP150:U	-0.317		  6	-0.581,-0.188			    7,6			 150/150	K,R,Q,T,N,E,D
  62	   L	  LEU151:U	-1.196		  9	-1.327,-1.133			    9,9			 150/150	F,L
  63	   I	  ILE152:U	 0.515		  3	 0.131, 0.790			    5,2			 150/150	S,I,H,M,G,L,T,E,V,N,R,K,Q
  64	   Y	  TYR153:U	-1.268		  9	-1.382,-1.229			    9,9			 150/150	Y,H
  65	   T	  THR154:U	 0.573		  3	 0.261, 0.790			    4,2			 150/150	K,R,A,G,M,N,T,L,V,D,S,I,Y
  66	   L	  LEU155:U	-0.400		  6	-0.646,-0.278			    7,6			 150/150	I,M,V,L
  67	   Y	  TYR156:U	 0.787		  2	 0.410, 1.046			    4,1			 150/150	M,A,H,L,T,K,S,I,F,Y
  68	   Y	  TYR157:U	-1.196		  9	-1.327,-1.133			    9,9			 150/150	Y,F
  69	   W	  TRP158:U	 0.070		  5	-0.278, 0.261			    6,4			 150/150	Y,F,W,S,Q,R,K,N,G
  70	   K	         -	-0.584		  7	-0.768,-0.439			    8,6			 150/150	K,R
  71	   S	         -	-0.484		  7	-0.709,-0.361			    7,6			 150/150	S,K,Q,A,G,D,T,E,N
  72	   S	         -	-0.059		  5	-0.361, 0.131			    6,5			 150/150	S,L,E,T,A,G,Q,R,K
  73	   S	         -	-1.219		  9	-1.327,-1.181			    9,9			 150/150	T,A,S
  74	   S	  SER163:U	-0.960		  8	-1.133,-0.880			    9,8			 150/150	T,S
  75	   G	  GLY164:U	-1.204		  9	-1.382,-1.133			    9,9			 150/150	R,G
  76	   K	  LYS165:U	-0.839		  8	-0.985,-0.709			    8,7			 150/150	K,S,R,Q,I,A,T
  77	   K	  LYS166:U	-1.272		  9	-1.382,-1.229			    9,9			 150/150	K,R
  78	   T	  THR167:U	 1.302		  1	 0.790, 1.392			    2,1			 150/150	P,F,W,D,T,L,N,E,C,M,A,Y,I,S,V,Q,R,K
  79	   A	  ALA168:U	 1.364		  1	 0.790, 1.915			    2,1			 150/150	S,I,Y,A,H,L,E,N,V,T,D,K,R,Q
  80	   K	  LYS169:U	 1.386		  1	 0.790, 1.915			    2,1			 150/150	H,M,D,V,L,E,N,T,R,K,Q,S,Y,F,I
  81	   T	  THR170:U	-0.757		  8	-0.934,-0.646			    8,7			 149/150	S,Y,F,K,G,V,T,E
  82	   N	  ASN171:U	 0.566		  3	 0.261, 0.790			    4,2			 149/150	Q,K,R,T,N,E,D,G,A,H,S
  83	   T	  THR172:U	-0.557		  7	-0.768,-0.439			    8,6			 149/150	S,I,R,G,D,E,T,N
  84	   N	  ASN173:U	-0.515		  7	-0.709,-0.361			    7,6			 149/150	S,E,L,T,N,D,H,Q,R
  85	   E	  GLU174:U	 1.038		  1	 0.584, 1.392			    3,1			 149/150	S,I,M,G,D,L,V,N,T,E,R,K,Q
  86	   F	  PHE175:U	-0.780		  8	-0.985,-0.646			    8,7			 149/150	I,F,T,L,V,A
  87	   L	  LEU176:U	 0.188		  4	-0.093, 0.410			    5,4			 149/150	P,S,F,I,T,V,E,L,D,K
  88	   I	  ILE177:U	 0.483		  3	 0.131, 0.790			    5,2			 149/150	I,F,V,E,L,A,M
  89	   D	  ASP178:U	 1.082		  1	 0.584, 1.392			    3,1			 149/150	P,S,D,L,N,E,G,A,Q,R,K
  90	   V	  VAL179:U	-0.350		  6	-0.581,-0.188			    7,6			 149/150	I,M,T,L,V
  91	   D	  ASP180:U	-0.708		  7	-0.880,-0.581			    8,7			 149/150	S,E,D
  92	   K	  LYS181:U	 0.700		  3	 0.261, 1.046			    4,1			 149/150	Q,R,K,T,N,E,H,G,S,P
  93	   G	  GLY182:U	 0.120		  5	-0.278, 0.410			    6,4			 148/150	D,T,E,M,A,G,K,P,S
  94	   E	  GLU183:U	 0.596		  3	 0.261, 0.790			    4,2			 149/150	F,I,S,Q,R,K,D,V,E,T,N,G
  95	   N	  ASN184:U	-0.238		  6	-0.439,-0.093			    6,5			 149/150	D,V,N,G,Y,K,S
  96	   Y	  TYR185:U	-1.329		  9	-1.417,-1.277			    9,9			 149/150	Y
  97	   C	  CYS186:U	-1.294		  9	-1.417,-1.229			    9,9			 149/150	C
  98	   F	  PHE187:U	-1.189		  9	-1.327,-1.133			    9,9			 149/150	F,V,L
  99	   S	  SER188:U	 1.456		  1	 0.790, 1.915			    2,1			 149/150	Q,K,V,L,T,N,M,H,C,I,F,Y,S
 100	   V	  VAL189:U	-0.624		  7	-0.825,-0.511			    8,7			 149/150	A,V,I
 101	   Q	  GLN190:U	-0.976		  8	-1.133,-0.880			    9,8			 149/150	A,H,E,L,R,Q
 102	   A	  ALA191:U	-0.928		  8	-1.085,-0.825			    9,8			 149/150	T,V,G,A,P,I
 103	   V	  VAL192:U	 1.646		  1	 1.046, 1.915			    1,1			 149/150	R,K,N,T,E,V,L,H,M,G,A,Y,F,I
 104	   I	  ILE193:U	-0.923		  8	-1.085,-0.825			    9,8			 149/150	I,F,V,T
 105	   P	  PRO194:U	 1.121		  1	 0.584, 1.392			    3,1			 149/150	S,F,I,P,R,K,Q,A,L,T,N
 106	   S	  SER195:U	-1.103		  9	-1.229,-1.036			    9,9			 149/150	S,I,Y,T,N,D
 107	   R	  ARG196:U	-1.124		  9	-1.277,-1.036			    9,9			 148/150	R,N,T,G,P
 108	   T	  THR197:U	 1.200		  1	 0.790, 1.392			    2,1			 148/150	K,R,A,G,H,L,V,T,N,D,S,F,P
 109	   V	  VAL198:U	 3.248		  1	 1.915, 3.290			    1,1			 146/150	F,P,M,A,G,D,T,N,E,L,S,Y,I,R,K,Q,V
 110	   N	  ASN199:U	-0.466		  7	-0.646,-0.361			    7,6			 144/150	K,D,T,N,E,G,A,S
 111	   R	  ARG200:U	-0.180		  6	-0.439, 0.014			    6,5			 144/150	S,P,Q,R,K,N,H,A
 112	   K	  LYS201:U	 0.380		  4	 0.014, 0.584			    5,3			 139/150	I,Y,S,P,Q,K,R,V,T,E,N,D,A
 113	   S	  SER202:U	-0.926		  8	-1.085,-0.825			    9,8			 128/150	K,S,F,G,L,N
 114	   T	  THR203:U	 0.320		  4	-0.093, 0.584			    5,3			 124/150	P,S,G,A,V,E,L,T,Q
 115	   D	  ASP204:U	-0.447		  7	-0.709,-0.278			    7,6			 123/150	K,Q,G,D,T,E,V
 116	   S	  SER205:U	-1.191		  9	-1.327,-1.133			    9,9			 119/150	E,L,Q,S
 117	   P	  PRO206:U	 3.284		  1	 1.915, 3.290			    1,1			 119/150	S,I,F,P,K,R,M,A,V,N,E,L,T,D
 118	   V	  VAL207:U	 0.933		  2	 0.410, 1.392			    4,1			 119/150	S,F,I,Q,M,A,H,E,T,V,D
 119	   E	  GLU208:U	 1.128		  1	 0.584, 1.392			    3,1			 118/150	H,A,E,T,N,L,V,R,K,Q,F,I
 120	   C	  CYS209:U	-0.972		  8	-1.181,-0.825			    9,8			 118/150	C,Q,K
 121	   M	  MET210:U	-0.416		  6	-0.985,-0.093			    8,5			   8/150	C,M


*Below the confidence cut-off - The calculations for this site were performed on less than 6 non-gaped homologue sequences,
or the confidence interval for the estimated score is equal to- or larger than- 4 color grades.
