Support Vector Machines with Linear Kernel 

509 samples
 15 predictor
  2 classes: 'HS', 'NHS' 

No pre-processing
Resampling: Cross-Validated (10 fold, repeated 3 times) 
Summary of sample sizes: 458, 459, 459, 459, 457, 458, ... 
Resampling results across tuning parameters:

  cost  ROC        Sens       Spec     
  0.1   0.8122175  0.4493590  0.9497076
  0.3   0.8085960  0.4589744  0.9436797
  0.5   0.8072862  0.4561966  0.9419478
  0.7   0.8066706  0.4591880  0.9384840
  0.9   0.8063494  0.4591880  0.9367521
  1.1   0.8061412  0.4591880  0.9358974
  1.3   0.8054179  0.4566239  0.9367521
  1.5   0.8054489  0.4566239  0.9358974
  1.7   0.8050251  0.4538462  0.9350202
  1.9   0.8048919  0.4538462  0.9341430

ROC was used to select the optimal model using the largest value.
The final value used for the model was cost = 0.1.
