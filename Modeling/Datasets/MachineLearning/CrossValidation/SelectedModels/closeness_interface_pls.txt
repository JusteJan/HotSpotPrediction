Partial Least Squares Generalized Linear Models  

509 samples
 15 predictor
  2 classes: 'HS', 'NHS' 

No pre-processing
Resampling: Cross-Validated (10 fold, repeated 3 times) 
Summary of sample sizes: 459, 458, 458, 458, 457, 458, ... 
Resampling results across tuning parameters:

  nt  alpha.pvals.expli  ROC        Sens       Spec     
  1   0.05               0.8109560  0.3876068  0.9367746
  1   0.10               0.8100724  0.3876068  0.9367746
  2   0.05               0.8196864  0.4589744  0.9419703
  2   0.10               0.8197962  0.4670940  0.9410706
  3   0.05               0.8196864  0.4589744  0.9419703
  3   0.10               0.8188460  0.4670940  0.9410706

ROC was used to select the optimal model using the largest value.
The final values used for the model were nt = 2 and alpha.pvals.expli = 0.1.
