Support Vector Machines with Linear Kernel 

518 samples
 14 predictor
  2 classes: 'HS', 'NHS' 

No pre-processing
Resampling: Cross-Validated (10 fold, repeated 3 times) 
Summary of sample sizes: 466, 466, 466, 467, 466, 466, ... 
Resampling results across tuning parameters:

  cost  ROC        Sens       Spec     
  0.1   0.8173954  0.4079060  0.9478632
  0.3   0.8144532  0.4134615  0.9427350
  0.5   0.8139218  0.4185897  0.9410256
  0.7   0.8137245  0.4185897  0.9418803
  0.9   0.8130095  0.4211538  0.9410256
  1.1   0.8126151  0.4211538  0.9393162
  1.3   0.8123110  0.4211538  0.9393162
  1.5   0.8122452  0.4211538  0.9401709
  1.7   0.8121083  0.4211538  0.9401709
  1.9   0.8123000  0.4211538  0.9401709

ROC was used to select the optimal model using  the largest value.
The final value used for the model was cost = 0.1. 
