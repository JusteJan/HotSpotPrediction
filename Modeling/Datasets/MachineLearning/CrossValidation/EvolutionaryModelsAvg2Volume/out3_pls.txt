Partial Least Squares Generalized Linear Models  

518 samples
 14 predictor
  2 classes: 'HS', 'NHS' 

No pre-processing
Resampling: Cross-Validated (10 fold, repeated 3 times) 
Summary of sample sizes: 466, 466, 466, 466, 466, 467, ... 
Resampling results across tuning parameters:

  nt  alpha.pvals.expli  ROC        Sens       Spec     
  1   0.05               0.8044762  0.3784188  0.9410256
  1   0.10               0.8036544  0.3784188  0.9393162
  2   0.05               0.8179925  0.4339744  0.9461538
  2   0.10               0.8154504  0.4284188  0.9452991
  3   0.05               0.8110399  0.4442308  0.9384615
  3   0.10               0.8115659  0.4335470  0.9427350

ROC was used to select the optimal model using  the largest value.
The final values used for the model were nt = 2 and alpha.pvals.expli = 0.05. 
