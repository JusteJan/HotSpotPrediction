Support Vector Machines with Linear Kernel 

518 samples
 14 predictor
  2 classes: 'HS', 'NHS' 

No pre-processing
Resampling: Cross-Validated (10 fold, repeated 3 times) 
Summary of sample sizes: 467, 467, 466, 466, 466, 466, ... 
Resampling results across tuning parameters:

  cost  ROC        Sens       Spec     
  0.1   0.8125301  0.4352564  0.9350427
  0.3   0.8109851  0.4455128  0.9316239
  0.5   0.8103632  0.4455128  0.9299145
  0.7   0.8097167  0.4429487  0.9282051
  0.9   0.8097195  0.4455128  0.9273504
  1.1   0.8097304  0.4455128  0.9290598
  1.3   0.8095880  0.4480769  0.9282051
  1.5   0.8100482  0.4480769  0.9290598
  1.7   0.8099167  0.4480769  0.9290598
  1.9   0.8099167  0.4480769  0.9290598

ROC was used to select the optimal model using  the largest value.
The final value used for the model was cost = 0.1. 
