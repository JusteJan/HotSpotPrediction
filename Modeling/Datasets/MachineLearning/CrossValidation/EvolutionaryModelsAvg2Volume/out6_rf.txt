Random Forest 

518 samples
 14 predictor
  2 classes: 'HS', 'NHS' 

No pre-processing
Resampling: Cross-Validated (10 fold, repeated 3 times) 
Summary of sample sizes: 466, 467, 466, 466, 466, 466, ... 
Resampling results across tuning parameters:

  mtry  ROC        Sens       Spec     
  1     0.8127055  0.2418803  0.9692308
  2     0.8064733  0.4010684  0.9350427
  3     0.8053254  0.4245726  0.9145299
  4     0.8049419  0.4115385  0.9119658
  5     0.8036818  0.4297009  0.9051282

ROC was used to select the optimal model using  the largest value.
The final value used for the model was mtry = 1. 
