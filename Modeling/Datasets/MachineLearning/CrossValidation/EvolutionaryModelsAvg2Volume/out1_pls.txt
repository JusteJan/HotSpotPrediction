Partial Least Squares Generalized Linear Models  

518 samples
 14 predictor
  2 classes: 'HS', 'NHS' 

No pre-processing
Resampling: Cross-Validated (10 fold, repeated 3 times) 
Summary of sample sizes: 467, 466, 466, 467, 466, 466, ... 
Resampling results across tuning parameters:

  nt  alpha.pvals.expli  ROC        Sens       Spec     
  1   0.05               0.8044050  0.4008547  0.9367521
  1   0.10               0.8025312  0.4008547  0.9341880
  2   0.05               0.8179980  0.4318376  0.9367521
  2   0.10               0.8153682  0.4397436  0.9418803
  3   0.05               0.8133027  0.4576923  0.9350427
  3   0.10               0.8121247  0.4634615  0.9324786

ROC was used to select the optimal model using  the largest value.
The final values used for the model were nt = 2 and alpha.pvals.expli = 0.05. 
