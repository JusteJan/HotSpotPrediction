Support Vector Machines with Linear Kernel 

518 samples
 14 predictor
  2 classes: 'HS', 'NHS' 

No pre-processing
Resampling: Cross-Validated (10 fold, repeated 3 times) 
Summary of sample sizes: 466, 467, 467, 466, 466, 466, ... 
Resampling results across tuning parameters:

  cost  ROC        Sens       Spec     
  0.1   0.8073197  0.4224359  0.9487179
  0.3   0.8036270  0.4250000  0.9444444
  0.5   0.8025531  0.4303419  0.9444444
  0.7   0.8023395  0.4275641  0.9435897
  0.9   0.8022354  0.4275641  0.9427350
  1.1   0.8023395  0.4301282  0.9427350
  1.3   0.8016710  0.4273504  0.9427350
  1.5   0.8009752  0.4303419  0.9418803
  1.7   0.8003342  0.4303419  0.9418803
  1.9   0.8003342  0.4301282  0.9418803

ROC was used to select the optimal model using  the largest value.
The final value used for the model was cost = 0.1. 
