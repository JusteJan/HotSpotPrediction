Partial Least Squares Generalized Linear Models  

518 samples
 14 predictor
  2 classes: 'HS', 'NHS' 

No pre-processing
Resampling: Cross-Validated (10 fold, repeated 3 times) 
Summary of sample sizes: 466, 466, 466, 466, 466, 466, ... 
Resampling results across tuning parameters:

  nt  alpha.pvals.expli  ROC        Sens       Spec     
  1   0.05               0.8056323  0.3935897  0.9384615
  1   0.10               0.8045584  0.3935897  0.9384615
  2   0.05               0.8198663  0.4386752  0.9367521
  2   0.10               0.8197239  0.4442308  0.9401709
  3   0.05               0.8161626  0.4414530  0.9341880
  3   0.10               0.8157353  0.4645299  0.9367521

ROC was used to select the optimal model using  the largest value.
The final values used for the model were nt = 2 and alpha.pvals.expli = 0.05. 
