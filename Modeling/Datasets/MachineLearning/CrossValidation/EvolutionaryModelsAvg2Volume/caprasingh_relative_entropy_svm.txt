Support Vector Machines with Linear Kernel 

518 samples
 14 predictor
  2 classes: 'HS', 'NHS' 

No pre-processing
Resampling: Cross-Validated (10 fold, repeated 3 times) 
Summary of sample sizes: 466, 466, 466, 466, 467, 466, ... 
Resampling results across tuning parameters:

  cost  ROC        Sens       Spec     
  0.1   0.8078512  0.4083333  0.9461538
  0.3   0.8071389  0.4104701  0.9435897
  0.5   0.8063911  0.4076923  0.9367521
  0.7   0.8060569  0.4102564  0.9350427
  0.9   0.8056816  0.4102564  0.9350427
  1.1   0.8055391  0.4102564  0.9350427
  1.3   0.8052761  0.4102564  0.9350427
  1.5   0.8053748  0.4102564  0.9350427
  1.7   0.8054898  0.4102564  0.9358974
  1.9   0.8050186  0.4102564  0.9358974

ROC was used to select the optimal model using  the largest value.
The final value used for the model was cost = 0.1. 
