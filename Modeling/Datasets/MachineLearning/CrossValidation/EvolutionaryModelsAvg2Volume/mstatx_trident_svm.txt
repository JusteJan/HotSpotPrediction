Support Vector Machines with Linear Kernel 

518 samples
 14 predictor
  2 classes: 'HS', 'NHS' 

No pre-processing
Resampling: Cross-Validated (10 fold, repeated 3 times) 
Summary of sample sizes: 467, 466, 466, 467, 466, 466, ... 
Resampling results across tuning parameters:

  cost  ROC        Sens       Spec     
  0.1   0.8069910  0.4064103  0.9470085
  0.3   0.8047556  0.4166667  0.9444444
  0.5   0.8031996  0.4166667  0.9427350
  0.7   0.8027175  0.4192308  0.9435897
  0.9   0.8021861  0.4192308  0.9435897
  1.1   0.8019669  0.4192308  0.9435897
  1.3   0.8014738  0.4192308  0.9435897
  1.5   0.8013697  0.4217949  0.9444444
  1.7   0.8010410  0.4217949  0.9427350
  1.9   0.8009643  0.4217949  0.9427350

ROC was used to select the optimal model using  the largest value.
The final value used for the model was cost = 0.1. 
