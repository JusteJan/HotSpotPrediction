Random Forest 

518 samples
 14 predictor
  2 classes: 'HS', 'NHS' 

No pre-processing
Resampling: Cross-Validated (10 fold, repeated 3 times) 
Summary of sample sizes: 466, 467, 466, 466, 466, 467, ... 
Resampling results across tuning parameters:

  mtry  ROC        Sens       Spec     
  1     0.8089963  0.2301282  0.9743590
  2     0.8077115  0.3967949  0.9316239
  3     0.8057829  0.4017094  0.9136752
  4     0.8040872  0.4173077  0.9119658
  5     0.8037311  0.4226496  0.9128205

ROC was used to select the optimal model using  the largest value.
The final value used for the model was mtry = 1. 
