Partial Least Squares Generalized Linear Models  

518 samples
 14 predictor
  2 classes: 'HS', 'NHS' 

No pre-processing
Resampling: Cross-Validated (10 fold, repeated 3 times) 
Summary of sample sizes: 466, 466, 466, 467, 466, 466, ... 
Resampling results across tuning parameters:

  nt  alpha.pvals.expli  ROC        Sens       Spec     
  1   0.05               0.7961703  0.3506410  0.9367521
  1   0.10               0.7941321  0.3557692  0.9358974
  2   0.05               0.8051666  0.3891026  0.9401709
  2   0.10               0.8046899  0.3816239  0.9427350
  3   0.05               0.7964168  0.4275641  0.9324786
  3   0.10               0.7999288  0.4179487  0.9324786

ROC was used to select the optimal model using  the largest value.
The final values used for the model were nt = 2 and alpha.pvals.expli = 0.05. 
