Support Vector Machines with Linear Kernel 

518 samples
 17 predictor
  2 classes: 'HS', 'NHS' 

No pre-processing
Resampling: Cross-Validated (10 fold, repeated 3 times) 
Summary of sample sizes: 466, 466, 466, 466, 466, 467, ... 
Resampling results across tuning parameters:

  cost  ROC        Sens       Spec     
  0.1   0.8134287  0.4474359  0.9427350
  0.3   0.8132424  0.4525641  0.9376068
  0.5   0.8123439  0.4576923  0.9367521
  0.7   0.8117521  0.4549145  0.9367521
  0.9   0.8120425  0.4549145  0.9350427
  1.1   0.8122452  0.4549145  0.9350427
  1.3   0.8122124  0.4549145  0.9358974
  1.5   0.8118069  0.4549145  0.9358974
  1.7   0.8120151  0.4549145  0.9358974
  1.9   0.8116754  0.4549145  0.9367521

ROC was used to select the optimal model using  the largest value.
The final value used for the model was cost = 0.1. 
