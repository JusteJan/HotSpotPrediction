Support Vector Machines with Linear Kernel 

518 samples
 16 predictor
  2 classes: 'HS', 'NHS' 

No pre-processing
Resampling: Cross-Validated (10 fold, repeated 3 times) 
Summary of sample sizes: 466, 466, 466, 466, 466, 466, ... 
Resampling results across tuning parameters:

  cost  ROC        Sens       Spec     
  0.1   0.8094455  0.4410256  0.9410256
  0.3   0.8087552  0.4435897  0.9393162
  0.5   0.8079279  0.4435897  0.9384615
  0.7   0.8072321  0.4435897  0.9376068
  0.9   0.8064596  0.4435897  0.9376068
  1.1   0.8064157  0.4435897  0.9376068
  1.3   0.8065911  0.4435897  0.9376068
  1.5   0.8064267  0.4382479  0.9376068
  1.7   0.8062295  0.4408120  0.9376068
  1.9   0.8059665  0.4408120  0.9376068

ROC was used to select the optimal model using  the largest value.
The final value used for the model was cost = 0.1. 
