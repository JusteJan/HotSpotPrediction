Support Vector Machines with Linear Kernel 

518 samples
 16 predictor
  2 classes: 'HS', 'NHS' 

No pre-processing
Resampling: Cross-Validated (10 fold, repeated 3 times) 
Summary of sample sizes: 466, 467, 466, 466, 466, 466, ... 
Resampling results across tuning parameters:

  cost  ROC        Sens       Spec     
  0.1   0.8132917  0.4442308  0.9410256
  0.3   0.8117631  0.4470085  0.9376068
  0.5   0.8107221  0.4442308  0.9358974
  0.7   0.8099112  0.4442308  0.9341880
  0.9   0.8092319  0.4467949  0.9341880
  1.1   0.8094291  0.4467949  0.9350427
  1.3   0.8094236  0.4467949  0.9333333
  1.5   0.8099606  0.4442308  0.9324786
  1.7   0.8095113  0.4416667  0.9324786
  1.9   0.8095770  0.4416667  0.9324786

ROC was used to select the optimal model using  the largest value.
The final value used for the model was cost = 0.1. 
