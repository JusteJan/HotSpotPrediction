Partial Least Squares Generalized Linear Models  

518 samples
 17 predictor
  2 classes: 'HS', 'NHS' 

No pre-processing
Resampling: Cross-Validated (10 fold, repeated 3 times) 
Summary of sample sizes: 466, 467, 466, 466, 466, 467, ... 
Resampling results across tuning parameters:

  nt  alpha.pvals.expli  ROC        Sens       Spec     
  1   0.05               0.8150559  0.4053419  0.9376068
  1   0.10               0.8148586  0.4027778  0.9376068
  2   0.05               0.8224140  0.4418803  0.9367521
  2   0.10               0.8220962  0.4393162  0.9376068
  3   0.05               0.8203758  0.4470085  0.9376068
  3   0.10               0.8182939  0.4395299  0.9401709

ROC was used to select the optimal model using  the largest value.
The final values used for the model were nt = 2 and alpha.pvals.expli = 0.05. 
