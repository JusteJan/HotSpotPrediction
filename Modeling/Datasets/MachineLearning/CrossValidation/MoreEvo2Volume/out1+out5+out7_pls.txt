Partial Least Squares Generalized Linear Models  

518 samples
 16 predictor
  2 classes: 'HS', 'NHS' 

No pre-processing
Resampling: Cross-Validated (10 fold, repeated 3 times) 
Summary of sample sizes: 467, 466, 466, 466, 466, 466, ... 
Resampling results across tuning parameters:

  nt  alpha.pvals.expli  ROC        Sens       Spec     
  1   0.05               0.8105194  0.4032051  0.9367521
  1   0.10               0.8097085  0.4032051  0.9367521
  2   0.05               0.8182610  0.4220085  0.9367521
  2   0.10               0.8176090  0.4247863  0.9410256
  3   0.05               0.8128041  0.4350427  0.9367521
  3   0.10               0.8167762  0.4529915  0.9367521

ROC was used to select the optimal model using  the largest value.
The final values used for the model were nt = 2 and alpha.pvals.expli = 0.05. 
