Support Vector Machines with Linear Kernel 

518 samples
 16 predictor
  2 classes: 'HS', 'NHS' 

No pre-processing
Resampling: Cross-Validated (10 fold, repeated 3 times) 
Summary of sample sizes: 466, 466, 466, 467, 466, 466, ... 
Resampling results across tuning parameters:

  cost  ROC        Sens       Spec     
  0.1   0.8127986  0.4497863  0.9435897
  0.3   0.8094236  0.4472222  0.9367521
  0.5   0.8083224  0.4472222  0.9384615
  0.7   0.8083114  0.4446581  0.9376068
  0.9   0.8080868  0.4444444  0.9376068
  1.1   0.8076813  0.4444444  0.9376068
  1.3   0.8077252  0.4444444  0.9376068
  1.5   0.8080156  0.4444444  0.9367521
  1.7   0.8080539  0.4444444  0.9367521
  1.9   0.8076430  0.4418803  0.9350427

ROC was used to select the optimal model using  the largest value.
The final value used for the model was cost = 0.1. 
