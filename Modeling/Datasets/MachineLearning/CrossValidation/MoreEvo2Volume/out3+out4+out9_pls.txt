Partial Least Squares Generalized Linear Models  

518 samples
 16 predictor
  2 classes: 'HS', 'NHS' 

No pre-processing
Resampling: Cross-Validated (10 fold, repeated 3 times) 
Summary of sample sizes: 466, 466, 466, 466, 466, 466, ... 
Resampling results across tuning parameters:

  nt  alpha.pvals.expli  ROC        Sens       Spec     
  1   0.05               0.8101085  0.4089744  0.9341880
  1   0.10               0.8081416  0.4089744  0.9333333
  2   0.05               0.8221948  0.4474359  0.9384615
  2   0.10               0.8177679  0.4474359  0.9341880
  3   0.05               0.8208142  0.4474359  0.9384615
  3   0.10               0.8127438  0.4446581  0.9376068

ROC was used to select the optimal model using  the largest value.
The final values used for the model were nt = 2 and alpha.pvals.expli = 0.05. 
