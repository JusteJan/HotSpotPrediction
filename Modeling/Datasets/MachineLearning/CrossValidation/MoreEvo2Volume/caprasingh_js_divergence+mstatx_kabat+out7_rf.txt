Random Forest 

518 samples
 16 predictor
  2 classes: 'HS', 'NHS' 

No pre-processing
Resampling: Cross-Validated (10 fold, repeated 3 times) 
Summary of sample sizes: 466, 466, 466, 466, 467, 466, ... 
Resampling results across tuning parameters:

  mtry  ROC        Sens       Spec     
  1     0.8102372  0.2467949  0.9760684
  2     0.8097496  0.4104701  0.9367521
  3     0.8067445  0.4264957  0.9282051
  4     0.8056569  0.4316239  0.9205128
  5     0.8037667  0.4316239  0.9145299

ROC was used to select the optimal model using  the largest value.
The final value used for the model was mtry = 1. 
