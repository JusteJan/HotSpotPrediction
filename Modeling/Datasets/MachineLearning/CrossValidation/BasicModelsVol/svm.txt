Support Vector Machines with Linear Kernel 

518 samples
 13 predictor
  2 classes: 'HS', 'NHS' 

No pre-processing
Resampling: Cross-Validated (10 fold, repeated 3 times) 
Summary of sample sizes: 466, 466, 467, 466, 466, 466, ... 
Resampling results across tuning parameters:

  cost  ROC        Sens       Spec     
  0.1   0.8009643  0.3600427  0.9521368
  0.3   0.7991617  0.3728632  0.9487179
  0.5   0.7991398  0.3782051  0.9470085
  0.7   0.7977646  0.3756410  0.9461538
  0.9   0.7970305  0.3756410  0.9452991
  1.1   0.7972003  0.3756410  0.9452991
  1.3   0.7972441  0.3756410  0.9452991
  1.5   0.7972058  0.3784188  0.9452991
  1.7   0.7970085  0.3784188  0.9452991
  1.9   0.7973702  0.3784188  0.9461538

ROC was used to select the optimal model using  the largest value.
The final value used for the model was cost = 0.1. 
