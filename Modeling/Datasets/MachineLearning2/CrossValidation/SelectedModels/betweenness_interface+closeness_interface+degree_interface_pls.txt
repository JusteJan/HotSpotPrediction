Partial Least Squares Generalized Linear Models  

509 samples
 17 predictor
  2 classes: 'HS', 'NHS' 

No pre-processing
Resampling: Cross-Validated (10 fold, repeated 3 times) 
Summary of sample sizes: 457, 458, 457, 459, 458, 458, ... 
Resampling results across tuning parameters:

  nt  alpha.pvals.expli  ROC        Sens       Spec     
  1   0.05               0.8143953  0.4002137  0.9420153
  1   0.10               0.8138178  0.4029915  0.9420153
  2   0.05               0.8205920  0.4626068  0.9446469
  2   0.10               0.8194939  0.4596154  0.9446244
  3   0.05               0.8186771  0.4790598  0.9324786
  3   0.10               0.8185326  0.4572650  0.9350877

ROC was used to select the optimal model using the largest value.
The final values used for the model were nt = 2 and alpha.pvals.expli = 0.05.
