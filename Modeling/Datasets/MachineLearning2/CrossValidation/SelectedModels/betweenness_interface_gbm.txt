Stochastic Gradient Boosting 

509 samples
 15 predictor
  2 classes: 'HS', 'NHS' 

No pre-processing
Resampling: Cross-Validated (10 fold, repeated 3 times) 
Summary of sample sizes: 458, 458, 458, 458, 457, 458, ... 
Resampling results across tuning parameters:

  interaction.depth  n.minobsinnode  n.trees  ROC        Sens       Spec     
  1                   5              2000     0.8050679  0.2181624  0.9715025
  1                   5              2100     0.8060521  0.2235043  0.9715025
  1                   5              2200     0.8066600  0.2288462  0.9697931
  1                   5              2300     0.8076630  0.2444444  0.9680387
  1                   5              2400     0.8081020  0.2555556  0.9663293
  1                   5              2500     0.8082460  0.2666667  0.9654521
  1                   5              2600     0.8087400  0.2722222  0.9654521
  1                   5              2700     0.8091498  0.2854701  0.9628430
  1                   5              2800     0.8096346  0.2905983  0.9611111
  1                   5              2900     0.8099456  0.2987179  0.9602564
  1                   5              3000     0.8096245  0.3121795  0.9559379
  1                   5              3100     0.8099039  0.3260684  0.9524966
  1                   5              3200     0.8098311  0.3339744  0.9499100
  1                   5              3300     0.8106660  0.3367521  0.9481781
  1                   5              3400     0.8115227  0.3444444  0.9464462
  1                   5              3500     0.8114442  0.3581197  0.9438596
  1                   5              3600     0.8120628  0.3743590  0.9438596
  1                   5              3700     0.8122635  0.3743590  0.9412731
  1                   5              3800     0.8121318  0.3794872  0.9404184
  1                   5              3900     0.8123475  0.3850427  0.9404184
  1                   5              4000     0.8127219  0.3933761  0.9404184
  1                   5              4100     0.8130051  0.3961538  0.9386640
  1                   5              4200     0.8134235  0.4068376  0.9386640
  1                   5              4300     0.8132627  0.4096154  0.9386640
  1                   5              4400     0.8129909  0.4149573  0.9378093
  1                   5              4500     0.8133249  0.4147436  0.9360774
  1                   5              4600     0.8135348  0.4173077  0.9352227
  1                   5              4700     0.8134543  0.4226496  0.9352227
  1                   5              4800     0.8135256  0.4226496  0.9352227
  1                   5              4900     0.8135091  0.4226496  0.9352227
  1                   5              5000     0.8135147  0.4254274  0.9343680
  1                  10              2000     0.8052769  0.2181624  0.9706478
  1                  10              2100     0.8060675  0.2288462  0.9697706
  1                  10              2200     0.8066356  0.2316239  0.9689159
  1                  10              2300     0.8073455  0.2529915  0.9680612
  1                  10              2400     0.8081421  0.2585470  0.9671840
  1                  10              2500     0.8082592  0.2690171  0.9663293
  1                  10              2600     0.8082947  0.2826923  0.9636977
  1                  10              2700     0.8085763  0.2910256  0.9602564
  1                  10              2800     0.8089832  0.3068376  0.9585245
  1                  10              2900     0.8089879  0.3094017  0.9559379
  1                  10              3000     0.8096439  0.3205128  0.9507872
  1                  10              3100     0.8100732  0.3258547  0.9499325
  1                  10              3200     0.8108364  0.3314103  0.9482006
  1                  10              3300     0.8106924  0.3502137  0.9473459
  1                  10              3400     0.8114012  0.3664530  0.9464687
  1                  10              3500     0.8114801  0.3692308  0.9456140
  1                  10              3600     0.8120954  0.3717949  0.9438821
  1                  10              3700     0.8125848  0.3797009  0.9438821
  1                  10              3800     0.8131175  0.3824786  0.9430049
  1                  10              3900     0.8135903  0.3905983  0.9421278
  1                  10              4000     0.8133933  0.4012821  0.9412506
  1                  10              4100     0.8137970  0.4038462  0.9395412
  1                  10              4200     0.8134464  0.4119658  0.9386640
  1                  10              4300     0.8136546  0.4119658  0.9386640
  1                  10              4400     0.8137989  0.4147436  0.9378093
  1                  10              4500     0.8142811  0.4173077  0.9360774
  1                  10              4600     0.8140638  0.4173077  0.9360774
  1                  10              4700     0.8140090  0.4200855  0.9352227
  1                  10              4800     0.8139363  0.4200855  0.9352227
  1                  10              4900     0.8135744  0.4200855  0.9352227
  1                  10              5000     0.8132786  0.4254274  0.9343680
  2                   5              2000     0.8119456  0.3256410  0.9498650
  2                   5              2100     0.8120114  0.3444444  0.9481556
  2                   5              2200     0.8131086  0.3527778  0.9455691
  2                   5              2300     0.8127582  0.3611111  0.9446919
  2                   5              2400     0.8132884  0.3745726  0.9429600
  2                   5              2500     0.8125288  0.3799145  0.9420828
  2                   5              2600     0.8121032  0.3876068  0.9386415
  2                   5              2700     0.8123359  0.3955128  0.9377868
  2                   5              2800     0.8124348  0.3955128  0.9369321
  2                   5              2900     0.8132275  0.4010684  0.9369321
  2                   5              3000     0.8130795  0.4008547  0.9352227
  2                   5              3100     0.8125807  0.4008547  0.9343455
  2                   5              3200     0.8127889  0.4089744  0.9343455
  2                   5              3300     0.8130028  0.4061966  0.9334908
  2                   5              3400     0.8129826  0.4141026  0.9317364
  2                   5              3500     0.8131781  0.4115385  0.9308592
  2                   5              3600     0.8129439  0.4194444  0.9299820
  2                   5              3700     0.8129422  0.4194444  0.9291048
  2                   5              3800     0.8131523  0.4168803  0.9273729
  2                   5              3900     0.8137895  0.4168803  0.9256410
  2                   5              4000     0.8135214  0.4168803  0.9256410
  2                   5              4100     0.8129046  0.4168803  0.9247638
  2                   5              4200     0.8127571  0.4168803  0.9247638
  2                   5              4300     0.8129485  0.4168803  0.9247638
  2                   5              4400     0.8125360  0.4194444  0.9221772
  2                   5              4500     0.8130146  0.4247863  0.9204229
  2                   5              4600     0.8122023  0.4247863  0.9204229
  2                   5              4700     0.8125271  0.4247863  0.9204229
  2                   5              4800     0.8125307  0.4220085  0.9204229
  2                   5              4900     0.8118895  0.4271368  0.9195682
  2                   5              5000     0.8121013  0.4271368  0.9195682
  2                  10              2000     0.8123629  0.3369658  0.9490103
  2                  10              2100     0.8126459  0.3504274  0.9473009
  2                  10              2200     0.8130020  0.3585470  0.9464462
  2                  10              2300     0.8134767  0.3692308  0.9455915
  2                  10              2400     0.8139813  0.3720085  0.9438596
  2                  10              2500     0.8136272  0.3771368  0.9438596
  2                  10              2600     0.8142627  0.3880342  0.9430049
  2                  10              2700     0.8140584  0.3933761  0.9395187
  2                  10              2800     0.8138409  0.3933761  0.9395187
  2                  10              2900     0.8140509  0.3959402  0.9377868
  2                  10              3000     0.8139999  0.4010684  0.9369096
  2                  10              3100     0.8144894  0.4064103  0.9351777
  2                  10              3200     0.8139197  0.4115385  0.9351777
  2                  10              3300     0.8134118  0.4143162  0.9325686
  2                  10              3400     0.8133954  0.4198718  0.9325911
  2                  10              3500     0.8133410  0.4198718  0.9317364
  2                  10              3600     0.8132209  0.4198718  0.9291498
  2                  10              3700     0.8128101  0.4170940  0.9291498
  2                  10              3800     0.8132153  0.4170940  0.9282726
  2                  10              3900     0.8134947  0.4170940  0.9291498
  2                  10              4000     0.8136501  0.4170940  0.9274179
  2                  10              4100     0.8130894  0.4222222  0.9256860
  2                  10              4200     0.8131495  0.4273504  0.9221772
  2                  10              4300     0.8127770  0.4299145  0.9221772
  2                  10              4400     0.8124373  0.4326923  0.9221772
  2                  10              4500     0.8128757  0.4352564  0.9213225
  2                  10              4600     0.8119848  0.4352564  0.9221772
  2                  10              4700     0.8121290  0.4380342  0.9221772
  2                  10              4800     0.8119846  0.4380342  0.9221772
  2                  10              4900     0.8114897  0.4380342  0.9221772
  2                  10              5000     0.8115516  0.4380342  0.9213000

Tuning parameter 'shrinkage' was held constant at a value of 0.001
ROC was used to select the optimal model using the largest value.
The final values used for the model were n.trees = 3100, interaction.depth = 2, shrinkage = 0.001 and n.minobsinnode = 10.
