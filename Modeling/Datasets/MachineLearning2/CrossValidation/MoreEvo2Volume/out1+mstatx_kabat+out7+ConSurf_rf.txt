Random Forest 

518 samples
 17 predictor
  2 classes: 'HS', 'NHS' 

No pre-processing
Resampling: Cross-Validated (10 fold, repeated 3 times) 
Summary of sample sizes: 466, 466, 466, 466, 466, 466, ... 
Resampling results across tuning parameters:

  mtry  ROC        Sens       Spec     
  1     0.8192582  0.2702991  0.9743590
  2     0.8183131  0.4188034  0.9358974
  3     0.8171762  0.4452991  0.9162393
  4     0.8169598  0.4294872  0.9179487
  5     0.8126726  0.4425214  0.9145299

ROC was used to select the optimal model using  the largest value.
The final value used for the model was mtry = 1. 
