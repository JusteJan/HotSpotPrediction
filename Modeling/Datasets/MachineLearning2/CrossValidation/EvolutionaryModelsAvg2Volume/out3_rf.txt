Random Forest 

518 samples
 14 predictor
  2 classes: 'HS', 'NHS' 

No pre-processing
Resampling: Cross-Validated (10 fold, repeated 3 times) 
Summary of sample sizes: 466, 466, 466, 466, 467, 466, ... 
Resampling results across tuning parameters:

  mtry  ROC        Sens       Spec     
  1     0.8132643  0.2369658  0.9692308
  2     0.8116042  0.4091880  0.9222222
  3     0.8118672  0.4194444  0.9170940
  4     0.8088977  0.4166667  0.9059829
  5     0.8078622  0.4162393  0.9059829

ROC was used to select the optimal model using  the largest value.
The final value used for the model was mtry = 1. 
