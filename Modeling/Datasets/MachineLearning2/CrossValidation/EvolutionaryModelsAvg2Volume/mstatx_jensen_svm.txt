Support Vector Machines with Linear Kernel 

518 samples
 14 predictor
  2 classes: 'HS', 'NHS' 

No pre-processing
Resampling: Cross-Validated (10 fold, repeated 3 times) 
Summary of sample sizes: 466, 466, 467, 466, 467, 466, ... 
Resampling results across tuning parameters:

  cost  ROC        Sens       Spec     
  0.1   0.8075005  0.3841880  0.9461538
  0.3   0.8059829  0.3944444  0.9427350
  0.5   0.8060048  0.3995726  0.9418803
  0.7   0.8056158  0.4021368  0.9427350
  0.9   0.8057199  0.4021368  0.9427350
  1.1   0.8052542  0.3995726  0.9427350
  1.3   0.8049173  0.3995726  0.9435897
  1.5   0.8049803  0.3944444  0.9435897
  1.7   0.8053200  0.3944444  0.9435897
  1.9   0.8055172  0.3944444  0.9418803

ROC was used to select the optimal model using  the largest value.
The final value used for the model was cost = 0.1. 
