Random Forest 

518 samples
 14 predictor
  2 classes: 'HS', 'NHS' 

No pre-processing
Resampling: Cross-Validated (10 fold, repeated 3 times) 
Summary of sample sizes: 467, 466, 466, 466, 467, 466, ... 
Resampling results across tuning parameters:

  mtry  ROC        Sens       Spec     
  1     0.8084950  0.2202991  0.9743590
  2     0.8126945  0.3918803  0.9290598
  3     0.8119439  0.3965812  0.9205128
  4     0.8078649  0.4128205  0.9145299
  5     0.8088812  0.4198718  0.9102564

ROC was used to select the optimal model using  the largest value.
The final value used for the model was mtry = 2. 
