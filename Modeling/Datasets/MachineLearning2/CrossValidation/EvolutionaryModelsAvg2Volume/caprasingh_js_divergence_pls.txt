Partial Least Squares Generalized Linear Models  

518 samples
 14 predictor
  2 classes: 'HS', 'NHS' 

No pre-processing
Resampling: Cross-Validated (10 fold, repeated 3 times) 
Summary of sample sizes: 466, 466, 467, 466, 466, 466, ... 
Resampling results across tuning parameters:

  nt  alpha.pvals.expli  ROC        Sens       Spec     
  1   0.05               0.7981810  0.3596154  0.9393162
  1   0.10               0.7978523  0.3596154  0.9393162
  2   0.05               0.8152586  0.4108974  0.9444444
  2   0.10               0.8151326  0.4136752  0.9452991
  3   0.05               0.8098345  0.4108974  0.9393162
  3   0.10               0.8114618  0.4322650  0.9470085

ROC was used to select the optimal model using  the largest value.
The final values used for the model were nt = 2 and alpha.pvals.expli = 0.05. 
